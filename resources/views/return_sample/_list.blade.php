
@if(isset($data))
    <tr>
        <td>
            <label style="font-weight:bold">{{$data->trf_id}}</label><br>
            Submit date : {{date_format(date_create($data->created_at),'Y-m-d H:i:s')}}
        </td>
        <td>
            <label style="font-weight:bold">Category </label> : {{$data->category}}<br>
            <label style="font-weight:bold">Category Specimen </label> : {{$data->category_specimen}}<br>
            <label style="font-weight:bold">Type Specimen </label> : {{$data->type_specimen}}
        </td>
        <td>
    
            @if($data->document_type=="MO")
                <label><b>No. MO  </b></label>  : {{$data->document_no}}<br>
                <label><b>Style </b></label>  : {{$data->style}}<br>
                <label><b>Article </b></label>  : {{$data->article_no}}<br>
                <label><b>Size </b></label>  : {{$data->size}}<br>
                <label><b>Color </b></label>  : {{$data->color}}<br>
                <label><b>Item </b></label>  : {{$data->item}}<br>

            @elseif($data->document_type=="PO SUPPLIER")
                <label><b>PO Supplier </b></label>  : {{$data->document_no}}<br>
                <label><b>Item </b></label>  : {{$data->item}}<br>
                <label><b>Supplier </b></label>  : {{$data->manufacture_name}}<br>
                <label><b>No. Roll </b></label>  : {{$data->nomor_roll}}<br>
                <label><b>Bacth No. </b></label>  : {{$data->batch_number}}<br>
                <label><b>Barcode Supplier </b></label>  : {{$data->barcode_supplier}}<br>
            
            @elseif($data->document_type=="ITEM")
                <label><b>Item </b></label>  : {{$data->document_no}}<br>
                <label><b>Style </b></label>  : {{$data->style}}<br>
                <label><b>Article </b></label>  : {{$data->article_no}}<br>
                <label><b>Size </b></label>  : {{$data->size}}<br>
                <label><b>Color </b></label>  : {{$data->color}}<br>

            @elseif($data->document_type=="PO BUYER")
                <label><b>PO Buyer </b></label>  : {{$data->document_no}}<br>
                <label><b>Style </b></label>  : {{$data->style}}<br>
                <label><b>Article </b></label>  : {{$data->article_no}}<br>
                <label><b>Size </b></label>  : {{$data->size}}<br>
                <label><b>Color </b></label>  : {{$data->size}}<br>
                <label><b>Item </b></label>  : {{$data->item}}<br>
                <label><b>Destination </b></label>  : {{$data->export_to}}<br>
            
            @endif
        </td>
        <td>
            {{$data->buyer}}
        </td>
        <td>{{$data->lab_location}} / <br>
            {{$data->asal_specimen}}
        </td>
        <td>
            {{$data->username}} <br>
            ({{$data->nik}})
        </td>
    </tr>
@endif