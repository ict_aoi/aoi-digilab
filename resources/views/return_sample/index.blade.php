@extends('layouts.app', ['active' => 'menu-return-specimen'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Return Specimen</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
                <!-- <h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
					
				</div> -->
				<h3 class="panel-title">RETURN SPECIMEN</h3>
            </div>
			<div class="panel-body">

				<div class="row form-group">
					{{-- <form action="#" id="form-seacrh" method="GET"> --}}
                        <div class="col-md-9">
                            <label><b>Scan In Here</b></label>
                            <input type="text" class="form-control key" id="key" required>
                        </div>
                        <div class="col-md-3">
                            <label><b>User</b></label>
                            <input type="text" class="form-control" style="font-weight: bold; font-size:15px;" id="pic" readonly>
                            <input type="hidden" id="nik_pic">
                            <input type="hidden" id="name_pic">
                        </div>
                        {{-- <div class="col-md-1">
                            <button type="submit" id="btn-submit" class="btn btn-primary">Search</button>
                        </div> --}}
                    {{-- </form> --}}
				</div>

                <div class="row form-group">
                    <div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
                            <thead>
                                <th>TRF ID</th>
                                <th>Category</th>
                                <th>Specimen</th>
                                <th>Buyer</th>
                                <th>Lab. Location/ Origin</th>
                                <th>Submit By</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection





@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('#key').focus();
 	
    $('.key').keypress(function(event){
        var nikpic = $('#nik_pic').val();
        var keyword = $(this).val();

        
        if (event.which==13 && nikpic=="") {
            
            if(keyword==""){
                alert(422,"NIK Required ! ! !");
            }else{
                getNikpic(keyword);
            }

            $(this).val('');
            $(this).focus();
        }else if(event.which==13 && nikpic!=""){
            if(keyword==""){
                alert(422,"TRF Required ! ! !");
            }else{
                getTrf(keyword);
            }

            $(this).val('');
            $(this).focus();
        }

        
    });
});

function getNikpic(textnik){
    $.ajax({
        type: 'get',
        url : "{{ route('return.scanNikPic') }}",
        data:{
            nik:textnik
        },
        beforeSend: function() {
            loading();
        },
        success: function(response) {
         
            if (response.status==200) {
                var retpic = response.output;

                $('#pic').val(retpic.name+" - "+retpic.nik);
                $('#nik_pic').val(retpic.nik);
                $('#name_pic').val(retpic.name);
            }else{
                alert(response.status,response.output);
            }

            $.unblockUI();
        },
        error: function(response) {
            $.unblockUI();
            var notif = response.data;
            alert(notif.status,notif.output);

        }
    });
}

function getTrf(texttrf){
    $.ajax({
        type: 'get',
        url : "{{ route('return.scanTrf') }}",
        data:{
            key:texttrf
        },
        beforeSend: function() {
            loading();
        },
        success: function(response) {
         
            // var datares = response.data;

            // console.log(datares);
            // alert(datares.status,datares.output);

            $('#table-list > tbody').append(response);

            $.unblockUI();
        },
        error: function(response) {
            $.unblockUI();


            alert(response.status,response.responseText);

        }
    });
}
    
   
</script>
@endsection
