@extends('layouts.app', ['active' => 'menu-testing-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li><a href="{{route('result.setResult',['id'=>$trfinfo->id_trf])}}"> List Result</a></li>
			<li class="active">Input Result</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
            	<div class="row">
            		<h3>TESTING RESULT</h3>
            	</div>
				<div class="row">
					<table style="width: 100%;">
						<tr>
							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold;">{{$trfinfo->document_type}}</label> <br> <label style="font-size:16px;">{{$trfinfo->document_no}}</label>
							</td>

							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold;">Style</label> <br> <label style="font-size:16px;">{{$trfinfo->season}} / {{$trfinfo->style}}</label>
							</td>

							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold;">Size</label> <br>  <label style="font-size:16px;">{{$trfinfo->size}}</label>
							</td>

							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold;">Article</label> <br> <label style="font-size:16px;">{{$trfinfo->article_no}}</label>
							</td>
						</tr>
						<tr>
							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold; ">Method</label> <br> <label style="font-size:16px;">{{$trfinfo->method_code}} <br> {{$trfinfo->method_name}}</label>
							</td>

							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold;">Category</label> <br> <label style="font-size:16px;">{{$trfinfo->category}}</label>
							</td>

							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold;">Category Specimen</label> <br> <label style="font-size:16px;">{{$trfinfo->category_specimen}}</label>
							</td>

							<td width="20%" valign="top">
								<label style="font-size:16px; font-weight:bold;">Type Specimen</label> <br> <label style="font-size:16px;">{{$trfinfo->type_specimen}}</label>
							</td>
						</tr>
					</table>
					<input type="hidden" name="trfid" id="trfid" value="{{$trfinfo->id_trf}}">
					<input type="hidden" name="trfmethid" id="trfmethid" value="{{$trfinfo->id_meth}}">
					<input type="hidden" name="trfdocid" id="trfid" value="{{$trfinfo->id_doc}}">
				</div>
            </div>
			<div class="panel-body">
				<div class="row">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list" style="width: 100%;">
							<thead>
								<th>#</th>
								<th>Requirement</th>
								<th>Measuring <br> Position</th>
								<th>Pass Value</th>
								<th>Result</th> 
								<th>Action</th>
							</thead>

							<tbody>
								@php($x=1)
								@foreach($input as $in)
									<tr style="background: {{$in->active==false ? '#ff6666' : ''}}">
										<?php 
											$key = $in->setid."-".$trfinfo->id_doc;

											$rest = \App\Http\Controllers\HelperController::resultData($in->setid,$trfinfo->id_doc);

										?>
										<td>{{$x++}}</td>
										<td>
											<b>Composition : </b>{{$in->komposisi_specimen}} <br>
											<b>Treatment : </b>{{$in->perlakuan_test}} <br>
											<b>Parameter : </b>{{$in->parameter}} <br>
										</td>
										<td>{{ ucwords($in->measuring_position)}} <b>{{$in->code}}</b></td>
										<td>{{\App\Http\Controllers\HelperController::operatorVal($in->reqid)}}</td>
										<td>
											
												@if(count($rest)==1)

													@foreach($rest as $res)
													
														<label><b>Testing Result </b> : {{ $res->result }} {{$res->uom}}</label> <br>

														@if($res->result_status=='PASS')
															<span class="label bg-success">PASS</span>
														@elseif($res->result_status=='FAILED')
															<span class="label bg-danger">FAILED</span>
														@else
															<span class="label bg-grey-400">{{$res->result_status}}</span>
														@endif
													
													@endforeach
													
												@elseif(count($rest)>1)
													@foreach($rest as $res)
													
														<label><b>Testing Result {{$res->number_test}}</b> : {{ $res->result }} {{$res->uom}}</label> <br>

														@if($res->result_status=='PASS')
															<span class="label bg-success">PASS</span>
														@elseif($res->result_status=='FAILED')
															<span class="label bg-danger">FAILED</span>
														@else
															<span class="label bg-grey-400">{{$res->result_status}}</span>
														@endif
														<hr>
													@endforeach

												@endif
										</td>
										<td>

											<ul class="icons-list" style=" position:absolute;">
												<li class="dropdown">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-menu9"></i>
													</a>

													<ul class="dropdown-menu dropdown-menu-right">
														@if(count($rest)==0 && $in->active==true && $in->status!="CLOSED")
															<li><a href="#"class="ignore-click inputrest btn_{{$key}}" 
																data-trfid="{{$in->id}}" 
																data-trfmethid="{{$in->trfmethid}}" data-setreqid="{{$in->setid}}" 
																data-reqid="{{$in->reqid}}" 
																data-docid="{{$trfinfo->id_doc}}"
																data-spcid="{{isset($trfinfo->spcid)?$trfinfo->spcid:null}}"  
																data-bfrtest="{{$in->before_test}}" 
																data-stdtest="{{$in->standart_value}}" 
																data-totin="{{$in->number_test}}"
																data-measuring="{{$in->measuring_position}}" 
																 onclick="showModal(this);"><i class="icon-shield-check" ></i> Result</a>
																</li>

															<li><a href="#"class="ignore-click innactive btn_{{$key}}" data-setreqid="{{$in->setid}}" data-stype="off" onclick="powerSet(this);"><i class="icon-cross2" ></i> Inactive</a>
																</li>
														@elseif(count($rest)==0 && $in->active==false && $in->status!="CLOSED")

															<li><a href="#"class="ignore-click reactive btn_{{$key}}" data-setreqid="{{$in->setid}}" data-stype="on" onclick="powerSet(this);"><i class="icon-checkmark2" ></i> Re-Active</a>
																</li>
														@elseif(count($rest)>0 && $in->status!="CLOSED")
															<li><a href="#"class="ignore-click btn_{{$key}}" data-setreqid="{{$in->setid}}"  onclick="image(this);"><i class="icon-file-picture2" ></i> Result Image</a>
															</li>


															<li><a href="#"class="ignore-click btn_{{$key}}" data-setreqid="{{$in->setid}}"
															data-docid="{{$trfinfo->id_doc}}"  onclick="delResult(this);"><i class="icon-bin" ></i> Delete</a>
															</li>
														@elseif($in->status=="CLOSED")
															<li><a href="#"class="ignore-click btn_{{$key}}" data-setreqid="{{$in->setid}}"  onclick="image(this);"><i class="icon-file-picture2" ></i> Result Image</a>
															</li>
														@endif
														

													</ul>
												</li>
											</ul>

										</td>
									</tr>
								@endforeach
							</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
	@include('result.md_input')
@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/webcam.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$(document).on('show.bs.modal','.modal',function(){
		const zIndex = 1040 + 10 * $('.modal:visible').length;

		$(this).css('z-index',zIndex);
		setTimeout(()=>$('.modal-backdrop').not('.modal-stack').css('z-index',zIndex - 1).addClass('modal-stack'));
	});

	$(document).on('hidden.bs.modal','.modal', ()=> $('.modal:visible').length && $(document.body).addClass('modal-open'));

	$('#modal_cam').on('hidden.bs.modal',function(e){
		Webcam.reset();
		$('#modal_cam .my_camera').empty();
		$('#modal_cam .txinc').val('');
	});

	$('#modal_add').on('hidden.bs.modal',function(e){
		$('#tbimg-body').empty();
		$('#form-input').empty();
		$('#txsuprest').val('');
	});

	$('#btn-submit-test').on('click',function(event){
		event.preventDefault();
		var arrimg = [];
		var arrRest = [];
		
		var trfid = $('#modal_add .trfid').val();
		var mehtid = $('#modal_add .mehtid').val();
		var docid = $('#modal_add .docid').val();
		var setid = $('#modal_add .setid').val();
		var reqid = $('#modal_add .reqid').val();
		var spcid = $('#modal_add .spcid').val();
		var measuring_poss = $('#modal_add .measuringid').val();

		
		var txsupl = $('#modal_add .txsuprest').val();
		var txbefore = $('#modal_add .txbefore').val();
		var txstdvl = $('#modal_add .txstdvl').val();



		var count = document.getElementById('tbimg-body').rows.length;

			

			if (count>0) {
				for (var i = 0; i <= count; i++) {
					var no = i+1;
					var tximage = $('#tximg_'+no).val();

					
					if (tximage!="" || tximage!='' || tximage!=null) {
						arrimg.push({
							'image':tximage,
							'imgremark':$('#imgcom_'+no).val()
						});
					}
				}
			}

		var tabrest = document.getElementById('tbrest-body').rows.length;

		for (var x = 1; x <= tabrest; x++) {
			var texRest = $('#tbrest-body .txresult_'+x).val();

			console.log(texRest);

			if (texRest=="" || texRest=='' || texRest==null) {
				alert(422,'Result test required ! ! !');

				return false;
			}

			arrRest.push({
							'increment':x,
							'texremark':$('#tbrest-body .txremark_'+x).val().trim(),
							'texresult':$('#tbrest-body .txresult_'+x).val().trim(),
							'texcomment':$('#tbrest-body .comment_'+x).val().trim()
						});
		}

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('result.saveResultReg') }}",
            data:{
                trfid:trfid,
                trfdocid:docid,
                trfmehtid:mehtid,
                setreqid:setid,
                supplier_result:txsupl,
                before_test:txbefore,
                standart_val:txstdvl,
                result_test:arrRest,
                spcid:spcid,
                measuring_poss:measuring_poss,
                images:arrimg
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var notif = response.data;
                alert(notif.status,notif.output);
                window.location.href = "{{ route('result.formInput')}}?trfid="+trfid+"&trfmethid="+mehtid+"&trfdocid="+docid;
            },
            error: function(response) {
                $.unblockUI();
                alert(response['status'],response['responseJSON']['message']);

            }
        });
		
	});
});

function showModal(e){
	
	var trfid = e.getAttribute('data-trfid');
	var mehtid = e.getAttribute('data-trfmethid');
	var docid = e.getAttribute('data-docid');
	var setid = e.getAttribute('data-setreqid');
	var reqid = e.getAttribute('data-reqid');
	var spcid = e.getAttribute('data-spcid');
	var totin = e.getAttribute('data-totin');
	var measuring = e.getAttribute('data-measuring');

	

	var setBfr = e.getAttribute('data-bfrtest');
	var setStd = e.getAttribute('data-stdtest');
	var tbrest = $('#tbrest-body');
    tbrest.empty();

    
    $('#modal_add .trfid').val(trfid);
   	$('#modal_add .mehtid').val(mehtid);
   	$('#modal_add .setid').val(setid);
   	$('#modal_add .docid').val(docid);
   	$('#modal_add .reqid').val(reqid);
   	$('#modal_add .spcid').val(spcid);
   	$('#modal_add .measuringid').val(measuring);
   
   	if (setBfr==0) {
        document.getElementById("txbefore").disabled=true;
   	}else{
        document.getElementById("txbefore").disabled=false;
   	}

   	if (setStd==0) {
        document.getElementById("txstdvl").disabled=true;
   	}else{
        document.getElementById("txstdvl").disabled=false;
   	}
    

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url : "{{ route('result.inputRest') }}",
	        data:{reqid:reqid,totin:totin},
	        beforeSend: function() {
	            loading();
	        },
	        success: function(response) {
	            $.unblockUI();
	            
	           	var form = response.form;
	           	var spcrq = response.spcreq;

	        	$('#modal_add .lb_meth').text(spcrq['method_code']+' '+spcrq['method_name']);
	        	$('#modal_add .lb_ctg').text(spcrq['category']);
	        	$('#modal_add .lb_ctgspc').text(spcrq['category_specimen']);
	        	$('#modal_add .lb_type').text(spcrq['type_specimen']);
	        	$('#modal_add .lb_treat').text(spcrq['perlakuan_test']);
	        	$('#modal_add .lb_param').text(spcrq['parameter']);


	      		for (var i = 0; i < form.length; i++) {
	      			tbrest.append('<tr><td width="30px"><b>'+form[i]['i']+'</b></td ><td width="300px"><input type="text" class="form-control txremark_'+form[i]['i']+'" value="'+form[i]['setwash']+'"></td><td>'+form[i]['finput']+'</td><td colspan="2"><input type="text" class="form-control comment_'+form[i]['i']+'"></td></tr>');
	      		}

	        },
	        error: function(response) {
	            $.unblockUI();
	            var notif = response.data;
	             alert(response['status'],response['responseJSON']['message']);

	        }
	    });

	

   	

   	$('#modal_add').modal('show');

}

function addRow(){
	var tblimg = document.getElementById('tbimg-body');
	const lgth = tblimg.rows.length;
	const inc = lgth+1;
	var row = tblimg.insertRow(lgth);

	var cell0 = row.insertCell(0);
	cell0.innerHTML = inc;

	var cell1 = row.insertCell(1);
	cell1.innerHTML = '<center><img src="" width="300px" class="img_'+inc+'" id="img_'+inc+'"></center><input type="hidden" name="tximg_'+inc+'" class="tximg_'+inc+'" id="tximg_'+inc+'" data-inc="'+inc+'" value="">';

	var cell2 = row.insertCell(2);
	cell2.innerHTML = '<textarea type="text" name="imgcom_'+inc+'" id="imgcom_'+inc+'" class="imgcom_'+inc+' form-control"> </textarea>';

	var cell3 = row.insertCell(3);
	cell3.innerHTML = '<button type="button" class="btn btn-warning btnimg_'+inc+'" id="btnimg_'+inc+'" data-inc="'+inc+'" onclick="openCamera(this);"><i class="icon-camera"></i></button>';

}

function openCamera(e){
	var inc = e.getAttribute('data-inc');

	Webcam.set({
	   	width: 640,
	   	height: 480,
	   	dest_width: 640,
		dest_height: 480,
	   	image_format: 'png',
	   	jpeg_quality: 100,
	   	constraints: {
              facingMode: 'environment'
          }
	});

	Webcam.attach( '#modal_cam .my_camera' );
	$('#modal_cam .txinc').val(inc);
	$('#modal_cam').modal('show');
}

function takeSnapShot(){
	var imginc = $('#modal_cam .txinc').val();

	Webcam.snap( function(data_uri) {
       document.getElementById('img_'+imginc).src = data_uri;
       $('#tximg_'+imginc).val(data_uri);
    } );

    $('#modal_cam').modal('hide');
}

function image(e){
	var setreqid= e.getAttribute('data-setreqid');

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
    $.ajax({
        type: 'get',
        url : "{{ route('result.getImage') }}",
        data:{setreqid:setreqid},
        beforeSend: function() {
            loading();
        },
        success: function(response) {
            $.unblockUI();
            
            $('#modal_img .modal-body').empty();
          	$('#modal_img  .modal-body').prepend(response);
          	$('#modal_img').modal('show');
        },
        error: function(response) {
            $.unblockUI();
            var notif = response.data;
             alert(response['status'],response['responseJSON']['message']);

        }
    });
}

function delResult(e){
	var setreqid = e.getAttribute('data-setreqid');
	var docid = e.getAttribute('data-docid');

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
    $.ajax({
        type: 'post',
        url : "{{ route('result.delResultReg') }}",
        data:{setreqid:setreqid,docid:docid},
        beforeSend: function() {
            loading();
        },
        success: function(response) {
            var notif = response.data;

            alert(notif.status,notif.output);

            if (notif.status==200) {
            	window.location.reload();
            }
        },
        error: function(response) {
            $.unblockUI();
            var notif = response.data;
             alert(response['status'],response['responseJSON']['message']);

        }
    });
}

function powerSet(e){
	var setId = e.getAttribute('data-setreqid');
	var typePow = e.getAttribute('data-stype');

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
    $.ajax({
        type: 'get',
        url : "{{ route('result.inactiveRequirement') }}",
        data:{setId:setId,typePow:typePow},
        beforeSend: function() {
            loading();
        },
        success: function(response) {
            var notif = response.data;

            alert(notif.status,notif.output);

            if (notif.status==200) {
            	window.location.reload();
            }
        },
        error: function(response) {
            $.unblockUI();
            var notif = response.data;
             alert(response['status'],response['responseJSON']['message']);

        }
    });
}

function comma(e){
	var theEvent = e || window.event;
    var key = theEvent.keyCode || theEvent.which;


    key = String.fromCharCode(key);
    var regex = /[^,;:'""\|!@#$%^&*()}{+=?/]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) {
            theEvent.preventDefault();
        }
    }
}

function delTabInput(e){
	let tbrest = document.getElementById('table-result');
	var mxrow = tbrest.tBodies[0].rows.length;

	if (mxrow>1) {
		tbrest.deleteRow(mxrow);
	}else{
		  alert(422,"Minimum row form input ! ! !");
	}
	
}
</script>
@endsection