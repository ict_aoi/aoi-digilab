@extends('layouts.app', ['active' => 'menu-testing-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">List TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
                <!-- <h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
					
				</div> -->
				<h3 class="panel-title">TESTING LIST</h3>
            </div>
			<div class="panel-body">
				<div class="row {{Auth::user()->admin==false ? 'hidden' : '' }}">
					<label><b>Lab Location</b></label>
                    <select id="labloct" class="select">
						@php($userid= Auth::user()->id)
                        @foreach($labloct as $fc)
                            {{-- <option value="{{$fc->factory_name}}" {{$fc->id==auth::user()->factory_id ? 'selected' : ''}}>{{$fc->factory_name}}</option> --}}
							<option value="{{$fc->name}}" {{ ($fc->id==auth::user()->factory_id) ? 'selected' : ''}}>{{$fc->name}}</option>
                        @endforeach
                    </select>  
				</div>
				<div class="row">
					<div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                   <!-- <th> No</th> -->
                                    <th>#</th>
									<th>TRF No. </th>
									<th>Specimen</th>
									<th>Category</th>
<!-- 									<th>Category Speciment</th>
									<th>Type Speciment</th> -->
									<th>Methods</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection





@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: false,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
		deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

 	var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
	        type: 'GET',
	        url: "{{ route('result.getDataIndex')}}",
	        data: function (d) {
                return $.extend({},d,{
                    'labloct' : $('#labloct').val()
                });
            }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
		initComplete:function(){
            $('.dataTables_filter input').unbind();
            $('.dataTables_filter input').bind('keyup',function(e){
                var code = e.keyCode || e.which;
				if (code == 13) {
					table.search(this.value).draw();
					
				}
            });
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id'},
	        {data: 'specimen', name: 'specimen'},
	        {data: 'category', name: 'category', sortable: false, orderable: true, searchable: true},
	        // {data: 'category_specimen', name: 'category_specimen', sortable: true, orderable: false, searchable: true},
	        // {data: 'type_specimen', name: 'type_specimen', sortable: false, orderable: true, searchable: true},
	        {data: 'methods', name: 'methods', sortable: false, orderable: true, searchable: true},
	        {data: 'action', name: 'action',searchable:false,orderable:false},
	    ]
    });

    $('#labloct').change(function(){
    	table.clear();
    	table.draw();
    });

    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});


	$('#table-list').on('click','.reopen',function(event){
		event.preventDefault();


		var trfid = $(this).data('id');


		
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
		});
	    $.ajax({
	        type: 'post',
	        url : "{{ route('result.reOpen') }}",
	        data:{trfid:trfid},
	        beforeSend: function() {
	            loading();
	        },
	        success: function(response) {
	            $.unblockUI();
	           	var notif = response.data;
	           	alert(notif.status,notif.output);
	           	table.clear();
	           	table.draw();
	        },
	        error: function(response) {
	            $.unblockUI();
	            var notif = response.data;
	             alert(response['status'],response['responseJSON']['message']);

	        }
	    });

	});
});

function delReq(e){
	var id = e.getAttribute('data-id');

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('result.delRequirement') }}",
        data:{id:id},
        beforeSend: function() {
            loading();
        },
        success: function(response) {
            $.unblockUI();
            console.log(response);
           var notif = response.data;
            alert(notif.status,notif.output);
            window.location.reload();
        },
        error: function(response) {
            $.unblockUI();
            if (response.status==422) {
            	alert(response.status,response.responseJSON);
            }else{
            	alert(response.status,response.responseJSON['message']);
            }
            

        }
    });
}
 	

   
</script>
@endsection
