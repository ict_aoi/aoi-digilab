@extends('layouts.app', ['active' => 'menu-testing-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Set Requirement</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
   				<input type="hidden" name="trfid" id="trfid" value="{{$trfid}}">
				<h3 class="panel-title">Setting Requirement Test</h3>
            </div>
			<div class="panel-body">
				<div class="row">
					<div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                    <!-- <th> No</th> -->
                                    <th>#</th>
									<th>TRF No. </th>
									<th>Methode</th>
									<th>Composition</th>
									<th>Test Treatment</th>
									<th>Parameter</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection





@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

 	var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
    	buttons:[
    		{
                text: 'Save',
                className: 'btn btn-sm bg-primary',
                action: function (e, dt, node, config)
                {
                  saveData();
                }
            },
    	],
        ajax: {
	        type: 'GET',
	        url: "{{ route('result.getSetData')}}",
	        data:{id:$('#trfid').val()}
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id'},
	        {data: 'method_code', name: 'method_code'},
	        {data: 'komposisi_specimen', name: 'komposisi_specimen', sortable: false, orderable: false, searchable: false},
	        {data: 'perlakuan_test', name: 'perlakuan_test', sortable: false, orderable: false, searchable: false},
	        {data: 'parameter', name: 'parameter', sortable: false, orderable: false, searchable: false},
	        {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id_meth', name: 'trf_id_meth',visible:false},
	    ]
    });

    // $('#table-list ').on('click','.setReq',function(){
    // 	var trfid = $(this).data('trfid');
    // 	var trfmethid = $(this).data('trfmethid');
    // 	var ctgid = $(this).data('ctgid');
    // 	var methid = $(this).data('methid');

    // 	$('#modal_req_set').modal('shwo');
    // });	
});
 	
function evKomps(e){
	var trfmethid = e.getAttribute('data-trfidmeth');
	var trfid = e.getAttribute('data-trfid');
	var ctg_id = e.getAttribute('data-ctgid');
	var meth_id = e.getAttribute('data-methid');
	var id = e.getAttribute('id');
	var comps = e.value; 
	
	var compvl = 'comp_'+trfmethid;

	var teravl = $('#table-list .treat_'+trfmethid);
	if ($('#'+compvl).val()=="") {
		teravl.empty();
	}else{
		$.ajax({
	        type: 'get',
	        url :"{{ route('result.getTestTreat')}}",
	        data :{ctg_id:ctg_id,meth_id:meth_id,comps:comps} ,
	        success: function(response) {
	           var data = response.data;
	           teravl.empty();
	           teravl.append('<option value="">--Choose Test Treatment--</option>');
	           for (var i = 0; i < data.length; i++) {
	           		teravl.append('<option value="'+data[i]['perlakuan_test']+'">'+data[i]['perlakuan_test']+'</option>');
	           }
	             
	        },
	        error: function(response) {
	            console.log(response);
	        }
	    });
	}

}

function evTreat(e){
	var trfmethid = e.getAttribute('data-trfidmeth');
	var trfid = e.getAttribute('data-trfid');
	var ctg_id = e.getAttribute('data-ctgid');
	var meth_id = e.getAttribute('data-methid');
	var id = e.getAttribute('id');
	var treat = e.value; 
	
	var trtvlpvl = 'treat_'+trfmethid;

	var paramvl = $('#table-list .param_'+trfmethid);
	var compvl = $('#table-list .comp_'+trfmethid).val();
	if ($('#'+trtvlpvl).val()=="" && compvl=="") {
		paramvl.empty();
	}else{
		$.ajax({
	        type: 'get',
	        url :"{{ route('result.getParmater')}}",
	        data :{ctg_id:ctg_id,meth_id:meth_id,compvl:compvl,treat:treat} ,
	        success: function(response) {
	           var data = response.data;
	           paramvl.empty();
	           paramvl.append('<option value="">--Choose Test Parameter--</option>');
	           for (var i = 0; i < data.length; i++) {
	           		paramvl.append('<option value="'+data[i]['parameter']+'">'+data[i]['parameter']+'</option>');
	           }
	             
	        },
	        error: function(response) {
	            console.log(response);
	        }
	    });
	}

}

function saveData(){
	var tbls = $('#table-list').DataTable().rows().data();

	var rest = [];

	for (var i = 0; i< tbls.length; i++) {
		// console.log(tbls[i]);
		var trfmethid = tbls[i]['trf_id_meth'];
		var methid = tbls[i]['id_method'];
		var ctgid = tbls[i]['ctg_id'];
		var category_specimen = tbls[i]['category_specimen'];
		var komposisi = $('#table-list .comp_'+tbls[i]['trf_id_meth']).val();
		var perlakuan = $('#table-list .treat_'+tbls[i]['trf_id_meth']).val();
		var parameter =$('#table-list .param_'+tbls[i]['trf_id_meth']).val();
		var meth_code = tbls[i]['method_code'];
		// console.log(parameter,meth_code);
		if (komposisi=='' || perlakuan=='' || parameter=='') {
			alert(422,'Composition / Treatment / Parameter Required on method '+meth_code);
			return false;
		}

		rest.push({
			'trfmethid':trfmethid,
			'methid':methid,
			'ctgid':ctgid,
			'komposisi':komposisi,
			'perlakuan':perlakuan,
			'parameter':parameter,
			'category_specimen':category_specimen
		});
	}
	
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('result.createRequirement') }}",
            data:{data:rest,trfid:$('#trfid').val()},
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
               var notif = response.data;
                alert(notif.status,notif.output);
                window.location.href = "{{ route('result.index') }}";
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });
	

	
}
</script>
@endsection
