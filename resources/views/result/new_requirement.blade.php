@extends('layouts.app', ['active' => 'menu-testing-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Set Requirement</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
   				<input type="hidden" name="trfid" id="trfid" value="{{$trf->id}}">
				<h3 class="panel-title">Setting Requirement Test</h3>
            </div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-2">
						<center>
							<label><b>TRF No.</b></label> : <br>{{$trf->trf_id}}
						</center>
						<input type="hidden" name="trfidx" id="trfidx" value="{{$trf->id}}">
					</div>

					<div class="col-md-3">
						<center>
						<label><b>Origin</b></label> : <br>{{$trf->asal_specimen}} ( {{$trf->buyer}} )
						</center>
					</div>

					<div class="col-md-2">
						<center>
						<label><b>Category</b></label> : <br>{{$trf->category}}
						</center>
					</div>

					<div class="col-md-2">
						<center>
						<label><b>Category Speciment</b></label> : <br>{{$trf->category_specimen}}
						</center>
					</div>

					<div class="col-md-2">
						<center>
						<label><b>Type Speciment</b></label> : <br>{{$trf->type_specimen}}
						</center>
					</div>

					<div class="col-md-1">
						<button class="btn btn-primary" id="btn-save"><i class="icon-plus2"></i> SAVE</button>
					</div>

					
				</div>
				<div class="row">
					<div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                    <!-- <th> No</th> -->
                                    <th width="30px">#</th>
									<th width="300px">Methode</th>
									<th width="300px">Composition</th>
									<th width="300px">Test Treatment</th>
									<th width="300px">Parameter</th>
								</tr>
							</thead>
							<tbody>
								@php($i=1)
								@foreach($data as $dt)

									<tr>
										<td>
											{{$i}}

											<input type="hidden" name="idtrf_{{$i}}" value="{{$dt->id}}">
											<input type="hidden" name="idctg_{{$i}}" value="{{$dt->ctg_id}}">
											<input type="hidden" name="trfidmeth_{{$i}}" value="{{$dt->trf_id_meth}}">
											<input type="hidden" name="idmethod_{{$i}}" value="{{$dt->id_method}}">
										</td>
										<td>
											{{$dt->method_code}} <br> {{$dt->method_name}}

										</td>
										<td>
											
											<select class="select getComp_{{$dt->trf_id_meth}}" data-trf="{{$dt->id}}" data-ctg="{{$dt->ctg_id}}" data-mth="{{$dt->id_method}}" data-trfmtid="{{$dt->trf_id_meth}}" data-ctgspc="{{$dt->category_specimen}}" name="getComp_{{$i}}" onchange="getTreat(this);">
												<option value="">--Choose Composition--</option>
												@php($opt = explode("|",$dt->komposisi))
												@foreach($opt as $op)
													<option value="{{$op}}">{{$op}}</option>
												@endforeach
											</select>
										</td>
										<td>
											
											<!-- <select class="select getTreat_{{$dt->trf_id_meth}}" data-trf="{{$dt->id}}" data-ctg="{{$dt->ctg_id}}" data-mth="{{$dt->id_method}}" data-trfmtid="{{$dt->trf_id_meth}}" data-ctgspc="{{$dt->category_specimen}}" name="getTreat_{{$i}}" onchange="getParams(this);">
												<option value="">--Choose Test Treatment--</option>
											</select> -->

											<select multiple="multiple" data-placeholder="Choosse Test Treatment" class="select getTreat_{{$dt->trf_id_meth}}" data-trf="{{$dt->id}}" data-ctg="{{$dt->ctg_id}}" data-mth="{{$dt->id_method}}" data-trfmtid="{{$dt->trf_id_meth}}" data-ctgspc="{{$dt->category_specimen}}" name="getTreat_{{$i}}" onchange ="getParams(this)">
                                     			
                                    		</select>
										</td>
										<td>
											<select multiple="multiple" data-placeholder="Choosse Parameter" class="select getParam_{{$dt->trf_id_meth}}" id="param" name="getParam_{{$i}}" >
                                     			
                                    		</select>
										</td>
									</tr>
									@php($i++)
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('js')

<script type="text/javascript">
$(document).ready(function(){
	$('#btn-save').click(function(){
		var arss = [];
		
		var tbls = document.getElementById("table-list").rows;

		for (var x = 1; x <= tbls.length-1; x++) {

			var idtrf = $('input[name="idtrf_'+x+'"]').val();
			var idctg = $('input[name="idctg_'+x+'"]').val();
			var trfidmeth = $('input[name="trfidmeth_'+x+'"]').val();
			var idmethod = $('input[name="idmethod_'+x+'"]').val();

			var composisi = $('select[name="getComp_'+x+'"] option:selected').val();
			var treatment = $('select[name="getTreat_'+x+'"]').val();
			var parameter = $('select[name="getParam_'+x+'"]').val();

			if (parameter==='' || parameter===null || parameter==='undefined') {
				alert(422,"Completed Data Requirment !!!");
				arss = [];
				return false;
			}

			arss.push({
				'idtrf':idtrf,
				'trfidmeth':trfidmeth,
				'idctg':idctg,
				'idmethod':idmethod,
				'composisi':composisi,
				'treatment':treatment,
				'parameter':parameter
			});
		}

		if (arss.length>0) {
			$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	            type: 'post',
	            url : "{{ route('result.createRequirement') }}",
	            data:{data:arss,trfid:$('#trfidx').val()},
	            beforeSend: function() {
	                loading();
	            },
	            success: function(response) {
	                $.unblockUI();
	                
	               var notif = response.data;
	                alert(notif.status,notif.output);
	                window.location.href = "{{ route('result.index') }}";

	            },
	            error: function(response) {
	                $.unblockUI();
	                var notif = response.data;
	                alert(response['status'],response['responseJSON']['message']);

	            }
	        });
		}else{
			alert(422,"Requirement not set !!!");
		}
		
	});


});

function getTreat(e){
	var idctg = e.getAttribute('data-ctg');
	var idmth = e.getAttribute('data-mth');
	var idtrfmth = e.getAttribute('data-trfmtid');
	var comps = e.value;
	
	var sTreat = 'getTreat_'+idtrfmth;
	var sParam = 'getParam_'+idtrfmth;

	if (comps=='') {
		$('#table-list .'+sTreat).empty();
		// $('#table-list .'+sParam).empty();
		$('#table-list  .'+sTreat).append('<option value="">--Choose Test Treatment--</option>');
	}else{
		$.ajax({
	        type: 'get',
	        url :"{{ route('result.getTestTreat')}}",
	        data :{idctg:idctg,idmth:idmth,comps:comps} ,
	        success: function(response) {
	           var data = response.data;


	           $('#table-list  .'+sTreat).empty();
	           $('#table-list  .'+sTreat).append('<option value="">--Choose Test Treatment--</option>');
	           for (var i = 0; i < data.length; i++) {
	           		$('#table-list .'+sTreat).append('<option value="'+data[i]['perlakuan_test']+'">'+data[i]['perlakuan_test']+'</option>');
	           }
	             
	        },
	        error: function(response) {
	            console.log(response);
	        }
	    });
	}
}

function getParams(e){
	var idctg = e.getAttribute('data-ctg');
	var idmth = e.getAttribute('data-mth');
	var idtrfmth = e.getAttribute('data-trfmtid');
	var ctgspc = e.getAttribute('data-ctgspc');
	var treat = [];
	var val = $('.getTreat_'+idtrfmth).select2('data');

	var sParam = 'getParam_'+idtrfmth;
	var vComp = $('#table-list .getComp_'+idtrfmth).val();

	for (var i = 0; i < val.length; i++) {
		treat.push(
				val[i]['id']
			);
	}

	if (treat.length<1) {
		$('#table-list .'+sParam).empty();
	}else{
		$.ajax({
	        type: 'get',
	        url :"{{ route('result.getParmater')}}",
	        data :{idctg:idctg,idmth:idmth,vComp:vComp,treat:treat,ctgspc:ctgspc} ,
	        success: function(response) {
	           var dimApp = response.dimApp;
	           var data = response.data;

	         	
	           $('#table-list  .'+sParam).empty();



	           for (var i = 0; i < data.length; i++) {
	           		$('#table-list .'+sParam).append('<option value="'+data[i]['parameter']+'">'+data[i]['parameter']+'</option>');
	           }

	           
	           if (dimApp.length>0) {
	           		$('#table-list .'+sParam).val(dimApp).trigger('change');
	      //      		for (var i = 0; i < dimApp.length; i++) {
		 				// $('#table-list .'+sParam).val(dimApp[i]['parameter']).trigger('change');
		     //       }
		           // $('#table-list .'+sParam);
		           // $('#table-list .'+sParam).prop('disabled',true);
		          
	           }
	           

	             
	        },
	        error: function(response) {
	            console.log(response);
	        }
	    });
	}
}
</script>
@endsection