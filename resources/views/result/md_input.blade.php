
<div id="modal_add" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content ">
            <div class="modal-header">
                <center><h4>INPUT RESULT</h4></center>
            </div>
            <div class="modal-body">
                <!-- <form action="#" enctype="multipart/form-data" id="form-result"> -->
                    <div class="row">
                        <div class="col-md-2">
                            <label><b>Method </b></label><br>
                            <label class="lb_meth"></label>
                        </div>
                        <div class="col-md-2">
                            <label><b>Category </b></label><br>
                            <label class="lb_ctg"></label>
                        </div>
                        <div class="col-md-2">
                            <label><b>Catgory Specimen </b></label><br>
                            <label class="lb_ctgspc"></label>
                        </div>
                        <div class="col-md-2">
                            <label><b>Type Specimen </b></label><br>
                            <label class="lb_type"></label>
                        </div>
                        <div class="col-md-2">
                            <label><b>Test Treatment </b></label><br>
                            <label class="lb_treat"></label>
                        </div>
                        <div class="col-md-2">
                            <label><b>Parameter </b></label><br>
                            <label class="lb_param"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label><b>SUPPLIER RESULT</b></label>
                            <input type="text" class="form-control txsuprest" name="txsuprest" id="txsuprest" placeholder="Supplire Result">
                        </div>

                        <div class="col-md-3 setBefore">
                            <label><b>BEFORE TEST</b></label>
                            <input type="text" class="form-control txbefore" name="txbefore" id="txbefore" placeholder="Before Test" disabled>
                        </div>

                        <div class="col-md-3 setStandart">
                            <label><b>STANDART VALUE</b></label>
                            <input type="text" class="form-control txstdvl" name="txstdvl" id="txstdvl" placeholder="Standart Value" disabled>
                        </div>
                        <div class="col-md-3">
                            <label><b>FABRIC COMPOSITION</b></label><br>
                            <label class="fibCom"></label>                       
                        </div>

                        <!-- <div class="col-md-3">
                            <label><b>Test Result</b></label>
                            <div class="form-input">
                                
                            </div>
                        </div> -->
                        <input type="hidden" name="trfid" class="trfid" id="trfid">
                        <input type="hidden" name="mehtid" class="mehtid" id="mehtid">
                        <input type="hidden" name="docid" class="docid" id="docid">
                        <input type="hidden" name="setid" class="setid" id="setid">
                        <input type="hidden" name="reqid" class="reqid" id="reqid">
                        <input type="hidden" name="measuringid" class="measuringid" id="measuringid">
                    </div>
                    <hr>
                    <div class="row">
                        <!-- <div class="col-md-12">
                            <label><b>Comment</b></label>
                            <textarea class="form-control txcomment" id="txcomment" placeholder="Comment"></textarea>
                        </div> -->
                        <center><h3>TEST RESULT</h3></center>
                        <table class ="table table-basic table-condensed tbresult" width="100%" id="table-result">
                            <thead>
                                <th>#</th>
                                <th></th>
                                <th><center>Result</center></th>
                                <th width="375"><center>Comment</center></th>
                                <th width="60"><button type="button" class="btn btn-default"><i class="icon-trash"  onclick="delTabInput();"></i></button></th>
                            </thead>
                            <tbody id="tbrest-body">
                                
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="row">
                        <center><h3>IMAGE</h3></center>
                        <table  class ="table table-basic table-condensed tbsetrow" width="100%" id="table-image">
                            <thead>
                                <th width="10%">#</th>
                                <th width="40%"><center>Image</center></th>
                                <th width="40%"><center>Remark</center></th>
                                <th width="10%"><button class="btn btn-default" type="button" onclick="addRow();"><i class="icon-plus2" ></i></button></th>
                            </thead>
                            <tbody id="tbimg-body">
                                <tr>
                                    <td>1</td>
                                    <td>
                                        <center>
                                        <img src="" width="300px" class="img_1" id="img_1"></center>
                                        <input type="hidden" name="tximg_1" class="tximg_1" id="tximg_1" data-inc="1" value="">
                                    </td>
                                    <td>
                                        <textarea type="text" name="imgcom_1" id="imgcom_1" class="imgcom_1 form-control"> </textarea>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-warning btnimg_1" id="btnimg_1" data-inc="1" onclick="openCamera(this);"><i class="icon-camera"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <center>
                            <button class="btn btn-primary" type="button" id="btn-submit-test"><i class="icon-plus2"></i> SAVE</button>
                            <button class="btn btn-default" data-dismiss="modal" type="button"> CLOSED</button>
                        </center>
                    </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
</div>

<div id="modal_cam" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4>Image Capture</h4></center>
            </div>
            <div class="modal-body">
                <center>
                    <div class="row">
                        <div id="my_camera" class="my_camera" ></div>
                        <input type="hidden" name="txinc" class="txinc" id="txinc">
                    </div>
                    <div class="row">
                        <button class="btn btn-success" id="btn-take" onclick="takeSnapShot();">TAKE</button>
                         <button class="btn btn-default" data-dismiss="modal" type="button"> CLOSED</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
</div>


<div id="modal_img" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <center><h3>IMAGE RESULT</h3></center>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>
