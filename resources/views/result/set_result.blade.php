@extends('layouts.app', ['active' => 'menu-testing-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Set Requirement</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
          
            	<div class="row">
            		<h3 class="panel-title">Set Result Test</h3>
            	</div>
            	<div class="row">
            		<input type="hidden" name="trfid" id="trfid" value="{{$data->id}}">
            		<div class="col-md-4">
            			<h4><b>TRF No. : </b> <br> {{$data->trf_id}}</h4>
            		</div>
            		<div class="col-md-4">
            			<h4><b>Buyer : </b> <br> {{$data->buyer}}</h4>
            		</div>
            		<div class="col-md-4">
            			<h4><b>Origin : </b> <br> {{$data->asal_specimen}}</h4>
            		</div>
            	</div>
				
            </div>
			<div class="panel-body">
				<div class="row">
					<div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                    <!-- <th> No</th> -->
                                    <th>#</th>
									<th>TRF No. </th>
									<th>Methode</th>
									<th>Specimen</th>
									<th>Composition</th>
									<th>Test Treatment</th>
									<th>Parameter</th>
									<th>Pass Value</th>
									<th>Supplier Result</th>
									<th>Test Result</th>
									<th>Image</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

	
@include('result.md_input')


@section('js')
<!-- <script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script> -->
<script type="text/javascript" src="{{url('assets/js/webcam.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: true,
        autoLength: true,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

 	var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
	        type: 'GET',
	        url: "{{ route('result.getResultReg') }}",
	        data:{id:$('#trfid').val()}
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id', sortable: false, orderable: false, searchable: false},
	        {data: 'method_code', name: 'method_code', sortable: false, orderable: false, searchable: false},
	        {data: 'specimen', name: 'specimen', sortable: false, orderable: false, searchable: false},
	        {data: 'komposisi_specimen', name: 'komposisi_specimen', sortable: false, orderable: false, searchable: false},
	        {data: 'perlakuan_test', name: 'perlakuan_test', sortable: false, orderable: false, searchable: false},
	        {data: 'parameter', name: 'parameter', sortable: false, orderable: false, searchable: false},
	        {data: 'value', name: 'value', sortable: false, orderable: false, searchable: false},
	        {data: 'supplier_result', name: 'supplier_result', sortable: false, orderable: false, searchable: false},
	        {data: 'result', name: 'result', sortable: false, orderable: false, searchable: false},
	        {data: 'image', name: 'image', sortable: false, orderable: false, searchable: false},
	        {data: 'status', name: 'status', sortable: false, orderable: false, searchable: false},
	        {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false},

	    ]
    });

    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});


    

    $('#table-list').on('click','.save',function(){
    	var reqid= $(this).data('reqid');
    	var result= $(this).data('result');
    	var setreq= $(this).data('setreq');
    	var docid= $(this).data('docid');


    	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url : "{{ route('result.inputRest') }}",
	        data:{reqid:reqid,result:result,setreq:setreq,docid:docid},
	        beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
               var form = response.form;

               $('#modal_add .forminp').append(form);

               $('#modal_add .idsetreq').val(setreq);
               $('#modal_add .iddoc').val(docid);
               openCamera();
                $('#modal_add').modal('show');
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
	    });
    }); 

    $('#table-list').on('click','.del',function(event){
  		event.preventDefault();

    	var setreq = $(this).data('setreq');
    	var docid = $(this).data('docid');
    	var trfid = $(this).data('id');
    	var restid = $(this).data('restid');

    		$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax({
		        type: 'post',
		        url : "{{ route('result.delResultReg') }}",
		        data:{restid:restid,trfid:trfid},
		        beforeSend: function() {
	                loading();
	            },
	            success: function(response) {
	                $.unblockUI();
	                
	               var notif = response.data;
	                alert(notif.status,notif.output);
	                window.location.reload();
	            },
	            error: function(response) {
	                $.unblockUI();
	                var notif = response.data;
	                alert(notif.status,notif.output);

	            }
		    });

    });

    $("#form-result").submit(function(event){
    	event.preventDefault();

    	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form-result').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);
                 
                $('#modal_add').modal('hide');
                
                table.clear();
                table.draw();
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });

   $('#modal_add').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();

         delPhoto();
         Webcam.reset();
        $('#modal_add .forminp').empty();
    });
 


});

function openCamera(){
	 Webcam.set({
	   	width: 640,
	   	height: 480,
	   	dest_width: 480,
		dest_height: 360,
	   	image_format: 'png',
	   	jpeg_quality: 100,
	   	constraints: {
              facingMode: 'environment'
          }
	});

	Webcam.attach( '#modal_add .my_camera' );
}

function takeSnapShot(){
	Webcam.snap( function(data_uri) {
       $('#modal_add .imagetag').val(data_uri);
       document.getElementById('imgrest').innerHTML = 
        '<img src="'+data_uri+'"/>';
    } );
}


function delPhoto(){
	document.getElementById('imgrest').innerHTML='';
	$('#modal_add .imagetag').val('');
} 




</script>
@endsection
