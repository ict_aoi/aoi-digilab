@extends('layouts.app', ['active' => 'menu-testing-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">List Result</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
          
            	<div class="row">
            		<h3 class="panel-title">Set Result Test</h3>
            	</div>
            	<div class="row">
            		<br>
            		<table style="border: 1px; width: 100%;">
            			<tr>
            				<td>
            					<label style="font-size:18px;"><b>TRF ID</b> :  {{$data->trf_id}}</label>
            					<input type="hidden" name="trfid" id="trfid" value="{{$data->id}}">
            				</td>

            				<td>
            					<label style="font-size:18px;"><b>Buyer</b> :  {{$data->buyer}}</label>
            				</td>

            				<td>
            					<label style="font-size:18px;"><b>Origin</b> :  {{$data->asal_specimen}}</label>
            				</td>
            			</tr>

            			<tr>
            				<td>
            					<label style="font-size:18px;"><b>Category</b> :  {{$data->category}}</label>
            				</td>

            				<td>
            					<label style="font-size:18px;"><b>Category Speciment</b> :  {{$data->category_specimen}}</label>
            				</td>

            				<td>
            					<label style="font-size:18px;"><b>Type Speciment</b> :  {{$data->type_specimen}}</label>
            				</td>
            			</tr>
            		</table>
            	</div>
				
            </div>
			<div class="panel-body">
				<div class="row">
					<div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                    <!-- <th> No</th> -->
                                    <th>#</th>
									<th>Methode</th>
									<th>Specimen</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: true,
        autoLength: true,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
	        type: 'GET',
	        url: "{{ route('result.getResultReg') }}",
	        data:{id:$('#trfid').val()}
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'method_code', name: 'method_code'},
	        {data: 'specimen', name: 'specimen'},
	        {data: 'status', name: 'status'},
	        {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false},

	    ]
    });

    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});



	


	
});


function tblRst(datas){
	var tbMdesc = $('#modal_escl .table-mdrest >tbody');
    tbMdesc.empty();
    for (var i = 0; i < datas.length; i++) {
    
    	var oprt = operation(datas[i]);
    	var sts = setSattus(datas[i]['result_status']);
    	tbMdesc.append('<tr><td>'+datas[i]['number_test']+'</td><td>'+oprt+'</td><td>'+datas[i]['standart_value']+'</td><td>'+datas[i]['supplier_result']+'</td><td>'+datas[i]['before_test']+'</td><td>'+datas[i]['result']+'</td><td>'+sts+'</td></tr>');
    	
    }

}

function operation(dreq){

	switch(dreq['operator']){
		case "1":
			var rt = dreq['value1'];
		break;


		case "2" || "3":
			var rt = dreq['symbol']+' '+dreq['value1'];
		break;

		case "4":
			var arr = [dreq['value1'],dreq['value2'],dreq['value3'],dreq['value4'],dreq['value5'],dreq['value6'],dreq['value7']];
			var fil = arr.filter(function(number){
				return number!='';
			});

			var rt = fil.join(', ');
		break;

		case "5":
			var rt = dreq['value1']+' '+dreq['symbol']+' '+dreq['value2'];
		break;
	}

	return rt;
}

function setSattus(status){

	switch(status){
		case "PASS":
			return '<label class="label label-success label-rounded ">PASS</label>';
		break;


		case "FAILED":
			return '<label class="label label-danger label-rounded ">FAILED</label>';
		break;

		default :
			return '<label class="label label-primary label-rounded ">ERROR</label>';
		break;
	}
}
</script>
@endsection