@extends('layouts.desk')
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Create TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Create TRF</span> </h4>
				</div>
            </div>
			<div class="panel-body">
                <div class="row form-group">
                    <label style="color:red;"><b>* Required Value</b></label>
                </div>
				<div class="row form-group">
                        <div class="col-lg-4">
                            <div class="row">
                                <label><b>Dept. Origin</b> <b style="color:red;">*</b></label>
                                <select class="select" id="origin" required>
                                    <option value="null" data-asal="null">--Chosse Origin--</option>
                                    <option value="SSM" data-asal="DEVELOPMENT">Development</option>
                                    <option value="WMS" data-asal="PRODUCTION">Warehouse Material</option>
                                    <option value="FGMS" data-asal="PRODUCTION">Final QA / Finish Goods</option>
                                    <option value="CDMS" data-asal="PRODUCTION">Cutting / Secondary Process</option>
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Buyer</b> <b style="color:red;">*</b></label>
                                <select class="select" id="buyer" onchange="getCtg(this);" required>
                                    <option value="null">--Chosse Buyer--</option>
                                    @foreach($buyer as $by)
                                        <option value="{{$by->buyer}}">{{$by->buyer}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Category</b> <b style="color:red;">*</b></label>
                                <select class="select" id="category" onchange="getCtgSpm(this);" required >

                                </select>
                            </div>
                            <div class="row">
                                <label><b>Category Specimen</b> <b style="color:red;">*</b></label>
                                <select class="select" id="category_specimen" onchange="getType(this);" required>

                                </select>
                            </div>

                            <div class="row">
                                <label><b>Type Specimen</b> <b style="color:red;">*</b></label>
                                <select class="select" id="type_specimen" onchange="getMeth(this);" required>

                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="row">
                                <label><b>Lab Location</b></label>
                                <select class="select" id="labloction" disabled>

                                    @foreach($fact as $fc)
                                        {{-- <option value="{{$fc->factory_name}}" {{$factory_id==$fc->id ? 'selected' : ''}}>{{$fc->factory_name}}</option> --}}
                                        <option value="{{$fc->name}}" {{str_contains($fc->default_origin,$factory_id) ? 'selected' : ''}}>{{$fc->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row">
                                <label  style="font-weight: bold;" class="display-block">Return Specimen Test <label style="color: red; font-weight: bold;">*</label></label>
                                
                                <label style="font-weight: bold; margin-right:100px;"><input type="radio" name="return_test" id="return_test" value="true"> YES</label>
                                <label style="font-weight: bold; margin-right:100px;"><input type="radio" name="return_test" id="return_test" value="false" checked> NO</label>
                            </div>

                            <div class="row">
                                <label style="font-weight: bold;" class="display-block">Part To Test <label style="color: red; font-weight: bold;">*</label></label>
                                <textarea name="part_to_test" id="part_to_test" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="row">
                                <label style="font-weight: bold;" class="display-block">Date Information<label style="color: red; font-weight: bold;">*</label></label>

                                <label style="font-weight: bold; margin-right:100px;" class="radio-inline"><input type="radio" name="radio_date_info" id="radio_date_info" value="buy_ready"> Buy ready (special for development testing)</label><br>

                                <label style="font-weight: bold; margin-right:100px;" class="radio-inline"><input type="radio" name="radio_date_info" id="radio_date_info" value="podd"> PODD</label><br>

                                <label style="font-weight: bold; margin-right:100px;" class="radio-inline"><input type="radio" name="radio_date_info" id="radio_date_info" value="output_sewing"> 1st Output Sewing</label> <br>

                                <input type="text" class="form-control date_info" id="anytime-weekday" required>
                            </div>
                            
                            <div class="row">
                                <table width="100%">
                                    <tr>
                                        <td><label style="font-weight: bold;" class="display-block">Pre-Production</label></td>
                                        <td><label style="font-weight: bold;" class="display-block">1st Bulk Testing</label></td>
                                        <td><label style="font-weight: bold;" class="display-block">Re-Order Testing</label></td>
                                        <td><label style="font-weight: bold;" class="display-block">Re-Test</label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="preproductiontesting_m">M (Model Level)</label>
                                        </td>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="bulktesting_m"> M (Model Level)</label>
                                        </td>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="reordertesting_m">M (Model Level)</label>
                                        </td>
                                        <td rowspan="3">
                                            <label> <input type="radio" name="test_req" id="test_req" value="retest"> Re Test</label> 
                                            <textarea name="no_trf" id="no_trf" class="form-control"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="preproductiontesting_a"> A (Article Level)</label>
                                        </td>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="bulktesting_a"> A (Article Level)</label>
                                        </td>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="reordertesting_a"> A (Article Level)</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="preproductiontesting_selective"> Selective</label>
                                        </td>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="bulktesting_selective"> Selective</label>
                                        </td>
                                        <td>
                                            <label> <input type="radio" name="test_req" id="test_req" value="reordertesting_selective"> Selective</label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>


				</div>

			</div>
		</div>

        
	</div>
    <div class="row">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="row">
                        <button type="button" class="btn btn-primary" id="add-spc">Add</button>
                    </div>
                    <div class="row table-responsive">
                        <table class="table table-basic table-condensed" id="table-specimen">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Doc Type</th>
                                    <th>Doc. Number</th>
                                    <th>Season</th>
                                    <th>Style</th>
                                    <th>Article</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Item</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row" id="specimen_data"></div>
                <div class="row">
                    <button class="btn btn-success submit" type="button" id="submit"><span class="icon-database-check"></span> SAVE</button>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('desk.printBarcode')}}" id="printBarcode"></a>
{{-- <a href="{{ route('desk.printDetail')}}" id="printDetail"></a> --}}
@endsection

@section('modal')
    @include('selfdesk.home.modal_doc.modal_doc')
@endsection

@section('js')
<script text="text/javascript">
var dataset = [];
$(document).ready(function(){
    $('#origin').change(function(){
        var origin = $(this).val();

        $("input[type=radio][value=buy_ready]").prop("disabled",true);
        $("input[type=radio][value=podd]").prop("disabled",true);
        $("input[type=radio][value=output_sewing]").prop("disabled",true);

        $("input[type=radio][value=buy_ready]").prop("checked",false);
        $("input[type=radio][value=podd]").prop("checked",false);
        $("input[type=radio][value=output_sewing]").prop("checked",false);

        if (origin=='SSM') {
            $("#lab_loct").prop('disabled',false);
            $("input[type=radio][value=buy_ready]").prop("disabled",false);
        }else if ($(this).val()=='WMS') {
            $("#lab_loct").prop('disabled',true);
            $("input[type=radio][value=podd]").prop("disabled",false);
        }else if ($(this).val()=='FGMS') {
            $("#lab_loct").prop('disabled',true);
            $("input[type=radio][value=podd]").prop("disabled",false);
            $("input[type=radio][value=output_sewing]").prop("disabled",false);
        }else if ($(this).val()=='CDMS') {
            $("#lab_loct").prop('disabled',true);
            $("input[type=radio][value=podd]").prop("disabled",false);
            $("input[type=radio][value=output_sewing]").prop("disabled",false);
        }

    });

    $('#add-spc').click(function(e){
        e.preventDefault();

        if ($('#category').val()==='' || $('#category').val()===null || $('#category_specimen').val()==='' || $('#category_specimen').val()===null || $('#type_specimen').val()==='' || $('#type_specimen').val()===null || $('#origin').val()==='' || $('#origin').val()===null) {
            alert(422,'Check Data Category, Category Specimen, Type Specimen, Origin required ! ! !');

            return false;
        }

        $('#modal_doc .txcategory').val($('#category').val());
        $('#modal_doc .txcategory_specimen').val($('#category_specimen').val());
        $('#modal_doc .txtype_specimen').val($('#type_specimen').val());
        $('#modal_doc .txorigin').val($('#origin').find(':selected').data('asal'));

        $('#modal_doc .key').val('');
        $('#modal_doc').modal('show');
    });


    $('#form-search-doc').submit(function(event){
        event.preventDefault();

        var origin = $('#modal_doc .txorigin').val();
        var category_specimen = $('#modal_doc .txcategory_specimen').val();
        var type_specimen = $('#modal_doc .txtype_specimen').val();
        var key = $('#modal_doc .key').val();

        if (category_specimen=="FABRIC") {

            switch (origin) {
                case "PRODUCTION":
                var type = 'BARCODE ROLL';
                    break;
                
                case "DEVELOPMENT":
                var type = 'BARCODE FABRIC DEV';
                    break;
            }

        }else if (category_specimen=="ACCESORIES/TRIM") {
            
            switch (origin) {
                case "PRODUCTION":
                var type = 'BARCODE ACCESORIES';
                    break;
                
                case "DEVELOPMENT":
                var type = 'BARCODE ACC DEV';
                    break;
            }
            
        }else if (category_specimen=="GARMENT") {
            switch (origin) {
                case "PRODUCTION":
                var type = 'PO BUYER';
                    break;
                
                case "DEVELOPMENT":
                var type = 'MO';
                    break;
            }
        }else if (category_specimen=="PANEL" || category_specimen=="MOCKUP" ) {
            switch (origin) {
                case "PRODUCTION":
                var type = 'BARCODE BUNDLE';
                    break;
                
                case "DEVELOPMENT":
                var type = 'MO';
                    break;
            }
        }else if(category_specimen=="STRIKE OFF"){
            switch (origin) {
                case "PRODUCTION":
                var type = 'BARCODE ACCESORIES';
                    break;
                
                // case "DEVELOPMENT":
                // var type = 'MO';
                //     break;

                case "DEVELOPMENT":
                var type = 'BARCODE FABRIC DEV';
                    break;
            }
        }

        $.ajax({
            type: 'get',
            url :$('#form-search-doc').attr('action'),
            data :{
                origin:origin,
                category_specimen:category_specimen,
                type_specimen:type_specimen,
                type:type,
                key:key
            } ,
            beforeSend:function(){
                loading();
            },
            success: function(response) {
                $.unblockUI();
            //    console.log(response.data);
                tableDoc(response.data);
                // console.log(response.data);

            },
            error: function(response) {
                $.unblockUI();
            
            }
        });
    });

    $('#modal_doc .table-doc > tbody').on('click','tr',function(){
       
       var parse = $('#modal_doc .table-doc').DataTable().row(this).data();

       var category_specimen = parse.category_specimen;
       var type_specimen = parse.type_specimen;
       
       dataset.push(parse);

    $('#modal_doc').modal('hide');
    drawTableSpecimen(dataset);
   });

   $('#submit').on('click',function(event){
        event.preventDefault();

        var origin              = $('#origin').val();
        var asal                = $('#origin').find(':selected').data('asal');
        var buyer               = $('#buyer').val();
        var category            = $('#category').val();
        var category_specimen   = $('#category_specimen').val();
        var type_specimen       = $('#type_specimen').val();
        var lab_location        = $('#labloction').val();
        var return_test         = $('input[name=return_test]:checked').val();
        var part_to_test        = $('#part_to_test').val(); 
        var radio_date_info     = $('input[name=radio_date_info]:checked').val();
        var date_info           = $('.date_info').val();
        var test_req            = $('input[name=test_req]:checked').val();
        var no_trf              = $('#no_trf').val();


        if (origin==null || category==null || category_specimen==null || type_specimen==null || radio_date_info==null || date_info==null || part_to_test==null || return_test==null) {
            
            alert(422,"Completed the data TRF ! ! !");

            return false;
        }

        if(test_req=="retest" && (no_trf==null || no_trf=='') ){
            alert(422,"Previous TRF Required ! ! !");
            return false;
        }

        if(dataset.length<1){
            alert(422,"Document specimen required ! ! !");

            return false;
        }

        completedDataSumary(category_specimen,type_specimen,asal);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('desk.saveTrf') }}",
            data:{
                origin : origin,
                asal : asal,
                buyer : buyer,
                category : category,
                category_specimen : category_specimen,
                type_specimen : type_specimen,
                lab_location : lab_location,
                return_test : return_test,
                part_to_test : part_to_test,
                radio_date_info : radio_date_info,
                date_info : date_info,
                test_req : test_req,
                no_trf : no_trf,
                data : dataset
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                // console.log(response);
                var notif = response.data;
                alert(notif.status,notif.output);
                

                var trflist = notif.list;
                if (trflist.length>0) {
                    var urlBarcode = $('#printBarcode').attr('href')+'?data='+notif.list;

                    window.open(urlBarcode);
                    window.location.href ="{{ route('desk.index') }}";
                }
                
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });
   });
   
});

function getCtg(e){
    var buyer = e.value;

    if (buyer=="") {
        $('#category').empty();
        $('#category').append('<option value="null">--Filter Category--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('desk.getCtg') }}",
            data :{buyer:buyer} ,
            success: function(response) {
               var data = response.data;
               $('#category').empty();
               $('#category').append('<option value="null">--Filter Category--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#category').append('<option value="'+data[i]['category']+'">'+data[i]['category']+'</option>');
               }

            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}

function getCtgSpm(e){
   var category = e.value;
   var buyer = $('#buyer').val();
   var origin = $('#origin').val();

    if (buyer=="" && category=="") {
        $('#category_specimen').empty();
        $('#category_specimen').append('<option value="null">--Filter Category Specimen--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('desk.getCtgSpc') }}",
            data :{buyer:buyer,category:category,origin:origin} ,
            success: function(response) {
               var data = response.data;
               $('#category_specimen').empty();
               $('#category_specimen').append('<option value="null">--Filter Category Specimen--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#category_specimen').append('<option value="'+data[i]['category_specimen']+'">'+data[i]['category_specimen']+'</option>');
               }

            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}

function getType(e){
   var category_specimen = e.value;
   var buyer = $('#buyer').val();
   var category = $('#category').val();

    if (buyer=="" && category=="" && category_specimen=="") {
        $('#type_specimen').empty();
        $('#type_specimen').append('<option value="null">--Filter Type Specimen--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('desk.getTypeSpc') }}",
            data :{buyer:buyer,category:category,category_specimen:category_specimen} ,
            success: function(response) {
               var data = response.data;
               $('#type_specimen').empty();
               $('#type_specimen').append('<option value="null">--Filter Type Specimen--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#type_specimen').append('<option value="'+data[i]['type_specimen']+'">'+data[i]['type_specimen']+'</option>');
               }

            },
            error: function(response) {
                console.log(response);
            }
        });
    }

    // setSpc(category_specimen,type_specimen);
}

function getMeth(e){

    var type_specimen = e.value;
    var category_specimen = $('#category_specimen').val();
    var category  = $('#category').val();
    var buyer = $('#buyer').val();
    var asal = $('#origin').find(':selected').data('asal');

    setSpc(category_specimen,type_specimen,asal);
}

function setSpc(category_specimen,type_specimen,asal){
    $('#specimen_data').empty();
    
    if (category_specimen=='GARMENT') {
        $('#specimen_data').append(`@include('create_trf.specimen._garment')`);
    }else if (category_specimen=='ACCESORIES/TRIM' && asal!="DEVELOPMENT") {
        $('#specimen_data').append(`@include('create_trf.specimen._accesories')`);
    }else if (category_specimen=='FABRIC' && asal!="DEVELOPMENT") {
        $('#specimen_data').append(`@include('create_trf.specimen._fabric')`);
    }else if ((category_specimen=='STRIKE OFF' || category_specimen=='PANEL') && type_specimen=='EMBROIDERY') {
        $('#specimen_data').append(`@include('create_trf.specimen._embro')`);
    }else if ((category_specimen=='STRIKE OFF' || category_specimen=='PANEL')  && type_specimen=='BADGE') {
        $('#specimen_data').append(`@include('create_trf.specimen._badge_strikeoff')`);
    }else if (category_specimen=='STRIKE OFF' && type_specimen=='RIVERT') {
        $('#specimen_data').append(`@include('create_trf.specimen._rivert')`);
    }else if((category_specimen=='STRIKE OFF' || category_specimen=='PANEL') && type_specimen=='BONDING'){
        $('#specimen_data').append(`@include('create_trf.specimen._bonding_strikeoff')`);
    }else if (category_specimen=='MOCKUP' ) {
        $('#specimen_data').append(`@include('create_trf.specimen._mockup')`);
    }else if(category_specimen=='STRIKE OFF' && type_specimen=='HEAT TRANSFER'){
        $('#specimen_data').append(`@include('create_trf.specimen._heat_strikeoff')`);
    }else if(category_specimen=='PANEL' && type_specimen=='HEAT TRANSFER'){
        $('#specimen_data').append(`@include('create_trf.specimen._heat_panel')`);
    }else if(category_specimen=='STRIKE OFF' && type_specimen=='PAD PRINT'){
        $('#specimen_data').append(`@include('create_trf.specimen._pad_strikeoff')`);
    }else if(category_specimen=='PANEL' && type_specimen=='PAD PRINT'){
        $('#specimen_data').append(`@include('create_trf.specimen._pad_panel')`);
    }else if((category_specimen=='STRIKE OFF' || category_specimen=='PANEL') && type_specimen=='PRINTING'){
        $('#specimen_data').append(`@include('create_trf.specimen._printing_strikeoff')`);
    }else if (category_specimen=='FABRIC' && asal=="DEVELOPMENT") {
        $('#specimen_data').append(`@include('create_trf.specimen._dev_fabric')`);
    }else if (category_specimen=='ACCESORIES/TRIM' && asal=="DEVELOPMENT") {
        $('#specimen_data').append(`@include('create_trf.specimen._dev_acc')`);
    }

    dataset = [];
}

function tableDoc(dts){

    $('#modal_doc .table-doc').DataTable().destroy();
    $('#modal_doc .table-doc').DataTable({
        "searching":true,
        "paging":true,
        data:dts,
        columns: [
            {data: 'no',searchable:false, orderable:true,render:function(data,type,row){
                if(row['document_type']=='PO SUPPLIER' && (row['barcode_supplier']!='' || row['barcode_supplier']!=null || row['barcode_supplier']!='null')){
                    var barcode_roll = '<br><label style="font-weight: bold;">Barcode </label> '+row['barcode_supplier']+'<br>';
                    var component = '';
                }else if(row['document_type']=='PO BUYER' && (row['nomor_roll']!='' || row['nomor_roll']!=null || row['nomor_roll']!='null')){
                    var barcode_roll = '<br><label style="font-weight: bold;">Barcode </label> '+row['nomor_roll']+'';

                    if(row['component']!='' || row['component']!=null || row['component']!='null'){
                        var component = ' - <label style="font-weight: bold;">Component </label> '+row['component']+'<br>';
                    }else{
                        var component = '';
                    }
                   
                }else{
                    var barcode_roll = '';
                    var component = '';
                }

                if (row['po_buyer']!=='undefined' || row['po_buyer']!=='null' ) {
                    var pobuyer = '<label style="font-weight: bold;">PO Buyer </label> '+row['po_buyer'];
                }else{
                    var pobuyer = '';
                }

                return row['document_no']+barcode_roll+component+pobuyer;
            }},
            {data: 'document_no',searchable:false, orderable:true},
            {data: 'season',searchable:false, orderable:true},
            {data: 'style',searchable:true, orderable:true},
            {data: 'article_no',searchable:true, orderable:true},
            {data: 'size',searchable:true, orderable:true},
            {data: 'color',searchable:true, orderable:true},
            {data: 'manufacture_name',searchable:true, orderable:true},
            // {data: 'item',searchable:true, orderable:true},
            {data: 'manufacture_name',searchable:true,render:function(data,type,row){
                if (row['description']!='null' || row['description']!='' || row['description']!=null ) {
                    var descitem = row['description'];
                }else{
                    var descitem = "";
                }
                return row['item']+'<br>'+descitem;
            }},
            {data: 'export_to',searchable:true, orderable:true},
        ]
    });
}

function drawTableSpecimen(data){
    $('#table-specimen > tbody').empty();

    for (let i = 0; i < data.length; i++) {
        var no = i+1;
  
        $('#table-specimen > tbody').append('<tr><td><button class="btn btn-danger btn-xs" data-ix="'+i+'" onclick="delTabSpc(this);"><span class="icon-trash"></span></button></td><td>'+data[i]['document_type']+'</td><td>'+data[i]['document_no']+'</td><td>'+data[i]['season']+'</td><td>'+data[i]['style']+'</td><td>'+data[i]['article_no']+'</td><td>'+data[i]['size']+'</td><td>'+data[i]['color']+'</td><td>'+data[i]['item']+'</td></tr>');
    }
}


function delTabSpc(e){
   var idx = e.getAttribute('data-ix');

   dataset.splice(idx,1);

   drawTableSpecimen(dataset);
}

function completedDataSumary(category_specimen,type_specimen,asal) {
    for (let dt = 0; dt < dataset.length; dt++) {
       
        if (category_specimen=='GARMENT') {
            dataset[dt]['test_condition']=$('#test_condition').val();
            dataset[dt]['temperature']=$('#temperature').val();
            dataset[dt]['qty']=$('#qty').val();
            dataset[dt]['machine']=$('#machine').val();
            dataset[dt]['sample_type']=$('#sample_type').val();
            dataset[dt]['manufacture_name']=$('#supplier').val();
            dataset[dt]['fabric_properties']=$('#fabric_properties').val();
            dataset[dt]['material_shell_panel']=$('#material_shell').val()
            dataset[dt]['fibre_composition']=$('#fibre_composition').val();
            dataset[dt]['care_instruction']=$('#care_instruction').val();
            dataset[dt]['remark']=$('#remark').val();

        }else if(category_specimen=='FABRIC' && asal!="DEVELOPMENT"){
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();
        }else if(category_specimen=="ACCESORIES/TRIM" && asal!="DEVELOPMENT"){
            dataset[dt]['remark']=$('#fabric_supplier').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
        }else if((category_specimen=='STRIKE OFF' || category_specimen=='PANEL') && type_specimen=='EMBROIDERY'){
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();
            dataset[dt]['manufacture_name']=$('#supplier_embro').val();
            dataset[dt]['item']=$('#embro_item').val();
            dataset[dt]['description']=$('#embro_description').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fabric_item']=$('#item_fabric').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['interlining_color']=$('#embro_color').val();
            dataset[dt]['garment_size']=$('#embro_size').val();
        }else if ((category_specimen=='STRIKE OFF' || category_specimen=='PANEL')  && type_specimen=='BADGE'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['size']=$('#badge_size').val();
            dataset[dt]['garment_size']=$('#garment_size').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['description']=$('#badge_description').val();
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();

        }else if (category_specimen=='STRIKE OFF' && type_specimen=='RIVERT' ) {
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();
            dataset[dt]['manufacture_name']=$('#supplier_rivert').val();
            dataset[dt]['item']=$('#rivert_item').val();
            dataset[dt]['description']=$('#rivert_description').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fabric_item']=$('#item_fabric').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['interlining_color']=$('#rivert_color').val();
            dataset[dt]['garment_size']=$('#rivert_size').val();
        }else if (category_specimen=='MOCKUP' ){
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['interlining_color']=$('#interlining').val();
            dataset[dt]['machine']=$('#machine').val();
            dataset[dt]['temperature']=$('#temperature').val();
            dataset[dt]['pressure']=$('#pressure').val();
            dataset[dt]['duration']=$('#duration').val();
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['component']=$('#component').val();
        }else if((category_specimen=='STRIKE OFF' || category_specimen=='PANEL')  && type_specimen=='BONDING' ){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['temperature']=$('#temperature').val();
            dataset[dt]['pressure']=$('#pressure').val();
            dataset[dt]['duration']=$('#duration').val();
            dataset[dt]['machine']=$('#machine').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fabric_properties']=$('#fabric_supplier').val();
            dataset[dt]['remark']=$('#remark').val();
        }else if((category_specimen=='PANEL' || category_specimen=='STRIKE OFF') && type_specimen=='FUSE'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['machine']=$('#machine').val();
            dataset[dt]['temperature']=$('#temperature').val();
            dataset[dt]['pressure']=$('#pressure').val();
            dataset[dt]['duration']=$('#duration').val();
            dataset[dt]['item']=$('#item').val();
            dataset[dt]['manufacture_name']=$('#fuse_supplier').val();
            dataset[dt]['remark']=$('#remark').val();
        }else if(category_specimen=='STRIKE OFF' && type_specimen=='HEAT TRANSFER'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['pelling']=$('#pelling').val();
            dataset[dt]['pad']=$('#pad').val();
            dataset[dt]['temperature']=$('#temperature').val();
            dataset[dt]['pressure']=$('#pressure').val();
            dataset[dt]['duration']=$('#duration').val();
            dataset[dt]['machine']=$('#machine').val();
            dataset[dt]['component']=$('#component').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['remark']=$('#remark').val();
        }else if(category_specimen=='PANEL' && type_specimen=='HEAT TRANSFER'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['item']=$('#item').val();
            dataset[dt]['color']=$('#color').val();
            dataset[dt]['manufacture_name']=$('#heat_supplier').val();
            dataset[dt]['pelling']=$('#pelling').val();
            dataset[dt]['pad']=$('#pad').val();
            dataset[dt]['temperature']=$('#temperature').val();
            dataset[dt]['pressure']=$('#pressure').val();
            dataset[dt]['duration']=$('#duration').val();
            dataset[dt]['machine']=$('#machine').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['remark']=$('#remark').val();
        }else if((category_specimen=='PANEL' ||category_specimen=='STRIKE OFF' ) && type_specimen=='INTERLINING'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['item']=$('#item').val();
            dataset[dt]['color']=$('#color').val();
            dataset[dt]['manufacture_name']=$('#interlining_supplier').val();
            dataset[dt]['temperature']=$('#temperature').val();
            dataset[dt]['pressure']=$('#pressure').val();
            dataset[dt]['duration']=$('#duration').val();
            dataset[dt]['machine']=$('#machine').val();
            dataset[dt]['remark']=$('#remark').val();
        }else if(category_specimen=='STRIKE OFF'  && type_specimen=='PAD PRINT'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['thread']=$('#thread').val();
            dataset[dt]['garment_size']=$('#size_page').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();
        }else if(category_specimen=='PANEL'  && type_specimen=='PAD PRINT'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['thread']=$('#thread').val();
            dataset[dt]['garment_size']=$('#size_page').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();
            dataset[dt]['manufacture_name']=$('#pad_print_supplier').val();
            dataset[dt]['item']=$('#item').val();
            dataset[dt]['color']=$('#color').val();
            dataset[dt]['description']=$('#description').val();
        }else if((category_specimen=='STRIKE OFF' || category_specimen=='PANEL')  && type_specimen=='PRINTING'){
            dataset[dt]['style_name']=$('#style_name').val();
            dataset[dt]['size']=$('#garment_size').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_type']=$('#fabric_type').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
            dataset[dt]['fibre_composition']=$('#fabric_composition').val();

        }else if(category_specimen=='FABRIC' && asal=="DEVELOPMENT"){
            // dataset[dt]['fibre_composition']=$('#fibre_composition').val();
            dataset[dt]['style']=$('#style').val();
            dataset[dt]['article_no']=$('#article').val();
        }else if(category_specimen=='ACCESORIES/TRIM' && asal=="DEVELOPMENT"){
            dataset[dt]['style']=$('#style').val();
            dataset[dt]['article_no']=$('#article').val();
            dataset[dt]['fabric_item']=$('#fabric_item').val();
            dataset[dt]['fabric_color']=$('#fabric_color').val();
        }
        // else if(category_specimen=='PANEL'  && type_specimen=='PRINTING'){
        //     dataset[dt]['style_name']=$('#style_name').val();
        //     dataset[dt]['description']=$('#description').val();
        //     dataset[dt]['garment_size']=$('#size').val();
        //     dataset[dt]['item']=$('#item').val();
        //     dataset[dt]['color']=$('#color').val();
        //     dataset[dt]['fabric_item']=$('#fabric_item').val();
        //     dataset[dt]['fabric_type']=$('#fabric_type').val();
        //     dataset[dt]['fabric_color']=$('#fabric_color').val();
        //     dataset[dt]['fibre_composition']=$('#fabric_composition').val();
        //     dataset[dt]['manufacture_name']=$('#supplier_print').val();

        // }

        

    }

}
</script>
@endsection