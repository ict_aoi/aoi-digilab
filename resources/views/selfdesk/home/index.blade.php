@extends('layouts.desk')
@section('header')
<!-- <div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
            <li class="active">List TRF</li>
        </ul>
    </div>
</div> -->
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class=" panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <!-- <form action="{{ route('desk.getData') }}" method="GET" id="form-filter"> -->
                        <div class="col-md-9">
                            <label><b>Search key</b></label>
                            <input type="text" name="key" id="key" class="form-control" placeholder="No. Trf / Method / NIK / Username / Style / Article">
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-primary" id="btn-filter" type="button" style="margin-top: 30px;">Search</button>
                            <button class="btn btn-success" id="btn-create" type="button" style="margin-top: 30px;">Create</button>
                            <button class="btn btn-warning" id="btn-return" type="button" style="margin-top: 30px;">Return</button>
                        </div>
                    <!-- </form> -->
                    
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
                            <thead>
                                <tr>
                                    <!-- <th> No</th> -->
                                    <th>#</th>
                                    <th>TRF No.</th>
                                    <th>Origin Specimen</th>
                                    <th>Test Required</th>
                                    <th>Date Information</th>
                                    <th>Buyer</th>
                                    <th>Lab Location</th>
                                    <th>User</th>
                                    <th>Specimen</th>
                                    <th>Method</th>
                                    <th>Return Test</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('desk.printBarcode')}}" id="printBarcode"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: "{{ route('desk.getData') }}",
            data: function (d) {
                return $.extend({},d,{
                    'key' : $('#key').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id'},
            {data: 'asal_specimen', name: 'asal_specimen'},
            {data: 'test_required', name: 'test_required'},
            {data: 'date_information_remark', name: 'date_information_remark'},
            {data: 'buyer', name: 'buyer'},
            {data: 'lab_location', name: 'lab_location'},
            {data: 'nik', name: 'nik'},
            {data: 'id_category', name: 'id_category'},
            {data: 'method_code', name: 'method_code'},
            {data: 'return_test_sample', name: 'return_test_sample'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
        ]
    });


    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });

    $('#btn-filter').click(function(event){
        event.preventDefault();

        table.clear();
        table.draw();
    });

    $('#btn-create').click(function(){
        window.location.href="{{route('desk.createTrf')}}";
    });

    $('#btn-return').click(function(){
        window.location.href="{{route('desk.return.index')}}";
    });

    // $('#deskPrintBarcode').click(function(){
    //     var trfid = $(this).data('id');

    //     var urlBarcode = $('#printBarcode').attr('href')+'?data='+trfid;

    //     window.open(urlBarcode);
    // });

    table.on('click','.deskPrintBarcode',function(){
        var trfid = $(this).data('id');

        var arr = JSON.stringify([trfid]);

        var urlBarcode = $('#printBarcode').attr('href')+'?data='+arr;
        window.open(urlBarcode);
    });

});
</script>
@endsection