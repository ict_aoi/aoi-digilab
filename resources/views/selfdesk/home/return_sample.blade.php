@extends('layouts.desk')
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Return Sample</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-header-content">
				<div class="panel-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Scan Return</span> </h4>
				</div>
			</div>

			<div class="panel-body">
				<input type="text" class="form-control scanbarcode" id="scanbarcode" required placeholder="Scan in Here">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-header-content">
				<div class="panel-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Return TRF Specimen</span> </h4>
				</div>
			</div>
			<div class="panel-body">
		
				@php($userfact = \Request::session()->get('factory_id'))
				<div class="row">
					<form action="{{ route('desk.return.getData') }}" id="form-search" method="GET">
						<div class="col-md-2">
							<label><b>Factory</b></label>
							<select name="factory_id" id="factory_id" class="select">
								@foreach($factory as $fc)
									<option value="{{$fc->id}}" {{($fc->id==$userfact) ? "selected" : ""}}>{{$fc->factory_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<label><b>Origin</b></label>
							<select name="origin" id="origin" class="select">
								<option value="DEVELOPMENT">DEVELOPMENT</option>
								<option value="PRODUCTION">PRODUCTION</option>
							</select>
						</div>
						<div class="col-md-2">
							<label><b>Buyer</b></label>
							<select name="buyer" id="buyer" class="select">
								@foreach($buyer as $by)
									<option value="{{$by->buyer}}">{{$by->buyer}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-5">
							<label><b>TRF Submit Date</b></label>
							<input type="text" name="submitdate" id="submitdate" class="form-control daterange-basic">
						</div>
						<div class="col-md-1">
							<button class="btn btn-warning" id="btn-submit" type="submit" style="margin-top: 30px;">Submit</button>
						</div>
					</form>
					
				</div>
				<br>
				<div class="row">
					<div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>TRF No.</th>
									<th>Category</th>
									<th>Specimen</th>
									<th>Buyer</th>
									<th>Lab. Location / Origini</th>
									<th>Submit By</th>
									<th>Status</th>
									<th>Return Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('js')
<script text="text/javascript">
$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });


	var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: $('#form-search').attr('action'),
            data: function (d) {
                return $.extend({},d,{
                    'factory_id' : $('#factory_id').val(),
					'asal' : $('#origin').val(),
					'buyer' : $('#buyer').val(),
					'submitdate' : $('#submitdate').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id'},
            {data: 'category', name: 'category'},
            {data: 'specimen', name: 'specimen'},
            {data: 'buyer', name: 'buyer'},
            {data: 'lab_location', name: 'lab_location'},
            {data: 'nik', name: 'nik'},
            {data: 'status', name: 'status'},
            {data: 'return_status', name: 'return_status'}
        ]
    });

	table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#factory_id').on('change',function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#origin').on('change',function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#buyer').on('change',function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('.scanbarcode').keypress(function(event){
		var barcode = $(this).val();

		if (event.which==13) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				type: 'post',
				url : "{{ route('desk.return.scanBarcodeReturn') }}",
				data:{
					barcode:barcode
			},
			beforeSend: function() {
					loading();
			},
			success: function(response) {
				var data_response = response.data;

				alert(data_response.status,data_response.output);
				
				$.unblockUI();
			},
			error: function(response) {
					$.unblockUI();
					alert(response.status,response.responseJSON);
					// console.log(response);
				}
			});


			$('#scanbarcode').val('');
			$('#scanbarcode').focus();
		}
	});
});
</script>
@endsection