<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">PO Buyer / MO</label>
		<input type="text" name="document_no" class="form-control document_no" placeholder="Enter to search po buyer / mo" onblur="getBarcodeRoll(this);">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Style</label>
		<input type="text" name="style" class="form-control style">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Article</label>
		<input type="text" name="article_no" class="form-control article_no" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Season</label>
		<input type="text" name="season" class="form-control season" >
	</div>

	
</div>

<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Style Name</label>
		<input type="text" name="style_name" class="form-control style_name" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Item Badge</label>
		<input type="text" name="item" class="form-control item" disabled>
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Color Badge</label>
		<input type="text" name="color" class="form-control color" disabled>
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Badge Description</label>
		<input type="text" name="description" class="form-control description" disabled>
	</div>

	
</div>
<div class="row">
	<div class="col-md-4">
		<label style="font-weight:bold;">Fabric Color</label>
		<input type="text" name="fabric_color" class="form-control fabric_color" disabled>
	</div>
	<div class="col-md-8">
		<label style="font-weight:bold;">Fabric Material</label>
		<input type="text" class="form-control fibre_composition" disabled>
	</div>
</div>