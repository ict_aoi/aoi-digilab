<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Barcode Roll</label>
		<input type="text" name="nomor_roll" class="form-control nomor_roll" placeholder="Enter to search barcode roll" onblur="getBarcodeRoll(this);">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">PO Supplier</label>
		<input type="text" name="document_no" class="form-control document_no" style="text-transform: uppercase;">
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Supplier Name</label>
		<input type="text" name="manufacture_name" class="form-control manufacture_name" style="text-transform: uppercase;">
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Item</label>
		<input type="text" name="item" class="form-control item" style="text-transform: uppercase;">
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Color</label>
		<input type="text" name="color" class="form-control color" style="text-transform: uppercase;">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Season</label>
		<input type="text" name="season" class="form-control season" style="text-transform: uppercase;">
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Batch No.</label>
		<input type="text" name="batch_number" class="form-control batch_number" style="text-transform: uppercase;">
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Yards</label>
		<input type="text" name="yds_roll" class="form-control yds_roll" style="text-transform: uppercase;">
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Style</label>
		<input type="text" name="style" class="form-control style" style="text-transform: uppercase;">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Invoice</label>
		<input type="text" name="invoice" class="form-control invoice" style="text-transform: uppercase;">
	</div>
	<div class="col-md-6">
		<label style="font-weight:bold;">Fibre Composition</label>
		<textarea class="fibre_composition form-control" style="text-transform: uppercase;"></textarea>
	</div>
</div>