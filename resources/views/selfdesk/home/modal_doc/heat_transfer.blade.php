<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">PO Buyer / MO</label>
		<input type="text" name="document_no" class="form-control document_no" placeholder="Enter to search po buyer / mo" onblur="getBarcodeRoll(this);">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Style</label>
		<input type="text" name="style" class="form-control style">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Article</label>
		<input type="text" name="article_no" class="form-control article_no" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Season</label>
		<input type="text" name="season" class="form-control season" >
	</div>

	
</div>

<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Style Name</label>
		<input type="text" name="style_name" class="form-control style_name" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Peeling</label>
		<input type="text" name="pelling" class="form-control pelling">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Machine</label>
		<input type="text" name="machine" class="form-control machine" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Pad</label>
		<input type="text" name="pad" class="form-control pad" >
	</div>

	
</div>

<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Temperature (&degC)</label>
		<input type="text" name="temperature" class="form-control temperature" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Pressure</label>
		<input type="text" name="pressure" class="form-control pressure">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Duration</label>
		<input type="text" name="duration" class="form-control duration" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Remark</label>
		<input type="text" name="remark" class="form-control remark" >
	</div>
</div>