<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">PO Buyer / MO</label>
		<input type="text" name="document_no" class="form-control document_no" placeholder="Enter to search po buyer / mo" onblur="getBarcodeRoll(this);">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Article</label>
		<input type="text" name="article_no" class="form-control article_no" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Style</label>
		<input type="text" name="style" class="form-control style" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Season</label>
		<input type="text" name="season" class="form-control season" >
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Style Name</label>
		<input type="text" name="style_name" class="form-control style_name">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Qty</label>
		<input type="text" name="qty" class="form-control qty" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Duration</label>
		<input type="text" name="duration" class="form-control duration" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Temperature</label>
		<input type="text" name="temperature" class="form-control temperature" >
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Pressure</label>
		<input type="text" name="pressure" class="form-control pressure" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Machine</label>
		<input type="text" name="machine" class="form-control machine" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Fabric Item</label>
		<input type="text" name="fabric_item" class="form-control fabric_item" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Fabric Type</label>
		<input type="text" name="fabric_type" class="form-control fabric_type" >
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Fabric Color</label>
		<input type="text" name="fabric_color" class="form-control fabric_color" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Interlining Color</label>
		<input type="text" name="interlining_color" class="form-control interlining_color" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Component</label>
		<input type="text" name="component" class="form-control component" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Fabric Composition</label>
		<input type="text" name="fibre_composition" class="form-control fibre_composition" >
	</div>
</div>