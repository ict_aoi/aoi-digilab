
<div id="modal_add" class="modal fade" >
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <center><h4>Add Specimen</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" name="mdcategory" class="mdcategory">
                <input type="hidden" name="mdcategoryspc" class="mdcategoryspc">
                <input type="hidden" name="mdtypespc" class="mdtypespc">
                <input type="hidden" name="mdorigin" class="mdorigin">
                <div class="md-content">
                    
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button class="btn btn-default btn-addspc" id="btn-addspc" type="button">ADD SPECIMEN</button>
                </center>
            </div>
        </div>
    </div>
</div>


<div id="modal_update" class="modal fade" >
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <center><h4>Add Specimen</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" name="mdcategory" class="mdcategory">
                <input type="hidden" name="mdcategoryspc" class="mdcategoryspc">
                <input type="hidden" name="mdtypespc" class="mdtypespc">
                <input type="hidden" name="mdorigin" class="mdorigin">
                <input type="hidden" name="mdidx" class="mdidx">
                <input type="hidden" name="mddocid" class="mddocid">
                <div class="md-content">
                    
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button class="btn btn-default btn-update" id="btn-update" type="button">UPDATE SPECIMEN</button>
                </center>
            </div>
        </div>
    </div>
</div>