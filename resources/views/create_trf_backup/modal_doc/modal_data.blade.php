<div id="modal_doc_fab" class="modal fade" >
    <div class="modal-dialog modal-full">
        <div class="modal-content ">
            <div class="modal-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class ="table table-basic table-condensed table-list" id="table-list" width="100%">
                            <thead>
                                <tr>
                                    <th>Doc. Number</th>
                                    <th>Style</th>
                                    <th>Article</th>
                                    <th>Size</th>
                                    <th>Item</th>
                                    <th>Season</th>
                                    <th>Order No.</th>
                                    <th>Supplier Name</th>
                                    <th>Country</th>
                                    <th>Style Name</th>
                                    <th>Desc.</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>