<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">PO Buyer / MO</label>
		<input type="text" name="document_no" class="form-control document_no" placeholder="Enter to search po buyer / mo" onblur="getBarcodeRoll(this);">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Style</label>
		<input type="text" name="style" class="form-control style">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Article</label>
		<input type="text" name="article_no" class="form-control article_no" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Season</label>
		<input type="text" name="season" class="form-control season" >
	</div>

	
</div>

<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Style Name</label>
		<input type="text" name="style_name" class="form-control style_name" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Print Description</label>
		<input type="text" name="description" class="form-control description" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Print Size</label>
		<input type="text" name="size" class="form-control size" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Print Item</label>
		<input type="text" name="item" class="form-control item" >
	</div>
	
</div>

<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Print Color</label>
		<input type="text" name="color" class="form-control color">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Garment Size</label>
		<input type="text" name="garment_size" class="form-control garment_size" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Item Fabric</label>
		<input type="text" name="fabric_item" class="form-control fabric_item" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Fabric Type</label>
		<input type="text" name="fabric_type" class="form-control fabric_type" >
	</div>
	

</div>

<div class="row">
	<div class="col-md-4">
		<label style="font-weight:bold;">Fabric Color</label>
		<input type="text" name="fabric_color" class="form-control fabric_color">
	</div>
	<div class="col-md-4">
		<label style="font-weight:bold;">Supplier Print</label>
		<input type="text" name="manufacture_name" class="form-control manufacture_name" >
	</div>

	<div class="col-md-4">
		<label style="font-weight:bold;">Fabric Composition</label>
		<input type="text" name="fibre_composition" class="form-control fibre_composition" >
	</div>	
</div>