<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">PO Buyer / MO</label>
		<input type="text" name="document_no" class="form-control document_no" placeholder="Enter to search po buyer / mo" onblur="getBarcodeRoll(this);">

		<input type="hidden" name="item" class="form-control item" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Style</label>
		<input type="text" name="style" class="form-control style" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Article</label>
		<input type="text" name="article_no" class="form-control article_no" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Size</label>
		<input type="text" name="size" class="form-control size" >
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Season</label>
		<input type="text" name="season" class="form-control season" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Color</label>
		<input type="text" name="color" class="form-control color" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Style Name</label>
		<input type="text" name="style_name" class="form-control style_name" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Temperature</label>
		<input type="text" name="temperature" class="form-control temperature" >
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Machine Model</label>
		<input type="text" name="machine" class="form-control machine" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Material Shell & Panel</label>
		<input type="text" name="material_shell_panel" class="form-control material_shell_panel" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Country / Destination</label>
		<input type="text" name="export_to" class="form-control export_to" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Qty</label>
		<input type="number" name="qty" class="form-control qty" min="1" value="1" >
	</div>

</div>

<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Type Of Sample</label>
		<input type="text" name="sample_type" class="form-control sample_type" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Test Condition</label>
		<input type="text" name="test_condition" class="form-control test_condition" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Fibre Composition</label>
		<input type="text" name="fibre_composition" class="form-control fibre_composition" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Fabric Properties</label>
		<input type="text" name="fabric_properties" class="form-control fabric_properties" >
	</div>

	
</div>
<div class="row">
	<div class="col-md-6">
		<label style="font-weight:bold;">Remark</label>
		<input type="text" name="remark" class="form-control remark" >
	</div>
</div>