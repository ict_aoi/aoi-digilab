<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Barcode</label>
		<input type="text" name="barcode_roll" class="form-control barcode_roll" placeholder="Enter to search barcode roll" onblur="getBarcodeRoll(this);">
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Season</label>
		<input type="text" name="season" class="form-control season" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Style</label>
		<input type="text" name="style" class="form-control style" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">Article</label>
		<input type="text" name="article_no" class="form-control article_no" >
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">PO Supplier</label>
		<input type="text" name="barcode_supplier" class="form-control barcode_supplier" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Color Acc</label>
		<input type="text" name="color" class="form-control color" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Item Acc</label>
		<input type="text" name="item" class="form-control item" >
	</div>
	<div class="col-md-3">
		<label style="font-weight:bold;">Qty</label>
		<input type="text" name="qty" class="form-control qty" >
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label style="font-weight:bold;">Supplier Acc</label>
		<input type="text" name="manufacture_name" class="form-control manufacture_name" >
	</div>

	<div class="col-md-3">
		<label style="font-weight:bold;">PO Buyer</label>
		<input type="text" name="po_buyer" class="form-control po_buyer" >
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<label style="font-weight:bold;">Fabric Item</label>
		<input type="text" name="fabric_item" class="form-control fabric_item" >
	</div>
	<div class="col-md-6">
		<label style="font-weight:bold;">Fabric Color</label>
		<input type="text" name="fabric_color" class="form-control fabric_color" >
	</div>
</div>