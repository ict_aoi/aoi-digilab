<style type="text/css">
@page{
    margin : 10 10 10 10;
}

.tab{
    /*font-family: sans-serif;*/
    font-size: 18px;
}
.tab tr  td{
    vertical-align: top;
    margin-bottom: 30px;
}

.img_barcode {
    display: block;
    padding: 0px;
    margin-right: 10px;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
}

.img_barcode > img {
    width: 200px;
    height:40px;
}

.spc{
    border-collapse: collapse;
    width: 100%;
}
.spc tr td{
    border: 1px solid black;
    padding-left: 10px;
}
.spc td{
   height: 10px;
}

.head{
    border-collapse: collapse;
    width: 100%;
    page-break-inside: avoid;

}
.head tr td{
    border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 12px;
}

.head td{
    height: 10px;

}
</style>


<table width="100%">
    <tr>
        <td rowspan="2">
            <center><img src="{{ public_path('assets/icon/aoi.png') }}" alt="Image"width="125"></center>
        </td>
        <td>
            <center><label style="font-size: 20px; font-weight: bold;">APPAREL MATERIAL / GARMENT</label> </center>
        </td>
        <td rowspan="2">
            <center><img src="{{ public_path('assets/icon/bbi_logo.png') }}" alt="Image"width="135"></center>
        </td>
    </tr>
    <tr>
        <td>
            <center><label style="font-size: 20px; font-weight: bold;">TEST REQUISITION FORM</label></center>
        </td>
    </tr>
</table>
<br>
<table width="100%" class="tab">
    <!-- <tr>
        <td colspan="3">
            <div class="img_barcode">
                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data->trf_id, 'C128',2,35) }}" alt="barcode"   />
            </div>
        </td>
    </tr> -->

    <tr>
        <td width="130">No. TRF</td>
        <td width="20">:</td>
        <td>{{$data->trf_id}}</td>
        <td rowspan="4">
            <img style="float: right; padding-right: 50px; width:200px;" src="data:image/png;base64, {!! $qrcode !!}">
        </td>
    </tr>
    
    <tr>
        <td width="130">Submitted by</td>
        <td width="20">:</td>
        <td colspan="2">{{$submit_by}}</td>
    </tr>

    <tr>
        <td width="130">Date of Submitted</td>
        <td width="20">:</td>
        <td colspan="2">{{date_format(date_create($data->created_at),'d-M-Y H:i:s')}}</td>
    </tr>
    <tr>
        <td width="130">Factory</td>
        <td width="20">:</td>
        <td colspan="2">{{$data->factory_name}}</td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    <tr>
        <td width="130">Buyer</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->buyer}}</td>
    </tr>
    <tr>
        <td width="130">Lab Location</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->lab_location}}</td>
    </tr>
    <tr>
        <td width="130">Origin of speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->asal_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Category</td>
        <td width="20">:</td>
        <td>{{ $data->category}}</td>
    </tr>
    <tr>
        <td width="130">Category Speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->category_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Type of Speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->type_specimen}}</td>
    </tr>

    

    <tr>
        <td width="130">Test Required</td>
        <td width="20">:</td>
        <td>{{ $test_required}}</td>
    </tr>

    <tr>
        <td width="130">Date Information</td>
        <td width="20">:</td>
        <td colspan="2">{{ explode("<br>",$dateinfo)[0] }} <br>{{ date_format(date_create(explode("<br>",$dateinfo)[1]),'d-M-Y') }}</td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    

     <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    <tr>
        <td width="130">Return Test</td>
        <td width="20">:</td>
        <td colspan="2">{{ isset($data->return_test_sample)==true ? "YES" : "NO"}}</td>
    </tr>
    <tr>
        <td width="130">Part of speciment tested</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->part_of_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Testing methode</td>
        <td width="20">:</td>
        <td colspan="2">
            @foreach($method as $mtd)
                <label>+ {{$mtd->method_code}} / {{$mtd->method_name}}</label><br>
            @endforeach
        </td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>
    <tr>
        <td colspan="4">
            Remarks: Reporting maximum of 4 days
        </td>
    </tr>
    <tr>
        <td width="130">Status</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->status}}</td>
    </tr>
    <tr>
        <td width="130">Date of validate</td>
        <td width="20">:</td>
        <td colspan="2">{{$labdate}}</td>
    </tr>
    <tr>
        <td width="130">Name of validate</td>
        <td width="20">:</td>
        <td colspan="2">{{ $labpic }}</td>
    </tr>


</table>
<br>



@php( $i = 1)
@foreach($docm as $dc)
    @if($data->category_specimen=='FABRIC')
        @include('report.form.fabric',['idt'=>$dc,'inc'=>$i])
    @elseif($data->category_specimen=='MOCKUP')
        @include('report.form.mockup',['idt'=>$dc,'inc'=>$i])
    @elseif($data->category_specimen=='ACCESORIES/TRIM' )
        @include('report.form.accesories',['idt'=>$dc,'inc'=>$i])
    @elseif($data->category_specimen=='GARMENT' )
        @include('report.form.garment',['idt'=>$dc,'inc'=>$i])
    @elseif(($data->category_specimen=='STRIKE OFF' && $data->type_specimen=='HEAT TRANSFER') ||  $data->category_specimen=='PANEL' )
        @include('report.form.heat_transfer',['idt'=>$dc,'inc'=>$i])
    @elseif($data->category_specimen=='STRIKE OFF' && $data->type_specimen=='EMBROIDERY' )
        @include('report.form.embro',['idt'=>$dc,'inc'=>$i])
    @elseif($data->category_specimen=='STRIKE OFF' && $data->type_specimen=='PRINTING' )
        @include('report.form.printing',['idt'=>$dc,'inc'=>$i])
    @elseif($data->category_specimen=='STRIKE OFF' && $data->type_specimen=='PAD PRINT' )
        @include('report.form.pad_print',['idt'=>$dc])
    @elseif($data->category_specimen=='STRIKE OFF' && $data->type_specimen=='BADGE' )
        @include('report.form.badge',['idt'=>$dc,'inc'=>$i])
    @else
        @include('report.form.error',['idt'=>$dc,'inc'=>$i])
    @endif
    <br>
@php($i++)
@endforeach
