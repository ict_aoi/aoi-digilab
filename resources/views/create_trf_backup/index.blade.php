@extends('layouts.app', ['active' => 'create_trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Create TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp TESTING REQUEST LIST</span> </h4>
				</div>
            </div>
			<div class="panel-body">
                <div class="row {{Auth::user()->admin==false ? 'hidden' : '' }}">
                    <select class="form-control select" id="factory_id">
                        <option value="">--Choose Factory--</option>
                        @foreach($fact as $fc)
                            <option value="{{$fc->id}}" {{$fc->id==auth::user()->factory_id ? 'selected' : ''}}>{{$fc->factory_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row table-responsive">
                    <table class="table table-basic table-condensed" id="table-list">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No TRF</th>
                                <th>Origin of Specimen</th>
                                <th>Test Required</th>
                                <th>Date Information</th>
                                <th>Buyer</th>
                                <th>Lab Location</th>
                                <th>User</th>
                                <th>Category</th>
                                <th>Category Specimen</th> 
                                <th>Type Specimen</th>
                                <th>Methods</th>
                                <th>Return Test Sample </th>
                                <th>Status</th>
                                <th>Remark</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
			</div>
		</div>


	</div>
</div>

@endsection



@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons:[
            {
                text:"Create TRF",
                className:"btn btn-primary",
                action : function (e, dt, node, config)
                {
                  window.location.href= "{{ route('trfcreate.formCreate') }}";
                }
            }
        ],
        ajax: {
            type: 'GET',
            url: "{{ route('trfcreate.getData') }}",
            data: function (d) {
                return $.extend({},d,{
                    'factory_id' : $('#factory_id').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id',searchable:true, orderable:true},
            {data: 'asal_specimen', name: 'asal_specimen',searchable:true, orderable:true},
            {data: 'test_required', name: 'test_required',searchable:true, orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:true, orderable:true},
            {data: 'buyer', name: 'buyer',searchable:true, orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:true, orderable:true},
            {data: 'nik', name: 'nik',searchable:true, orderable:true},
            {data: 'category', name: 'category',searchable:true, orderable:true},
            {data: 'category_specimen', name: 'category_specimen',searchable:true, orderable:true},
            {data: 'type_specimen', name: 'type_specimen',searchable:true, orderable:true},
            {data: 'method_code', name: 'method_code',searchable:true, orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:true, orderable:true},
            {data: 'status', name: 'status',searchable:true, orderable:true},
            {data: 'remarks', name: 'remarks',searchable:true, orderable:true},
            {data: 'action', name: 'action',searchable:true, orderable:true},
        ]
    });

    $('#factory_id').change(function(){
        table.clear();
        table.draw();
    });

    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });
});

</script>
@endsection
