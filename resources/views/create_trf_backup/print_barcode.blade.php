<script type="text/javascript">
    window.onload = function(){
        window.print();
    }
</script>
<style type="text/css">
@page{
    margin : 5 5 5 5;
}

.img_barcode {
    display: block;
    padding: 0px;
    margin-right: 2px;
    margin-left: 2px;
    margin-top: 2px;
    margin-bottom: 2px;
}

.img_barcode > img {
    width: 250px;
    height: 50px;

}



</style>

<table>
    <tr>
        <td><center>
            <div class="barcode">
                <div class="img_barcode">
                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data->trf_id, 'C128',2,35) }}" alt="barcode"   />
                </div>
            </div>
        </center></td>
    </tr>
    <tr>
        <td><center><label style="font-size: 15px; font-weight: bold;">{{ $data->trf_id }}</label></center></td>
    </tr>
</table>


