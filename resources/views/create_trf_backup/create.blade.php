@extends('layouts.app', ['active' => 'create_trf'])
@section('header')
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
            <li class="active">Create TRF</li>
        </ul>
    </div>
</div>
@endsection
 
@section('content')
<div class="content">
    <div class="row">
        <div class=" panel panel-flat">
            <div class="page-header-content">
                <div class="page-title">
                    <h4></i> <span class="text-semibold">&nbsp &nbsp  CREATE TESTING REQUEST FORM</span> </h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <label style="color:red;"><b>* Required Value</b></label>
                </div>
                <div class="row form-group">
                    
                      
                        <div class="col-lg-6">
                            <div class="row">
                                <label><b>Dept. Origin</b> <b style="color:red;">*</b></label>
                                <select class="select" id="origin" required>
                                    <option value="null" data-asal="null">--Chosse Origin--</option>
                                    <option value="SSM" data-asal="DEVELOPMENT">Development</option>
                                    <option value="WMS" data-asal="PRODUCTION">Warehouse Material</option>
                                    <option value="FGMS" data-asal="PRODUCTION">Final QA / Finish Goods</option>
                                    <option value="CDMS" data-asal="PRODUCTION">Cutting / Secondary Process</option>
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Buyer</b> <b style="color:red;">*</b></label>
                                <select class="select" id="buyer" onchange="getCtg(this);" required>
                                    <option value="null">--Chosse Buyer--</option>
                                    @foreach($buyer as $by)
                                        <option value="{{$by->buyer}}">{{$by->buyer}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Category</b> <b style="color:red;">*</b></label>
                                <select class="select" id="category" onchange="getCtgSpm(this);" required >
                       
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Category Specimen</b> <b style="color:red;">*</b></label>
                                <select class="select" id="category_specimen" onchange="getType(this);" required>
                                  
                                </select>
                            </div>

                            <div class="row">
                                <label><b>Type Specimen</b> <b style="color:red;">*</b></label>
                                <select class="select" id="type_specimen" onchange="getMeth(this);" required>
                                  
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Testing Methods</b> <b style="color:red;">*</b></label>
                                <div class="multi-select-full">
                                    <select multiple="multiple" data-placeholder="Choosse Methods" class="select" id="test_methods">
                                     
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <label><b>Return Test</b> </label>
                                <select class="select" id="returntest">
                                    <option value="false">NO</option>
                                    <option value="true">YES</option>
                                </select>
                            </div>
                            
                        </div>
 
                        <div class="col-lg-6">
                            <div class="row">
                                <label><b>Lab Location</b></label>
                                <select class="select" id="labloction">
                                    @foreach($labloc as $lab) 
                                        <option value="{{$lab->name}}" {{Auth::user()->factory_id==$lab->id ? 'selected' : ''}}>{{$lab->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                
                                <div class="radio">

                                    <div class="col-md-3">
                                        <label class="display-block"><b> Pre-Production</b></label>
                                        <label class="radio"><input type="radio" name="test_req" value="preproductiontesting_m">
                                            M (Model Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="preproductiontesting_a">
                                            A (Article Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="preproductiontesting_selective">
                                            Selective
                                        </label>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label class="display-block"><b> 1 Bulk Testing</b></label>
                                        <label class="radio"><input type="radio" name="test_req" value="bulktesting_m">
                                            M (Model Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="bulktesting_a">
                                            A (Article Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="bulktesting_selective">
                                            Selective
                                        </label>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="display-block"><b> Re-Order Testing</b></label>
                                        <label class="radio"><input type="radio" name="test_req" value="reordertesting_m">
                                            M (Model Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="reordertesting_a">
                                            A (Article Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="reordertesting_selective">
                                            Selective
                                        </label>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="display-block"><b> Re-Test</b></label>
                                        <label class="radio"><input type="radio" name="test_req" value="retest">
                                            Re-Test
                                        </label>
                                        <input type="text" name="no_trf" id="no_trf" class="form-control">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <label><b>Date Infromation</b> <b style="color:red;">*</b></label>
                                    <div class="form-group">
                                        <label class="radio-inline"><input type="radio" name="radio_date_info" value="buy_ready" >Buy ready (special for development testing)</label>
                                        <label class="radio-inline"><input type="radio" name="radio_date_info" value="podd">PODD</label>
                                        <label class="radio-inline"><input type="radio" name="radio_date_info" value="output_sewing">1st output sewing</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="date_info" class="form-control date_info" id="anytime-weekday" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label><b>Part yang akan ditest</b> <b style="color:red;">*</b></label>
                                
                                <textarea name="part_test" id="part_test" class="form-control" placeholder="part yang akan ditest" required=""></textarea>
                            </div>
                        </div>
               
                    
                </div>

                
            </div>
        </div>

        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row form-group">
                    <div class="row">
                        <button class="btn btn-primary" id="btn-add"><span class="icon-plus2"></span> Add</button>    
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class ="table table-basic table-condensed table-doclist" id="table-doclist">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Doc Type</th>
                                        <th>Doc. Number</th>
                                        <th>Season</th>
                                        <th>Style</th>
                                        <th>Article</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Item</th>
                                        <th>Fibre Composition</th>
                                        <th>Manufacture Name</th>
                                        <th>Exported To</th>
                                        <th>No. Roll</th>
                                        <th>Batch No.</th>
                                        <th>PO. Supplier</th>
                                        <th>Yds Roll</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <button class="btn btn-success" id="btn-save"><span class="icon-add"></span> Save</button>    
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modal')
    @include('create_trf.modal_doc.modal')
    @include('create_trf.modal_doc.modal_data')
@endsection

 
@section('js')
<script type="text/javascript">
const arrs = [];
$(document).ready(function(){


    $('#btn-add').click(function(){
        var ctg = $('#category').val();
        var ctg_spc = $('#category_specimen').val();
        var type_spc = $('#type_specimen').val();
        var origin = $('#origin').find(':selected').data('asal');


        $('#modal_add .md-content').empty();
        if (ctg==null ||ctg=='' || ctg=='null' || ctg_spc==null ||ctg_spc=='' || ctg_spc=='null' || type_spc==null ||type_spc=='' || type_spc=='null' || origin==null ||origin=='' || origin=='null'  ) {

            alert(422,"Please select Origin / Catgory / Category Specimen / Type Specimen / Buyer");

            return false;
        }else{

            $.ajax({
                type: 'get',
                url :"{{ route('trfcreate.mapsModalDoc')}}",
                data :{ctgspc:ctg_spc,typespc:type_spc} ,
                success: function(response) {
                    $('#modal_add .mdcategory').val(ctg);
                    $('#modal_add .mdcategoryspc').val(ctg_spc);
                    $('#modal_add .mdtypespc').val(type_spc);
                    $('#modal_add .mdorigin').val(origin);

                    $('#modal_add .md-content').prepend(response);
                    $('#modal_add').modal('show');
                },
                error: function(response) {
                    console.log(response);
                }
                    
            });
            

        }


    });

    $('#origin').change(function(){
        $("input[type=radio][value=buy_ready]").prop("disabled",true);
        $("input[type=radio][value=podd]").prop("disabled",true);
        $("input[type=radio][value=output_sewing]").prop("disabled",true);

        if ($(this).val()=='SSM') {
            $("#labloction").prop('disabled',false);
            $("input[type=radio][value=buy_ready]").prop("disabled",false);
        }else if ($(this).val()=='WMS') {
            $("#labloction").prop('disabled',true);
            $("input[type=radio][value=podd]").prop("disabled",false);
        }else if ($(this).val()=='FGMS') {
            $("#labloction").prop('disabled',true);
            $("input[type=radio][value=podd]").prop("disabled",false);
            $("input[type=radio][value=output_sewing]").prop("disabled",false);
        }else if ($(this).val()=='CDMS') {
            $("#labloction").prop('disabled',true);
            $("input[type=radio][value=podd]").prop("disabled",false);
            $("input[type=radio][value=output_sewing]").prop("disabled",false);
        }

    });



    $('#modal_add').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });

    $('#btn-save').click(function(event){
        event.preventDefault();

        var origin = $('#origin').val();
        var buyer = $('#buyer').val();
        var category = $('#category').val();
        var category_specimen = $('#category_specimen').val();
        var type_specimen = $('#type_specimen').val();
        var test_methods = $('#test_methods').val();
        var returntest = $('#returntest').val();
        // var addInfo = $('#addInfo').val();
        var part_test = $('#part_test').val();
        var no_trf = $('#no_trf').val();
        var date_info = $('.date_info').val();
        var radio_date_info =$('input[name=radio_date_info]:checked').val();
        var test_req =$('input[name=test_req]:checked').val();
        var asal = $('#origin').find(':selected').data('asal');
        var labloction = $('#labloction').val();

 

        if (origin=='' || buyer=='' || category=='' || category_specimen=='' || returntest=='' || part_test=='' || radio_date_info==''  || date_info=='' || test_methods=='' || asal=='' || labloction=='') {

            alert(422,"Origin / Buyer / Category / Category Specimen / Return Test / Part Test / Date Info / Test Requirement Required ! ! !");

            return false;
        }

        if (test_req=="retest" && no_trf=="") {
            alert(422,"Previous number TRF required ! ! !");
        }

        if (arrs.length==0) {
            alert(422,"Document Speciment Required ! ! !");

            return false;
        }


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('trfcreate.createTrf') }}",
            data:{
                origin:origin,
                asal:asal,
                buyer:buyer,
                category_specimen:category_specimen,
                category:category,
                type_specimen:type_specimen,
                test_methods:test_methods,
                test_req:test_req,
                radio_date_info:radio_date_info,
                date_info:date_info,
                part_test:part_test,
                returntest:returntest,
                no_trf:no_trf,
                datas:arrs,
                labloction:labloction
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
               var notif = response.data;
                alert(notif.status,notif.output);
                window.location.href="{{ route('trfcreate.index') }}";
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });



    });

    


    $(document).on('show.bs.modal', '.modal', function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
    });

    $(document).on('hidden.bs.modal', '.modal', function () {
            $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

    $('#modal_doc_fab .table-list > tbody').on('click','tr',function(){
        var parse = $('#modal_doc_fab .table-list').DataTable().row(this).data();
 
        if (parse['specimen']=="FABRIC") {
            $('#modal_add .nomor_roll').val(parse['barcode_roll']);
            $('#modal_add .document_no').val(parse['docno']);
            $('#modal_add .manufacture_name').val(parse['supplier_name']);
            $('#modal_add .item').val(parse['item']);
            $('#modal_add .color').val(parse['color']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .batch_number').val(parse['batch_no']);
            $('#modal_add .yds_roll').val(parse['qty']);
            $('#modal_add .fibre_composition').val(parse['fabric_composition']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .invoice').val(parse['invoice']);

        }else if(parse['specimen']=="ACCESORIES/TRIM"){
            $('#modal_add .barcode_roll').val(parse['barcode_roll']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .article_no').val(parse['article']);
            $('#modal_add .barcode_supplier').val(parse['docno']);
            $('#modal_add .color').val(parse['color']);
            $('#modal_add .item').val(parse['item']);
            $('#modal_add .qty').val(parse['qty']);
            $('#modal_add .manufacture_name').val(parse['supplier_name']);
            $('#modal_add .po_buyer').val(parse['purchase_no']);
        }else if(parse['specimen']=="MOCKUP"){
            $('#modal_add .document_no').val(parse['docno']);
            $('#modal_add .article_no').val(parse['article']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .style_name').val(parse['style_name']);
            $('#modal_add .fibre_composition').val(parse['invoice']);
        }else if((parse['specimen']=="STRIKE OFF" && parse['type']=="HEAT TRANSFER") ||  parse['specimen']=="PANEL"){
            $('#modal_add .document_no').val(parse['docno']);
            $('#modal_add .article_no').val(parse['article']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .style_name').val(parse['style_name']);
        }else if(parse['specimen']=="STRIKE OFF" && (parse['type']=="EMBROIDERY" || parse['type']=="PRINTING")){
            $('#modal_add .document_no').val(parse['docno']);
            $('#modal_add .article_no').val(parse['article']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .garment_size').val(parse['size']);
            $('#modal_add .color').val(parse['color']);
            $('#modal_add .style_name').val(parse['style_name']);
            $('#modal_add .fibre_composition').val(parse['fabric_composition']);
        }else if(parse['specimen']=="STRIKE OFF" && parse['type']=="PAD PRINT"){
            $('#modal_add .document_no').val(parse['docno']);
            $('#modal_add .article_no').val(parse['article']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .style_name').val(parse['style_name']);
        }else if(parse['specimen']=="STRIKE OFF" && parse['type']=="BADGE"){
            $('#modal_add .document_no').val(parse['docno']);
            $('#modal_add .article_no').val(parse['article']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .style_name').val(parse['style_name']);
            $('#modal_add .fibre_composition').val(parse['fabric_composition']);
        }else if(parse['specimen']=="GARMENT"){
            $('#modal_add .document_no').val(parse['docno']);
            $('#modal_add .article_no').val(parse['article']);
            $('#modal_add .style').val(parse['style']);
            $('#modal_add .season').val(parse['season']);
            $('#modal_add .style_name').val(parse['style_name']);
            $('#modal_add .size').val(parse['size']);
            $('#modal_add .color').val(parse['color']);
            $('#modal_add .fibre_composition').val(parse['fabric_composition']);
            $('#modal_add .export_to').val(parse['country']);
            $('#modal_add .item').val(parse['item']);

        }

        $('#modal_doc_fab').modal('hide');
    });

    $('#modal_add .btn-addspc').on('click',function(event){
        event.preventDefault();

        var mdcategory = $('#modal_add .mdcategory').val();
        var mdcategoryspc = $('#modal_add .mdcategoryspc').val();
        var mdtypespc = $('#modal_add .mdtypespc').val();
        var mdorigin = $('#modal_add .mdorigin').val();

        if (mdcategoryspc==="FABRIC") {

                 arrs.push({
                        
                        'document_type'     :   "PO SUPPLIER",
                        'document_no'       :   $('#modal_add .document_no').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   '',
                        'size'              :   '',
                        'color'             :   $('#modal_add .color').val(),
                        'fibre_composition' :   $('#modal_add .fibre_composition').val(),
                        'manufacture_name'  :   $('#modal_add .manufacture_name').val(),
                        'export_to'         :   '',
                        'nomor_roll'        :   $('#modal_add .nomor_roll').val(),
                        'batch_number'      :   $('#modal_add .batch_number').val(),
                        'item'              :   $('#modal_add .item').val(),
                        'barcode_supplier'  :   '',
                        'yds_roll'          :   $('#modal_add .yds_roll').val(),
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   $('#modal_add .invoice').val(),
                        'fabric_color'      :   '',
                        'interlining_color' :   '',
                        'qty'               :   '',
                        'machine'           :   '',
                        'temperature'       :   '',
                        'pressure'          :   '',
                        'duration'          :   '',
                        'style_name'        :   '',
                        'fabric_item'       :   '',
                        'fabric_type'       :   '',
                        'thread'            :   '',
                        'material_shell_panel'  :   '',
                        'product_category'  :   '',
                        'remark'            :   '',
                        'pad'               :   '',
                        'pelling'           :   '',
                        'component'         :   '',
                        'garment_size'      :   '',
                        'sample_type'       :   '',
                        'test_condition'    :   '',
                        'fabric_properties' :   '',
                        'description'       : ''
                 });
        }else if(mdcategoryspc==='ACCESORIES/TRIM'){
            
            arrs.push({
                        
                        'document_type'     :   "PO SUPPLIER",
                        'document_no'       :   $('#modal_add .barcode_supplier').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   $('#modal_add .article_no').val(),
                        'size'              :   '',
                        'color'             :   $('#modal_add .color').val(),
                        'fibre_composition' :   '',
                        'manufacture_name'  :   $('#modal_add .manufacture_name').val(),
                        'export_to'         :   '',
                        'nomor_roll'        :   '',
                        'batch_number'      :   '',
                        'item'              :   $('#modal_add .item').val(),
                        'barcode_supplier'  :   $('#modal_add .po_buyer').val(),
                        'yds_roll'          :   '',
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   '',
                        'fabric_color'      :   $('#modal_add .fabric_color').val(),
                        'interlining_color' :   '',
                        'qty'               :   '',
                        'machine'           :   '',
                        'temperature'       :   '',
                        'pressure'          :   '',
                        'duration'          :   '',
                        'style_name'        :   '',
                        'fabric_item'       :   $('#modal_add .fabric_item').val(),
                        'fabric_type'       :   '',
                        'thread'            :   '',
                        'material_shell_panel'  :   '',
                        'product_category'  :   '',
                        'remark'            :   '',
                        'pad'               :   '',
                        'pelling'           :   '',
                        'component'         :   '',
                        'garment_size'      :   '',
                        'sample_type'       :   '',
                        'test_condition'    :   '',
                        'fabric_properties' :   '',
                        'description'       : ''
                 });
        }else if(mdcategoryspc==='MOCKUP'){
            switch(mdorigin){
                case "PRODUCTION" : 
                    var org = "PO BUYER";
                break;

                case "DEVELOPMENT" : 
                    var org = "MO";
                break;
            }

            arrs.push({
                        
                        'document_type'     :   org,
                        'document_no'       :   $('#modal_add .document_no').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   $('#modal_add .article_no').val(),
                        'size'              :   '',
                        'color'             :   '',
                        'fibre_composition' :   $('#modal_add .fibre_composition').val(),
                        'manufacture_name'  :   '',
                        'export_to'         :   '',
                        'nomor_roll'        :   '',
                        'batch_number'      :   '',
                        'item'              :   '',
                        'barcode_supplier'  :   '',
                        'yds_roll'          :   '',
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   '',
                        'fabric_color'      :   $('#modal_add .fabric_color').val(),
                        'interlining_color' :   $('#modal_add .interlining_color').val(),
                        'qty'               :   $('#modal_add .qty').val(),
                        'machine'           :   $('#modal_add .machine').val(),
                        'temperature'       :   $('#modal_add .temperature').val(),
                        'pressure'          :   $('#modal_add .pressure').val(),
                        'duration'          :   $('#modal_add .duration').val(),
                        'style_name'        :   $('#modal_add .style_name').val(),
                        'fabric_item'       :   $('#modal_add .fabric_item').val(),
                        'fabric_type'       :   $('#modal_add .fabric_type').val(),
                        'thread'            :   '',
                        'material_shell_panel'  :   '',
                        'product_category'  :   '',
                        'remark'            :   '',
                        'pad'               :   '',
                        'pelling'           :   '',
                        'component'         :   $('#modal_add .component').val(),
                        'garment_size'      :   '',
                        'sample_type'       :   '',
                        'test_condition'    :   '',
                        'fabric_properties' :   '',
                        'description'       : ''
                 });
        }else if((mdcategoryspc==='STRIKE OFF'  && mdtypespc=="HEAT TRANSFER" ) || mdcategoryspc==='PANEL'){

            switch(mdorigin){
                case "PRODUCTION" : 
                    var org = "PO BUYER";
                break;

                case "DEVELOPMENT" : 
                    var org = "MO";
                break;
            }

            arrs.push({ 
                        
                        'document_type'     :   org,
                        'document_no'       :   $('#modal_add .document_no').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   $('#modal_add .article_no').val(),
                        'size'              :   '',
                        'color'             :   '',
                        'fibre_composition' :   '',
                        'manufacture_name'  :   '',
                        'export_to'         :   '',
                        'nomor_roll'        :   '',
                        'batch_number'      :   '',
                        'item'              :   '',
                        'barcode_supplier'  :   '',
                        'yds_roll'          :   '',
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   '',
                        'fabric_color'      :   '',
                        'interlining_color' :   '',
                        'qty'               :   '',
                        'machine'           :   $('#modal_add .machine').val(),
                        'temperature'       :   $('#modal_add .temperature').val(),
                        'pressure'          :   $('#modal_add .pressure').val(),
                        'duration'          :   $('#modal_add .duration').val(),
                        'style_name'        :   $('#modal_add .style_name').val(),
                        'fabric_item'       :   '',
                        'fabric_type'       :   '',
                        'thread'            :   '',
                        'material_shell_panel'  :   '',
                        'product_category'  :   '',
                        'remark'            :   $('#modal_add .remark').val(),
                        'pad'               :   $('#modal_add .pad').val(),
                        'pelling'           :   $('#modal_add .pelling').val(),
                        'component'         :   '',
                        'garment_size'      :   '',
                        'sample_type'       :   '',
                        'test_condition'    :   '',
                        'fabric_properties' :   '',
                        'description'       : ''
                 });
        }else if(mdcategoryspc==='STRIKE OFF' && (mdtypespc=="PRINTING" || mdtypespc=="EMBROIDERY" )){

            switch(mdorigin){
                case "PRODUCTION" : 
                    var org = "PO BUYER";
                break;

                case "DEVELOPMENT" : 
                    var org = "MO";
                break;
            }

            arrs.push({
                        
                        'document_type'     :   org,
                        'document_no'       :   $('#modal_add .document_no').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   $('#modal_add .article_no').val(),
                        'size'              :   $('#modal_add .size').val(),
                        'color'             :   $('#modal_add .color').val(),
                        'fibre_composition' :   $('#modal_add .fibre_composition').val(),
                        'manufacture_name'  :   $('#modal_add .manufacture_name').val(),
                        'export_to'         :   '',
                        'nomor_roll'        :   '',
                        'batch_number'      :   '',
                        'item'              :   $('#modal_add .item').val(),
                        'barcode_supplier'  :   '',
                        'yds_roll'          :   '',
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   '',
                        'fabric_color'      :   $('#modal_add .fabric_color').val(),
                        'interlining_color' :   '',
                        'qty'               :   '',
                        'machine'           :   '',
                        'temperature'       :   '',
                        'pressure'          :   '',
                        'duration'          :   '',
                        'style_name'        :   $('#modal_add .style_name').val(),
                        'fabric_item'       :   $('#modal_add .fabric_item').val(),
                        'fabric_type'       :   $('#modal_add .fabric_type').val(),
                        'thread'            :   '',
                        'material_shell_panel'  :   '',
                        'product_category'  :   '',
                        'remark'            :   '',
                        'pad'               :   '',
                        'pelling'           :   '',
                        'component'         :   '',
                        'garment_size'      :   $('#modal_add .garment_size').val(),
                        'sample_type'       :   '',
                        'test_condition'    :   '',
                        'fabric_properties' :   '',
                        'description'       : $('#modal_add .description').val()
                 });
        }else if(mdcategoryspc==='STRIKE OFF' && mdtypespc=="PAD PRINT" ){

            switch(mdorigin){
                case "PRODUCTION" : 
                    var org = "PO BUYER";
                break;

                case "DEVELOPMENT" : 
                    var org = "MO";
                break;
            }

            arrs.push({
                        
                        'document_type'     :   org,
                        'document_no'       :   $('#modal_add .document_no').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   $('#modal_add .article_no').val(),
                        'size'              :   $('#modal_add .size').val(),
                        'color'             :   $('#modal_add .pad_color').val(),
                        'fibre_composition' :   $('#modal_add .fibre_composition').val(),
                        'manufacture_name'  :   $('#modal_add .manufacture_name').val(),
                        'export_to'         :   '',
                        'nomor_roll'        :   '',
                        'batch_number'      :   '',
                        'item'              :   $('#modal_add .item').val(),
                        'barcode_supplier'  :   '',
                        'yds_roll'          :   '',
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   '',
                        'fabric_color'      :   $('#modal_add .fabric_color').val(),
                        'interlining_color' :   '',
                        'qty'               :   '',
                        'machine'           :   '',
                        'temperature'       :   '',
                        'pressure'          :   '',
                        'duration'          :   '',
                        'style_name'        :   $('#modal_add .style_name').val(),
                        'fabric_item'       :   $('#modal_add .fabric_item').val(),
                        'fabric_type'       :   $('#modal_add .fabric_type').val(),
                        'thread'            :   $('#modal_add .thread').val(),
                        'material_shell_panel'  :   '',
                        'product_category'  :   '',
                        'remark'            :   '',
                        'pad'               :   '',
                        'pelling'           :   '',
                        'component'         :   '', 
                        'garment_size'      :   '',
                        'sample_type'       :   '',
                        'test_condition'    :   '',
                        'fabric_properties' :   '',
                        'description'       :   $('#modal_add .description').val()
                 });
        }else if(mdcategoryspc==='STRIKE OFF' && mdtypespc=="BADGE" ){

            switch(mdorigin){
                case "PRODUCTION" : 
                    var org = "PO BUYER";
                break;

                case "DEVELOPMENT" : 
                    var org = "MO";
                break;
            }

            arrs.push({
                        
                        'document_type'     :   org,
                        'document_no'       :   $('#modal_add .document_no').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   $('#modal_add .article_no').val(),
                        'size'              :   $('#modal_add .size').val(),
                        'color'             :   $('#modal_add .color').val(),
                        'fibre_composition' :   $('#modal_add .fibre_composition').val(),
                        'manufacture_name'  :   '',
                        'export_to'         :   '',
                        'nomor_roll'        :   '',
                        'batch_number'      :   '',
                        'item'              :   $('#modal_add .item').val(),
                        'barcode_supplier'  :   '',
                        'yds_roll'          :   '',
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   '',
                        'fabric_color'      :   $('#modal_add .fabric_color').val(),
                        'interlining_color' :   '',
                        'qty'               :   '',
                        'machine'           :   '',
                        'temperature'       :   '',
                        'pressure'          :   '',
                        'duration'          :   '',
                        'style_name'        :   $('#modal_add .style_name').val(),
                        'fabric_item'       :   '',
                        'fabric_type'       :   '',
                        'thread'            :   '',
                        'material_shell_panel'  :   '',
                        'product_category'  :   '',
                        'remark'            :   $('#modal_add .description').val(),
                        'pad'               :   '',
                        'pelling'           :   '',
                        'component'         :   '',
                        'garment_size'      :   '',
                        'sample_type'       :   '',
                        'test_condition'    :   '',
                        'fabric_properties' :   '',
                        'description'       : ''
                 });
        }else if(mdcategoryspc==='GARMENT'){

            switch(mdorigin){
                case "PRODUCTION" : 
                    var org = "PO BUYER";
                break;

                case "DEVELOPMENT" : 
                    var org = "MO";
                break;
            }

            arrs.push({
                        
                        'document_type'     :   org,
                        'document_no'       :   $('#modal_add .document_no').val(),
                        'style'             :   $('#modal_add .style').val(),
                        'article_no'        :   $('#modal_add .article_no').val(),
                        'size'              :   $('#modal_add .size').val(),
                        'color'             :   $('#modal_add .color').val(),
                        'fibre_composition' :   $('#modal_add .fibre_composition').val(),
                        'manufacture_name'  :   '',
                        'export_to'         :   $('#modal_add .export_to').val(),
                        'nomor_roll'        :   '',
                        'batch_number'      :   '',
                        'item'              :   $('#modal_add .item').val(),
                        'barcode_supplier'  :   '',
                        'yds_roll'          :   '',
                        'season'            :   $('#modal_add .season').val(),
                        'invoice'           :   '',
                        'fabric_color'      :   '',
                        'interlining_color' :   '',
                        'qty'               :   $('#modal_add .qty').val(),
                        'machine'           :   $('#modal_add .machine').val(),
                        'temperature'       :   $('#modal_add .temperature').val(),
                        'pressure'          :   '',
                        'duration'          :   '',
                        'style_name'        :   $('#modal_add .style_name').val(),
                        'fabric_item'       :   '',
                        'fabric_type'       :   '',
                        'thread'            :   '',
                        'material_shell_panel'  :   $('#modal_add .material_shell_panel').val(),
                        'product_category'  :   '',
                        'remark'            :   $('#modal_add .remark').val(),
                        'pad'               :   '',
                        'pelling'           :   '',
                        'component'         :   '',
                        'garment_size'      :   '',
                        'sample_type'       :   $('#modal_add .sample_type').val(),
                        'test_condition'    :   $('#modal_add .test_condition').val(),
                        'fabric_properties' :   $('#modal_add .fabric_properties').val(),
                        'description'       :   ''
                 });
        }
        drawDocTable(arrs);

        $('#modal_add').modal('hide');
    });

});

function getCtg(e){
    var buyer = e.value;

    if (buyer=="") {
        $('#category').empty();
        $('#category').append('<option value="null">--Filter Category--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trfcreate.getCtg') }}",
            data :{buyer:buyer} ,
            success: function(response) {
               var data = response.data;
               $('#category').empty();
               $('#category').append('<option value="null">--Filter Category--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#category').append('<option value="'+data[i]['category']+'">'+data[i]['category']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }


    
}

function getCtgSpm(e){
   var category = e.value;
   var buyer = $('#buyer').val();
   var origin = $('#origin').val();

    if (buyer=="" && category=="") {
        $('#category_specimen').empty();
        $('#category_specimen').append('<option value="null">--Filter Category Specimen--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trfcreate.getCtgSpc') }}",
            data :{buyer:buyer,category:category,origin:origin} ,
            success: function(response) {
               var data = response.data;
               $('#category_specimen').empty();
               $('#category_specimen').append('<option value="null">--Filter Category Specimen--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#category_specimen').append('<option value="'+data[i]['category_specimen']+'">'+data[i]['category_specimen']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}

function getType(e){
   var category_specimen = e.value;
   var buyer = $('#buyer').val();
   var category = $('#category').val();

    if (buyer=="" && category=="" && category_specimen=="") {
        $('#type_specimen').empty();
        $('#type_specimen').append('<option value="null">--Filter Type Specimen--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trfcreate.getTypeSpc') }}",
            data :{buyer:buyer,category:category,category_specimen:category_specimen} ,
            success: function(response) {
               var data = response.data;
               $('#type_specimen').empty();
               $('#type_specimen').append('<option value="null">--Filter Type Specimen--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#type_specimen').append('<option value="'+data[i]['type_specimen']+'">'+data[i]['type_specimen']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}

function getMeth(e){
   var category_specimen = $('#category_specimen').val();
   var buyer = $('#buyer').val();
   var category = $('#category').val();
   var type = e.value;

    if (buyer=="" && category=="" && category_specimen) {
        $('#test_methods').empty();

    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trfcreate.getMeth') }}",
            data :{buyer:buyer,category:category,category_specimen:category_specimen,type_specimen:type} ,
            success: function(response) {
               var data = response.data;
               $('#test_methods').empty();

               for (var i = 0; i < data.length; i++) {
                    $('#test_methods').append('<option value="'+data[i]['master_method_id']+'">'+data[i]['method_code']+' '+data[i]['method_name']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}



function getBarcodeRoll(e){
    var key = e.value;
    var ctg_spc = $('#modal_add .mdcategoryspc').val();
    var type_spc = $('#modal_add .mdtypespc').val();
    var origin = $('#modal_add .mdorigin').val();

    if (key.trim()!="") {
        $.ajax({
            type: 'get',
            url :"{{ route('getSpecimenData.getData') }}",
            data :{origin:origin,ctgspc:ctg_spc,typespc:type_spc,key:key} ,
            beforeSend:function(){
                loading();
            },
            success: function(response) {
                $.unblockUI();
               var data = response.data;

                if (data.length>0) {
                    datatablesDoc(data);
                }else{
                    alert(422,"Data not found ! ! !");
                }
               
            },
            error: function(response) {
                $.unblockUI();
                console.log(response);
            }
        });

    }
    
    
}


function datatablesDoc(dts){

    $('#modal_doc_fab .table-list').DataTable().destroy();
    $('#modal_doc_fab .table-list').DataTable({
        "searching":true,
        "paging":true,
        data:dts,
        columns: [
            {data: 'docno',searchable:false, orderable:true, render:function(data,type,row){
                

                if (row['barcode_roll']===null || row['barcode_roll']==='') {
                    var barcode_roll ='';
                }else{
                    var barcode_roll = '<label style="font-weight: bold;">Barcode </label> '+row['barcode_roll']+'<br>';
                }

                return barcode_roll+row['docno'];
            }},
            {data: 'style',searchable:true, orderable:true},
            {data: 'article',searchable:true, orderable:true},
            {data: 'size',searchable:true, orderable:true},
            {data: 'item',searchable:true, orderable:true},
            {data: 'season',searchable:true, orderable:true},
            {data: 'purchase_no',searchable:true, orderable:true},
            {data: 'supplier_name',searchable:true, orderable:true},
            {data: 'country',searchable:true, orderable:true},
            {data: 'style_name',searchable:true, orderable:true},
            {data: 'desc',searchable:true, orderable:true,render:function(data,type,row){

               
                if (row['fabric_composition']===null || row['fabric_composition']==='') {
                    var fabric_composition ='';
                }else{
                    var fabric_composition = '<label style="font-weight: bold;">Composition</label> '+row['fabric_composition']+' <br>';
                }

                if (row['weight']===null || row['weight']==='') {
                    var weight ='';
                }else{
                    var weight = '<label style="font-weight: bold;">Weight</label> '+row['weight']+'<br>';
                }

                if (row['qty']===null || row['qty']==='') {
                    var qty ='';
                }else{
                    var qty = '<label style="font-weight: bold;">Qty</label> '+row['qty']+' <br>';
                }

                return fabric_composition+' '+weight+' '+qty;

            }}
        ]
    });

    $('#modal_doc_fab').modal('show');
}

function drawDocTable(list){
    $('#table-doclist >tbody').empty();
    
    for (var i = 0; i < list.length; i++) {
        var no = i+1;
        $('#table-doclist >tbody').append('<tr><td>'+no+'</td><td>'+list[i]['document_type']+'</td><td>'+list[i]['document_no']+'</td><td>'+list[i]['season']+'</td><td>'+list[i]['style']+'</td><td>'+list[i]['article_no']+'</td><td>'+list[i]['size']+'</td><td>'+list[i]['color']+'</td><td>'+list[i]['item']+'</td><td>'+list[i]['fibre_composition']+'</td><td>'+list[i]['manufacture_name']+'</td><td>'+list[i]['export_to']+'</td><td>'+list[i]['nomor_roll']+'</td><td>'+list[i]['batch_number']+'</td><td>'+list[i]['barcode_supplier']+'</td><td>'+list[i]['yds_roll']+'</td></tr>');
    }
}
</script>
@endsection
