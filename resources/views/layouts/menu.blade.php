<ul class="navigation navigation-main navigation-accordion">

	<!-- Main -->
	@permission(['menu-dashboard'])
	<li class="{{ $active == 'dashboard' ? 'active' : ''}}"><a href="{{ route('home.index') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
	@endpermission

	@permission(['menu-user-management'])
	<li class="navigation-header"><span>User Management</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'user_account' ? 'active' : ''}}"><a href="{{ route('admin.user_account')}}"><i class="icon-theater"></i> <span>User Account</span></a></li>
	<li class="{{ $active == 'role_user' ? 'active' : ''}}"><a href="{{route('admin.formRole')}}"><i class="icon-menu2"></i> <span>Role</span></a></li>
	@endpermission


	@permission(['menu-master'])
	<li class="navigation-header"><span>Master</span> <i class="icon-menu"></i></li>
	<li>
		<a href="#"><i class="icon-books"></i> <span>Master Data</span></a>
		<ul>
			<li class="{{ $active == 'master_category' ? 'active' : ''}}"><a href="{{ route('category.index')}}"><span>Master Category</span></a></li>
			<li class="{{ $active == 'master_method' ? 'active' : ''}}"><a href="{{ route('method.index')}}"><span>Master Method</span></a></li>
			<li class="{{ $active == 'master_requirements' ? 'active' : ''}}"><a href="{{ route('requirements.index')}}"><span>Master Requirements</span></a></li>

			<li class="{{ $active == 'master_specialtest' ? 'active' : ''}}"><a href="{{ route('dimensiapp.index') }}"><span>Master Mapping Parameter</span></a></li>

			<li class="{{ $active == 'master_holiday' ? 'active' : ''}}"><a href="{{ route('holiday.index') }}"><span>Master Holiday</span></a></li>
		</ul>
	</li>
	@endpermission


	@permission(['menu-create-trf','menu-trf'])
	<li class="navigation-header"><span>TESTING REQUEST FORM</span> <i class="icon-menu"></i></li>

	@permission(['menu-create-trf'])
	<li class="{{ $active == 'create_trf' ? 'active' : ''}}"><a href="{{ route('trf.create.index')}}"><span><i class="icon-quill4"></i> Create TRF</span></a></li>
	@endpermission

	@permission(['menu-trf'])
	<li class="{{ $active == 'menu-dashboard-trf' ? 'active' : ''}}"><a href="{{ route('dashboardTrf.index')}}"><span><i class="icon-list-numbered"></i> Waiting List TRF</span></a></li>
	@endpermission
	
	@endpermission

	@permission(['menu-testing-trf'])
	<li class="navigation-header"><span>TESTING RESULT</span> <i class="icon-menu"></i></li>

	<li class="{{ $active == 'menu-testing-trf' ? 'active' : ''}}"><a href="{{ route('result.index') }}"><i class="icon-pencil7"></i><span>Testing Result List</span></a></li>
	@endpermission

	@permission(['menu-return-specimen'])
	<li class="navigation-header"><span>Return Specimen</span> <i class="icon-menu"></i></li>
	
	<li class="{{ $active == 'menu-return-specimen' ? 'active' : ''}}"><a href="{{ route('return.index') }}"><i class="icon-undo2"></i><span>Return Specimen</span></a></li>
	@endpermission

	@permission(['menu-report-test','menu-report-logbook','menu-all-specimen','menu-return-specimen','menu-method-result'])
	<li class="navigation-header"><span>REPORT</span> <i class="icon-menu"></i></li>

		@permission(['menu-report-test'])
		{{-- <li class="{{ $active == 'menu-report' ? 'active' : ''}}"><a href="{{ route('report.testing.reportTest') }}"><i class="icon-book"></i><span>Report Test</span></a></li>

		<li class="{{ $active == 'menu-report-specimen' ? 'active' : ''}}"><a href="{{ route('report.speciment.reportSpecimen')}}"><i class="icon-umbrella"></i><span>Report Specimen</span></a></li> --}}

		<li class="{{ $active == 'menu-report-trf' ? 'active' : ''}}"><a href="{{ route('report.trf.reportTrf') }}"><i class="icon-svg"></i><span>Report TRF</span></a></li>

		@permission(['menu-report-logbook'])
		<li class="{{ $active == 'menu-report-logbook' ? 'active' : ''}}"><a href="{{route('report.logbook.reportlogbook')}}"><i class="icon-book"></i><span>Log Book TRF</span></a></li>
		@endpermission
		
		@permission(['menu-all-specimen'])
		<li class="{{ $active == 'menu-all-specimen' ? 'active' : ''}}"><a href="{{ route('report.allspecimen.reportAllspc')}}"><i class="icon-list"></i><span>Report All Specimen</span></a></li>
		@endpermission

		@permission(['menu-return-specimen'])
		<li class="{{ $active == 'menu-report-returnspecimen' ? 'active' : ''}}"><a href="{{ route('report.allspecimen.reportReturn')}}"><i class="icon-undo"></i><span>Report Return Specimen</span></a></li>
		@endpermission

		@permission(['menu-method-result'])
		<li class="{{ $active == 'menu-method-result' ? 'active' : ''}}"><a href="{{ route('report.methodresult.index') }}"><i class="icon-finish"></i><span>Report Method Result</span></a></li>
		@endpermission
	@endpermission

	@permission(['menu-escalation-trf'])
	<li class="{{ $active == 'menu-escalation-trf' ? 'active' : ''}}"><a href="{{ route('esc.index') }}"><i class="icon-eye8"></i><span>Escalation</span></a></li>
	@endpermission

	@endpermission

	<!-- /page kits -->

</ul>
