<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>Add Off Day</h4></center>
			</div>
			<div class="modal-body">
					<form action="{{ route('holiday.add') }}" method="POST" id="form_add">
						@csrf

						<div class="row">
							<div class="col-md-12">
								<label><b>Off Date</b></label>
								<input type="text" class="form-control daterange-basic off_date" name="off_date" id="off_date">
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<label><b>Reason</b></label>
								<!-- <input type="text" class="form-control" name="reason" id="reason"> -->
								<textarea class="form-control reason" name="reason" id="reason"></textarea>
							</div>
						</div>
						<div class="row">
							<button type="submit" id="btn-save" class="btn btn-primary">SAVE</button>
						</div>
					</form>
			</div>
		</div>
	</div>
</div>