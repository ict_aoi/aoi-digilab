@extends('layouts.app', ['active' => 'master_holiday'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Master Holiday</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Master Holiday</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                    <th>No </th>
									<th>Created At</th>
									<th>Off Date</th>
									<th>Day Reason</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('modal')
    @include('master.holiday.md_add')
@endsection
@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Add',
                className: 'btn btn-sm bg-warning',
                action: function (e, dt, node, config)
                {
                    $('#modal_add').modal('show');
                }
            },
       ],
        ajax: {
            type: 'GET',
            url: "{{ route('holiday.getData') }}",
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'off_date', name: 'off_date'},
            {data: 'reason', name: 'reason'},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
        ]
    });


    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });


    $('#form_add').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_add').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);
                table.clear();
                table.draw();
                $('#modal_add').modal('hide');
                 
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });


   

    $('#table-list').on('click','.deleteHoliday',function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{route('holiday.delete')}}",
            data :{id:$(this).data('id')} ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);
                table.clear();
                table.draw();
                 
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });

    $("#modal_add").on('hidden.bs.modal', function () {
        $('#off_date').val('');
        $('#reason').val('');
        var now =  new Date();

       set(now);
    });


    $('.daterange-basic').on('show.daterangepicker', function(e){
        var modalZindex = $(e.target).closest('.modal').css('z-index');
        $('.daterangepicker').css('z-index', modalZindex+1);
    });



});

function set(now){
    $('.daterange-basic').daterangepicker({
        startDate : moment(now),
        endDate : moment(now)
    });
}
</script>
@endsection