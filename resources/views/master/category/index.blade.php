@extends('layouts.app', ['active' => 'master_category'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master Data</a></li>
			<li class="active">Master Category</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Master Category</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="category-table">
							<thead>
								<tr>
                                    <th>No </th>
									<th>Created At</th>
									<th>Category</th>
									<th>Category Specimen</th>
									<th>Type Specimen</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('category.excel') }}" id="exportExcel"></a>
@endsection

@section('modal')
	@include('master.category.md_upload')
	@include('master.category.md_add')
	@include('master.category.md_edit')
@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){
	

	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#category-table').DataTable({
        buttons: [
            // {
            //     text: 'Upload',
            //     className: 'btn btn-sm bg-primary',
            //     action: function (e, dt, node, config)
            //     {
            //       $('#modal_upload').modal('show');
            //     }
            // },
            {
                text: 'Add',
                className: 'btn btn-sm bg-warning',
                action: function (e, dt, node, config)
                {
                  $('#modal_add').modal('show');
                }
            },
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                   var filter = table.search();

                   window.location.href = $('#exportExcel').attr('href')+'?filter='+filter;
                }
            },
       ],
        ajax: {
	        type: 'GET',
	        url: "{{ route('category.data') }}",
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'created_at', name: 'created_at'},
	        {data: 'category', name: 'category'},
	        {data: 'category_specimen', name: 'category_specimen'},
	       	{data: 'type_specimen', name: 'type_specimen'},
	        {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
	    ]
    });


	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});


	$('#form_upload').submit(function(event){
		event.preventDefault();


		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_upload').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);
                 
                $('#modal_upload').modal('hide');

                window.location.reload();
                 
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']);
                console.log(response);
            }
        });
	});


	$('#category-table').on('click','.delctg',function(){
		// event.preventDefault();


		var id = $(this).data('id');

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('category.delete') }}",
            data :{id:id},
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);

                window.location.reload();
                 
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']);
                console.log(response);
            }
        });
	});


	$('#category-table').on('click','.edctg',function(){
		// event.preventDefault();


		var id = $(this).data('id');
		var catg = $(this).data('catg');
		var ctgsp = $(this).data('ctgsp');
		var type = $(this).data('type');
		var sequence = $(this).data('sequence');


		$('#modal_edit .txctg').val(catg);
		$('#modal_edit .txctgsp').val(ctgsp);
		$('#modal_edit .txtype').val(type);
		$('#modal_edit .txid').val(id);
		$('#modal_edit .txsquen').val(sequence);
		$('#modal_edit').modal('show');
		
	});


	$('#form_edit').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_edit').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);

                window.location.reload();
                 
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']);
                console.log(response);
            }
        });
	});

	$('#form_add').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_add').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);
                 
                $('#modal_add').modal('hide');

                window.location.reload();
                 
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']);
                console.log(response);
            }
        });
	});


	$('.modal').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });
});






</script>
@endsection