<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>Edit Category</h4></center>
			</div>
			<div class="modal-body">
					<form action="{{ route('category.update') }}" method="POST" id="form_edit">
						@csrf

						<div class="row">
							<div class="col-md-12">
								<label><b>Category</b></label>
								<input type="text" name="txctg" id="txctg" class="form-control txctg" style="text-transform: uppercase;" required>
								<input type="hidden" name="txid" id="txid" class="txid">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label><b>Category Specimen</b></label>
								<input type="text" name="txctgsp" id="txctgsp" class="form-control txctgsp"  style="text-transform: uppercase;" required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label><b>Type Specimen</b></label>
								<input type="text" name="txtype" id="txtype" class="form-control txtype" style="text-transform: uppercase;" required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label><b>Sequence</b></label>
								<input type="number" name="txsquen" id="txsquen" class="form-control txsquen" style="text-transform: uppercase;" >
							</div>
						</div>
						<div class="row">
							<button type="submit" id="btn-save" class="btn btn-primary">SAVE</button>
						</div>
					</form>
			</div>
		</div>
	</div>
</div>