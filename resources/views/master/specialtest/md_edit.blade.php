<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>EDIT MAPPING TEST</h4></center>
			</div>
			<div class="modal-body">
				<form action="{{ route('dimensiapp.updateData') }}" method="POST" id="form_edit" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<div class="col-md-3">
							<label style="font-size:16px; font-weight: bold;">Method</label><br>
                            <label class="selmeth" style="font-size:16px;"></label><br>
                            <label class="selmethname" style="font-size:16px;"></label>
						</div>
						<div class="col-md-3">
							<label style="font-size:16px; font-weight: bold;">Category Specimen</label><br>
                            <label class="selctgspc" style="font-size:16px;"></label>
						</div>
						<div class="col-md-3">
							<label style="font-size:16px; font-weight: bold;">Parameter</label><br>
                            <label class="selctparam" style="font-size:16px;"></label>
						</div>
						<div class="col-md-3">
							<label style="font-size:16px; font-weight: bold;">Buyer</label><br>
                            <label class="txbuyer" style="font-size:16px;"></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<label style="font-weight:bold;">
								Measuring Position
							</label> <br>
							<input type="text" name="txmeasur" class="form-control txmeasur">
						</div>

						<div class="col-md-2">
							<label style="font-weight:bold;">
								Code
							</label> <br>
							<input type="text" name="txcode" class="form-control txcode">
						</div>

						<div class="col-md-2">
							<label style="font-weight:bold;">
								Number Test
							</label> <br>
							<input type="number" min="1" name="txnum" class="form-control txnum">
						</div>

						<div class="col-md-2">
							<label style="font-weight:bold;">
								Squence
							</label> <br>
							<input type="number" min="0" name="txsque" class="form-control txsque">
						</div>
						<div class="col-md-2">
							<label style="font-weight:bold;">
								Before Test
							</label> <br>
							<select class="select selbefore" name="selbefore">
								<option value="yes">YES</option>
								<option value="no">NO</option>
							</select>
							
						</div>

						<div class="col-md-2">
							<label style="font-weight:bold;">
								Standart Test
							</label> <br>
							<select class="select selstand" name="selstand">
								<option value="yes">YES</option>
								<option value="no">NO</option>
							</select>
							
						</div>
					</div>
					<hr>
					<div class="row">
						<center>
							<input type="hidden" name="txid" class="txid">
							<button type="submit" class="btn btn-primary">Update</button>
						</center>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>