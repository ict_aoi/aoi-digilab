@extends('layouts.app', ['active' => 'master_specialtest'])
@section('header')
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Master Data</a></li>
            <li class="active">Master Mapping Parameter</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class=" panel panel-flat">
            <div class="page-header-content">
                <div class="page-title">
                    <h4></i> <span class="text-semibold">&nbsp &nbsp  Master Mapping Parameter</span> </h4>
                </div>
            </div>
            <div class="panel-body">

                <div class="row form-group">
                    <div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="table-list">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Created At</th>
                                    <th>Method Code</th>
                                    <th>Category Specimen</th>
                                    <th>Parameter</th>
                                    <th>Measuring Position</th>
                                    <th>Code</th>
                                    <th>Number Of Test</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('dimensiapp.excel') }}" id="exportExcel"></a>
@endsection

@section('modal')
    @include('master.specialtest.md_upload')
    @include('master.specialtest.md_add')
    @include('master.specialtest.md_edit')
@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var table = $('#table-list').DataTable({
        buttons: [
            // {
            //     text: 'Upload',
            //     className: 'btn btn-sm bg-primary',
            //     action: function (e, dt, node, config)
            //     {
            //       $('#modal_upload').modal('show');
            //     }
            // },
            {
                text: 'Add',
                className: 'btn btn-sm bg-warning',
                action: function (e, dt, node, config)
                {
                  showAdd();
                }
            },
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                   var filter = table.search();

                   window.location.href = $('#exportExcel').attr('href')+'?filter='+filter;
                }
            },
       ],
        ajax: {
            type: 'GET',
            url: "{{ route('dimensiapp.getData') }}",
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at', orderable: true, searchable: true},
            {data: 'methods', name: 'methods', orderable: true, searchable: true},
            {data: 'category_specimen', name: 'category_specimen', orderable: true, searchable: true},
            {data: 'parameter', name: 'parameter', orderable: true, searchable: true},
            {data: 'measuring_position', name: 'measuring_position', orderable: true, searchable: true},
            {data: 'code', name: 'code', orderable: true, searchable: true},
            {data: 'number_test', name: 'number_test', orderable: true, searchable: true},
            {data: 'action', name: 'action',searchable:false,sortable:false,orderable:false},
        ]
    });


    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });


    $('#form_upload').submit(function(event){
        event.preventDefault();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_upload').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();

                var response = response.data;

                alert(response.status,response.output);

                $('#modal_upload').modal('hide');

                window.location.reload();
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });



    $('#table-list').on('click','.delSpcTest',function(){
        var id = $(this).data('id');


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('dimensiapp.delete') }}",
            data :{id:id},
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();

                var response = response.data;

                alert(response.status,response.output);


                window.location.reload();
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });


    $('#table-list').on('click','.edSpcTest',function(e){
        e.preventDefault();

        var id = $(this).data('id');

        $.ajax({
            type: 'get',
            url : "{{ route('dimensiapp.getDataEdit') }}",
            data :{id:id},
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();

                var dMap    = response.data;
                var meth    = response.meth;
                
                $('#modal_edit .selmeth').text(meth['method_code']);
                $('#modal_edit .selctgspc').text(dMap['category_specimen']);
                $('#modal_edit .selctparam').text(dMap['parameter']);
                $('#modal_edit .txbuyer').text(dMap['buyer']);
                $('#modal_edit .selmethname').text(meth['method_name']);
                

                $('#modal_edit .txbuyer').val(dMap['buyer']);
                $('#modal_edit .txmeasur').val(dMap['measuring_position']);
                $('#modal_edit .txcode').val(dMap['code']);
                $('#modal_edit .txnum').val(dMap['number_test']);
                $('#modal_edit .txsque').val(dMap['sequence']); 
                $('#modal_edit .txid').val(id); 

                if (dMap['before_test']==true) {
                     $('#modal_edit .selbefore').val("yes").trigger('change'); 
                }else{
                    $('#modal_edit .selbefore').val("no").trigger('change'); 
                }

                if (dMap['standart_value']==true) {
                     $('#modal_edit .selstand').val("yes").trigger('change'); 
                }else{
                    $('#modal_edit .selstand').val("no").trigger('change'); 
                }
               
        

                $('#modal_edit').modal('show');
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
        
    });



    $('#form_edit').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_edit').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();

                var response = response.data;

                alert(response.status,response.output);
                 
                
                window.location.reload();
                
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });

    $('#form_add').submit(function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_add').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();

                var response = response.data;

                alert(response.status,response.output);
                 
                
                window.location.reload();
                
            },
            error: function(response) {
                $.unblockUI();
                alert(response['status'],response['responseJSON']);
                console.log(response);
            }
        });
    });


    $('.modal').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });

});

function showAdd(){
    $('#modal_add .selMeth').empty();
    $('#modal_add .selMeth').append('<option value="">--Choose Method--</option>');
    $.ajax({
        type: 'get',
        url : "{{ route('dimensiapp.getMeth') }}",
        success: function(response) {
            var meth = response.meth;

            for (var i = 0; i < meth.length; i++) {
                $('#modal_add .selMeth').append('<option value="'+meth[i]['master_method_id']+'">'+meth[i]['method_code']+' '+meth[i]['method_name']+'</option>');
            }

            $('#modal_add').modal('show');
        },
        error: function(response) {

            alert(response['status'],response['responseJSON']);
            console.log(response);
        }
    });
}

function getCtgSpc(){
    var meth = $('#modal_add .selMeth').val();

    var where = {'master_method_id':meth};

    $('#modal_add .selctgspc').empty();
    $('#modal_add .selctgspc').append('<option value="">--Choose Category Specimen--</option>');
    $.ajax({
        type: 'get',
        url : "{{ route('dimensiapp.getCtgParm') }}",
        data:{select:"category_specimen",where:where},
        success: function(response) {
            var data = response.data;

            for (var i = 0; i < data.length; i++) {
                $('#modal_add .selctgspc').append('<option value="'+data[i]['category_specimen']+'">'+data[i]['category_specimen']+'</option>');
            }
        },
        error: function(response) {

            alert(response['status'],response['responseJSON']);
            console.log(response);
        }
    });

    $('#modal_add .selctparam').empty();
    $('#modal_add .selctbuyer').empty();
}


function getParam(){
    var meth = $('#modal_add .selMeth').val();
    var ctgspc = $('#modal_add .selctgspc').val();

    var where = {'master_method_id':meth,'category_specimen':ctgspc};

    $('#modal_add .selctparam').empty();
    $('#modal_add .selctparam').append('<option value="">--Choose Parameter--</option>');
    $.ajax({
        type: 'get',
        url : "{{ route('dimensiapp.getCtgParm') }}",
        data:{select:"parameter",where:where},
        success: function(response) {
            var data = response.data;

            for (var i = 0; i < data.length; i++) {
                $('#modal_add .selctparam').append('<option value="'+data[i]['parameter']+'">'+data[i]['parameter']+'</option>');
            }
        },
        error: function(response) {

            alert(response['status'],response['responseJSON']);
            console.log(response);
        }
    });
    
}

function getBuyer(){
    var meth = $('#modal_add .selMeth').val();
    var ctgspc = $('#modal_add .selctgspc').val();
    var param = $('#modal_add .selctparam').val();

    var where = {'master_method_id':meth,'category_specimen':ctgspc,'parameter':param};

    $('#modal_add .selctbuyer').empty();
    $('#modal_add .selctbuyer').append('<option value="">--Choose Buyer--</option>');
    $.ajax({
        type: 'get',
        url : "{{ route('dimensiapp.getCtgParm') }}",
        data:{select:"buyer",where:where},
        success: function(response) {
            var data = response.data;

            for (var i = 0; i < data.length; i++) {
                $('#modal_add .selctbuyer').append('<option value="'+data[i]['buyer']+'">'+data[i]['buyer']+'</option>');
            }
        },
        error: function(response) {

            alert(response['status'],response['responseJSON']);
            console.log(response);
        }
    });
}

</script>
@endsection
