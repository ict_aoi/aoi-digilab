<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>Add Mapping Parameter</h4></center>
			</div>
			<div class="modal-body">
				<form action="{{ route('dimensiapp.addMapp') }}" method="POST" id="form_add">
					<div class="row">
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Method
							</label>
							<select class="select-search selMeth" name="selMeth" required onchange="getCtgSpc();"></select>
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Category Specimen
							</label>
							<select class="select selctgspc" name="selctgspc" required onchange="getParam();"></select>
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Parameter
							</label>
							<select class="select selctparam" name="selctparam" required onchange="getBuyer();"></select>
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Buyer
							</label>
							<select class="select selctbuyer" name="selctbuyer" required></select>
						</div>
					</div>

					<div class="row">
						<div class="col-md-2">
							<label style="font-weight:bold; font-size: 16;">
								Measuring Position
							</label>
							<input type="text" name="txmeasur" class="form-control txmeasur">
						</div>
						<div class="col-md-2">
							<label style="font-weight:bold; font-size: 16;">
								Code
							</label>
							<input type="text" name="txcode" class="form-control txcode">
						</div>
						<div class="col-md-2">
							<label style="font-weight:bold; font-size: 16;">
								Number Test
							</label>
							<input type="number" min="1" name="txnumb" class="form-control txnumb" value="1" required>
						</div>
						<div class="col-md-2">
							<label style="font-weight:bold; font-size: 16;">
								Sequence
							</label>
							<input type="number" name="txseq" class="form-control txseq">
						</div>
						<div class="col-md-2">
							<label style="font-weight:bold; font-size: 16;">
								Before Test
							</label>
							<select name="selctbefore" class="select selctbefore">
								<option value="false">NO</option>
								<option value="true">YES</option>
							</select>
						</div>
						<div class="col-md-2">
							<label style="font-weight:bold; font-size: 16;">
								Standart Test
							</label>
							<select name="selctstdr" class="select selctstdr">
								<option value="false">NO</option>
								<option value="true">YES</option>
							</select>
						</div>
					</div>
					<hr>
					<div class="row">
						<center>
							<button class="btn btn-primary" type="submit">Save</button>
						</center>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>