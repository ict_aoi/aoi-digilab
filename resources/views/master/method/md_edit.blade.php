<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>Edit Method</h4></center>
			</div>
			<div class="modal-body">
				<di class="row">
					<div class="col-lg-12">
						<form action="{{ route('method.update') }}" method="POST" id="form_edit" >
							@csrf
							<div class="row">
								<label><b>Method Code</b></label>
								<input type="text" name="txcode" id="txcode" class="form-control txcode" style="text-transform:uppercase;" required>
								<input type="hidden" name="txid" id="txid" class="txid">
							</div>

							<div class="row">
								<label><b>Method Name</b></label>
								<input type="text" name="txname" id="txname" class="form-control txname" style="text-transform:uppercase;" required>
							</div>

							<div class="row">
								<label><b>Category</b></label>
								<select class="select form-control txctg" id="txctg">
									<option value="COLOUR FASTNESS TESTS">COLOUR FASTNESS TESTS</option>
									<option value="FUNCTIONAL TESTS">FUNCTIONAL TESTS</option>
									<option value="PHYSICAL TESTS">PHYSICAL TESTS</option>
								</select>
							</div>

							<div class="row">
								<label><b>Type</b></label>
								<select class="select form-control txtype" id="txtype">
									<option value="NON-MANDATORY">NON-MANDATORY</option>
									<option value="MANDATORY">MANDATORY</option>
								</select>
							</div>
							<div class="row">
								<button class="btn btn-primary" type="submit" id="btn-save">Update</button>
							</div>
						</form>
					</div>
				</di>
				
			</div>
		</div>
	</div>
</div>