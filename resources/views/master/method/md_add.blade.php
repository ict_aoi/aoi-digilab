<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>Add Method</h4></center>
			</div>
			<div class="modal-body">
				<di class="row">
					<div class="col-lg-12">
						<form action="{{ route('method.addMethod') }}" method="POST" id="form_add" >
							@csrf
							<div class="row">
								<label><b>Method Code</b></label>
								<input type="text" name="txcode" id="txcode" class="form-control txcode" style="text-transform:uppercase;" required>
							</div>

							<div class="row">
								<label><b>Method Name</b></label>
								<input type="text" name="txname" id="txname" class="form-control txname" style="text-transform:uppercase;" required>
							</div>

							<div class="row">
								<label><b>Category</b></label>
								<select class="select form-control txctg" id="txctg" name="txctg">
									<option value="COLOUR FASTNESS TESTS">COLOUR FASTNESS TESTS</option>
									<option value="FUNCTIONAL TESTS">FUNCTIONAL TESTS</option>
									<option value="PHYSICAL TESTS">PHYSICAL TESTS</option>
								</select>
							</div>

							<div class="row">
								<label><b>Type</b></label>
								<select class="select form-control txtype" id="txtype" name="txtype">
									<option value="NON-MANDATORY">NON-MANDATORY</option>
									<option value="MANDATORY">MANDATORY</option>
								</select>
							</div>

							<div class="row">
								<label><b>Availability</b></label>
								<div class="multi-select-full">
                                    <select multiple="multiple" data-placeholder="Choosse Availability" class="select" id="txavab" name="txavab">
                                     	<option value="AOI1">AOI1</option>
                                     	<option value="AOI2">AOI2</option>
                                     	<option value="AOI3">AOI3</option>
                                    </select>
                                </div>
							</div>

							<div class="row">
								<label><b>Sequence</b></label>
								<input type="number" name="txsquen" id="txsquen" class="form-control txsquen" style="text-transform:uppercase;">
							</div>

							<div class="row">
								<button class="btn btn-primary" type="submit" id="btn-save">Update</button>
							</div>
						</form>
					</div>
				</di>
				
			</div>
		</div>
	</div>
</div>