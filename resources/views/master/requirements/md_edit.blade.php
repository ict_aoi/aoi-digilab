<div id="modal_edit" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4>Edit Requirements</h4></center>
            </div>
            <div class="modal-body">
                <form action="{{ route('requirements.update') }}" method="POST" id="form_edit" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <label style="font-size:16px; font-weight: bold;">Method</label><br>
                            <label class="selmeth" style="font-size:16px;"></label><br>
                            <label class="selmethname" style="font-size:16px;"></label>
                        </div>
                        <div class="col-md-3">
                            <!-- <label><b>Category</b></label>
                            <select class="form-control selcateg select" name="selcateg"></select> -->

                            <label style="font-size:16px; font-weight: bold;">Category</label><br>
                            <label class="selcateg" style="font-size:16px;"></label>
                        </div>
                        <div class="col-md-3">
                            <!-- <label><b>Category Specimen</b></label>
                            <select class="form-control selctgspc select" name="selctgspc"></select> -->
                            <label style="font-size:16px; font-weight: bold;">Category Specimen</label><br>
                            <label class="selctgspc" style="font-size:16px;"></label>
                        </div>
                        <div class="col-md-3">
                            <!-- <label><b>Type Specimen</b></label>
                            <select class="form-control seltypespc select" name="seltypespc"></select> -->

                            <label style="font-size:16px; font-weight: bold;">Category Specimen</label><br>
                            <label class="selctgspc" style="font-size:16px;"></label>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-md-4">
                            <label style="font-weight:bold;">Composition</label><br>
                            <textarea class="form-control txcomps" name="txcomps"></textarea>
                        </div>
                        <div class="col-md-4">
                            <label style="font-weight:bold;">Test Treatment</label><br>
                            <textarea class="form-control txtreat" name="txtreat"></textarea>
                        </div>
                        <div class="col-md-4">
                            <label style="font-weight:bold;">Parameter</label><br>
                            <textarea class="form-control txparam" name="txparam"></textarea>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Buyer</label><br>
                            <input type="text" name="txbuyer" class="form-control txbuyer" readonly>
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Operator</label><br>
                            <select class="select txopert" name="txopert"></select>
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Uom</label><br>
                            <input type="text" name="txuom" class="form-control txuom">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Remark</label><br>
                            <input type="text" name="txremark" class="form-control txremark">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Value 1</label><br>
                            <input type="text" name="txval1" class="form-control txval1">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Value 2</label><br>
                            <input type="text" name="txval2" class="form-control txval2">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Value 3</label><br>
                            <input type="text" name="txval3" class="form-control txval3">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Value 4</label><br>
                            <input type="text" name="txval4" class="form-control txval4">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Value 5</label><br>
                            <input type="text" name="txval5" class="form-control txval5">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Value 6</label><br>
                            <input type="text" name="txval6" class="form-control txval6">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">Value 7</label><br>
                            <input type="text" name="txval7" class="form-control txval7">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight:bold;">False</label><br>
                            <input type="text" name="txfalse" class="form-control txfalse">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <input type="hidden" name="txid" class="txid">
                        <center><button class="btn btn-success" type="submit">Update</button></center>
                    </div>
                </form>
            </div>
        </div> 
    </div>
</div>