@extends('layouts.app', ['active' => 'master_requirements'])
@section('header')
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Master Data</a></li>
            <li class="active">Master Requirements</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class=" panel panel-flat">
            <div class="page-header-content">
                <div class="page-title">
                    <h4></i> <span class="text-semibold">&nbsp &nbsp  Master Requirements</span> </h4>
                </div>
            
            </div>
            <div class="panel-body">
                
                <div class="row form-group">
                    <div class="table-responsive">
                        <table class ="table table-basic table-condensed" id="requirements-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Created At</th>
                                    <th>Buyer</th>
                                    <th>Method Code</th>
                                    <th>Method Name</th>
                                    <th>Category</th>
                                    <th>Composition Specimen</th>
                                    <th>Parameter</th>
                                    <th>Test Treatment</th>
                                    <th>Operation</th>
                                    <th>UOM</th>
                                    <th>Value 1</th>
                                    <th>Value 2</th>
                                    <th>Value 3</th>
                                    <th>Value 4</th>
                                    <th>Value 5</th>
                                    <th>Value 6</th>
                                    <th>Value 7</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('requirements.excel') }}" id="exportExcel"></a>
@endsection

@section('modal')
    @include('master.requirements.md_upload')
    @include('master.requirements.md_add')
    @include('master.requirements.md_edit')

@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#requirements-table').DataTable({
        buttons: [
            // {
            //     text: 'Upload',
            //     className: 'btn btn-sm bg-primary',
            //     action: function (e, dt, node, config)
            //     {
            //       $('#modal_upload').modal('show');
            //     }
            // },
            {
                text: 'Add',
                className: 'btn btn-sm bg-warning',
                action: function (e, dt, node, config)
                {
                  showAdd();
                }
            },
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                   var filter = table.search();

                   window.location.href = $('#exportExcel').attr('href')+'?filter='+filter;
                }
            },
       ],
        ajax: {
            type: 'GET',
            url: "{{ route('requirements.data') }}",
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'buyer', name: 'buyer'},
            {data: 'method_code', name: 'method_code'},
            {data: 'method_name', name: 'method_name'},
            {data: 'category', name: 'category'},
            {data: 'komposisi_specimen', name: 'komposisi_specimen'},
            {data: 'parameter', name: 'parameter'},
            {data: 'perlakuan_test', name: 'perlakuan_test'},
            {data: 'operator_name', name: 'operator_name'},
            {data: 'uom', name: 'uom'},
            {data: 'value1', name: 'value1'},
            {data: 'value2', name: 'value2'},
            {data: 'value3', name: 'value3'},
            {data: 'value4', name: 'value4'},
            {data: 'value5', name: 'value5'},
            {data: 'value6', name: 'value6'},
            {data: 'value7', name: 'value7'},
            {data: 'remarks', name: 'remarks'},
            {data: 'action', name: 'action',searchable:false,sortable:false,orderable:false},
        ]
    });


    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });

    $('#form_upload').submit(function(event){
        event.preventDefault();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_upload').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);
                 
                $('#modal_upload').modal('hide');
                
                window.location.reload();
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });


    $('#requirements-table').on('click','.delRequ',function(){

        var id= $(this).data('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('requirements.delete') }}",
            data :{id:id},
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var response = response.data;

                alert(response.status,response.output);
                 
                
                window.location.reload();
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });

    $('#requirements-table').on('click','.editRequ',function(event){
        event.preventDefault();

        var id = $(this).data('id');
     

        
        $.ajax({
            type: 'get',
            url : "{{ route('requirements.getDataDetail') }}",
            data :{id:id},
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                var dataReq = response.dataReq;

                
                $('#modal_edit .selmeth').text(dataReq['method_code']);
                $('#modal_edit .selmethname').text(dataReq['method_name']);
                $('#modal_edit .selcateg').text(dataReq['category']);
                $('#modal_edit .selctgspc').text(dataReq['category_specimen']);
                $('#modal_edit .selctgspc').text(dataReq['type_specimen']);
                var oprt = response.oprt;
                for (var i = 0; i < oprt.length; i++) {
                    $('#modal_edit .txopert').append('<option value="'+oprt[i]['id']+'">'+oprt[i]['operator_name']+'</option>');
                }

                $('#modal_edit .txopert').val(dataReq['operator_id']);

                $('#modal_edit .txcomps').val(dataReq['komposisi_specimen']);
                $('#modal_edit .txtreat').val(dataReq['perlakuan_test']);
                $('#modal_edit .txparam').val(dataReq['parameter']);
                $('#modal_edit .txbuyer').val(dataReq['buyer']);
                $('#modal_edit .txuom').val(dataReq['uom']);
                $('#modal_edit .txremark').val(dataReq['remarks']);
                $('#modal_edit .txval1').val(dataReq['value1']);
                $('#modal_edit .txval2').val(dataReq['value2']);
                $('#modal_edit .txval3').val(dataReq['value3']);
                $('#modal_edit .txval4').val(dataReq['value4']);
                $('#modal_edit .txval5').val(dataReq['value5']);
                $('#modal_edit .txval6').val(dataReq['value6']);
                $('#modal_edit .txval7').val(dataReq['value7']);
                $('#modal_edit .txfalse').val(dataReq['false']);
                $('#modal_edit .txid').val(id);

            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });

        $('#modal_edit').modal('show');
    });

    $('#form_edit').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_edit').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();

                var response = response.data;

                alert(response.status,response.output);
                 
                
                window.location.reload();
                
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']['message']);
                console.log(response);
            }
        });
    });

    $('#form_add').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_add').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();

                var response = response.data;

                alert(response.status,response.output);
                 
                
                window.location.reload();

                $('.modal').modal('show');
                
            },
            error: function(response) {
                $.unblockUI();
                 alert(response['status'],response['responseJSON']);
                console.log(response);
            }
        });
    });

    $('.modal').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });
});

function showAdd(){
    $('#modal_add .selMeth').empty();
    $('#modal_add .selCtg').empty();
    $('#modal_add .selOpt').empty();
    $('#modal_add .txbuyer').empty();
    $.ajax({
        type: 'get',
        url : "{{ route('requirements.getMethCtg') }}",
        success: function(response) {

            $('#modal_add .txbuyer').append('<option value="">--Choose Buyer--</option>');

            var byr = response.byr;

            for (var i = 0; i < byr.length; i++) {
                 $('#modal_add .txbuyer').append('<option value="'+byr[i]['buyer']+'">'+byr[i]['buyer']+'</option>');
            }

            $('#modal_add .selMeth').append('<option value="">--Choose Method--</option>');

            var meth = response.meth;

            for (var i = 0; i < meth.length; i++) {
                 $('#modal_add .selMeth').append('<option value="'+meth[i]['id']+'">'+meth[i]['method_code']+' '+meth[i]['method_name']+'</option>');
            }


            $('#modal_add .selCtg').append('<option value="">--Choose Category--</option>');

            var ctg = response.ctg;

            for (var j = 0; j < ctg.length; j++) {
                 $('#modal_add .selCtg').append('<option value="'+ctg[j]['category']+'">'+ctg[j]['category']+'</option>');
            }


            $('#modal_add .selOpt').append('<option value="">--Choose Operation--</option>');

            var opt = response.opt;

            for (var k = 0; k < opt.length; k++) {
                 $('#modal_add .selOpt').append('<option value="'+opt[k]['id']+'">'+opt[k]['operator_name']+' ( '+opt[k]['symbol']+' )</option>');
            }

            $('#modal_add').modal('show');
        },
        error: function(response) {
             alert(response['status'],response['responseJSON']);
            console.log(response);
        }
    });

    
}

function getCtgSpc(){
    var category = $('#modal_add .selCtg').val();

    var where = {'category':category};

    $('#modal_add .selCtgspc').empty();

    $.ajax({
        type: 'get',
        url : "{{ route('requirements.getCtgSpcType') }}",
        data : {select:'category_specimen',where:where},
        success: function(response) {
            $('#modal_add .selCtgspc').append('<option value="">--Choose Category Specimen--</option>');
             var data = response.data;
            for (var x = 0; x < data.length; x++) {
                $('#modal_add .selCtgspc').append('<option value="'+data[x]['category_specimen']+'">'+data[x]['category_specimen']+'</option>');
            }
        },
        error: function(response) {
             alert(response['status'],response['responseJSON']);
            console.log(response);
        }
    });
}


function getTypeSpc(){
    var category = $('#modal_add .selCtg').val();
    var category_specimen = $('#modal_add .selCtgspc').val();

    var where = {'category':category,'category_specimen':category_specimen};

    $('#modal_add .selTypespc').empty();

    $.ajax({
        type: 'get',
        url : "{{ route('requirements.getCtgSpcType') }}",
        data : {select:'type_specimen',where:where},
        success: function(response) {
            $('#modal_add .selTypespc').append('<option value="">--Choose Type Specimen--</option>');
             var data = response.data;
            for (var z = 0; z < data.length; z++) {
                $('#modal_add .selTypespc').append('<option value="'+data[z]['type_specimen']+'">'+data[z]['type_specimen']+'</option>');
            }
        },
        error: function(response) {
             alert(response['status'],response['responseJSON']);
            console.log(response);
        }
    });
}


</script>
@endsection