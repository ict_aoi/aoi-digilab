<div id="modal_upload" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4>UPLOAD Requirements</h4></center>
            </div>
            <div class="modal-body">
                <div class="col-lg-8">
                    <form action="{{ route('requirements.import') }}" method="POST" id="form_upload" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <label><b>File Upload :</b></label>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <input type="file" name="file" id="file" class="file-styled" required>
                            </div>
                            <div class="col-lg-4">
                                <button class="btn btn-success" type="submit" id="btn-submit">Upload</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <!-- <button class="btn btn-warning" style="margin-top:35px;">Template</button> -->
                    <a href="{{route('requirements.export')}}" class="btn btn-warning" style="margin-top:35px;">Template</a>
                </div>
            </div>
        </div> 
    </div>
</div>


