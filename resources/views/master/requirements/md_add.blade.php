<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>Add Requirement</h4></center>
			</div>
			<div class="modal-body">
				<form action="{{ route('requirements.addRequirment') }}" method="POST" id="form_add">
					<div class="row">
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Method
							</label>
							<select class="select-search selMeth" name="selMeth"></select>
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Category
							</label>
							<select class="select selCtg" onchange="getCtgSpc();" name="selCtg"></select>
						</div> 

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Category Specimen
							</label>
							<select class="select selCtgspc" onchange="getTypeSpc();" name="selctgspc"></select>
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Type Specimen
							</label>
							<select class="select selTypespc" name="selTypespc"></select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Operating
							</label>
							<select class="select selOpt" name="selOpt"></select>
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Buyer
							</label>
							<select class="select txbuyer" name="txbuyer" required></select>
							<!-- <input type="text" name="txbuyer" class="form-control txbuyer" required> -->
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								UOM
							</label>
							<input type="text" name="txuom" class="form-control txuom">
						</div>

						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Remark
							</label>
							<textarea name="txremark" class="form-control txremark"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label style="font-weight:bold; font-size: 16;">
								Specimen Composition
							</label>
							<input type="text" name="txcomps" class="form-control txcomps">
						</div>

						<div class="col-md-4">
							<label style="font-weight:bold; font-size: 16;">
								Test Treatment
							</label>
							<input type="text" name="txtreat" class="form-control txtreat">
						</div>

						<div class="col-md-4">
							<label style="font-weight:bold; font-size: 16;">
								Parameter
							</label>
							<input type="text" name="txparam" class="form-control txparam">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Value 1
							</label>
							<input type="text" name="val1" class="form-control val1" required>
						</div>
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Value 2
							</label>
							<input type="text" name="val2" class="form-control val2">
						</div>
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Value 3
							</label>
							<input type="text" name="val3" class="form-control val3">
						</div>
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Value 4
							</label>
							<input type="text" name="val4" class="form-control val4">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Value 5
							</label>
							<input type="text" name="val5" class="form-control val5">
						</div>
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Value 6
							</label>
							<input type="text" name="val6" class="form-control val6">
						</div>
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Value 7
							</label>
							<input type="text" name="val7" class="form-control val7">
						</div>
						<div class="col-md-3">
							<label style="font-weight:bold; font-size: 16;">
								Fail
							</label>
							<input type="text" name="txfail" class="form-control txfail">
						</div>
					</div>
					<hr>
					<div class="row">
						<center><button type="submit" class="btn btn-primary">SAVE</button></center>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>