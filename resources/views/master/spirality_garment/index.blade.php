@extends('layouts.app', ['active' => 'master_specialtest'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master Data</a></li>
			<li class="active">Master Dimensi Appearance</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Master Dimensi Appearance</span> </h4>
				</div>
            </div>
			<div class="panel-body">
				
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>No</th>
									<th>Created At</th>
									<th>Method Code</th>
									<th>Parameter</th>
									<th>Measuring Position</th>
									<th>Code</th>
									<th>Category</th>
									<th>Number Of Test</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('spirality_garment.excel') }}" id="exportExcel"></a>
@endsection

<!-- @section('modal')
<div id="modal_upload" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>UPLOAD SPIRALITY GARMENT</h4></center>
			</div>
			<div class="modal-body">
				<div class="col-lg-8">
					<form action="{{ route('spirality_garment.import') }}" method="POST" id="form_upload" enctype="multipart/form-data">
						@csrf
						<div class="row">
							<label><b>File Upload :</b></label>
						</div>
						<div class="row">
							<div class="col-lg-8">
								<input type="file" name="file" id="file" class="file-styled">
							</div>
							<div class="col-lg-4">
								<button class="btn btn-success" type="submit" id="btn-submit">Upload</button>
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<!-- <button class="btn btn-warning" style="margin-top:35px;">Template</button> -->
					<a href="{{route('spirality_garment.export')}}" class="btn btn-warning" style="margin-top:35px;">Template</a>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection -->

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
	var table = $('#table-list').DataTable({
        buttons: [
            // {
            //     text: 'Upload',
            //     className: 'btn btn-sm bg-primary',
            //     action: function (e, dt, node, config)
            //     {
            //       $('#modal_upload').modal('show');
            //     }
            // },
            // {
            //     text: 'Export to Excel',
            //     className: 'btn btn-sm bg-success exportExcel',
            //     action: function (e, dt, node, config)
            //     {
            //        var filter = table.search();

            //        window.location.href = $('#exportExcel').attr('href')+'?filter='+filter;
            //     }
            // },
       ],
        ajax: {
            type: 'GET',
            url: "#",
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
			{data: 'method_code', name: 'method_code'},
			{data: 'parameter', name: 'parameter'},
			{data: 'measuring_position', name: 'measuring_position'},
			{data: 'code', name: 'code'},
			{data: 'category_speciment', name: 'category_speciment'},
			{data: 'value_test', name: 'value_test'},
            {data: 'action', name: 'action',searchable:false,sortable:false,orderable:false},
        ]
    });


    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

});






</script>
@endsection