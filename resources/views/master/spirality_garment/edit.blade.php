@extends('layouts.app', ['active' => 'master_spirality_garment'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master Data</a></li>
			<li class="active">Master Spirality Garment</li>
		</ul>
	</div>
</div>
@endsection



@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
            
            </div>
			<div class="panel-body">
				@foreach($spirality as $sp)
				<form action="{{ route ('spirality_garment.update')}}" id="form-update" method="POST">
					{{ csrf_field() }}

				
					<div class="row">
						<div class="col-lg-12">
							<div class="col-md-12">
								<div class="row">
									<label class="display-block text-semibold">Method Code</label>
									<input type="hidden" name="id" id="id" value = "{{ $sp->id}}"class="form-control">
							
									<input type="text" name="method_code" id="method_code" value = "{{ $sp->method_code}}" class="form-control" placeholder="Input Method code " readonly>
								</div>
							</div>
					
						</div>

						<div class="col-lg-12">
							<div class="col-md-12">
								<div class="row">
									<label class="display-block text-semibold">Parameter</label>
									
									<input type="text" name="parameter" id="parameter" class="form-control" value = "{{ $sp->parameter}}" placeholder="Input Parameter">
								</div>
							</div>		
					
						</div>

						
						<div class="col-lg-12">
							<div class="col-md-12">
								<div class="row">
									<label class="display-block text-semibold">Measuring Position</label>
									
									<input type="text" name="measuring_position" id="measuring_position" class="form-control" value = "{{ $sp->measuring_position}}" placeholder="Input Measuring Position">
								</div>
							</div>
					
						</div>

						<div class="col-lg-12">
							<div class="col-md-12">
								<div class="row">
									<label class="display-block text-semibold">Code</label>
									
									<input type="text" name="code" id="code" class="form-control" value = "{{ $sp->code}}" placeholder="Input Code">
								</div>
							</div>
					
						</div>
				
					<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Update <i class="icon-floppy-disk position-right"></i></button>

				</form>
				@endforeach
		
			</div>
		</div>
	</div>
</div>
@endsection




@section('js')
<script type="text/javascript">

</script>
@endsection