<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Style Name</label>
        <input type="text" id="style_name" class="form-control style_name">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Item Heat Transfer</label>
        <input type="text" id="heat_item" class="form-control heat_item">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Color Heat Transfer</label>
        <input type="text" id="heat_color" class="form-control heat_color">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Supplier Heat Transfer</label>
        <input type="text" id="heat_supplier" class="form-control heat_supplier">
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Peeling</label>
        <input type="text" id="pelling" class="form-control pelling">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Pad</label>
        <input type="text" id="pad" class="form-control pad">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Temperature (&degC)</label>
        <input type="text" id="temperature" class="form-control temperature">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Pressure</label>
        <input type="text" id="pressure" class="form-control pressure">
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Duration</label>
        <input type="text" id="duration" class="form-control duration">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Machine</label>
        <input type="text" id="machine" class="form-control machine">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Item Fabric</label>
        <input type="text" id="fabric_item" class="form-control fabric_item">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Fabric Type</label>
        <input type="text" id="fabric_type" class="form-control fabric_type">
    </div>

</div>

<div class="row">
    <div class="col-lg-6">
        <label style="font-weight: bold;">Fabric Color</label>
        <input type="text" id="fabric_color" class="form-control fabric_color">
    </div>

    <div class="col-lg-6">
        <label style="font-weight: bold;">Remark</label>
        <input type="text" id="remark" class="form-control remark">
    </div>
</div>