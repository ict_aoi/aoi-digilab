<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Style Name</label>
        <input type="text" id="style_name" class="form-control style_name">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Print Size</label>
        <input type="text" id="print_size" class="form-control print_size">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Print Item</label>
        <input type="text" id="print_item" class="form-control print_item">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Print Color</label>
        <input type="text" id="print_color" class="form-control print_color">
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Print Description</label>
        <input type="text" id="print_description" class="form-control print_description">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Fabric Type</label>
        <input type="text" id="fabric_type" class="form-control fabric_type">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Fabric Color</label>
        <input type="text" id="fabric_color" class="form-control fabric_color">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Item Fabric</label>
        <input type="text" id="item_fabric" class="form-control item_fabric">
    </div>
</div>

<div class="row">
    <div class="col-lg-5">
        <label style="font-weight: bold;">Supplier Print</label>
        <input type="text" id="supplier_print" class="form-control supplier_print">
    </div>

    <div class="col-lg-7">
        <label style="font-weight: bold;">Fabric Composition</label>
        <input type="text" id="fabric_composition" class="form-control fabric_composition">
    </div>

</div>