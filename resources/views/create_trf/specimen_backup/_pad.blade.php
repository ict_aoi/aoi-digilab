<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Pad Print Item</label>
        <input type="text" id="pad_print_item" class="form-control pad_print_item">
    </div>
    
    <div class="col-lg-3">
        <label style="font-weight: bold;">Pad Print Color</label>
        <input type="text" id="pad_print_color" class="form-control pad_print_color">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Pad Print Description</label>
        <input type="text" id="pad_print_description" class="form-control pad_print_description">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Supplier Pad Print</label>
        <input type="text" id="supplier_pad" class="form-control supplier_pad">
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Thread</label>
        <input type="text" id="thread" class="form-control thread">
    </div>
    
    <div class="col-lg-3">
        <label style="font-weight: bold;">Style Name</label>
        <input type="text" id="style_name" class="form-control style_name">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Size Page</label>
        <input type="text" id="size" class="form-control size">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Item Fabric</label>
        <input type="text" id="item_fabric" class="form-control item_fabric">
    </div>
</div>


<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Color</label>
        <input type="text" id="fabric_color" class="form-control fabric_color">
    </div>
    
    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Type</label>
        <input type="text" id="fabric_type" class="form-control fabric_type">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Composition</label>
        <input type="text" id="fabric_composition" class="form-control fabric_composition">
    </div>
</div>