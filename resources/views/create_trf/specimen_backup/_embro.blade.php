<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Embro Size</label>
        <input type="text" id="embro_size" class="form-control embro_size">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Embro Item</label>
        <input type="text" id="embro_item" class="form-control embro_item">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Embro Color</label>
        <input type="text" id="embro_color" class="form-control embro_color">
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Embro Description</label>
        <input type="text" id="embro_description" class="form-control embro_description">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Item Fabric</label>
        <input type="text" id="item_fabric" class="form-control item_fabric">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Type</label>
        <input type="text" id="fabric_type" class="form-control fabric_type">
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Color</label>
        <input type="text" id="fabric_color" class="form-control fabric_color">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Composition</label>
        <input type="text" id="fabric_composition" class="form-control fabric_composition">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Supplier Embro</label>
        <input type="text" id="supplier_embro" class="form-control supplier_embro">
    </div>
</div>