<div class="row">
    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Style Name</label>
            <input type="text" id="style_name" class="form-control style_name">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Item Badge</label>
            <input type="text" id="item_badge" class="form-control item_badge">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Color Badge</label>
            <input type="text" id="color_badge" class="form-control color_badge">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Badge Description</label>
            <textarea name="badge_description" id="badge_description" class="form-control badge_description"></textarea>
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Color</label>
            <input type="text" id="fabric_color" class="form-control fabric_color">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Matrial</label>
            <input type="text" id="fabric_material" class="form-control fabric_material">
        </div>
    </div>
</div>