<div id="modal_doc" class="modal fade" >
    <div class="modal-dialog modal-full">
        <div class="modal-content ">
            <div class="modal-body">
                <div class="row">
                    <form action="{{ route('trf.create.getDataSpecimen') }}" id="form-search-doc">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-9">
                            <input type="hidden" name="txcategory" class="txcategory" id="txcategory">
                            <input type="hidden" name="txcategory_specimen" class="txcategory_specimen" id="txcategory_specimen">
                            <input type="hidden" name="txtype_specimen" class="txtype_specimen" id="txtype_specimen">
                            <input type="hidden" name="txorigin" class="txorigin" id="txorigin">
                            <label class="labelkey" style="font-weight: bold;">Keyword</label>
                            <input type="text" name="key" id="key" class="form-control key" required>
                        </div>
                        <div class="col-lg-1">
                            <button type="submit" class="btn btn-primary" style="margin-top: 30px;">Search</button>
                        </div>
                    </form>
                </div>

                <div class="row table-responsive">
                    <table class="table table-basic table-condensed table-doc" id="table-doc">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Doc. Number</th>
                                <th>Season</th>
                                <th>Style</th>
                                <th>Article</th>
                                <th>Size</th>
                                <th>Color</th>
                                <th>Supplier</th>
                                <th>Item</th>
                                <th>Destination</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>