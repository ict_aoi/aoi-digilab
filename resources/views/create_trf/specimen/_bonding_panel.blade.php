<div class="row">

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Style Name</label>
            <input type="text" id="style_name" class="form-control style_name">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Bonding Item</label>
            <input type="text" id="bonding_item" class="form-control bonding_item">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Bonding Color</label>
            <input type="text" id="bonding_color" class="form-control bonding_color">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Bonding Supplier</label>
            <input type="text" id="bonding_supplier" class="form-control bonding_supplier">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Temperature</label>
            <input type="text" id="temperature" class="form-control temperature">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Pressure</label>
            <input type="text" id="pressure" class="form-control pressure">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Duration</label>
            <input type="text" id="duration" class="form-control duration">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Machine</label>
            <input type="text" id="machine" class="form-control machine">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Item Fabric</label>
            <input type="text" id="item_fabric" class="form-control item_fabric">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Type</label>
            <input type="text" id="fabric_type" class="form-control fabric_type">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Color</label>
            <input type="text" id="fabric_color" class="form-control fabric_color">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Remark</label>
            <input type="text" id="remark" class="form-control remark">
        </div>
    </div>
</div>