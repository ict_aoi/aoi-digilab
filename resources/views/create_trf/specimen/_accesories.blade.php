<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Item</label>
        <input type="text" id="fabric_item" class="form-control fabric_item">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Color</label>
        <input type="text" id="fabric_color" class="form-control fabric_color">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Supplier</label>
        <input type="text" id="fabric_supplier" class="form-control fabric_supplier">
    </div>
</div>