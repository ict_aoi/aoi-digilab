<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Rivert Size</label>
        <input type="text" id="rivert_size" class="form-control rivert_size">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Rivert Item</label>
        <input type="text" id="rivert_item" class="form-control rivert_item">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Rivert Color</label>
        <input type="text" id="rivert_color" class="form-control rivert_color">
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Rivert Description</label>
        <input type="text" id="rivert_description" class="form-control rivert_description">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Item Fabric</label>
        <input type="text" id="item_fabric" class="form-control item_fabric">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Type</label>
        <input type="text" id="fabric_type" class="form-control fabric_type">
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Color</label>
        <input type="text" id="fabric_color" class="form-control fabric_color">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fabric Composition</label>
        <input type="text" id="fabric_composition" class="form-control fabric_composition">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Supplier Rivert</label>
        <input type="text" id="supplier_rivert" class="form-control supplier_rivert">
    </div>
</div>