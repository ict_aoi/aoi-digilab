<div class="row">
    <div class="row">
        <div class="col-lg-3">
            <label style="font-weight: bold;">Style Name</label>
            <input type="text" id="style_name" class="form-control style_name">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Print Description</label>
            <input type="text" id="description" class="form-control description">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Print Size</label>
            <input type="text" id="size" class="form-control size">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Print Item</label>
            <input type="text" id="item" class="form-control item">
        </div>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <label style="font-weight: bold;">Print Color</label>
            <input type="text" id="color" class="form-control color">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Item</label>
            <input type="text" id="fabric_item" class="form-control fabric_item">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Type</label>
            <input type="text" id="fabric_type" class="form-control fabric_type">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Color</label>
            <input type="text" id="fabric_color" class="form-control fabric_color">
        </div>

        
    </div>

    <div class="row">
        <div class="col-lg-6">
            <label style="font-weight: bold;">Fabric Composition</label>
            <input type="text" id="fabric_composition" class="form-control fabric_composition">
        </div>

        <div class="col-lg-6">
            <label style="font-weight: bold;">Supplier Print</label>
            <input type="text" id="supplier_print" class="form-control supplier_print">
        </div>
    </div>
</div>