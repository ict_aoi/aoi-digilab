<div class="row">
    <div class="row">
            <div class="col-lg-4">
                <label style="font-weight: bold;">Style Name</label>
                <input name="style_name" id="style_name" class="form-control"> 
            </div>

            <div class="col-lg-4">
                <label style="font-weight: bold;">Machine</label>
                <input name="machine" id="machine" class="form-control"> 
            </div>

            <div class="col-lg-4">
                <label style="font-weight: bold;">Temperature</label>
                <input name="temperature" id="temperature" class="form-control"> 
            </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Pressure</label>
            <input name="pressure" id="pressure" class="form-control"> 
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Duration</label>
            <input name="duration" id="duration" class="form-control"> 
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fuse Item</label>
            <input name="item" id="item" class="form-control"> 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <label style="font-weight: bold;">Fuse Supplier</label>
            <input name="fuse_supplier" id="fuse_supplier" class="form-control"> 
        </div>

        <div class="col-lg-6">
            <label style="font-weight: bold;">Remark</label>
            <input name="remark" id="remark" class="form-control"> 
        </div>

    </div>

</div>