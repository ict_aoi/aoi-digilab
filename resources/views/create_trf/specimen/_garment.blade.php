<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Test Condition</label>
        <input type="text" id="test_condition" class="form-control test_condition">
    </div>

    <div class="col-lg-2">
        <label style="font-weight: bold;">Temperature (&degC)</label>
        <input type="text" id="temperature" class="form-control temperature">
    </div>

    <div class="col-lg-2">
        <label style="font-weight: bold;">Qty</label>
        <input type="text" id="qty" class="form-control qty">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Machine Model</label>
        <input type="text" id="machine" class="form-control machine">
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Type Of Specimen</label>
        <input type="text" id="sample_type" class="form-control sample_type">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Supplier</label>
        <input type="text" id="supplier" class="form-control supplier">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Fabric Properties</label>
        <input type="text" id="fabric_properties" class="form-control fabric_properties">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Care Instruction</label>
        <input type="text" id="care_instruction" class="form-control care_instruction">
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label style="font-weight: bold;">Material Shell & Panel</label>
        <input type="text" id="material_shell" class="form-control material_shell">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Fibre Composition</label>
        <input type="text" id="fibre_composition" class="form-control fibre_composition">
    </div>

    <div class="col-lg-4">
        <label style="font-weight: bold;">Remark</label>
        <input type="text" id="remark" class="form-control remark">
    </div>
</div>