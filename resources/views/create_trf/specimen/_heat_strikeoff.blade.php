<div class="row">
    <div class="row">
        <div class="col-lg-3">
            <label style="font-weight: bold;">Style Name</label>
            <input name="style_name" id="style_name" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Pelling</label>
            <input name="pelling" id="pelling" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Pad</label>
            <input name="pad" id="pad" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Temperature</label>
            <input name="temperature" id="temperature" class="form-control"> 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <label style="font-weight: bold;">Pressure</label>
            <input name="pressure" id="pressure" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Duration</label>
            <input name="duration" id="duration" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Machine</label>
            <input name="machine" id="machine" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Component</label>
            <input name="component" id="component" class="form-control"> 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Item</label>
            <input name="fabric_item" id="fabric_item" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Type</label>
            <input name="fabric_type" id="fabric_type" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Color</label>
            <input name="fabric_color" id="fabric_color" class="form-control"> 
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Remark</label>
            <input name="remark" id="remark" class="form-control"> 
        </div>
    </div>
</div>