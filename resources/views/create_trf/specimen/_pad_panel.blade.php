<div class="row">
    <div class="row">
        <div class="col-lg-3">
            <label style="font-weight: bold;">Style Name</label>
            <input type="text" id="style_name" class="form-control style_name">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Thread</label>
            <input type="text" id="thread" class="form-control thread">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Size Page</label>
            <input type="text" id="size_page" class="form-control size_page">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Color</label>
            <input type="text" id="fabric_color" class="form-control fabric_color">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <label style="font-weight: bold;">Pad Print Supplier</label>
            <input type="text" id="pad_print_supplier" class="form-control pad_print_supplier">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Pad Print Item</label>
            <input type="text" id="item" class="form-control item">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Pad Print Color</label>
            <input type="text" id="color" class="form-control color">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Pad Print Description</label>
            <input type="text" id="description" class="form-control description">
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Item</label>
            <input type="text" id="fabric_item" class="form-control fabric_item">
        </div>

        <div class="col-lg-3">
            <label style="font-weight: bold;">Fabric Type</label>
            <input type="text" id="fabric_type" class="form-control fabric_type">
        </div>

        <div class="col-lg-6">
            <label style="font-weight: bold;">Fabric Composition</label>
            <input type="text" id="fabric_composition" class="form-control fabric_composition">
        </div>
    </div>
</div>