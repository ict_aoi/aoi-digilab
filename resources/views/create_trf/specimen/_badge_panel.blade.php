<div class="row">
    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Style Name</label>
            <input type="text" id="style_name" class="form-control style_name">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Badge Item</label>
            <input type="text" id="badge_item" class="form-control badge_item">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Badge Color</label>
            <input type="text" id="badge_color" class="form-control badge_color">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Badge Description</label>
            <input type="text" id="badge_description" class="form-control badge_description">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Color</label>
            <input type="text" id="fabric_color" class="form-control fabric_color">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabic Material</label>
            <input type="text" id="fabric_material" class="form-control fabric_material">
        </div>
    </div>
</div>