<div class="row">
    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Style Name</label>
            <input type="text" id="style_name" class="form-control style_name">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Badge Size</label>
            <input type="text" id="badge_size" class="form-control badge_size">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Garment Size</label>
            <input type="text" id="garment_size" class="form-control garment_size">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Color</label>
            <input type="text" id="fabric_color" class="form-control fabric_color">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Item</label>
            <input type="text" id="fabric_item" class="form-control fabric_item">
        </div>

        <div class="col-lg-4">
            <label style="font-weight: bold;">Fabric Type</label>
            <input type="text" id="fabric_type" class="form-control fabric_type">
        </div>
    </div>

    <div class="row">
        
        <div class="col-lg-6">
            <label style="font-weight: bold;">Badge Description</label>
            <input type="text" id="badge_description" class="form-control badge_description">
        </div>

        <div class="col-lg-6">
            <label style="font-weight: bold;">Fabric Composition</label>
            <input type="text" id="fabric_composition" class="form-control fabric_composition">
        </div>
    </div>
</div>