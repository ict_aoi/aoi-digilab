<div class="row">
    <div class="col-lg-3">
        <label style="font-weight: bold;">Style</label>
        <input type="text" id="style" class="form-control style">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Article</label>
        <input type="text" id="article" class="form-control article">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Fabric Item</label>
        <input type="text" id="fabric_item" class="form-control fabric_item">
    </div>

    <div class="col-lg-3">
        <label style="font-weight: bold;">Fabric Color</label>
        <input type="text" id="fabric_color" class="form-control fabric_color">
    </div>
</div>

