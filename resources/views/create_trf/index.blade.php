@extends('layouts.app', ['active' => 'create_trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Create TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h4></i> <span class="text-semibold"> TRF LIST</span> </h4>
            </div>

            <div class="panel-body">
                <div class="row {{Auth::user()->admin==false ? 'hidden' : '' }}">
                    <label><b>Lab Loction</b></label>
                    @php($lablc = intval(Auth::user()->factory_id))
                  
                    <select name="labloc" id="labloc" class="select">
                        @foreach($lab as $lb)
                            <option value="{{$lb->name}}" {{ in_array($lablc,explode(',',$lb->default_origin)) ? 'selected' : ''}}>{{$lb->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="row">
                    <div class="row table-responsive">
                        <table class="table table-basic table-condensed" id="table-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No TRF</th>
                                    <th>Origin of Specimen</th>
                                    <th>Test Required</th>
                                    <th>Date Information</th>
                                    <th>Buyer</th>
                                    <th>Lab Location</th>
                                    <th>User</th>
                                    <th>Category</th>
                                    <th>Category Specimen</th> 
                                    <th>Type Specimen</th>
                                    <th>Methods</th>
                                    <th>Return Test Sample </th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        // stateSave: true,
        // autoWidth: false,
        // autoLength: false,
        // processing: true,
        // serverSide: true,
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
		deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons:[
            {
                text:"Create TRF",
                className:"btn btn-primary",
                action : function (e, dt, node, config)
                {
                  window.location.href= "{{ route('trf.create.createTrf') }}";
                }
            }
        ],
        ajax: {
            type: 'GET',
            url: "{{ route('trf.create.getData') }}",
            data: function (d) {
                return $.extend({},d,{
                    'lablocation' : $('#labloc').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        initComplete:function(){
            $('.dataTables_filter input').unbind();
            $('.dataTables_filter input').bind('keyup',function(e){
                var code = e.keyCode || e.which;
				if (code == 13) {
					table.search(this.value).draw();
					
				}
            })
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id' },
            {data: 'asal_specimen', name: 'asal_specimen' },
            {data: 'test_required', name: 'test_required' },
            {data: 'date_information_remark', name: 'date_information_remark' },
            {data: 'buyer', name: 'buyer' },
            {data: 'lab_location', name: 'lab_location' },
            {data: 'nik', name: 'nik' },
            {data: 'category', name: 'category' },
            {data: 'category_specimen', name: 'category_specimen' },
            {data: 'type_specimen', name: 'type_specimen' },
            {data: 'method_code', name: 'method_code' },
            {data: 'return_test_sample', name: 'return_test_sample' },
            {data: 'status', name: 'status' },
            {data: 'action', name: 'action',searchable:false, orderable:false},
        ]
    });

    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });

    $('#labloc').change(function(){
        table.clear();
        table.draw();
    });
});
</script>
@endsection