
<style type="text/css">
@page{
    margin : 10 10 10 10;
}

.tab{
    /*font-family: sans-serif;*/
    font-size: 18px;
}
.tab tr  td{
    vertical-align: top;
    margin-bottom: 30px;
}

.img_barcode {
    display: block;
    padding: 0px;
    margin-right: 10px;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
}

.img_barcode > img {
    width: 200px;
    height:40px;
}

.spc{
    border-collapse: collapse;
    width: 100%;
}
.spc tr td{
    border: 1px solid black;
    padding-left: 10px;
}
.spc td{
    height: 10px;
}

.head{
    border-collapse: collapse;
    width: 100%;
    page-break-inside: avoid;

}
.head tr td{
    border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 12px;
}

.head td{
    height: 10px;

}

.break{
    clear: both;
    page-break-after: always;
}
</style>

@foreach($list as $ls)

<table width="100%">
    <tr>
        <td rowspan="2">
            <center><img src="{{ public_path('assets/icon/aoi.png') }}" alt="Image"width="125"></center>
        </td>
        <td>
            <center><label style="font-size: 20px; font-weight: bold;">APPAREL MATERIAL / GARMENT</label> </center>
        </td>
        <td rowspan="2">
            <center><img src="{{ public_path('assets/icon/bbi_logo.png') }}" alt="Image"width="135"></center>
        </td>
    </tr>
    <tr>
        <td>
            <center><label style="font-size: 20px; font-weight: bold;">TEST REQUISITION FORM</label></center>
        </td>
    </tr>
</table>

<br>
<table width="100%" class="tab">
    <tr>
        <td colspan="3">
            <div class="img_barcode">
                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($ls->trf_id, 'C128',2,35) }}" alt="barcode"   />
            </div>
        </td>
    </tr>

    <tr>
        <td width="130">No. TRF</td>
        <td width="20">:</td>
        <td>{{$ls->trf_id}}</td>
        <td rowspan="4">
            
            {{-- @php($url = 'http://'.\Request::getHttpHost().'/api/print/report-specimen?id='.$ls->id.'&notrf='.$ls->trf_id)
            @php($qrcode = \App\Http\Controllers\HelperController::generateQr($url))
            <img style="float: right; padding-right: 50px; width:200px;" src="ls:image/png;base64, {!! base64_encode($qrcode) !!}"> --}}
        </td>
    </tr>
    
    <tr>
        <td width="130">Submitted by</td>
        <td width="20">:</td>
        <td colspan="2">{{$ls->username}} - {{$ls->nik}}</td>
    </tr>

    <tr>
        <td width="130">Date of Submitted</td>
        <td width="20">:</td>
        <td colspan="2">{{date_format(date_create($ls->created_at),'d-M-Y H:i:s')}}</td>
    </tr>
    <tr>
        <td width="130">Factory</td>
        <td width="20">:</td>
        <td colspan="2">{{$ls->factory_name}}</td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    <tr>
        <td width="130">Buyer</td>
        <td width="20">:</td>
        <td colspan="2">{{ $ls->buyer}}</td>
    </tr>
    <tr>
        <td width="130">Lab Location</td>
        <td width="20">:</td>
        <td colspan="2">{{ $ls->lab_location}}</td>
    </tr>
    <tr>
        <td width="130">Origin of speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $ls->asal_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Category</td>
        <td width="20">:</td>
        <td>{{ $ls->category}}</td>
    </tr>
    <tr>
        <td width="130">Category Speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $ls->category_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Type of Speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $ls->type_specimen}}</td>
    </tr>

    

    <tr>
        <td width="130">Test Required</td>
        <td width="20">:</td>
        <td>
            @if(isset($ls->test_required))
                @php($testr = \App\Http\Controllers\HelperController::testRequired($ls->test_required,$ls->previous_trf_id))
                {{$testr}}
            @endif
        </td>
    </tr>

    <tr>
        <td width="130">Date Information</td>
        <td width="20">:</td>
        <td colspan="2">
            @if($ls->date_information_remark=='buy_ready')
                BUY READY <br> {{ date_format(date_create($ls->date_information),'d-m-Y')}}
            @elseif($ls->date_information_remark=='podd')
                PODD <br> {{ date_format(date_create($ls->date_information),'d-m-Y')}}
            @elseif($ls->date_information_remark=='output_sewing')
                OUTPUT SEWING <br> {{ date_format(date_create($ls->date_information),'d-m-Y')}}
            @endif
        </td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    

     <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    <tr>
        <td width="130">Return Test</td>
        <td width="20">:</td>
        <td colspan="2">{{ isset($ls->return_test_sample)==true ? "YES" : "NO"}}</td>
    </tr>
    <tr>
        <td width="130">Part of speciment tested</td>
        <td width="20">:</td>
        <td colspan="2">{{ $ls->part_of_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Testing methode</td>
        <td width="20">:</td>
        <td colspan="2">
            @foreach(explode("|",$ls->methods) as $x)
               - {{$x}} <br>
            @endforeach
        </td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>
    <tr>
        <td colspan="4">
            Remarks: Reporting maximum of 4 days
        </td>
    </tr>
    <tr>
        <td width="130">Status</td>
        <td width="20">:</td>
        <td colspan="2">{{ $ls->status}}</td>
    </tr>
    <tr>
        <td width="130">Date of validate</td>
        <td width="20">:</td>
        <td colspan="2">{{isset($ls->verified_lab_date) ? date_format(date_create($ls->verified_lab_date),'d-m-Y  H:i:s') : ''}}</td>
    </tr>
    <tr>
        <td width="130">Name of validate</td>
        <td width="20">:</td>
        <td colspan="2">
            @if(isset($ls->verified_lab_by))
                {{\App\User::where('id',$ls->verified_lab_by)->orwhere('nik',$ls->verified_lab_by)->first()['name']}}
            @endif
        </td>
    </tr>


</table>

<br>
@if($ls->category_specimen=='FABRIC')
    @include('report.form.fabric',['idt'=>$ls])
@elseif($ls->category_specimen=='MOCKUP')
    @include('report.form.mockup',['idt'=>$ls])
@elseif($ls->category_specimen=='ACCESORIES/TRIM' )
    @include('report.form.accesories',['idt'=>$ls])
@elseif($ls->category_specimen=='GARMENT' )
    @include('report.form.garment',['idt'=>$ls])
@elseif($ls->category_specimen=='STRIKE OFF' && $ls->type_specimen=='HEAT TRANSFER' )
    @include('report.form.heat_transfer',['idt'=>$ls])
@elseif(($ls->category_specimen=='STRIKE OFF' && $ls->type_specimen=='EMBROIDERY' ) || ($ls->category_specimen=='STRIKE OFF' && $ls->type_specimen=='PRINTING' ))
    @include('report.form.print_embro',['idt'=>$ls])
@elseif($ls->category_specimen=='STRIKE OFF' && $ls->type_specimen=='PAD PRINT' )
    @include('report.form.pad_print',['idt'=>$ls])
@elseif($ls->category_specimen=='STRIKE OFF' && $ls->type_specimen=='BADGE' )
    @include('report.form.badge',['idt'=>$ls])
@elseif($ls->category_specimen=='STRIKE OFF' && $ls->type_specimen=='RIVERT' )
    @include('report.form.rivert',['idt'=>$ls])
@else
    @include('report.form.error',['idt'=>$ls])
@endif
<br>
<div class="break"></div>
@endforeach