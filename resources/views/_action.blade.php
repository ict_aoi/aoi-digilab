<ul class="icons-list">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="icon-menu9"></i>
		</a>

		<ul class="dropdown-menu dropdown-menu-right">

			@if(isset($edituser))
			<li><a href="{!! $edituser !!}"><i class="icon-paragraph-justify2"></i> Edit User</a></li>
			@endif

			@if(isset($innactive))
			<li><a href="{!! $innactive !!}" class="ignore-click innactive"><i class="icon-x"  ></i> Innactive User</a></li>
			@endif

			@if(isset($editrole))
			<li><a href="{!! $editrole !!}"><i class="icon-paragraph-justify2" ></i> Edit Role</a></li>
			@endif

			@if(isset($approval))
            <li><a href="#" data-id="{!! $approval['id'] !!}" data-trf="{!! $approval['trf'] !!}" onclick="approve(this);" data-status="VERIFIED"><i class="icon-checkmark4"></i> Approve</a></li>
            @endif
            @if (isset($reject))
            <li><a href="#" data-id="{!! $reject['id'] !!}" data-trf="{!! $reject['trf'] !!}" onclick="approve(this);" data-status="REJECT"><i class="icon-cross2"></i>Reject</a></li>
            @endif

			@if (isset($setMeth))
            <li><a href="#" class="ignore-click setMeth" data-id="{!! $setMeth['id'] !!}" data-idctg="{!! $setMeth['id_category'] !!}" data-buyer="{!! $setMeth['buyer'] !!}"><i class="icon-list-numbered"></i> Methode Testing</a></li>
            @endif

            @if (isset($editrf))
            <li><a href="{!! $editrf !!}"><i class="icon-compose"></i>Edit TRF</a></li>
            @endif

           <!--  @if (isset($t2_result))
            <li><a href="{!! $t2_result !!}">T2 Result</a></li>
            @endif -->


            @if (isset($spcTest))
            <li><a href="{!! $spcTest !!}"><i class="icon-clipboard3"></i>Set Test</a></li>
            @endif


            @if (isset($delctg))
            <li><a href="#" class="ignore-click delctg" data-id="{!! $delctg !!}"><i class="icon-trash"></i>Delete</a></li>
            @endif

            @if (isset($edctg))
            <li><a href="#" class="ignore-click edctg" data-id="{!! $edctg['id'] !!}" data-catg="{!! $edctg['catg'] !!}" data-ctgsp="{!! $edctg['ctgsp'] !!}" data-type="{!! $edctg['type'] !!}" data-sequence="{!! $edctg['sequence'] !!}"><i class="icon-compose"></i>Edit</a></li>
            @endif


            @if (isset($delmeth))
            <li><a href="#" class="ignore-click delmeth" data-id="{!! $delmeth !!}"><i class="icon-trash"></i>Delete</a></li>
            @endif

            @if (isset($edmeth))
            <li><a href="#" class="ignore-click edmeth" data-id="{!! $edmeth['id'] !!}" data-code="{!! $edmeth['code'] !!}" data-name="{!! $edmeth['name'] !!}" data-category="{!! $edmeth['category'] !!}" data-type="{!! $edmeth['type'] !!}"><i class="icon-compose"></i> Edit</a></li>
            @endif

            @if (isset($delRequ))
            <li><a href="#" class="ignore-click delRequ" data-id="{!! $delRequ !!}"><i class="icon-trash"></i>Delete</a></li>
            @endif

            @if (isset($editRequ))
            <li><a href="#" class="ignore-click editRequ" data-id="{!! $editRequ !!}" ><i class="icon-compose"></i>Edit</a></li>
            @endif

            @if (isset($delSpcTest))
            <li><a href="#" class="ignore-click delSpcTest" data-id="{!! $delSpcTest !!}"><i class="icon-trash"></i>Delete</a></li>
            @endif

            @if (isset($edSpcTest))
            <li><a href="#" class="ignore-click edSpcTest" data-id="{!! $edSpcTest !!}"><i class="icon-compose"></i>Edit</a></li>
            @endif


            @if(isset($setReq))
			<li><a href="{!! $setReq !!}"><i class="icon-paragraph-justify2" ></i> Set Requirement</a></li>
			@endif



			@if(isset($result))
			<li><a href="{!! $result !!}"><i class="icon-lab" ></i> Test Result</a></li>
			@endif

			@if(isset($reopen))
			<li><a href="#" data-id="{!! $reopen !!}" class="ignore-click reopen"><i class="icon-unlocked2" ></i> Re Open</a></li>
			@endif

			@if(isset($delReq))
			<li><a href="#" data-id="{!! $delReq !!}" class="ignore-click delSpcTest" onclick="delReq(this);"><i class="icon-trash" ></i> Delete Requirement</a></li>
			@endif


			@if(isset($printBcd))
			<li><a href="{!! $printBcd !!}" target="_blank"><i class="icon-printer2" ></i> Print Barcode</a></li>
			@endif
			@if(isset($detailBcd))
			<li><a href="{!! $detailBcd !!}" target="_blank"><i class="icon-shield-check" ></i> Print Detail</a></li>
			@endif


			@if(isset($dashbarcode))
			<li><a href="{!! $dashbarcode !!}" class="ignore-click" target="_blank"><i class="icon-barcode2" ></i> Barcode</a></li>
			@endif

			@if(isset($dashdetail))
			<li><a href="{!! $dashdetail !!}" class="ignore-click" target="_blank"><i class="icon-menu7" ></i> Detail TRF</a></li>
			@endif

			@if(isset($dashreport))
			<li><a href="{!! $dashreport !!}" class="ignore-click" target="_blank"><i class="icon-clipboard3" ></i> Report TRF</a></li>
			@endif




			@if(isset($trflist))
			<li><a href="{!! $trflist !!}"><i class="icon-pencil5" ></i> Input Result</a></li>
			@endif


			@if(isset($setPrintReportTest))
			<li><a href="#" class="ignore-click setPrintReportTest" data-id="{!! $setPrintReportTest['id'] !!}" data-meth="{!! $setPrintReportTest['meth'] !!}" data-doc="{!! $setPrintReportTest['doc'] !!}" data-tech="{!! $setPrintReportTest['tech'] !!}"><i class="icon-user-tie" ></i> Set Technician</a></li>
			@endif

			@if(isset($PrintReportTest))
			<li><a href="#" class="ignore-click PrintReportTest" data-id="{!! $PrintReportTest['id'] !!}" data-meth="{!! $PrintReportTest['meth'] !!}" data-doc="{!! $PrintReportTest['doc'] !!}"><i class="icon-printer2" ></i> Print</a></li>
			@endif


			@if(isset($setPrintReportSpc))
			<li><a href="#" class="ignore-click setPrintReportSpc" data-id="{!! $setPrintReportSpc['id'] !!}" data-doc="{!! $setPrintReportSpc['doc'] !!}" data-tech="{!! $setPrintReportSpc['tech'] !!}"><i class="icon-user-tie" ></i> Set Technician</a></li>
			@endif

			@if(isset($PrintReportSpc))
			<li><a href="#" class="ignore-click PrintReportSpc" data-id="{!! $PrintReportSpc['id'] !!}" data-doc="{!! $PrintReportSpc['doc'] !!}"><i class="icon-printer2" ></i> Print</a></li>
			@endif

			@if(isset($deskPrintBarcode))
			<li><a href="#" class="ignore-click deskPrintBarcode" data-id="{!! $deskPrintBarcode !!}"><i class="icon-printer2" ></i> Print Barcode</a></li>
			@endif


			@if(isset($releaseTrf))
			<li><a href="#" class="ignore-click releaseTrf" data-id="{!! $releaseTrf['id'] !!}"><i class="icon-stack-check" ></i> Release Report</a></li>
			@endif

			@if(isset($PrintReportTrf))
			<li><a href="#" class="ignore-click PrintReportTrf" data-id="{!! $PrintReportTrf['id'] !!}" data-sign="{!! $PrintReportTrf['sign'] !!}"><i class="icon-printer2" ></i> Print</a></li>
			@endif

			@if(isset($PrintReportTrf2))
			<li><a href="#" class="ignore-click PrintReportTrf2" data-id="{!! $PrintReportTrf2['id'] !!}" data-sign="{!! $PrintReportTrf2['sign'] !!}"><i class="icon-printer" ></i> Print Report</a></li>
			@endif

			@if(isset($deskPrintDetail))
			<li><a href="{!! $deskPrintDetail !!}" class="ignore-click" target="_blank"><i class="icon-printer4" ></i> Print Detail</a></li>
			@endif


			@if(isset($editHoliday))
			<li><a href="#" class="ignore-click editHoliday" data-id="{!! $editHoliday['id']!!}" data-date="{!! $editHoliday['off_date']!!} " data-reason="{!! $editHoliday['reason']!!} "><i class="icon-pencil5" ></i> Edit</a></li>
			@endif

			@if(isset($deleteHoliday))
			<li><a href="#" class="ignore-click deleteHoliday" data-id="{!! $deleteHoliday['id']!!}" ><i class="icon-trash" ></i> Delete</a></li>
			@endif

			@if(isset($escalation))
			<li><a href="#" class="ignore-click escalation" data-id="{!! $escalation !!}" ><i class="icon-eye" ></i> Escalation</a></li>
			@endif

			@if(isset($viewesc))
			<li><a href="#" class="ignore-click viewesc" data-id="{!! $viewesc !!}" ><i class="icon-eye" ></i> View</a></li>
			@endif


            @if (isset($input_rest))
                <li><a href="#" class="ignore-click input_rest"
                    data-trfid="{!! $input_rest['trfid'] !!}"
                    data-trfdocid="{!! $input_rest['trfdocid'] !!}"
                    data-trfmethid="{!! $input_rest['trfmethid'] !!}"
                    data-setreqid="{!! $input_rest['data']->setid !!}"
                    data-reqid="{!! $input_rest['data']->reqid !!}"
                    data-spcid="{!! isset($input_rest['data']->spcid) ? $input_rest['data']->spcid : null !!}"
                    data-bfrtest="{!! $input_rest['data']->before_test !!}"
                    data-stdtest="{!! $input_rest['data']->standart_value !!}"
                    data-totin="{!! $input_rest['data']->number_test !!}"
                    data-measuring="{!! $input_rest['data']->measuring_position !!}"
                    onclick="showModal(this);"><i class="icon-shield-check"></i> Result</a></li>
            @endif

            @if (isset($inactive))
                <li><a href="#" class="ignore-click inactive"
                    data-setreqid="{!! $inactive['setreqid']!!}" data-trf_id="{!! $inactive['trf_id']!!}" data-type="off" onclick="powerSet(this);"><i class="icon-cross2"></i> Inactive</a></li>
            @endif

            @if (isset($reactive))
                <li><a href="#" class="ignore-click reactive"
                    data-setreqid="{!! $reactive['setreqid']!!}" data-trf_id="{!! $reactive['trf_id']!!}" data-type="on" onclick="powerSet(this);"><i class="icon-checkmark2"></i> Re-Active</a></li>
            @endif

            @if (isset($images))
                <li><a href="#" class="ignore-click images" data-setreqid="{!! $images['setreqid'] !!}" data-trfdocid="{!! $images['trfdocid'] !!}" onclick="modalImage(this);"><i class="icon-file-picture2"></i> Result Image</a></li>
            @endif

            @if (isset($delete))
                <li><a href="#" class="ignore-click delete" data-setreqid="{!! $delete['setreqid'] !!}" data-trfdocid="{!! $delete['trfdocid'] !!}" onclick="delResult(this);"><i class="icon-bin"></i> Delete</a></li>
            @endif
		</ul>
	</li>
</ul>
