@extends('layouts.app', ['active' => 'menu-dashboard-trf'])
@section('header')
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Edit TRF</li>
        </ul>
    </div>
</div>
@endsection

@section('content')

<div class="content">
    <div class="row">
        <div class=" panel panel-flat">
            <div class="page-header-content">
                <div class="page-title">
                    <h4></i> <span class="text-semibold">&nbsp &nbsp CREATE TESTING REQUEST FORM</span> </h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <label style="color:red;"><b>* Required Value</b></label>
                </div>
                <div class="row form-group">

                    <input type="hidden" name="idx" id="idx">
                    <div class="col-lg-3">
                        <div class="row">
                            <label><b>Dept. Origin</b> <b style="color:red;">*</b></label>
                            <select class="select" id="origin" required disabled>
                                <option value="null" data-asal="null">--Chosse Origin--</option>
                                <option value="SSM" data-asal="DEVELOPMENT" {{$trf->platform=='SSM' ? "selected" : ""}}>Development</option>
                                <option value="WMS" data-asal="PRODUCTION" {{$trf->platform=='WMS' ? "selected" : ""}}>Warehouse Material</option>
                                <option value="FGMS" data-asal="PRODUCTION" {{$trf->platform=='FGMS' ? "selected" : ""}}>Final QA / Finish Goods</option>
                                <option value="CDMS" data-asal="PRODUCTION" {{$trf->platform=='CDMS' ? "selected" : ""}}>Cutting / Secondary Process</option>
                            </select>
                        </div>
                        <div class="row">
                            <label><b>Buyer</b> <b style="color:red;">*</b></label>
                            <select class="select" id="buyer" onchange="getCtg(this);" required>
                                <option value="null">--Chosse Buyer--</option>
                                @foreach($buyer as $by)
                                <option value="{{$by->buyer}}">{{$by->buyer}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <label><b>Category</b> <b style="color:red;">*</b></label>
                            <select class="select" id="category" onchange="getCtgSpm(this);" required>

                            </select>
                        </div>
                        <div class="row">
                            <label><b>Category Specimen</b> <b style="color:red;">*</b></label>
                            <select class="select" id="category_specimen" onchange="getType(this);" required>

                            </select>
                        </div>

                        <div class="row">
                            <label><b>Type Specimen</b> <b style="color:red;">*</b></label>
                            <select class="select" id="type_specimen" onchange="getMeth(this);" required>

                            </select>
                        </div>


                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label><b>Testing Methods</b> <b style="color:red;">*</b></label>
                            <div class="multi-select-full">
                                <select multiple="multiple" data-placeholder="Choosse Methods" class="select" id="test_methods">

                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <label><b>Lab Location</b></label>
                            <select class="select" id="labloction">
                                @foreach($labloc as $lab)
                                <option value="{{$lab->name}}" {{$lab->name==$trf->lab_location ? 'selected' : ''}}>{{$lab->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row">
                            <label style="font-weight: bold;" class="display-block">Return Specimen Test <label style="color: red; font-weight: bold;">*</label></label>

                            <label style="font-weight: bold; margin-right:100px;"><input type="radio" name="return_test" id="return_test" value="true"> YES</label>
                            <label style="font-weight: bold; margin-right:100px;"><input type="radio" name="return_test" id="return_test" value="false" checked> NO</label>
                        </div>

                        <div class="row">
                            <label style="font-weight: bold;" class="display-block">Part To Test <label style="color: red; font-weight: bold;">*</label></label>
                            <textarea name="part_to_test" id="part_to_test" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="row">
                            <div class="radio">

                                <div class="col-md-3">
                                    <label class="display-block"><b> Pre-Production</b></label>
                                    <label class="radio"><input type="radio" name="test_req" value="preproductiontesting_m">
                                        M (Model Level)
                                    </label>
                                    <label class="radio"><input type="radio" name="test_req" value="preproductiontesting_a">
                                        A (Article Level)
                                    </label>
                                    <label class="radio"><input type="radio" name="test_req" value="preproductiontesting_selective">
                                        Selective
                                    </label>
                                </div>

                                <div class="col-md-3">
                                    <label class="display-block"><b> 1 Bulk Testing</b></label>
                                    <label class="radio"><input type="radio" name="test_req" value="bulktesting_m">
                                        M (Model Level)
                                    </label>
                                    <label class="radio"><input type="radio" name="test_req" value="bulktesting_a">
                                        A (Article Level)
                                    </label>
                                    <label class="radio"><input type="radio" name="test_req" value="bulktesting_selective">
                                        Selective
                                    </label>
                                </div>

                                <div class="col-md-3">
                                    <label class="display-block"><b> Re-Order Testing</b></label>
                                    <label class="radio"><input type="radio" name="test_req" value="reordertesting_m">
                                        M (Model Level)
                                    </label>
                                    <label class="radio"><input type="radio" name="test_req" value="reordertesting_a">
                                        A (Article Level)
                                    </label>
                                    <label class="radio"><input type="radio" name="test_req" value="reordertesting_selective">
                                        Selective
                                    </label>
                                </div>

                                <div class="col-md-3">
                                    <label class="display-block"><b> Re-Test</b></label>
                                    <label class="radio"><input type="radio" name="test_req" value="retest">
                                        Re-Test
                                    </label>
                                    <input type="text" name="no_trf" id="no_trf" class="form-control">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <label><b>Date Infromation</b> <b style="color:red;">*</b></label>
                                <div class="form-group">
                                    <label class="radio-inline"><input type="radio" name="radio_date_info" id="radio_date_info" value="buy_ready">Buy ready (special for development testing)</label>
                                    <label class="radio-inline"><input type="radio" name="radio_date_info" id="radio_date_info" value="podd">PODD</label>
                                    <label class="radio-inline"><input type="radio" name="radio_date_info" id="radio_date_info" value="output_sewing">1st output sewing</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="date_info" class="form-control date_info" id="anytime-weekday" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row table-responsive">
                    <table class="table table-basic table-condensed" id="table-specimen">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Doc Type</th>
                                <th>Doc. Number</th>
                                <th>Season</th>
                                <th>Style</th>
                                <th>Article</th>
                                <th>Size</th>
                                <th>Color</th>
                                <th>Item</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <hr>
                <div class="row">
                    <div id="data_specimen">
                    </div>
                </div>
                <div class="row">
                    <input type="hidden" class="idx" id="idx">
                    <button class="btn btn-success submit" type="button" id="submit"><span class="icon-database-check"></span> UPDATE</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    var trfTesting = @json($trf);
    var trfDoc = @json($docl);
    var trftestmeth = [];
    var dataSetNew = [];

    var returnpage = @json($returnpage);

    const month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    $(document).ready(function() {

        $(window).on('load', function() {

            $('#buyer').val(trfTesting['buyer']).trigger('change');
            $('#origin').val(trfTesting['platform']).trigger('change');
            $('#labloction').val(trfTesting['lab_location']).trigger('change');
            document.querySelector("input[name=return_test][value=" + trfTesting['return_test_sample'] + "]").checked = true;
            $('#part_to_test').val(trfTesting['part_of_specimen']);
            $('#idx').val(trfTesting['id']);

            if (trfTesting['test_required'] != null) {
                document.querySelector("input[name=test_req][value=" + trfTesting['test_required'] + "]").checked = true;

                if (trfTesting['test_required'] == "retest") {
                    $('#no_trf').val(trfTesting['previous_trf_id']);
                }
            }

            document.querySelector("input[name=radio_date_info][value=" + trfTesting['date_information_remark'] + "]").checked = true;
            // var newDate = trfTesting['date_information'].getDate()+" "+month[trfTesting['date_information'].getMonth()]+" "+trfTesting['date_information'].getFullYear();
            var date = new Date(trfTesting['date_information']);
            var newDate = date.getDate() + " " + month[date.getMonth()] + " " + date.getFullYear();

            $('.date_info').val(newDate);

            setDataSpc(trfDoc, trfTesting);

            var lismeth = @json($meth);

            for (let x = 0; x < lismeth.length; x++) {
                trftestmeth.push(
                    lismeth[x]['master_method_id']
                );

            }

        });

        $('#submit').on('click', function(event) {
            event.preventDefault();

            var id = trfTesting['id'];
            var origin = $('#origin').val();
            var asal = $('#origin').find(':selected').data('asal');
            var buyer = $('#buyer').val();
            var category = $('#category').val();
            var category_specimen = $('#category_specimen').val();
            var type_specimen = $('#type_specimen').val();
            var method = $('#test_methods').val();
            var lab_location = $('#labloction').val();
            var return_test = $('input[name=return_test]:checked').val();
            var part_to_test = $('#part_to_test').val();
            var radio_date_info = $('input[name=radio_date_info]:checked').val();
            var date_info = $('.date_info').val();
            var test_req = $('input[name=test_req]:checked').val();
            var no_trf = $('#no_trf').val();

            if (origin == null || category == null || category_specimen == null || type_specimen == null || radio_date_info == null || date_info == null || part_to_test == null || return_test == null) {

                alert(422, "Completed the data TRF ! ! !");

                return false;
            }

            if (test_req == "test_req" && (no_trf == null || no_trf == '')) {
                alert(422, "Previous TRF Required ! ! !");
                return false;
            }
            completedDataSumary(category_specimen, type_specimen, asal);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('dashboardTrf.updateTrf') }}",
                data: {
                    id: id,
                    origin: origin,
                    asal: asal,
                    buyer: buyer,
                    category: category,
                    category_specimen: category_specimen,
                    type_specimen: type_specimen,
                    method: method,
                    lab_location: lab_location,
                    return_test: return_test,
                    part_to_test: part_to_test,
                    radio_date_info: radio_date_info,
                    date_info: date_info,
                    test_req: test_req,
                    no_trf: no_trf,
                    data: trfDoc
                },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    $.unblockUI();
                    var notif = response.data;
                    alert(notif.status, notif.output);
                    window.location.href = returnpage;
                    // window.location.href = "{{ route('dashboardTrf.index') }}";
                    // var urlBarcode = $('#printBarcode').attr('href')+'?data='+notif.list

                },
                error: function(response) {
                    $.unblockUI();
                    var notif = response.data;
                    alert(notif.status, notif.output);

                }
            });

        });

    });

    function getCtg(e) {
        var buyer = e.value;

        $('#category').empty();
        $('#category').append('<option value="">--Choose Category--</option>');

        $.ajax({
            type: 'get',
            url: "{{ route('dashboardTrf.getCtgMethReq') }}",
            data: {
                type: "category",
                buyer: buyer
            },
            success: function(response) {
                var data = response.data;
                for (let i = 0; i < data.length; i++) {
                    $('#category').append('<option value="' + data[i]['category'] + '">' + data[i]['category'] + '</option>');
                }
                $('#category').val(trfTesting['category']).trigger('change');
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status, notif.output);

            }
        });



    }

    function getCtgSpm(e) {
        var category = e.value;
        var buyer = $('#buyer').val();
        // var origin = $('#origin').val();

        $('#category_specimen').empty();
        $('#category_specimen').append('<option value="">--Choose Category Specimen--</option>');

        $.ajax({
            type: 'get',
            url: "{{ route('dashboardTrf.getCtgMethReq') }}",
            data: {
                buyer: buyer,
                category: category,
                type: "category_specimen"
            },
            success: function(response) {
                var data = response.data;
                for (let i = 0; i < data.length; i++) {
                    $('#category_specimen').append('<option value="' + data[i]['category_specimen'] + '">' + data[i]['category_specimen'] + '</option>');
                }

                $('#category_specimen').val(trfTesting['category_specimen']).trigger('change');
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status, notif.output);

            }
        });
    }

    function getType(e) {
        var category_specimen = e.value;
        var category = $('#category').val();
        var buyer = $('#buyer').val();
        var asal = $('#origin').find(':selected').data('asal');

        $('#type_specimen').empty();
        $('#type_specimen').append('<option value="">--Choose Type Specimen--</option>');

        $.ajax({
            type: 'get',
            url: "{{ route('dashboardTrf.getCtgMethReq') }}",
            data: {
                buyer: buyer,
                category: category,
                category_specimen: category_specimen,
                type: "type_specimen"
            },
            // success: function(response) {
            //     var data = response.data;
            //     for (let i = 0; i < data.length; i++) {
            //         $('#type_specimen').append('<option value="' + data[i]['type_specimen'] + '">' + data[i]['type_specimen'] + '</option>');
            //     }

            //     $('#type_specimen').val(trfTesting['type_specimen']).trigger('change');

            //     $.unblockUI();
            // },
            success: function(response) {
                var data = response.data;

                for (let i = 0; i < data.length; i++) {
                    $('#type_specimen').append('<option value="' + data[i]['type_specimen'] + '">' + data[i]['type_specimen'] + '</option>');
                }

                $('#type_specimen').val(trfTesting['type_specimen']).trigger('change');

                $('#type_specimen').on('change', function() {
                    var selectedTypeSpecimen = $(this).val();
                    
                    if (selectedTypeSpecimen !== trfTesting['type_specimen']) {
                        setSpc(category_specimen, selectedTypeSpecimen, asal);
                    }
                });

                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status, notif.output);

            }
        });
    }

    function getMeth(e) {
        var type_specimen = e.value;
        var category_specimen = $('#category_specimen').val();
        var category = $('#category').val();
        var buyer = $('#buyer').val();

        $('#test_methods').empty();

        $.ajax({
            type: 'get',
            url: "{{ route('dashboardTrf.getCtgMethReq') }}",
            data: {
                buyer: buyer,
                category: category,
                category_specimen: category_specimen,
                type_specimen: type_specimen,
                type: 'method'
            },
            success: function(response) {
                var data = response.data;
                var meth = @json($meth);
                for (let i = 0; i < data.length; i++) {

                    $('#test_methods').append('<option value="' + data[i]['master_method_id'] + '"></b>' + data[i]['method_code'] + '</b> - ' + data[i]['method_name'] + '</option>');
                }

                $('#test_methods').val(trftestmeth).trigger('change');
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status, notif.output);

            }
        });
    }

    // set doc baseon testing (no change)
    function setDataSpc(doc, testing) {
        // console.log(testing);
        $('#data_specimen').empty();

        if (testing['category_specimen'] == 'GARMENT') {
            $('#data_specimen').append(`@include('create_trf.specimen._garment')`);

            $('#test_condition').val(doc[0]['test_condition']);
            $('#temperature').val(doc[0]['temperature']);
            $('#qty').val(doc[0]['qty']);
            $('#machine').val(doc[0]['machine']);
            $('#sample_type').val(doc[0]['sample_type']);
            $('#supplier').val(doc[0]['manufacture_name']);
            $('#fabric_properties').val(doc[0]['fabric_properties']);
            $('#material_shell').val(doc[0]['material_shell_panel']);
            $('#fibre_composition').val(doc[0]['fibre_composition']);
            $('#care_instruction').val(doc[0]['care_instruction']);
            $('#remark').val(doc[0]['remark']);

        } else if (testing['category_specimen'] == 'ACCESORIES/TRIM' && testing['asal_specimen'] != 'DEVELOPMENT') {
            $('#data_specimen').append(`@include('create_trf.specimen._accesories')`);

            $('#fabric_supplier').val(doc[0]['remark']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#fabric_item').val(doc[0]['fabric_item']);

        } else if (testing['category_specimen'] == 'FABRIC' && testing['asal_specimen'] != 'DEVELOPMENT') {
            $('#data_specimen').append(`@include('create_trf.specimen._fabric')`);

            $('#fabric_composition').val(doc[0]['fibre_composition']);

        } else if ((testing['category_specimen'] == 'STRIKE OFF' || testing['category_specimen'] == 'PANEL') && testing['type_specimen'] == 'EMBROIDERY') {
            $('#data_specimen').append(`@include('create_trf.specimen._embro')`);

            $('#fabric_composition').val(doc[0]['fibre_composition']);
            $('#supplier_embro').val(doc[0]['manufacture_name']);
            $('#embro_item').val(doc[0]['item']);
            $('#embro_description').val(doc[0]['description']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#item_fabric').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#embro_color').val(doc[0]['interlining_color']);
            $('#embro_size').val(doc[0]['garment_size']);

        } else if ((testing['category_specimen'] == 'STRIKE OFF' || testing['category_specimen'] == 'PANEL') && testing['type_specimen'] == 'BADGE') {
            $('#data_specimen').append(`@include('create_trf.specimen._badge_strikeoff')`);

            $('#style_name').val(doc[0]['style_name']);
            $('#badge_size').val(doc[0]['size']);
            $('#garment_size').val(doc[0]['garment_size']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#badge_description').val(doc[0]['description']);
            $('#fabric_composition').val(doc[0]['fibre_composition']);

        } else if (testing['category_specimen'] == 'STRIKE OFF' && testing['type_specimen'] == 'RIVERT') {
            $('#data_specimen').append(`@include('create_trf.specimen._rivert')`);

            $('#fabric_composition').val(doc[0]['fibre_composition']);
            $('#supplier_rivert').val(doc[0]['manufacture_name']);
            $('#rivert_item').val(doc[0]['item']);
            $('#rivert_description').val(doc[0]['description']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#item_fabric').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#rivert_color').val(doc[0]['interlining_color']);
            $('#rivert_size').val(doc[0]['garment_size']);

        } else if ((testing['category_specimen'] == 'STRIKE OFF' || testing['category_specimen'] == 'PANEL') && testing['type_specimen'] == 'BONDING') {
            $('#data_specimen').append(`@include('create_trf.specimen._bonding_strikeoff')`);

            $('#style_name').val(doc[0]['style_name']);
            $('#temperature').val(doc[0]['temperature']);
            $('#pressure').val(doc[0]['pressure']);
            $('#duration').val(doc[0]['duration']);
            $('#machine').val(doc[0]['machine']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#fabric_supplier').val(doc[0]['fabric_properties']);
            $('#remark').val(doc[0]['remark']);


        } else if (testing['category_specimen'] == 'MOCKUP') {
            $('#data_specimen').append(`@include('create_trf.specimen._mockup')`);

            $('#fabric_composition').val(doc[0]['fibre_composition']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#interlining').val(doc[0]['interlining_color']);
            $('#machine').val(doc[0]['machine']);
            $('#temperature').val(doc[0]['temperature']);
            $('#pressure').val(doc[0]['pressure']);
            $('#duration').val(doc[0]['duration']);
            $('#style_name').val(doc[0]['style_name']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#component').val(doc[0]['component']);

        } else if (testing['category_specimen'] == 'STRIKE OFF' && testing['type_specimen'] == 'HEAT TRANSFER') {
            $('#data_specimen').append(`@include('create_trf.specimen._heat_strikeoff')`);

            $('#style_name').val(doc[0]['style_name']);
            $('#pelling').val(doc[0]['pelling']);
            $('#pad').val(doc[0]['pad']);
            $('#temperature').val(doc[0]['temperature']);
            $('#pressure').val(doc[0]['pressure']);
            $('#duration').val(doc[0]['duration']);
            $('#machine').val(doc[0]['machine']);
            $('#component').val(doc[0]['component']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#remark').val(doc[0]['remark']);

        } else if (testing['category_specimen'] == 'PANEL' && testing['type_specimen'] == 'HEAT TRANSFER') {
            $('#data_specimen').append(`@include('create_trf.specimen._heat_panel')`);

            $('#style_name').val(doc[0]['style_name']);
            $('#item').val(doc[0]['item']);
            $('#color').val(doc[0]['color']);
            $('#heat_supplier').val(doc[0]['manufacture_name']);
            $('#pelling').val(doc[0]['pelling']);
            $('#pad').val(doc[0]['pad']);
            $('#temperature').val(doc[0]['temperature']);
            $('#pressure').val(doc[0]['pressure']);
            $('#duration').val(doc[0]['duration']);
            $('#machine').val(doc[0]['machine']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#remark').val(doc[0]['remark']);

        } else if (testing['category_specimen'] == 'STRIKE OFF' && testing['type_specimen'] == 'PAD PRINT') {
            $('#data_specimen').append(`@include('create_trf.specimen._pad_strikeoff')`);

            $('#style_name').val(doc[0]['style_name']);
            $('#thread').val(doc[0]['thread']);
            $('#size_page').val(doc[0]['garment_size']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);

        } else if (testing['category_specimen'] == 'PANEL' && testing['type_specimen'] == 'PAD PRINT') {
            $('#data_specimen').append(`@include('create_trf.specimen._pad_panel')`);

            $('#style_name').val(doc[0]['style_name']);
            $('#thread').val(doc[0]['thread']);
            $('#size_page').val(doc[0]['garment_size']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#fabric_composition').val(doc[0]['fibre_composition']);
            $('#pad_print_supplier').val(doc[0]['manufacture_name']);
            $('#item').val(doc[0]['item']);
            $('#color').val(doc[0]['color']);
            $('#description').val(doc[0]['description']);

        } else if ((testing['category_specimen'] == 'STRIKE OFF' || testing['category_specimen'] == 'PANEL') && testing['type_specimen'] == 'PRINTING') {
            $('#data_specimen').append(`@include('create_trf.specimen._printing_strikeoff')`);

            $('#style_name').val(doc[0]['style_name']);
            $('#garment_size').val(doc[0]['size']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_type').val(doc[0]['fabric_type']);
            $('#fabric_color').val(doc[0]['fabric_color']);
            $('#fabric_composition').val(doc[0]['fibre_composition']);

        } else if (testing['category_specimen'] == 'FABRIC' && testing['asal_specimen'] == 'DEVELOPMENT') {
            $('#data_specimen').append(`@include('create_trf.specimen._dev_fabric')`);

            $('#style').val(doc[0]['style']);
            $('#article').val(doc[0]['article_no']);

        } else if (testing['category_specimen'] == 'ACCESORIES/TRIM' && testing['asal_specimen'] == 'DEVELOPMENT') {
            $('#data_specimen').append(`@include('create_trf.specimen._dev_acc')`);

            $('#style').val(doc[0]['style']);
            $('#article').val(doc[0]['article_no']);
            $('#fabric_item').val(doc[0]['fabric_item']);
            $('#fabric_color').val(doc[0]['fabric_color']);

        }


        $('#table-specimen > tbody').empty();

        for (let i = 0; i < doc.length; i++) {
            var no = i + 1;

            $('#table-specimen > tbody').append('<tr><td>' + no + '</td><td>' + doc[i]['document_type'] + '</td><td>' + doc[i]['document_no'] + '</td><td>' + doc[i]['season'] + '</td><td>' + doc[i]['style'] + '</td><td>' + doc[i]['article_no'] + '</td><td>' + doc[i]['size'] + '</td><td>' + doc[i]['color'] + '</td><td>' + doc[i]['item'] + '</td></tr>');
        }
    }

    function resetProperties(properties) {
        properties.forEach(prop => trfDoc[0][prop] = null);
    }

    // set new value trf doc
    function completedDataSumary(category_specimen, type_specimen, asal) {
        if (category_specimen == 'GARMENT') {
            trfDoc[0]['test_condition'] = $('#test_condition').val();
            trfDoc[0]['temperature'] = $('#temperature').val();
            trfDoc[0]['qty'] = $('#qty').val();
            trfDoc[0]['machine'] = $('#machine').val();
            trfDoc[0]['sample_type'] = $('#sample_type').val();
            trfDoc[0]['manufacture_name'] = $('#supplier').val();
            trfDoc[0]['fabric_properties'] = $('#fabric_properties').val();
            trfDoc[0]['material_shell_panel'] = $('#material_shell').val()
            trfDoc[0]['fibre_composition'] = $('#fibre_composition').val();
            trfDoc[0]['care_instruction'] = $('#care_instruction').val();
            trfDoc[0]['remark'] = $('#remark').val();

            resetProperties([
                'description', 'fabric_color', 'interlining_color', 'pressure', 'duration', 'style_name', 'fabric_item', 'fabric_type', 'thread', 'pad', 'pelling', 'garment_size', 'component'
            ]);


        } else if (category_specimen == 'FABRIC' && asal != "DEVELOPMENT") {
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();

            resetProperties([ 
                'description', 'fabric_color', 'interlining_color', 'machine', 'temperature', 'pressure', 
                'duration', 'style_name', 'fabric_item', 'fabric_type', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component'
            ]);

        } else if (category_specimen == "ACCESORIES/TRIM" && asal != "DEVELOPMENT") {
            trfDoc[0]['remark'] = $('#fabric_supplier').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();

            resetProperties([
                'fibre_composition', 
                'description', 'interlining_color', 'machine', 'temperature', 'pressure', 
                'duration', 'style_name', 'fabric_type', 'thread', 'material_shell_panel', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component'
            ]);

        } else if ((category_specimen == 'STRIKE OFF' || category_specimen == 'PANEL') && type_specimen == 'EMBROIDERY') {
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();
            trfDoc[0]['manufacture_name'] = $('#supplier_embro').val();
            trfDoc[0]['item'] = $('#embro_item').val();
            trfDoc[0]['description'] = $('#embro_description').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fabric_item'] = $('#item_fabric').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['interlining_color'] = $('#embro_color').val();
            trfDoc[0]['garment_size'] = $('#embro_size').val();

            resetProperties([
                'machine', 'temperature', 'pressure', 
                'duration', 'style_name', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'component'
            ]);

        } else if ((category_specimen == 'STRIKE OFF' || category_specimen == 'PANEL') && type_specimen == 'BADGE') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['size'] = $('#badge_size').val();
            trfDoc[0]['garment_size'] = $('#garment_size').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['description'] = $('#badge_description').val();
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();

            resetProperties([ 
                'interlining_color', 'machine', 'temperature', 'pressure', 
                'duration', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'component'
            ]);

        } else if (category_specimen == 'STRIKE OFF' && type_specimen == 'RIVERT') {
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();
            trfDoc[0]['manufacture_name'] = $('#supplier_rivert').val();
            trfDoc[0]['item'] = $('#rivert_item').val();
            trfDoc[0]['description'] = $('#rivert_description').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fabric_item'] = $('#item_fabric').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['interlining_color'] = $('#rivert_color').val();
            trfDoc[0]['garment_size'] = $('#rivert_size').val();

            resetProperties([
                'machine', 'temperature', 'pressure', 
                'duration', 'style_name', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'component'
            ]);

        } else if (category_specimen == 'MOCKUP') {
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['interlining_color'] = $('#interlining').val();
            trfDoc[0]['machine'] = $('#machine').val();
            trfDoc[0]['temperature'] = $('#temperature').val();
            trfDoc[0]['pressure'] = $('#pressure').val();
            trfDoc[0]['duration'] = $('#duration').val();
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['component'] = $('#component').val();

            resetProperties([
                'description', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size'
            ]);

        } else if ((category_specimen == 'STRIKE OFF' || category_specimen == 'PANEL') && type_specimen == 'BONDING') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['temperature'] = $('#temperature').val();
            trfDoc[0]['pressure'] = $('#pressure').val();
            trfDoc[0]['duration'] = $('#duration').val();
            trfDoc[0]['machine'] = $('#machine').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fabric_properties'] = $('#fabric_supplier').val();
            trfDoc[0]['remark'] = $('#remark').val();

            resetProperties([
             'garment_size', 'pad', 'sample_type', 'test_condition', 'description','fabric_composition', 'care_instruction', 'pelling', 'thread', 'material_shell_panel', 'component', 'fibre_composition'
            ]);

        } else if ((category_specimen == 'PANEL' || category_specimen == 'STRIKE OFF') && type_specimen == 'FUSE') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['machine'] = $('#machine').val();
            trfDoc[0]['temperature'] = $('#temperature').val();
            trfDoc[0]['pressure'] = $('#pressure').val();
            trfDoc[0]['duration'] = $('#duration').val();
            trfDoc[0]['item'] = $('#item').val();
            trfDoc[0]['manufacture_name'] = $('#fuse_supplier').val();
            trfDoc[0]['remark'] = $('#remark').val();

            resetProperties([
                'fibre_composition','description', 'fabric_color', 'interlining_color', 'fabric_item', 'fabric_type', 'thread', 'material_shell_panel', 'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component'
            ]);

        } else if (category_specimen == 'STRIKE OFF' && type_specimen == 'HEAT TRANSFER') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['pelling'] = $('#pelling').val();
            trfDoc[0]['pad'] = $('#pad').val();
            trfDoc[0]['temperature'] = $('#temperature').val();
            trfDoc[0]['pressure'] = $('#pressure').val();
            trfDoc[0]['duration'] = $('#duration').val();
            trfDoc[0]['machine'] = $('#machine').val();
            trfDoc[0]['component'] = $('#component').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['remark'] = $('#remark').val();

            resetProperties([
                'description', 'interlining_color', 'thread', 'material_shell_panel', 
                'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'fibre_composition'
            ]);

        } else if (category_specimen == 'PANEL' && type_specimen == 'HEAT TRANSFER') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['item'] = $('#item').val();
            trfDoc[0]['color'] = $('#color').val();
            trfDoc[0]['manufacture_name'] = $('#heat_supplier').val();
            trfDoc[0]['pelling'] = $('#pelling').val();
            trfDoc[0]['pad'] = $('#pad').val();
            trfDoc[0]['temperature'] = $('#temperature').val();
            trfDoc[0]['pressure'] = $('#pressure').val();
            trfDoc[0]['duration'] = $('#duration').val();
            trfDoc[0]['machine'] = $('#machine').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['remark'] = $('#remark').val();

            resetProperties([
                'description', 'interlining_color', 'thread', 'material_shell_panel',  
                'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component', 'fibre_composition'
            ]);

        } else if ((category_specimen == 'PANEL' || category_specimen == 'STRIKE OFF') && type_specimen == 'INTERLINING') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['item'] = $('#item').val();
            trfDoc[0]['color'] = $('#color').val();
            trfDoc[0]['manufacture_name'] = $('#interlining_supplier').val();
            trfDoc[0]['temperature'] = $('#temperature').val();
            trfDoc[0]['pressure'] = $('#pressure').val();
            trfDoc[0]['duration'] = $('#duration').val();
            trfDoc[0]['machine'] = $('#machine').val();
            trfDoc[0]['remark'] = $('#remark').val();

            resetProperties([ 
                'description', 'fabric_color', 'interlining_color', 'fabric_item', 'fabric_type', 'thread', 'material_shell_panel', 'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component', 'fibre_composition'
            ]);

        } else if (category_specimen == 'STRIKE OFF' && type_specimen == 'PAD PRINT') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['thread'] = $('#thread').val();
            trfDoc[0]['garment_size'] = $('#size_page').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();

            resetProperties([
                'description', 'fabric_color', 'interlining_color', 'machine', 'temperature', 'pressure', 
                'duration', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'component'
            ]);

        } else if (category_specimen == 'PANEL' && type_specimen == 'PAD PRINT') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['thread'] = $('#thread').val();
            trfDoc[0]['garment_size'] = $('#size_page').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();
            trfDoc[0]['manufacture_name'] = $('#pad_print_supplier').val();
            trfDoc[0]['item'] = $('#item').val();
            trfDoc[0]['color'] = $('#color').val();
            trfDoc[0]['description'] = $('#description').val();

            resetProperties([
                'interlining_color', 'machine', 'temperature', 'pressure', 
                'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'component'
            ]);

        } else if ((category_specimen == 'STRIKE OFF' || category_specimen == 'PANEL') && type_specimen == 'PRINTING') {
            trfDoc[0]['style_name'] = $('#style_name').val();
            trfDoc[0]['size'] = $('#garment_size').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_type'] = $('#fabric_type').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();
            trfDoc[0]['fibre_composition'] = $('#fabric_composition').val();

            resetProperties([
                
                'description', 'interlining_color', 'machine', 'temperature', 'pressure', 
                'duration', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component'
            ]);

        } else if (category_specimen == 'FABRIC' && asal == "DEVELOPMENT") {
            trfDoc[0]['style'] = $('#style').val();
            trfDoc[0]['article_no'] = $('#article').val();

            resetProperties([ 
                'description', 'fabric_color', 'interlining_color', 'machine', 'temperature', 'pressure', 
                'duration', 'style_name', 'fabric_item', 'fabric_type', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component', 'fibre_composition'
            ]);

        } else if (category_specimen == 'ACCESORIES/TRIM' && asal == "DEVELOPMENT") {

            trfDoc[0]['style'] = $('#style').val();
            trfDoc[0]['article_no'] = $('#article').val();
            trfDoc[0]['fabric_item'] = $('#fabric_item').val();
            trfDoc[0]['fabric_color'] = $('#fabric_color').val();

            resetProperties([
                'description', 'interlining_color', 'machine', 'temperature', 'pressure', 
                'duration', 'style_name', 'fabric_type', 'thread', 'material_shell_panel', 'remark', 
                'pad', 'pelling', 'sample_type', 'test_condition', 'fabric_properties', 'care_instruction', 'garment_size', 'component', 'fibre_composition'
            ]);

        }
    }

    // set doc baseon ctg spc & type spc - origin (change header)
    function setSpc(category_specimen,type_specimen,asal){
        $('#data_specimen').empty();
        
        if (category_specimen=='GARMENT') {
            $('#data_specimen').append(`@include('create_trf.specimen._garment')`);
        }else if (category_specimen=='ACCESORIES/TRIM' && asal!="DEVELOPMENT") {
            $('#data_specimen').append(`@include('create_trf.specimen._accesories')`);
        }else if (category_specimen=='FABRIC' && asal!="DEVELOPMENT") {
            $('#data_specimen').append(`@include('create_trf.specimen._fabric')`);
        }else if ((category_specimen=='STRIKE OFF' || category_specimen=='PANEL') && type_specimen=='EMBROIDERY') {
            $('#data_specimen').append(`@include('create_trf.specimen._embro')`);
        }else if ((category_specimen=='STRIKE OFF' || category_specimen=='PANEL')  && type_specimen=='BADGE') {
            $('#data_specimen').append(`@include('create_trf.specimen._badge_strikeoff')`);
        }else if (category_specimen=='STRIKE OFF' && type_specimen=='RIVERT') {
            $('#data_specimen').append(`@include('create_trf.specimen._rivert')`);
        }else if((category_specimen=='STRIKE OFF' || category_specimen=='PANEL') && type_specimen=='BONDING'){
            $('#data_specimen').append(`@include('create_trf.specimen._bonding_strikeoff')`);
        }else if (category_specimen=='MOCKUP' ) {
            $('#data_specimen').append(`@include('create_trf.specimen._mockup')`);
        }else if(category_specimen=='STRIKE OFF' && type_specimen=='HEAT TRANSFER'){
            $('#data_specimen').append(`@include('create_trf.specimen._heat_strikeoff')`);
        }else if(category_specimen=='PANEL' && type_specimen=='HEAT TRANSFER'){
            $('#data_specimen').append(`@include('create_trf.specimen._heat_panel')`);
        }else if(category_specimen=='STRIKE OFF' && type_specimen=='PAD PRINT'){
            $('#data_specimen').append(`@include('create_trf.specimen._pad_strikeoff')`);
        }else if(category_specimen=='PANEL' && type_specimen=='PAD PRINT'){
            $('#data_specimen').append(`@include('create_trf.specimen._pad_panel')`);
        }else if((category_specimen=='STRIKE OFF' || category_specimen=='PANEL') && type_specimen=='PRINTING'){
            $('#data_specimen').append(`@include('create_trf.specimen._printing_strikeoff')`);
        }else if (category_specimen=='FABRIC' && asal=="DEVELOPMENT") {
            $('#data_specimen').append(`@include('create_trf.specimen._dev_fabric')`);
        }else if (category_specimen=='ACCESORIES/TRIM' && asal=="DEVELOPMENT") {
            $('#data_specimen').append(`@include('create_trf.specimen._dev_acc')`);
        }

        dataset = [];
    }
</script>
@endsection