
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="{!! $detail !!}"> Detail</a></li>
            @endif
            @if (isset($approval))
                <li><a href="#" data-id="{!! $approval['id'] !!}" data-trf="{!! $approval['trf'] !!}" onclick="approve(this);" data-status="VERIFIED">Approve</a></li>
            @endif
            @if (isset($reject))
            <li><a href="#" data-id="{!! $reject['id'] !!}" data-trf="{!! $reject['trf'] !!}" onclick="approve(this);" data-status="REJECT">Reject</a></li>
            @endif
            @if (isset($handover))
            <li><a href="{!! $handover !!}">Handover</a></li>
            @endif
            @if (isset($chooseTesting))
            <li><a href="#" class="ignore-click chooseTesting" onclick="chooseTesting(this);" data-id="{!! $chooseTesting !!}">Approve</a></li>
            @endif
            @if (isset($inputT2Testing))
            <li><a href="#" onclick="inputT2Testing('{!! $inputT2Testing !!}')">Input T2 Result</a></li>
            @endif

            @if (isset($input_testing))
            <li><a href="#" onclick="inputTesting('{!! $input_testing !!}')">Input Testing</a></li>
            @endif

            @if (isset($t2_result))
            <li><a href="#"class="ignore-click t2_result" data-trfid="{!! $t2_result !!}">T2 Result</a></li>
            @endif


            
            
           
           
        </ul>
    </li>
</ul>
