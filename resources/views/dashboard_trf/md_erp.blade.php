
<div id="modal_erp" class="modal fade" >
    <div class="modal-dialog modal-full">
        <div class="modal-content ">
            <div class="modal-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class ="table table-basic table-condensed table-erp" id="table-erp" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Doc. Number</th>
                                    <th>Style</th>
                                    <th>Article</th>
                                    <th>Size</th>
                                    <th>Item</th>
                                    <th>Season</th>
                                    <th>PO Supplier</th>
                                    <th>Export To</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>