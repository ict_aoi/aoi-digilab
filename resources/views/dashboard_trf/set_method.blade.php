@extends('layouts.app', ['active' => 'menu-set-method'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
			<li class="active">TRF</li>
		</ul>
	</div>
</div>
@endsection
 
@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
                <h4></i> <span class="text-semibold"> Set Methode</span> </h4>
            </div>
			<div class="panel-body">
		
				
				<div class="row form-group">
					<div class="table-responsive">
						<table class="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                    <th>#</th>
                                    <th>Date Submit</th>
                                    <th>No TRF</th>
                                    <th>Origin of Specimen</th>
                                    <th>Test Required</th>
                                    <th>Date Information</th>
                                    <th>Buyer</th>
                                    <th>Lab Location</th>
                                    <th>User</th>
                                    <th>Category</th>
                                    <th>Category Specimen</th> 
                                    <th>Return Test Sample </th>
                                    <th>Status</th>
                                    <th>Remark</th>
                                    <th>Action</th>
                           
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('modal')


<div id="modal_set" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
       <!--          <center><h4>UPLOAD METHODE</h4></center> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-2">
                        <input type="hidden" name="trfid" id="trfid">
                    </div>
                    <div class="col-lg-8">
                        <label><b>Set Methode</b></label>
                        <div class="multi-select-full">
                            
                            <select multiple="multiple" data-placeholder="Choosse Methods" class="select" id="test_methods">
                                
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8"></div>
                    <div class="col-lg-3">
                        <button class="btn btn-success" id="btn-savemth">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function(){
    
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: "{{ route('dashboardTrf.dataSetMeth') }}",
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at',searchable:false, orderable:true},
            {data: 'trf_id', name: 'trf_id',searchable:true, orderable:true},
            {data: 'asal_specimen', name: 'asal_specimen',searchable:true, orderable:true},
            {data: 'test_required', name: 'test_required',searchable:false, orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false, orderable:true},
            {data: 'buyer', name: 'buyer',searchable:false, orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false, orderable:true},
            {data: 'nik', name: 'nik',searchable:false, orderable:true},
            {data: 'category', name: 'category',searchable:false, orderable:true},
            {data: 'category_specimen', name: 'category_specimen',searchable:false, orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false, orderable:true},
            {data: 'status', name: 'status',searchable:true, orderable:true},
            {data: 'remarks', name: 'remarks',searchable:true, orderable:true},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
        ]
    });


    $('#table-list').on('click','.setMeth',function(event){
        // $('#test_methods').val(null).trigger('change');
        event.preventDefault();
        
        var id = $(this).data('id');
        var idctg = $(this).data('idctg');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('dashboardTrf.getMethode') }}",
            data : {idctg:idctg},
            success: function(response) {

                
                var data = response.data;
               
                $('#test_methods').empty();
                for (var i = 0; i < data.length; i++) {
                   

                    $('#test_methods').append('<option value="'+data[i]['master_method_id']+'"  >'+data[i]['method_code']+' '+data[i]['method_name']+'</option>');
                }


                setSelect(id);
                 
            },
            error: function(response) {
         
                console.log(response);

                
            }
        });
       

        $('#modal_set').modal('show');
    });


    $("#btn-savemth").click(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('dashboardTrf.updateMth') }}",
            data:{id:$('#trfid').val(),data:$('#test_methods').val()},
            beforeSend:function(){
                loading();
            },
            success: function(response) {
                $.unblockUI();
                var response = response.data;

                alert(response.status,response.output);
            
                 $('#modal_set').modal('hide');
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']);
                console.log(response);

                
            }
        });

    });

    // $('#modal_set').on('hidden.bs.modal',function(e){
    //     $(this)
    //         .find('select')
    //         .val()
    //         .trigger('change')
    //         .end();
    // });

    $('#modal_set').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });


    
});

function setSelect(id){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('dashboardTrf.getTrfMEth') }}",
        data : {id:id},
        success: function(response) {

            $('#test_methods').val(response.data).trigger('change');
            // $('#test_methods').trigger('change');
             
        },
        error: function(response) {
     
            console.log(response);

            
        }
    });



}
	







</script>
@endsection
