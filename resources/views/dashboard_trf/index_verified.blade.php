@extends('layouts.app', ['active' => 'menu-trf-verified'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
			<li class="active">TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
					
				</div>
                {{-- <div class="heading-elements">
                        <a href="#" class="btn btn-default" id="btn-add"><i class="icon-plus2 position-left"></i>Create New</a>
                        <a class="btn btn-primary btn-icon" href="{{ route('method.export')}}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
                        <button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
                        
                    
                </div> --}}
            </div>
			<div class="panel-body">
				
				
				<div class="row form-group">
					<div class="table-responsive">
						<table class="table table-basic table-condensed" id="master_data_fabric_testing_table">
							<thead>
								<tr>
									<th>Date Submit</th>
                                    <th>Origin of Specimen</th>
                                    <th>Test Required</th>
                                    <th>Date Information</th>
                                    <th>Buyer</th>
                                    <th>Lab Location</th>
                                    <th>No TRF</th>
                                    <th>User</th>
                                    <th>Details</th>
                                    <th>Category</th>
                                    <th>Category Specimen</th> 
                                    <th>Type Specimen</th>
                                    <th>Testing Methods</th>
                                    <th>Return Test Sample </th>
                                    <th>Status</th>
                                    <th>On Time Delivery</th>
                                    <th>T1 Testing Result</th>
                                    <th>T2 Testing Result</th>
                                    <th>Remark</th>
                                    <th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')

<div id="chooseTestingModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            
            <div class="modal-body">
                <div class="table-responsive">
                   
                    <table class="table table-basic table-condensed" id="choose-testing-table">
                        <thead>
                            <tr>
                                <th>Method Name</th>
                                <th>Komposisi Specimen</th>
                                <th>Parameter</th>
                                <th>Perlakuan Test</th>
                                <th><input type="checkbox"  id="check_all"></th>
                            </tr>
                        </thead>
                    </table>
                    <form action="{{ route('dashboardTrf.saveDatareq')}}" method="POST" id="form_req">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="testing_id" name="testing_id"/>
                        <button type="button" name="saveReq" id ="saveReq" class="btn btn-primary" >Submit </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                   
                </div>
            </div>
            
        </div>
			
		</div>
	</div>
</div>


<div id="inputT2TestingModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="{{route('dashboardTrf.saveT2Testing')}}" id="inputT2TestingForm" action="post">
				@csrf
				<div class="row">
					<div class="col-lg-6">
						<label class="display-block text-semibold">Input T2 Testing</label>
						<input type="text" name="input_testing" id="input_testing" class="form-control" required="">
                        <input type="hidden" name="testing_id" id="testing_id" class="form-control" >
					</div>

					
				</div>
			
			
				<br>
				<div class="row">
					<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection


@section('js')
<script type="text/javascript">
	
	$('#master_data_fabric_testing_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        scrollX:true,
        ajax: {
            type: 'GET',
            url: '{{ route('dashboardTrf.data_verified')}}',
        },
        columns: [
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
            {data: 'asal_specimen', name: 'asal_specimen',searchable:true,visible:true,orderable:true},
            {data: 'test_required', name: 'test_required',searchable:true,visible:true,orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false,visible:true,orderable:true},
            {data: 'buyer', name: 'buyer',searchable:false,visible:true,orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false,visible:true,orderable:true},
            {data: 'no_trf', name: 'no_trf',searchable:true,visible:true,orderable:true},
            {data: 'nik', name: 'nik',searchable:false,visible:true,orderable:true},
            {data: 'orderno', name: 'orderno',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:false,visible:true,orderable:true},
            {data: 'category_specimen', name: 'category_specimen',searchable:false,visible:true,orderable:true},
            {data: 'type_specimen', name: 'type_specimen',searchable:false,visible:true,orderable:true},
            {data: 'testing_method_id', name: 'testing_method_id',searchable:false,visible:true,orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false,visible:true,orderable:true},
            {data: 'last_status', name: 'last_status',searchable:true,visible:true,orderable:true},
            {data: 'verified_lab_date', name: 'verified_lab_date',searchable:true,visible:true,orderable:true},
            {data: 't1_result', name: 't1_result',searchable:true,visible:true,orderable:true},
            {data: 't2_result', name: 't2_result',searchable:true,visible:true,orderable:true},
            {data: 'remark', name: 'remark',searchable:true,visible:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#master_data_fabric_testing_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();


    $('#saveReq').on('click', function() {
        var data = Array();
        $('.styled:checked').each(function(){
            data.push({
                req_id : $(this).data('id')
            });
        });
        var id = $('#testing_id').val();
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
        $.ajax({
            type: "post",
            url: '{{ route('dashboardTrf.saveDatareq')}}',
            data: {
                data: data,
                id: id,
                
            },
            beforeSend: function () {
                loading();
            },
            success: function (response) {
                var data = response.data
                if(data.status == '200')
                {
                    $.unblockUI();
                    var ids = data.ids;
                    window.location.href = "/trf/dashboard-trf/test/"+ids;
                  
                    alert(data.status,data.output);
                   
                    
                  
                }else
                {
                    $.unblockUI();
                    alert(data.status,data.output);

                }
               
               
            },
            error: function(response) {
	        	$.unblockUI();
	            var notif = response.data;
	            alert(notif.status,notif.output);
	            
	        }
        });
    });

    $('#check_all').click(function(){
        if($(this).is(':checked')){
            $('#choose-testing-table .styled').prop("checked",true);
        }else{
            $('#choose-testing-table .styled').prop("checked",false);
        }
    });

    

function get_data_table_modal()
{   
    var id = $('#testing_id').val();
    $('#choose-testing-table thead tr').clone(true).appendTo( '#choose-testing-table thead' );
    $('#choose-testing-table thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
	var table = $('#choose-testing-table').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '{{ route('dashboardTrf.getDatareq')}}',
            data:{id:id},
        },
        columns: [
            {data: 'method_name', name: 'method_name',searchable:true,visible:true,orderable:true},
            {data: 'komposisi_specimen', name: 'komposisi_specimen',searchable:true,visible:true,orderable:true},
            {data: 'parameter', name: 'parameter',searchable:true,visible:true,orderable:true},
            {data: 'perlakuan_test', name: 'perlakuan_test',searchable:true,visible:true,orderable:true},
            {data: 'selector', name: 'selector',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#choose-testing-table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    
}

function chooseTesting(url)
{
    
      $.ajax({
	        type: 'get',
	        url : url,
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
                var update = response;
                console.log(update);

                if(update.check == true)
                {
                    $.unblockUI();
                    window.location.href = "/trf/dashboard-trf/test/"+update.id;

                }else
                {
                    $.unblockUI();
                    var asal_specimen = update.asal;
                    if(asal_specimen == "PRODUCTION")
                    {
                        $('#testing_id').val(update.id);
                        get_data_table_modal();
                        $('#chooseTestingModal').modal('show');

                    }else
                    {
                        $('#testing_id').val(update.id);
                        get_data_table_dev_modal();
                        $('#chooseTestingDevModal').modal('show');
                    }
                    
                    
                }
	        	
                
               
                
	        },
	        error: function(response) {
	        	$.unblockUI();
	           var notif = response.data;
	            
	        }
	    });
   
}

function inputT2Testing(url)
{
    $.ajax({
	        type: 'get',
	        url : url,
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
                var update = response;
	        	$.unblockUI();
                $('#testing_id').val(response.id);
                $('#input_testing').val(response.t2_result);
                $('#inputT2TestingModal').modal('show');
                
               
                
	        },
	        error: function(response) {
	        	$.unblockUI();
	           var notif = response.data;
	            
	        }
	    });
}

$('#inputT2TestingForm').submit(function(event){
    console.log($('#input_testing').val());
    event.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#inputT2TestingForm').attr('action'),
        data:{
            input_testing 		: $('#input_testing').val(),
            testing_id          : $('#testing_id').val(),
            },
        beforeSend : function(){
            loading();
        },
        success: function(response) {
            var notif = response.data;

            if(notif.status == '200')
            {
                $.unblockUI();
                $('#inputT2TestingModal').modal('hide');
                alert(notif.status,notif.output);
                $('#master_data_fabric_testing_table').DataTable().ajax.reload();
            }else
            {
                $.unblockUI();
                alert(notif.status,notif.output);
            }
           
        },
        error: function(response) {
            $.unblockUI();
            var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    });
    
});

	   



</script>
@endsection
