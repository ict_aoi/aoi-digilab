@extends('layouts.app', ['active' => 'menu-dashboard-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Waiting List</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="panel-heading">
                <h4></i> <span class="text-semibold"> Waiting List</span> </h4>
            </div>
			<div class="panel-body">
		
				<div class="row {{Auth::user()->admin==false ? 'hidden' : '' }}">
                    <label><b>Lab Location</b></label>
                    <select id="labloct" class="select">
                        @php($userid= Auth::user()->id)
                        @foreach($labloct as $fc)
                            {{-- <option value="{{$fc->factory_name}}" {{$fc->id==auth::user()->factory_id ? 'selected' : ''}}>{{$fc->factory_name}}</option> --}}
                            <option value="{{$fc->name}}" {{ ($fc->id==auth::user()->factory_id) ? 'selected' : ''}}>{{$fc->name}}</option>
                        @endforeach
                    </select>            
                </div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
                                    <th>#</th>
                                    <th>Submit Date</th>
                                    <th>TRF</th>
                                    <th>Specimen</th>
                                    <!-- <th>Origin of Specimen</th> -->
                                    <!-- <th>Method Code</th> -->
                                    <th>Test Required</th>
                                    <th>Date Information</th>
                                    <!-- <th>Buyer</th> -->
                                    <th>Lab Location</th>
                                    <th>User</th>
                                    <th>Category</th>
                                    <!-- <th>Category Specimen</th>  -->
                                    <th>Return Test Sample </th>
                                    <th>Status</th>
                                    <th>Action</th>
                           
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@include('dashboard_trf.md_setmeth')

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: "{{ route('dashboardTrf.data') }}",
            data: function (d) {
                return $.extend({},d,{
                    'labloct' : $('#labloct').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        initComplete:function(){
            $('.dataTables_filter input').unbind();
            $('.dataTables_filter input').bind('keyup',function(e){
                var code = e.keyCode || e.which;
				if (code == 13) {
					table.search(this.value).draw();
					
				}
            });
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at',searchable:false, orderable:true},
            {data: 'trf_id', name: 'trf_id',searchable:true, orderable:true},
            {data: 'specimen', name: 'specimen',searchable:true, orderable:true},
            // {data: 'asal_specimen', name: 'asal_specimen',searchable:true, orderable:true},
            // {data: 'methods', name: 'methods',searchable:true, orderable:true},
            {data: 'test_required', name: 'test_required',searchable:false, orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false, orderable:true},
            // {data: 'buyer', name: 'buyer',searchable:false, orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false, orderable:true},
            {data: 'nik', name: 'nik',searchable:false, orderable:true},
            {data: 'category', name: 'category',searchable:false, orderable:true},
            // {data: 'category_specimen', name: 'category_specimen',searchable:false, orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false, orderable:true},
            {data: 'status', name: 'status',searchable:true, orderable:true},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false},
            {data: 'methods', name: 'methods', visible: false, searchable: true},
            {data: 'username', name: 'username', visible: false, searchable: true}
        ]
    });

    $('#labloct').change(function(){
        table.clear();
        table.draw();
    });

    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });

    $('#table-list').on('click','.setMeth',function(event){
        event.preventDefault();
        
        var id = $(this).data('id');
        var idctg = $(this).data('idctg');
        var buyer = $(this).data('buyer');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('dashboardTrf.getMethode') }}",
            data : {idctg:idctg,buyer:buyer},
            success: function(response) {

                
                var data = response.data;
                $('#mdtrfid').val(id);
                $('#test_methods').empty();
                for (var i = 0; i < data.length; i++) {
                   

                    $('#test_methods').append('<option value="'+data[i]['master_method_id']+'"  >'+data[i]['method_code']+' '+data[i]['method_name']+'</option>');
                }


                setSelect(id);
                 
            },
            error: function(response) {
         
                console.log(response);

                
            }
        });
       

        $('#modal_set').modal('show');

    });



    $("#btn-savemth").click(function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('dashboardTrf.updateMth') }}",
            data:{id:$('#mdtrfid').val(),data:$('#test_methods').val()},
            beforeSend:function(){
                loading();
            },
            success: function(response) {
                $.unblockUI();
                var response = response.data;

                alert(response.status,response.output);
            
                 $('#modal_set').modal('hide');
            },
            error: function(response) {
                $.unblockUI();
                alert(500,response['responseJSON']);
                console.log(response);

                
            }
        });

    });

    $('#modal_set').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });
});
	

function approve(e){
    var id = e.getAttribute('data-id');
    var trf = e.getAttribute('data-trf');
    var status = e.getAttribute('data-status');

    Swal.fire({
        title:"Are You sure "+status+" this TRF ?",
        showCancelButton : true,
        confirmButtonText : status
    }).then((result)=>{

        if (result.isConfirmed) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url : "{{ route('dashboardTrf.approval') }}",
                data: {id:id,trf:trf,status:status},
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    $.unblockUI();
                    
                    var data = response.data;

                    alert(data.status,data.output);
                    window.location.reload();
                    
                },
                error: function(response) {
                    $.unblockUI();

                    // console.log(response.responseJSON['message']);
                    alert(response.status,response.responseJSON['message']);

                    
                }
            });
        }
        

    });
  
    
}  



function setSelect(id){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('dashboardTrf.getTrfMEth') }}",
        data : {id:id},
        success: function(response) {
                console.log(response.data);
            $('#test_methods').val(response.data).trigger('change');
            // $('#test_methods').trigger('change');
             
        },
        error: function(response) {
     
            console.log(response);

            
        }
    });

}

</script>
@endsection
