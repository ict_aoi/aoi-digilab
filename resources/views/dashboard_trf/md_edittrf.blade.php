 
<div id="modal_add" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content ">
            <div class="modal-header">
                <center><h4>Add Specimen</h4></center>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label style="color:red;"><b>* Required Value</b></label>
                    <input type="text" name="mdidx" id="mdidx" class="mdidx hidden">
                    <input type="text" name="mddocid" id="mddocid" class="mddocid hidden">
                    <input type="text" name="mdtrfid" id="mdtrfid" class="mdtrfid hidden">
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Doc Type</b> <b style="color:red;">*</b></label>
                        <select class="select mddoctype" id="mddoctype">
                            <option value="">--Choosee Document--</option>
                            <option value="MO">MO</option>
                            <!-- <option value="ITEM">ITEM</option> -->
                            <option value="PO BUYER">PO BUYER</option>
                            <option value="PO SUPPLIER">PO SUPPLIER</option>
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label><b>Doc. Number</b> <b style="color:red;">*</b></label>
                        <input type="text" name="mddocno" class="form-control mddocno" >
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Season</b> <b style="color:red;">*</b></label>
                        <input type="text" name="mdarticle" class="form-control mdseason" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Style</b> <b style="color:red;">*</b></label>
                        <input type="text" name="mdstyle" class="form-control mdstyle" >
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Article</b> <b style="color:red;">*</b></label>
                        <input type="text" name="mdarticle" class="form-control mdarticle" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Size</b> <b style="color:red;">*</b></label>
                        <input type="text" name="mdsize" class="form-control mdsize" >
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Color</b></label>
                        <input type="text" name="mdcolor" class="form-control mdcolor" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Item</b> <b style="color:red;">*</b></label>
                        <input type="text" name="mditem" class="form-control mditem" >
                    </div>
                    
                </div>


                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Fibre Composition</b></label>
                        <input type="text" name="mdfibre" class="form-control mdfibre" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Fabric Finish</b></label>
                        <input type="text" name="mdfabfin" class="form-control mdfabfin" >
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Gauge</b></label>
                        <input type="text" name="mdgauge" class="form-control mdgauge" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Fabric Wight</b></label>
                        <input type="text" name="mdweight" class="form-control mdweight" >
                    </div>
                    
                </div>


                <div class="row">
                    <div class="col-lg-6">
                        <label><b>PLM No.</b></label>
                        <input type="text" name="mdplmno" class="form-control mdplmno" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Care Instruction</b></label>
                        <input type="text" name="mdcare" class="form-control mdcare" >
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Manufacture Name</b></label>
                        <input type="text" name="mdmanuf" class="form-control mdmanuf" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Exported To</b></label>
                        <input type="text" name="mdexport" class="form-control mdexport" >
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label><b>No. Roll</b></label>
                        <input type="text" name="mdroll" class="form-control mdroll" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Batch Number</b></label>
                        <input type="text" name="mdbatch" class="form-control mdbatch" >
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label><b>PO Supplier</b></label>
                        <input type="text" name="mdposupp" class="form-control mdposupp" >
                    </div>
                    <div class="col-lg-6">
                        <label><b>Yds Roll</b></label>
                        <input type="text" name="mdydsroll" class="form-control mdydsroll" >
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label><b>Additional Information</b></label>
                        <select class="form-control select addInfo" id="addInfo">
                            <option value="">--Choose Additional Information--</option>
                            <option value="TOP">TOP</option>
                            <option value="BOTTOM">BOTTOM</option>
                            <option value="SET">SET</option>
                            <option value="MOCKUP">MOCKUP</option>
                            <!-- <option value="PCS">PCS</option> -->
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label>Description</label>
                        <textarea class="form-control mddescrpt" id="mddescrpt" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <center>
                        <button class="btn btn-primary btmd-save" id="btn-md-save" style="margin-top: 27px; margin-left: 200px;">Save</button>
                    </center>
                </div>
                
            </div>
        </div>
    </div>
</div>
