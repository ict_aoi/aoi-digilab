@extends('layouts.app', ['active' => 'menu-testing-trf'])
@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        
        <div class="page-title">
            <h4><span class="text-semibold">Form Testing Lab</span></h4>
        </div>
       
        
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="#">Testing Lab</a></li>
          
        </ul>
    </div>
	
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class="panel panel-flat">
            <div class="panel-heading">
               
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 col-lg-6 col-sm-12">
                            <p>No TRF : <b>{{$master_data_fabric_testing->no_trf}}</b></p>
                            <p>Asal Specimen : <b>{{$master_data_fabric_testing->asal_specimen}}</b></p>
                            <p>Item : <b>{{$master_data_fabric_testing->item}}</b></p>

                            @php
                                if($master_data_fabric_testing->date_information_remark == 'buy_ready')
                                {
                                    $informasi_tanggal = 'Buy Ready';
                                } elseif ($master_data_fabric_testing->date_information_remark == 'podd') {
                                    $informasi_tanggal = 'PODD';
                                } elseif ($master_data_fabric_testing->date_information_remark == 'output_sewing') {
                                    $informasi_tanggal = '1st Output Sewing';
                                }
                            @endphp
                            <p>Informasi Tanggal : <b>{{$informasi_tanggal . ' at '. $master_data_fabric_testing->date_information}}</b></p>
                            <p>PO / MO : <b>{{$master_data_fabric_testing->orderno }}</b></p>
                            <p>Size : <b>{{$master_data_fabric_testing->size }}</b></p>
                            <p>Style : <b>{{$master_data_fabric_testing->style }}</b></p>
                        </div>
                        <div class="col-md-4 col-lg- 6col-sm-12">
                            <p>Article  : <b>{{$master_data_fabric_testing->article_no }}</b></p>
                            <p>Destination : <b>{{$master_data_fabric_testing->destination }}</b></p>

                            @php
                            if($master_data_fabric_testing->test_required = 'developtesting_t1')
                            {
                                $test_required = 'Develop Testing T1';
                            } elseif ($master_data_fabric_testing->test_required = 'developtesting_t2') {
                                $test_required = 'Develop Testing T2';
                            } elseif ($master_data_fabric_testing->test_required = 'developtesting_selective') {
                                $test_required = 'Develop Testing Selective';
                            } elseif($master_data_fabric_testing->test_required = 'bulktesting_m')
                            {
                                $test_required = 'Bulk Testing M';
                            } elseif ($master_data_fabric_testing->test_required = 'bulktesting_a') {
                                $test_required = 'Bulk Testing A';
                            } elseif ($master_data_fabric_testing->test_required = 'bulktesting_selective') {
                                $test_required = 'Bulk Testing Selective';
                            } elseif($master_data_fabric_testing->test_required = 'reordertesting_m')
                            {
                                $test_required = 'Re-Order Testing M';
                            } elseif ($master_data_fabric_testing->test_required = 'reordertesting_a') {
                                $test_required = 'Re-Order Testing A';
                            } elseif ($master_data_fabric_testing->test_required = 'reordertesting_selective') {
                                $test_required = 'Re-Order Testing Selective';
                            } else {
                                $test_required = 'Re-Test';
                            }
                        @endphp

                            <p>Test Required : <b>{{$test_required }}</b></p>
                            <p>Category : <b>{{$master_data_fabric_testing->category }}</b></p>
                            <p>Category Specimen : <b>{{$master_data_fabric_testing->category_specimen }}</b></p>
                            <p>Type Specimen : <b>{{$master_data_fabric_testing->type_specimen }}</b></p>
                        </div>
                    </div>
                </div>

               
            </div>
		</div>
	</div>

    <div class="row">
		<div class="panel panel-flat">
            <div class="panel-heading">
               
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
              
                    <form action="{{ route('dashboardTrf.sendInputDescription')}}" id="inputDescriptionForm" action="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="display-block text-semibold">Description</label>
                                <textarea name="description" id="description" class="form-control" >{{$master_data_fabric_testing->description}}</textarea>
                                <input type="hidden" name="trf_id" id="trf_id" class="form-control" value="{{$master_data_fabric_testing->id}}" >
                            </div>
                           
        
                            
                        </div>
                        <div class="row">
                            <button type="submit" class="btn border-slate text-slate-800 btn-flat pull-right" id="btn-save">Save</button>
                        </div>
                    </form>
               
               
            </div>
		</div>
	</div>

    <div class="row">
		<div class="panel panel-flat">
            <div class="panel-heading">
               
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-responsive" id="testing-table">
                        <thead>
                            <tr>
                                <th>Testing Method</th>
                                <th>Komposisi Specimen</th>
                                <th>Parameter</th>
                                <th>Perlakuan Test</th>
                                <th>Operator</th>
                                <th>Value</th>
                                <th>Testing Value</th>
                                <th>Testing Result</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <input type="hidden" value="{{$master_data_fabric_testing->id}}" id="id">
                </div>
               
            </div>
		</div>
	</div>
</div>

@endsection

@section('modal')

<div id="inputTestingModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="{{route('dashboardTrf.sendTesting')}}" id="inputTestingForm" action="post">
				@csrf
				<div class="row">
					<div class="col-lg-6">
						<label class="display-block text-semibold">Input Testing</label>
						<input type="text" name="input_testing" id="input_testing" class="form-control" required="">
                        <input type="hidden" name="req_id" id="req_id" class="form-control" >
                        <input type="hidden" name="trf_id" id="trf_id" class="form-control" >
					</div>

					
				</div>
			
			
				<br>
				<div class="row">
					<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection


@section('js')
<script type="text/javascript">

$('#testing-table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '{{ route('dashboardTrf.getRequirement')}}',
            data: {
                id:$('#id').val(),
            }
        },
        columns: [
            {data: 'method_name', name: 'method_name',searchable:false,visible:true,orderable:true},
            {data: 'komposisi_specimen', name: 'komposisi_specimen',searchable:false,visible:true,orderable:true},
            {data: 'parameter', name: 'parameter',searchable:false,visible:true,orderable:true},
            {data: 'perlakuan_test', name: 'perlakuan_test',searchable:false,visible:true,orderable:true},
            {data: 'operator_name', name: 'operator_name',searchable:false,visible:true,orderable:true},
            {data: 'value1', name: 'value1',searchable:false,visible:true,orderable:true},
            {data: 'value_testing', name: 'input_testing',searchable:false,visible:true,orderable:true},
            {data: 'testing_result', name: 'testing_result',searchable:false,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#testing-table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();







function inputTesting(url)
{
      $.ajax({
	        type: 'get',
	        url : url,
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
                var update = response;

                    $('#input_testing').val(update.value_testing);
                    $('#trf_id').val(update.testing_id);
                    $('#req_id').val(update.req_id);
            

	        	$.unblockUI();
                $('#inputTestingModal').modal('show');
	        },
	        error: function(response) {
	        	$.unblockUI();
	           var notif = response.data;
	            
	        }
	    });
   
}


$('#inputTestingForm').submit(function(event){
    
		event.preventDefault();
        console.log($('#trf_id').val());
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url : $('#inputTestingForm').attr('action'),
	        data:{
				input_testing 		: $('#input_testing').val(),
                testing_id          : $('#trf_id').val(),
                req_id              : $('#req_id').val()
				},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	        	$.unblockUI();
                $('#inputTestingModal').modal('hide');
	         	var notif = response.data;
	            alert(notif.status,notif.output);
                $('#testing-table').DataTable().ajax.reload();
	        },
	        error: function(response) {
	        	$.unblockUI();
	            var notif = response.data;
	            alert(notif.status,notif.output);
	            
	        }
	    });
		
});

$('#inputDescriptionForm').submit(function(event){
    
    event.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#inputDescriptionForm').attr('action'),
        data:{
            description 		: $('#description').val(),
            testing_id          : $('#trf_id').val(),
            },
        beforeSend : function(){
            loading();
        },
        success: function(response) {

            $.unblockUI();
             var notif = response.data;
            alert(notif.status,notif.output);
        },
        error: function(response) {
            $.unblockUI();
            var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    });
    
});

</script>
@endsection
