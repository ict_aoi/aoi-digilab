@section('modal')
<div id="modal_set" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <center><h4>UPLOAD METHODE</h4></center> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-2">
                        <input type="hidden" name="mdtrfid" id="mdtrfid">
                    </div>
                    <div class="col-lg-8">
                        <label><b>Set Methode</b></label>
                        <div class="multi-select-full">
                            <select multiple="multiple" data-placeholder="Choose Methods" class="select" id="test_methods">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-success" id="btn-savemth">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection