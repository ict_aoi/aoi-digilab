<div class="header">
    <table style="align-content: center; width:100%;">
        <tr>
            <td>
                <center><img src="{{url('assets/icon/aoi.png')}}" width="150px"></center>
            </td>
            <td><center><h1>Internal Test Report</h1></center></td>
            <td>
                @if($header->buyer=="ADIDAS")
                    <center><img src="{{url('assets/icon/adidas.png')}}" width="150px"></center>
                @elseif($header->buyer=="PUMA")
                    <center><img src="{{url('assets/icon/puma.png')}}" width="150px"></center>
                @elseif($header->buyer=="JAKO")
                    <center><img src="{{url('assets/icon/jako.png')}}" width="150px"></center>
                @endif
            </td>
        </tr>
    </table>
    <br>
    <table class="HeadTrf">
        <tr>
            <td><label class="labelHeadTrf">TRF No. : </label>
                <br> {{$header->trf_id}}
            </td>
            <td><label class="labelHeadTrf">Buyer : </label>
                <br> {{$header->buyer}}
            </td>
            <td><label class="labelHeadTrf">Dept Origin : </label>
                <br> {{$header->asal_specimen}}
            </td>
            <td><label class="labelHeadTrf">Factory : </label>
                <br> {{$header->factory_name}}
            </td>
            <td><label class="labelHeadTrf">Date In : </label>
                <br> {{date_format(date_create($header->verified_lab_date),'d-m-Y')}}
            </td>
            
        </tr>
        <tr>
            <td><label class="labelHeadTrf">Type Specimen : </label>
                <br> {{$header->type_specimen}}
            </td>
            <td><label class="labelHeadTrf">Category Specimen : </label>
                <br> {{$header->category_specimen}}
            </td>
            <td><label class="labelHeadTrf">Category : </label>
                <br> {{$header->category}}
            </td>
            <td><label class="labelHeadTrf">Lab Location : </label>
                <br> {{$header->lab_location}}
            </td >
            <td><label class="labelHeadTrf">Date Out : </label>
                <br> {{date_format(date_create($header->closed_at),'d-m-Y')}}
            </td>
        </tr>
    </table>

    <div>
        <center><h1>Specimen Identity</h1></center>
    </div>
    
    @if($header->category_specimen=="FABRIC")
        @include('report.export._fabric')
    @elseif($header->category_specimen=="GARMENT")
        @include('report.export._garment')
    @elseif($header->category_specimen=="STRIKE OFF" && $header->type_specimen=="HEAT TRANSFER")
        @include('report.export._heat')
    @elseif($header->category_specimen=="STRIKE OFF" && $header->type_specimen=="PAD PRINT")
        @include('report.export._pad')
    @elseif($header->category_specimen=="STRIKE OFF" && $header->type_specimen=="PRINTING")
        @include('report.export._printing')
    @elseif($header->category_specimen=="STRIKE OFF" && $header->type_specimen=="EMBROIDERY")
        @include('report.export._embro')
    @elseif($header->category_specimen=="STRIKE OFF" && $header->type_specimen=="BADGE")
        @include('report.export._badge')
    @elseif($header->category_specimen=="ACCESORIES/TRIM")
        @include('report.export._accesories')
    @elseif($header->category_specimen=="PANEL" || $header->category_specimen=="MOCKUP")
        @include('report.export._panelmock')
    @endif
    <br>
</div>

