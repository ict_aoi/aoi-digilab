<table class="specimenId">
    <tr>
        <td>
            Style : {{$header->style}}
        </td>

        <td>
            PO Number : {{$header->document_no}}
        </td>

        <td>
            Size : {{$header->size}}
        </td>
    </tr>

    <tr>
        <td>
            Article : {{$header->article_no}}
        </td>

        <td>
            Temperature : {{$header->temperature}}
        </td>

        <td>
            Color : {{$header->color}}
        </td>
    </tr>

    <tr>
        <td>
            Season : {{$header->season}}
        </td>

        <td>
            Fabric Properties : {{$header->fabric_properties}}
        </td>

        <td>
            Machine Model : {{$header->machine}}
        </td>
    </tr>

    <tr>
        <td>
            Style Name : {{$header->style_name}}
        </td>

        <td>
            Care Instruction : {{ $header->care_instruction }}
        </td>

        <td>
            Fibre Composition : {{$header->fibre_composition}}
        </td>
    </tr>
</table>