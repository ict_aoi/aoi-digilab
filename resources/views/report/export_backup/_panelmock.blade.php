<table class="specimenId">
    <tr>
        <td>
            Style : {{$header->style}}
        </td>

        <td>
            Peeling : {{$header->pelling}}
        </td>

        <td>
            Temperature : {{$header->temperature}}
        </td>
    </tr>

    <tr>
        <td>
            Article : {{$header->article_no}}
        </td>

        <td>
            Machine : {{$header->machine}}
        </td>

        <td>
            Pressure : {{$header->pressure}}
        </td>
    </tr>

    <tr>
        <td>
            Season : {{$header->season}}
        </td>

        <td>
            Pad : {{$header->pad}}
        </td>

        <td>
            Duration : {{$header->duration}}
        </td>
    </tr>

    <tr>
        <td colspan="3">
            Style Name : {{$header->style_name}}
        </td>
    </tr>

    <tr>
        <td colspan="3">
            Remark : {{$header->remark}}
        </td>
    </tr>
</table>