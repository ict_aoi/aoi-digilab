<table class="specimenId">
	<tr>
		<td>Style : {{$header->style}} </td>

		<td>Supplier Embro : {{$header->manufacture_name}} </td>

		<td>Embro Size : {{$header->size}} </td>
	</tr>

	<tr>
		<td>Article : {{$header->article_no}} </td>

		<td>Thread : {{$header->thread}} </td>

		<td>Item Fabric : {{$header->fabric_item}} </td>
	</tr>

	<tr>
		<td>Style Name : {{$header->style_name}} </td>

		<td>Embro Item : {{$header->item}} </td>

		<td>Fabric Type : {{$header->fabric_type}} </td>
	</tr>

	<tr>
		<td>Season : {{$header->season}} </td>

		<td>Embro Color : {{$header->color}} </td>

        <td>Embro Desc : {{$header->description}} </td>
		
	</tr>

	<tr>
		<td>Fabric Color : {{$header->fabric_color}} </td>

		<td colspan="2">Fabric Composition : {{$header->fibre_composition}} </td>

	</tr>

</table>