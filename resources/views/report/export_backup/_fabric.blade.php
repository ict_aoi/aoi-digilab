<table class="specimenId">
    <tr>
        <td>
            Style : {{$header->style}}
        </td>

        <td>
            Item : {{$header->item}}
        </td>

        <td>
            PO Supplier : {{$header->document_no}}
        </td>

        <td>
            Color : {{$header->color}}
        </td>
    </tr>

    <tr>
        <td>
            Season : {{$header->season}}
        </td>

        <td>
            Nomor Invoice : {{$header->invoice}}
        </td>

        <td colspan="2">
            Supplier : {{$header->manufacture_name}}
        </td>
    </tr>

    <tr>
        <td>
            No. Roll : {{$header->nomor_roll}}
        </td>

        <td>
            Lot : (belum ada)
        </td>

        <td colspan="2">
            Fibre Composition : {{$header->fibre_composition}}
        </td>
    </tr>

    <tr>
        <td>
            Batch Number : {{$header->batch_number}}
        </td>

        <td>
            Bercode WMS : {{$header->barcode_supplier}}
        </td>

        <td colspan="2">
            Date Receive : {{ date_format(date_create($header->verified_lab_date),'d-M-Y')}}
        </td>
    </tr>
    
    <tr>
        <td>
            Yards : {{$header->yds_roll}}
        </td>
        <td>
            PODD : {{ date_format(date_create($header->date_information),'d-M-Y')}}
        </td>
        <td colspan="2">
            Date Finished : {{ date_format(date_create($header->closed_at),'d-M-Y')}}
        </td>
    </tr>
</table>