
<table class="specimenId">
	<tr>
		<td>Style  : {{$header->style}}  </td>

		<td>Style Name  : {{$header->style_name}}  </td>

		<td>Badge Description  : {{$header->description}}  </td>
	</tr>

	<tr>
		<td>Article  : {{$header->article_no}}  </td>

		<td>Item Badge  : {{$header->item}}  </td>

		<td>Fabric Color  : {{$header->fabric_color}}  </td>
	</tr>

	<tr>
		<td>Season  : {{$header->season}}  </td>

		<td>Color Badge  : {{$header->color}}  </td>

		<td>Fabric Material  : {{$header->fibre_composition}}  </td>
	</tr>
	
</table>