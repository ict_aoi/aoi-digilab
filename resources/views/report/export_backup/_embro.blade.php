
<table class="specimenId">
	<tr>
		<td>Style  : {{$header->style}}  </td>

		<td>Embro Desc  : {{$header->description}}  </td>

		<td>Item Fabric  : {{$header->fabric_item}}  </td>
	</tr>

	<tr>
		<td>Article  : {{$header->article_no}}  </td>

		<td>Embro Size  : {{$header->size}}  </td>

		<td>Fabric Type  : {{$header->fabric_type}}  </td>
	</tr>

	<tr>
		<td>Season  : {{$header->season}}  </td>

		<td>Embro item  : {{$header->item}}  </td>

		<td>Fabric Color  : {{$header->fabric_color}}  </td>
	</tr>

	<tr>
		<td>Style Name  : {{$header->style_name}}  </td>

		<td>Garment Size  : {{$header->garment_size}}  </td>

		<td>Supplier Print  : {{$header->manufacture_name}}  </td>

	</tr>

	<tr>

        <td>Embro Color  : {{$header->color}}  </td>

		<td colspan="2">Fabric Composition  : {{$header->fibre_composition}}  </td>

	</tr>

</table>