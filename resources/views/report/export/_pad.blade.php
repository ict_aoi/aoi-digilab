<table class="specimenId">
	<tr>
        <td style="width:33%;">{{$header->document_type}} : {{$header->document_no}}</td>
        <td style="width:33%;">Style : {{$header->style}}</td>
        <td style="width:33%;">Article : {{$header->article_no}}</td>
    </tr>

	<tr>
        <td style="width:33%;">Style Name : {{$header->style_name}}</td>
        <td style="width:33%;">Season : {{$header->season}}</td>
		<td style="width:33%;">Supplier Pad Print : {{$header->manufacture_name}}</td>
    </tr>

	<tr>
        <td style="width:33%;">Thread : {{$header->thread}}</td>
        <td style="width:33%;">Pad Print Item : {{$header->item}}</td>
		<td style="width:33%;">Pad Print Color : {{$header->interlining_color}}</td>
    </tr>

	<tr>
        <td style="width:33%;">Pad Print Desc : {{$header->description}}</td>
		<td style="width:33%;">Size Page : {{$header->garment_size}}</td>
		<td style="width:33%;">Fabric Color : {{$header->fabric_color}}</td>
    </tr>

	<tr>
		<td style="width:33%;">Fabric Item : {{$header->fabric_item}}</td>
		<td style="width:33%;">Fabric Type : {{$header->fabric_type}}</td>
		<td style="width:33%;">Fabric Composition : {{$header->fibre_composition}}</td>
    </tr>
</table>