<table class="specimenId">
    <tr>
        <td colspan="2">Garment Maker : PT. APPAREL ONE INDONESIA</td>
        <td style="width: 33%;"> {{$header->document_type}} : {{$header->document_no}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Season : {{$header->season}}</td>
        <td style="width: 33%;">Style : {{$header->style}}</td>
        <td style="width: 33%;">Article : {{$header->article_no}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Color : {{$header->color}}</td>
        <td style="width: 33%;">Size : {{$header->size}}</td>
        <td style="width: 33%;">Qty : {{$header->qty}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Country : {{isset($header->export_to) ? $header->export_to : ''}}</td>
        <td style="width: 33%;">Supplier : {{$header->manufacture_name}}</td>
        <td style="width: 33%;">Type Of Specimen : {{$header->sample_type}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Test Condition : {{$header->test_condition}}</td>
        <td style="width: 33%;">Temperature : {{$header->temperature}}</td>
        <td style="width: 33%;">Machine Model : {{$header->machine}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Material Shell & Panel : {{$header->material_shell_panel}}</td>
        <td colspan="2">Fabric Properties : {{$header->fabric_properties}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Care Instruction : {{$header->care_instruction}}</td>
        <td colspan="2"> Fibre Composition : {{$header->fibre_composition}}</td>
    </tr>
    <tr>
        <td colspan="3"> Remark : {{$header->remark}}</td>
    </tr>
</table>