<table class="specimenId">
    <tr>
        <td style="width: 33%;">PO Supplier : {{$header->document_no}}</td>
        <td style="width: 33%;">Supplier Name : {{$header->manufacture_name}}</td>
        <td style="width: 33%;">Arrival Date : {{ isset($header->arrival_date) ? date_format(date_create($header->arrival_date),'d-M-Y') : ''}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Item : {{$header->item}}</td>
        <td style="width: 33%;">Color : {{$header->color}}</td>
        <td style="width: 33%;">
            @if($header->date_information_remark=="podd")
                PODD : {{ isset($header->date_information) ? date_format(date_create($header->date_information),'d-M-Y') : ''}}
            @elseif($header->date_information_remark=="buy_ready")
                Buy Ready : {{ isset($header->date_information) ? date_format(date_create($header->date_information),'d-M-Y') : ''}}
            @elseif($header->date_information_remark=="output_sewing")
                Output Sewing : {{ isset($header->date_information) ? date_format(date_create($header->date_information),'d-M-Y') : ''}}
            @endif
        </td>
    </tr>
    <tr>
        <td style="width: 33%;">Season : {{$header->season}}</td>
        <td style="width: 33%;">Nomor Roll : {{$header->nomor_roll}}</td>
        <td style="width: 33%;">Receive Date : {{isset($header->verified_lab_date) ? date_format(date_create($header->verified_lab_date),'d-M-Y') : ''}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Batch No. : {{$header->batch_number}}</td>
        <td style="width: 33%;">Yards : {{$header->yds_roll}}</td>
        <td style="width: 33%;">Finised Date : {{isset($header->closed_at) ? date_format(date_create($header->closed_at),'d-M-Y') : ''}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Style: {{$header->style}}</td>
        <td style="width: 33%;">Invoice No. : {{$header->invoice}}</td>
        <td style="width: 33%;">Barcode Wms : {{$header->barcode_supplier}}</td>
    </tr>
    <tr>
        <td colspan="3">Fabric Composition : {{$header->fibre_composition}}</td>
    </tr>
</table>