<script type="text/javascript">
	window.onload = function(){
		window.print();
	}
</script>

@include('report.export._css')

@include('report.export._header',['header'=>$header])

@php($fresult = "PASS")

{{-- @php($arr = ['PHX-AP0701','PHX-AP0450','PHM-AP0502','PHX-AP0519','PHM-AP0413','PHM-AP0419','PHX-AP0437','PHX-AP0438','PHM-AP0405']) --}}

@php($arr = [
	'PHX-AP0701',
	'PHX-AP0450',
	'PHM-AP0502',
	'PHX-AP0519',
	'PHM-AP0413',
	'PHM-AP0419',
	'PHX-AP0437',
	'PHX-AP0438',
	'PHM-AP0405', 
	'PHM-AP0503',
	'PHM-AP0501',
	'PHX-AP0519',
	'ISO 105-C06',
	'ISO 105-E01',
	'PHM-AP0502'
	])

<div class="content-list">
	@foreach($list as $x)

	<table style="width: 100%; border-collapse: collapse; margin-bottom:10px;">
	<tr>
		<td style="border: 1px solid black;"><center><h2>{{$x['header']}}</h2></center></td>
	</tr>
	</table>
			@foreach($x['content'] as $ls)
			@if(in_array($ls['method_code'],$arr) && isset($ls['rowmeth']))
				
				<table class="result">
					<thead>
						<th style="width: 150px;">Method</th>
						<th>Standart</th>
						<th>Treatment</th>
						<th style="width: 100px;">Parameter</th>
						<th>Measuring Position</th>
						<th>Result Cycle</th>
						<th>Result</th>
						<th>Pass/ Fail</th>
						<th>Remark</th>
						<th>Supplier Result</th>
					</thead>
					<tbody>
						@php($trfmeth = null)
						@php($stdr = null)
						@php($param = null)
						@php($cycle = null)
						@php($treat = null)
						@php($stdT	= null)
						@for($i=$ls['rowmeth']-1; $i>=0;$i--)
							
							<tr>
								@if($trfmeth!=$ls['list'][$i]->trf_meth_id)
									<td rowspan="{{$ls['rowmeth']}}" style="width: 100px; word-spacing:2px;"><b>{{$ls['method_code']}} </b><br> {{$ls['method_name']}}</td>
									@php($trfmeth=$ls['list'][$i]->trf_meth_id)
								@endif
								
								@if($stdr!=$ls['list'][$i]->standart)
									<td rowspan="{{$ls['list'][$i]->rowstd}}">{{$ls['list'][$i]->standart}}</td>
									@php($stdr=$ls['list'][$i]->standart)
								@endif

								@if($stdT!=$ls['list'][$i]->standart || $treat!=$ls['list'][$i]->perlakuan_test)
									<td rowspan="{{$ls['list'][$i]->rowtreat}}">{{$ls['list'][$i]->perlakuan_test}}</td>
									@php($stdT=$ls['list'][$i]->standart)
									@php($treat=$ls['list'][$i]->perlakuan_test)
								@endif

								@if($param!=$ls['list'][$i]->parameter)
									<td rowspan="{{$ls['list'][$i]->rowparam}}">{{$ls['list'][$i]->parameter}}</td>
									@php($param=$ls['list'][$i]->parameter)
								@endif
								<td>{{ucwords($ls['list'][$i]->measuring_position)}}  @if($ls['list'][$i]->code!="") <b>( {{$ls['list'][$i]->code}} )</b> @endif
								</td>
								
								@if($cycle!=$ls['list'][$i]->remark || $ls['list'][$i]->remark=="")
									<td rowspan="{{$ls['list'][$i]->rowcycle}}">{{$ls['list'][$i]->remark}}</td>
									@php($cycle=$ls['list'][$i]->remark)
								@endif

								<td>{{$ls['list'][$i]->result}} {{$ls['list'][$i]->uom}}</td>
								<td>{{$ls['list'][$i]->result_status}}</td>
								<td>{{$ls['list'][$i]->comment}}</td>
								<td>{{$ls['list'][$i]->supplier_result}}</td>
							</tr>

							@if($ls['list'][$i]->result_status=="PASS" && $fresult=="PASS")
								@php($fresult="PASS")
							@elseif($ls['list'][$i]->result_status=="FAILED")
								@php($fresult="FAILED")
							@else
								@php($fresult="FAILED")
							@endif
						@endfor
					</tbody>
				</table>
			@elseif(!in_array($ls['method_code'],$arr) && isset($ls['rowmeth']))

				<table class="result">
					<thead>
						<th style="width: 175px;">Method</th>
						<th>Standart</th>
						<th>Treatment</th>
						<th style="width: 100px">Parameter</th>
						<th>Result</th>
						<th>Pass/ Fail</th>
						<th>Remark</th>
						<th>Supplier Result</th>
					</thead>
					<tbody>
						@php($trfmeth = null)
						@php($stdr = null)
						@php($param = null)
						@php($treat = null)
						@php($stdT = null)
						@for($i=$ls['rowmeth']-1; $i>=0;$i--)
							
							<tr>
								@if($trfmeth!=$ls['list'][$i]->trf_meth_id)
									<td rowspan="{{$ls['rowmeth']}}" style="width: 100px; word-spacing:2px;"><b>{{$ls['method_code']}} </b><br> {{$ls['method_name']}}</td>
									@php($trfmeth=$ls['list'][$i]->trf_meth_id)
								@endif
								
								@if($stdr!=$ls['list'][$i]->standart)
									<td rowspan="{{$ls['list'][$i]->rowstd}}">{{$ls['list'][$i]->standart}}</td>
									@php($stdr=$ls['list'][$i]->standart)
								@endif

								@if(($treat!=$ls['list'][$i]->perlakuan_test) || $stdT!=$ls['list'][$i]->standart)
									<td rowspan="{{$ls['list'][$i]->rowtreat}}">{{$ls['list'][$i]->perlakuan_test}}</td>
									@php($treat=$ls['list'][$i]->perlakuan_test)
									@php($stdT=$ls['list'][$i]->standart)
								@endif

								@if($param!=$ls['list'][$i]->parameter)
									<td rowspan="{{$ls['list'][$i]->rowparam}}">{{$ls['list'][$i]->parameter}}</td>
									@php($param=$ls['list'][$i]->parameter)
								@endif
								<td>{{$ls['list'][$i]->result}} {{$ls['list'][$i]->uom}}</td>
								<td>{{$ls['list'][$i]->result_status}}</td>
								<td>{{$ls['list'][$i]->comment}}</td>
								<td>{{$ls['list'][$i]->supplier_result}}</td>
							</tr>

							@if($ls['list'][$i]->result_status=="PASS" && $fresult=="PASS")
								@php($fresult="PASS")
							@elseif($ls['list'][$i]->result_status=="FAILED")
								@php($fresult="FAILED")
							@else
								@php($fresult="FAILED")
							@endif

						@endfor
					</tbody>
				</table>
			@endif
			@endforeach
	@endforeach
	@include('report.export._sign',['fresult'=>$fresult,'sign'=>$header])
</div>

{{-- @include('report.export._footer') --}}


<div class="clearfix"></div>
@if(count($images)>0)
<table class="result">
	@foreach(array_chunk($images,2) as $im)
		<tr>
			<td>
				@if(isset($im[0]))
					<center>
						@php($img0='imagespc/'.$im[0]->id.'.png')
						<img src="{{ url($img0) }}" width="350px" style="margin:5px;">
					</center>
				@endif
			</td>
			<td>
				@if(isset($im[1]))
					<center>
						@php($img1='imagespc/'.$im[1]->id.'.png')
						<img src="{{ url($img1) }}" width="350px" style="margin:5px;">
					</center>
				@endif
			</td>
		</tr>

		<tr>
			<td>
				@if(isset($im[0]))
					{{$im[0]->remark}}
				@endif
			</td>

			<td>
				@if(isset($im[1]))
					{{$im[1]->remark}}
				@endif
			</td>
		</tr>
	@endforeach
</table>
@endif