
<script type="text/javascript">
	window.onload = function(){
		window.print();
	}
</script>

<style>
@page{
    margin: 10 10 10 10;
}
body{
    font-family: Arial;
    display: flex;
    flex-direction: column;
}
.header{
    width: 100%;
    padding-bottom: 10px;
}

.contents{
    position: relative;
    width: 100%;
    margin-bottom: 70px;
    
}

.footer{
    clear: both;
    position: fixed;
    bottom: 0;
    width: 100%;
    
}
.tablefooter{
    width: 100%;
}

.HeadTrf{
    width: 100%;
    border-collapse: collapse;
    align-content: center;
}

.HeadTrf tr td{
    border: 1px solid black;
    padding: 5px;
    vertical-align: top;
    font-size: 16px;
    width: 25%;
}

.labelHeadTrf{
    font-size: 16px;
    font-weight: bold;
    padding-bottom: 5px;
}

.specimenId{
    width: 100%;
    border-collapse: collapse;
    align-content: center;
}

.specimenId tr td{
    border: 1px solid black;
    padding: 5px;
    vertical-align: top;
    font-size: 16px;
    white-space: normal;
}

.result{
    width: 100%;
    border-collapse: collapse;
    align-content: center;
    margin-bottom: 10px;
    
}

.result thead tr th{
    border: 1px solid black;
    font-size: 16px;
    font-weight: bold;
    align-content: center;
    text-align: center;
    padding: 10px;
    white-space: normal; 

}

.result tbody tr td{
    border: 1px solid black;
    font-size: 14px;
    padding: 10px;
    /* word-break: break-word; */
    white-space: pre-wrap; 
    align-content: center;
    text-align: center;

}

.signature{
    width: 80%;
    border-collapse: collapse;
    text-align: center;
    align: center;
    page-break-inside: avoid;
}

.signature tr td{
    border: 1px solid black;
}
</style>

<div class="header">
    <table style="align-content: center; width:100%;">
        <tr>
            <td>
                <center><img src="{{url('assets/icon/aoi.png')}}" width="150px"></center>
            </td>
            <td><center><h1>Internal Test Report</h1></center></td>
            <td>
                <center><img src="{{url('assets/icon/adidas.png')}}" width="150px"></center>
            </td>
        </tr>
    </table>
    <br>
    <table class="HeadTrf">
        <tr>
            <td><label class="labelHeadTrf">TRF No. : </label>
                <br> {{$header->trf_id}}
            </td>
            <td><label class="labelHeadTrf">Buyer : </label>
                <br> {{$header->buyer}}
            </td>
            <td><label class="labelHeadTrf">Dept Origin : </label>
                <br> {{$header->asal_specimen}}
            </td>
            <td><label class="labelHeadTrf">Type Specimen : </label>
                <br> {{$header->type_specimen}}
            </td>
        </tr>
        <tr>
            <td><label class="labelHeadTrf">Category Specimen : </label>
                <br> {{$header->category_specimen}}
            </td>
            <td><label class="labelHeadTrf">Category : </label>
                <br> {{$header->category}}
            </td>
            <td><label class="labelHeadTrf">Factory : </label>
                <br> {{$header->factory_name}}
            </td>
            <td><label class="labelHeadTrf">Lab Location : </label>
                <br> {{$header->lab_location}}
            </td>
        </tr>
    </table>
</div>

<div class="contents">
    
    <div>
        <center><h1>Specimen Identity</h1></center>
    </div>
    <table class="specimenId">
        <tr>
            <td>
                Style : {{$header->style}}
            </td>

            <td>
                Peeling : {{$header->pelling}}
            </td>

            <td>
                Temperature : {{$header->temperature}}
            </td>
        </tr>

        <tr>
            <td>
                Article : {{$header->article_no}}
            </td>

            <td>
                Machine : {{$header->machine}}
            </td>

            <td>
                Pressure : {{$header->pressure}}
            </td>
        </tr>

        <tr>
            <td>
                Season : {{$header->season}}
            </td>

            <td>
                Pad : {{$header->pad}}
            </td>

            <td>
                Duration : {{$header->duration}}
            </td>
        </tr>

        <tr>
            <td colspan="3">
                Style Name : {{$header->style_name}}
            </td>
        </tr>

        <tr>
            <td colspan="3">
                Remark : {{$header->remark}}
            </td>
        </tr>
    </table>
    <br>

    @foreach ($list as $ls)
        <table class="result">
            <thead>
                <tr>
                    <th colspan="7"><center><h2>{{$header->category_specimen}}  {{$ls['category']}}</h2></center></th>
                </tr>
                <tr>
                    <th style="min-width: 120px;">
                        Method Code <br>
                        (Method Name)
                    </th>
                    <th style="width: 120px;">
                        Standard
                    </th>
                    <th style="width: 120px;">
                        Parameter
                    </th>
                    <th>
                        Supplier Result
                    </th>
                    <th style="width: 120px;">
                        Result Value
                    </th>
                    <th style="width: 60px;">
                        Pass/ <br> Fail
                    </th>
                    <th style="width: 65px;">
                        Remark/ <br> Comment
                    </th>
                </tr>
            </thead>
            
            <tbody>
                @php($cmeth=null)
                @php($cstd=null)
                @php($cparam=null)
                @for($x=count($ls['list']); $x>=1; $x--)
                    
                    @php($y=$x-1)
        
                    
                    <tr >
                        @if($cmeth==null || $cmeth!=$ls['list'][$y]->method_code)
                            <td rowspan="{{$ls['list'][$y]->rowmeth}}">{{$ls['list'][$y]->method_code}} <br> {{$ls['list'][$y]->method_name}}</td>
                            @php($cmeth=$ls['list'][$y]->method_code)
                        @endif
        
                        @if($cstd==null || $cstd!=$ls['list'][$y]->standart)
                        <td rowspan="{{$ls['list'][$y]->rowstd}}">{{$ls['list'][$y]->standart}} {{$ls['list'][$y]->uom}}</td>
                            @php($cstd=$ls['list'][$y]->standart)
                        @endif
        
                        @if($cparam==null || $cparam!=$ls['list'][$y]->parameter)
                        <td rowspan="{{$ls['list'][$y]->rowparam}}">{{$ls['list'][$y]->parameter}}</td>
                            @php($cparam=$ls['list'][$y]->parameter)
                        @endif
        
                        <td>{{$ls['list'][$y]->supplier_result}}</td>
                        <td>{{$ls['list'][$y]->result}}</td>
                        <td>{{$ls['list'][$y]->result_status}}</td>
                        <td>{{$ls['list'][$y]->comment}}</td>
                    </tr>
                @endfor
            </tbody>
        </table>
        
    @endforeach

    <table class="signature" align="center">
        <tr>
            <td colspan="2">
                <label >RESULT</label>
            </td>
            <td>
                <label >Signature</label>
            </td>
            <td>
                <label >Signature</label>
            </td>
        </tr>
        <tr>
            <td>
                <label >PASS</label>
            </td>
            <td>
                <label >FAIL</label>
            </td>
            <td>
                <label >Conducted Test by : <br>{{$sign->tech_name}}
                </label>
            </td>
            <td>
                Approve by : <br>{{$sign->head_name}}
            </td>
        </tr>
        <tr>
            <td style="height: 75px">
                @if($sign->final_result=="PASS")
                <div style="rotate: -45deg;">
                    <label style="font-size: 16px; color:red; font-wight:bold;">PASS</label>
                </div>
                @endif
            </td>
            <td>
                @if($sign->final_result=="FAILED")
                <div style="rotate: -45deg;">
                    <label style="font-size: 16px; color:red; font-wight:bold;">FAILED</label>
                </div>
                @endif
            </td>
            <td>
                @php($pathTech = 'signature/'.$sign->tech_nik.'.png')
    
                @if(file_exists(public_path($pathTech)))
                    <center><img src="{{ url($pathTech) }}" style="height:50px; width: auto;"></center>
                @endif
            </td>
            <td>
                @php($pathHead = 'signature/'.$sign->head_nik.'.png')
    
                @if(file_exists(public_path($pathHead)))
                    <center><img src="{{ url($pathHead) }}" style="height:50px; width: auto;"></center>
                @endif
            </td>
        </tr>
    </table>
</div>

<div class="footer">
    <table class="tablefooter">
        <tr>
            <td><label style="font-size: 12px;">Document Control : FR.09-01-QA.QC/00</label></td>
        </tr>
        <tr>
            <td><center><label style="font-size: 14px;font-weight: bold;">LAB TESTING PT. APPAREL ONE INDONESIA</label></center></td>
        </tr>
        <tr>
            <td><center><label style="font-size: 14px;">PT. Putra Wijaya Kusuma Sakti, Blok B5 KIW Jl. Raya Semarang Kendal KM. 12 Semarang</label></center></td>
        </tr>
        <tr>
            <td><center><label style="font-size: 14px;">Tlp. 024 - 8664484, Fax. 024 - 8664483</label></center></td>
        </tr>
    </table>
</div>