<table class="specimenId">
	<tr>
		<td style="width:33%;">{{$header->document_type}} :  {{$header->document_no}}</td>
		<td style="width:33%;">Season : {{$header->season}}</td>
		<td style="width:33%;">Style : {{$header->style}}</td>
	</tr>

	<tr>
		<td style="width:33%;">Article : {{$header->article_no}}</td>
		<td style="width:33%;">Style Name :  {{$header->style_name}}</td>
		<td style="width:33%;">Badge Color : {{$header->interlining_color}}</td>
		
	</tr>

	<tr>
		<td style="width:33%;">Badge Description : {{$header->description}}</td>
		<td style="width:33%;">Badge Item : {{$header->item}}</td>
		<td style="width:33%;">Fabric Color : {{$header->fabric_color}}</td>
	</tr>

	@if($header->category_specimen=="PANEL")
		<tr>
			<td colspan="3">Fabric Material : {{$header->fabric_properties}}</td>
		</tr>
	@else
		<tr>
			<td style="width:33%;">Badge Size : {{$header->size}}</td>
			<td style="width:33%;">Supplier Badge : {{$header->manufacture_name}}</td>
			<td style="width:33%;">Garment Size : {{$header->garment_size}}</td>
		</tr>
		<tr>
			<td style="width:33%;">Fabric Type : {{$header->fabric_type}}</td>
			<td style="width:33%;">Fabric Color : {{$header->fabric_color}}</td>
			<td style="width:33%;">Item Fabric : {{$header->fabric_item}}</td>
		</tr>
		<tr>
			<td colspan="3">Fabric Composition : {{$header->fibre_composition}}</td>
		</tr>
	@endif
</table>