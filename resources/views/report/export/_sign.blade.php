<table class="signature" align="center">
    <tr>
        <td colspan="4" style="text-align: right; padding-right: 10px; padding-bottom: 10px; padding-top: 10px;">
            Semarang, {{ isset($sign->release_date) ? date_format(date_create($sign->release_date),'d-m-Y') : ''}}
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label >RESULT</label>
        </td>
        <td>
            <label >Signature</label>
        </td>
        <td>
            <label >Signature</label>
        </td>
    </tr>
    <tr>
        <td>
            <label >PASS</label>
        </td>
        <td>
            <label >FAIL</label>
        </td>
        <td>
            <label >Conducted Test by : <br>{{$sign->name_tech}}
            </label>
        </td>
        <td>
            Approve by : <br>{{$sign->name_head}}
        </td>
    </tr>
    <tr>
        <td style="height: 75px">
            @if($fresult=="PASS")
            <div style="rotate: -45deg;">
                <label style="font-size: 16px; color:Black; font-wight:bold;">PASS</label>
            </div>
            @endif
        </td>
        <td>
            @if($fresult=="FAILED")
            <div style="rotate: -45deg;">
                <label style="font-size: 16px; color:red; font-wight:bold;">FAILED</label>
            </div>
            @endif
        </td>
        <td>
           
            @php($pathTech = 'signature/'.$sign->nik_tech.'.png')

            @if(file_exists(public_path($pathTech)))
                <center><img src="{{ url($pathTech) }}" style="height:50px; width: auto;"></center>
            @endif
        </td>
        <td>
            @php($pathHead = 'signature/'.$sign->nik_head.'.png')

            @if(file_exists(public_path($pathHead)))
                <center><img src="{{ url($pathHead) }}" style="height:50px; width: auto;"></center>
            @endif
        </td>
    </tr>
</table>
