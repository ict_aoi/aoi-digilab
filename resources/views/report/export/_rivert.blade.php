
<table class="specimenId">
	<tr>
		<td style="width:33%;">Style  : {{$header->style}}  </td>

		<td style="width:33%;">Rivert Desc  : {{$header->description}}  </td>

		<td style="width:33%;">Item Fabric  : {{$header->fabric_item}}  </td>
	</tr>

	<tr>
		<td style="width:33%;">Article  : {{$header->article_no}}  </td>

		<td style="width:33%;">Rivert Size  : {{$header->size}}  </td>

		<td style="width:33%;">Fabric Type  : {{$header->fabric_type}}  </td>
	</tr>

	<tr>
		<td style="width:33%;">Season  : {{$header->season}}  </td>

		<td style="width:33%;">Rivert item  : {{$header->item}}  </td>

		<td style="width:33%;">Fabric Color  : {{$header->fabric_color}}  </td>
	</tr>

	<tr>
		<td style="width:33%;">Style Name  : {{$header->style_name}}  </td>

		<td style="width:33%;">Garment Size  : {{$header->garment_size}}  </td>

		<td style="width:33%;">Supplier Print  : {{$header->manufacture_name}}  </td>

	</tr>

	<tr>

        <td style="width:33%;">Rivert Color  : {{$header->color}}  </td>

		<td colspan="2">Fabric Composition  : {{$header->fibre_composition}}  </td>

	</tr>

</table>