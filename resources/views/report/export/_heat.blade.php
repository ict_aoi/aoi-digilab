<table class="specimenId">
	<tr>
        <td style="width:33%;">{{$header->document_type}} : {{$header->document_no}}</td>
        <td style="width:33%;">Style : {{$header->style}}</td>
        <td style="width:33%;">Article : {{$header->article_no}}</td>
    </tr>

    <tr>
        <td style="width:33%;">Season : {{$header->season}}</td>
        <td style="width:33%;">Style Name : {{$header->style_name}}</td>
        <td style="width:33%;">Heat Transfer Item : {{$header->item}}</td>
    </tr>

	<tr>
        
        <td style="width:33%;">Heat Transfer Color : {{$header->color}}</td>
		<td colspan="2">Heat Transfer Supplier : {{$header->manufacture_name}}</td>
    </tr>

	<tr>
        <td style="width:33%;">Pelling : {{$header->pelling}}</td>
        <td style="width:33%;">Pad : {{$header->pad}}</td>
        <td style="width:33%;">Temperature : {{$header->temperature}}</td>
    </tr>

	<tr>
		<td style="width:33%;">Machine : {{$header->machine}}</td>
        <td style="width:33%;">Pressure : {{$header->pressure}}</td>
        <td style="width:33%;">Duration : {{$header->duration}}</td>
        
    </tr>

	<tr>
		<td style="width:33%;">Item Fabric : {{$header->fabric_item}}</td>
        <td style="width:33%;">Fabric Type : {{$header->fabric_type}}</td>
        <td style="width:33%;">Fabric Color : {{$header->fabric_color}}</td>
    </tr>
	<tr>
		@if($header->category_specimen=="PANEL")
			<td colspan="3">Remark : {{$header->remark}}</td>
		@else 
			<td style="width:33%;">Component : {{$header->component}}</td>
			<td colspan="2">Remark : {{$header->remark}}</td>
		@endif
	</tr>
</table>