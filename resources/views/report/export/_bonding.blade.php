<table class="specimenId">
    <tr>
        <td>{{$header->document_type}} :  {{$header->document_no}}</td>
        <td>Style : {{$header->style}}</td>
        <td>Season : {{$header->season}}</td>
    </tr>

    <tr>
        <td>Article : {{$header->article_no}}</td>
        <td>Style Name : {{$header->style_name}}</td>
        <td>Component : {{$header->component}}</td>
    </tr>

    <tr>
        <td>Bonding Item : {{$header->item}}</td>
        <td>Bonding Color : {{$header->color}}</td>
        <td>Bonding Supplier : {{$header->manufacture_name}}</td>
    </tr>

    <tr>
        <td>Temperature: {{$header->temperature}}</td>
        <td>Pressure : {{$header->pressure}}</td>
        <td>Duration : {{$header->duration}}</td>
    </tr>

    <tr>
        <td>Machine: {{$header->machine}}</td>
        <td>Remark : {{$header->remark}}</td>
        <td>Fabric Item : {{$header->fabric_item}}</td>
    </tr>

    <tr>
        <td>Fabric Type: {{$header->fabric_type}}</td>
        <td>Fabric Color : {{$header->fabric_color}}</td>
        <td>Fabric Supplier : {{$header->fabric_properties}}</td>
    </tr>
</table>