<table class="specimenId">
	<tr>
		<td style="width:33%;">{{$header->document_type}} : {{$header->document_no}}</td>
		<td style="width:33%;">Style : {{$header->style}}</td>
		<td style="width:33%;">Article : {{$header->article_no}}</td>
	</tr>

	<tr>
		<td style="width:33%;">Season : {{$header->season}}</td>
		<td style="width:33%;">Style Name : {{$header->style_name}}</td>
		<td style="width:33%;">Garment Size : {{$header->size}}</td>
	</tr>

	<tr>
		<td style="width:33%;">Print Size : {{$header->garment_size}}</td>
		<td colspan="2">Print Desc. : {{$header->description}}</td>
		
	</tr>
	<tr>
		<td style="width:33%;">Print Item : {{$header->item}}</td>
		<td colspan="2">Supplire print : {{$header->manufacture_name}}</td>	
	</tr>
	<tr>
		<td style="width:33%;">Print Color : {{$header->color}}</td>
		<td style="width:33%;">Fabric Type : {{$header->fabric_type}}</td>
		<td style="width:33%;">Fabric Item : {{$header->fabric_item}}</td>
	</tr>
	<tr>
		<td style="width:33%;">Fabric Color : {{$header->fabric_color}}</td>
		<td colspan="2">Fabric Composition : {{$header->fibre_composition}}</td>
	</tr>
</table>