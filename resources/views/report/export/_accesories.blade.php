<table class="specimenId">
    <tr>
        <td style="width: 33%;">Barcode : {{$header->barcode_supplier}}</td>
        <td style="width: 33%;">Season : {{$header->season}}</td>
        <td style="width: 33%;">PO Supplier : {{$header->document_no}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">PO Buyer : {{$header->po_buyer}}</td>
        <td style="width: 33%;">Style : {{$header->style}}</td>
        <td style="width: 33%;">Article : {{$header->article_no}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Item : {{$header->item}}</td>
        <td style="width: 33%;">Color : {{$header->color}}</td>
        <td style="width: 33%;">Supplier : {{$header->manufacture_name}}</td>
    </tr>
    <tr>
        <td style="width: 33%;">Qty : {{$header->qty}}</td>
        <td style="width: 33%;">Fabric Item : {{$header->fabric_item}}</td>
        <td style="width: 33%;">Fabric Color : {{$header->fabric_color}}</td>
    </tr>
    <tr>
        <td colspan="3">Fabric Supplier : {{$header->remark}}</td>
    </tr>
</table>