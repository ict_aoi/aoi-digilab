<table class="specimenId">
	<tr>
        <td style="width:33%;">{{$header->document_type}} : {{$header->document_no}}</td>
        <td style="width:33%;">Style : {{$header->style}}</td>
        <td style="width:33%;">Article : {{$header->article_no}}</td>
    </tr>

    <tr>
        <td style="width:33%;">Season : {{$header->season}}</td>
        <td style="width:33%;">Style Name: {{$header->style_name}}</td>
        <td style="width:33%;">Machine : {{$header->machine}}</td>
    </tr>

    <tr>
        <td style="width:33%;">Temperature : {{$header->temperature}}</td>
        <td style="width:33%;">Pressure: {{$header->pressure}}</td>
        <td style="width:33%;">Duration : {{$header->duration}}</td>
    </tr>

    <tr>
        <td style="width:33%;">Interlining Item : {{$header->fabric_item}}</td>
        <td style="width:33%;">Interlining Color: {{$header->interlining_color}}</td>
        <td style="width:33%;">Remark : {{$header->remark}}</td>
    </tr>

</table>