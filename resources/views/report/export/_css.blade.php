<style>
    @page{
        margin: 5 5 5 5;
    }
    body{
        font-family: Arial;
        display: flex;
      flex-direction: column;
    }
    .header{
        width: 100%;
        padding-bottom: 10px;
    }
    
    .contents{
        position: relative;
        width: 100%;
        margin-bottom: 70px;
        
    }
    
    .footer{
        clear: both;
        position: fixed;
        bottom: 0;
        width: 100%;
        
    }
    .tablefooter{
        width: 100%;
    }
    
    .HeadTrf{
        width: 100%;
        border-collapse: collapse;
        align-content: center;
    }
    
    .HeadTrf tr td{
        border: 1px solid black;
        padding: 5px;
        vertical-align: top;
        font-size: 16px;
        width: 20%;
    }
    
    .labelHeadTrf{
        font-size: 16px;
        font-weight: bold;
        padding-bottom: 5px;
    }
    
    .specimenId{
        width: 100%;
        border-collapse: collapse;
        align-content: center;
    }
    
    .specimenId tr td{
        border: 1px solid black;
        padding: 5px;
        vertical-align: top;
        font-size: 16px;
        white-space: normal;
    }
    
    .result{
        width: 100%;
        border-collapse: collapse;
        align-content: center;
        margin-bottom: 10px;
        
    }
    
    .result thead tr th{
        border: 1px solid black;
        font-size: 16px;
        font-weight: bold;
        align-content: center;
        text-align: center;
        padding: 10px;
        white-space: normal; 
    
    }
    
    .result tbody tr td{
        border: 1px solid black;
        font-size: 14px;
        padding: 7px;
        /* word-break: break-word; */
        /* word-break: break-all; */
        white-space:initial;
        align-content: center;
        text-align: center;
    }
    
    .result tbody tr{
        page-break-inside: avoid;
    }
    
    .signature{
        width: 80%;
        border-collapse: collapse;
        text-align: center;
        align: center;
        page-break-inside: avoid;
        margin-bottom: 100px;
        
    }
    
    .signature tr td{
        border: 1px solid black;
        align-content: center;
    }

    .clearfix{
        page-break-after: always;
        clear: both;
    }

    .content-list{
        margin-bottom: 100px;
    }
    </style>