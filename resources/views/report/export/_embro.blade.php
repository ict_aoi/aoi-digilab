<table class="specimenId">
	<tr>
		<td>{{$header->document_type}} : {{$header->document_no}}</td>
		<td>Style : {{$header->style}}</td>
		<td>Article : {{$header->article_no}}</td>
	</tr>

	<tr>
		<td>Season : {{$header->season}}</td>
		<td>Style : {{$header->style_name}}</td>
		<td>Garment Size : {{$header->size}}</td>
	</tr>

	<tr>
		<td>Embro Description : {{$header->description}}</td>
		<td>Embro Size : {{$header->garment_size}}</td>
		<td>Embro Item : {{$header->item}}</td>
	</tr>

	<tr>
		<td>Embro Color : {{$header->interlining_color}}</td>
		<td>Supplier Embro : {{$header->manufacture_name}}</td>
		<td>Fabric Item : {{$header->fabric_item}}</td>
	</tr>

	<tr>
		<td>Fabric Color : {{$header->fabric_color}}</td>
		<td>Fabric Type : {{$header->fabric_type}}</td>
		<td>Fabric Composition : {{$header->fibre_composition}}</td>
	</tr>
</table>