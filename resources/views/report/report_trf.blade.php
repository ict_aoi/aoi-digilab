@extends('layouts.app', ['active' => 'menu-report-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Report TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Report TRF</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				<div class="row">
					<form action="{{ route('report.trf.getDataReportTrf') }}" id="form-search" method="GET">
						<div class="col-md-2">
							<label><b>Factory Origin</b></label>
							<select class="select" id="factory_id" {{ $crl['fact'] }}>
								@foreach($factory as $fc)
									<option value="{{$fc->id}}" {{$fc->id==auth::user()->factory_id ? 'selected' :''}}>{{$fc->factory_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<label><b>Lab Location</b></label>
							<select class="select" id="labloc" {{ $crl['lab'] }}>

								@if(auth::user()->factory_id==1)
									@php($slab='AOI1')
								@elseif(auth::user()->factory_id==2)
									@php($slab='AOI2')
								@elseif(auth::user()->factory_id==3)
									@php($slab='AOI3')
								@elseif(auth::user()->factory_id==4)
									@php($slab='DEVELOPMENT CENTER')
								@else
									@php($slab='')
								@endif

								@foreach($labloc as $lc)
									<option value="{{$lc->name}}" {{$lc->name==$slab ? 'selected' : ''}}>{{$lc->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-7">
							<label><b>Search</b></label>
							<input type="text" name="txkey" id="txkey" class="form-control" placeholder="TRF No. / Buyer/ Document No. / Style / Article / Lab Location">
						</div>
						<div class="col-md-1">
							<button style="margin-top: 30px;" class="btn btn-primary" id="btn-search" type="submit">Search</button>
						</div>
					</form>
					
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>TRF No.</th>
									<th>Category</th>
									<th>Specimen</th>
									<th>Method</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">

$(document).ready(function(){
	
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
		deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                'factory_id' : $('#factory_id').val(),
	                'lab_location':$('#labloc').val(),
	                'key' : $('#txkey').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
		initComplete:function(){
		
			$('.dataTables_filter input').unbind();
			$('.dataTables_filter input').bind('keyup', function(e){
				var code = e.keyCode || e.which;
				if (code == 13) {
					table.search(this.value).draw();
					
				}
			});
		},
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id'},
			{data: 'category', name: 'category'},
	        {data: 'specimen', name: 'specimen'},
	        {data: 'method', name: 'method'},
	        {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
	    ]
    });


	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#factory_id').change(function(){
		table.clear();
		table.draw();
	});

	$('#labloc').change(function(){
		table.clear();
		table.draw();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#table-list').on('click','.releaseTrf',function(){
		var id = $(this).data('id');

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url :"{{route('report.trf.releaseReport')}}",
            data:{
            	id:id
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                var dresp = response.data_response;

               if(dresp.status==200){
				alert(dresp.status,dresp.output);
				window.open("{{route('report.trf.printReportTrf')}}?id="+id);
			   }else{
				alert(dresp.status,dresp.output);
			   }
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });
	});

	$('#table-list').on('click','.PrintReportTrf',function(){
		var id = $(this).data('id');

		window.open("{{route('report.trf.printReportTrf')}}?id="+id);
	});

	$('#table-list').on('click','.PrintReportTrf2',function(){
		var id = $(this).data('id');

		window.open("{{route('report.trf.printReportTrfExt')}}?id="+id);
	});
});






</script>
@endsection