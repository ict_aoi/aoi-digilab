@extends('layouts.app', ['active' => 'menu-report-returnspecimen'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Return Sample</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Report Return Sample</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				<div class="row">
					<form action="{{ route('report.allspecimen.getDataReturn') }}" id="form-search" method="GET">
						<div class="col-md-2">
   
                            <label style="font-weight: bold;">Factory</label>
                            <select name="factory_id" id="factory_id" class="form-control select">
                                @foreach($factory as $fc)
                                    <option value="{{$fc->id}}" {{ ($fc->id==Auth::user()->factory_id) ? "selected" : ""}}>{{$fc->factory_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label style="font-weight: bold;">Origin</label>
                            <select name="origin" id="origin" class="form-control select">
                                <option value="DEVELOPMENT">DEVELOPMENT</option>
                                <option value="PRODUCTION">PRODUCTION</option>
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label style="font-weight: bold;">Buyer</label>
                            <select name="buyer" id="buyer" class="form-control select">
                                @foreach($buyer as $by)
                                    <option value="{{$by->buyer}}">{{$by->buyer}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
							<label><b>TRF Submit Date</b></label>
							<input type="text" name="submitdate" id="submitdate" class="form-control daterange-basic">
						</div>

						<div class="col-md-1">
							<button class="btn btn-warning" id="btn-submit" type="submit" style="margin-top: 30px;">Submit</button>
						</div>
					</form>
					
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
                                    <th>TRF No.</th>
									<th>Category</th>
									<th>Specimen</th>
									<th>Buyer</th>
									<th>Lab. Location / Origini</th>
									<th>Submit By</th>
                                    <th>Tested By</th>
									<th>Status</th>
									<th>Return Status</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('report.allspecimen.exportReturnSample') }}" id="ExportReturn"></a>
@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script>
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#table-list').DataTable({
        buttons:[
    		{
                text: 'Export',
                className: 'btn btn-sm bg-success',
                action: function (e, dt, node, config)
                {
                  	var filter = table.search();

                  	var url = $('#ExportReturn').attr('href')+'?factory_id='+$('#factory_id').val()+'&asal='+$('#origin').val()+'&buyer='+$('#buyer').val()+'&submitdate='+$('#submitdate').val()+'&filter='+filter;

                  	window.open(url);
                }
            }
    	],
        ajax: {
            type: 'GET',
            url: $('#form-search').attr('action'),
            data: function (d) {
                return $.extend({},d,{
                    'factory_id' : $('#factory_id').val(),
					'asal' : $('#origin').val(),
					'buyer' : $('#buyer').val(),
					'submitdate' : $('#submitdate').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id'},
            {data: 'category', name: 'category'},
            {data: 'specimen', name: 'specimen'},
            {data: 'buyer', name: 'buyer'},
            {data: 'lab_location', name: 'lab_location'},
            {data: 'nik', name: 'nik'},
            {data: 'tested', name: 'tested'},
            {data: 'status', name: 'status'},
            {data: 'return_status', name: 'return_status'}
        ]
    });

	table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#factory_id').on('change',function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#origin').on('change',function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#buyer').on('change',function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

});
</script>
@endsection