@extends('layouts.app', ['active' => 'menu-escalation-trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Escalation</li>
		</ul>
	</div>
</div>
@endsection

@section('content')


<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Escalation</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				<div class="row">
					<form action="{{ route('esc.getDataEcl') }}" id="form-search" method="GET">
						<div class="col-md-2">
							<label><b>Factory Origin</b></label>
							<select class="select" id="factory_id" {{ auth::user()->admin==false ? 'disabled' : '' }}>
								@foreach($fact as $fc)
									<option value="{{$fc->id}}" {{$fc->id==auth::user()->factory_id ? 'selected' :''}}>{{$fc->factory_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-9">
							<label><b>Search</b></label>
							<input type="text" name="txkey" id="txkey" class="form-control" placeholder="TRF No. / Buyer/ Document No. / Style / Article / Lab Location">
						</div>
						<div class="col-md-1">
							<button style="margin-top: 30px;" class="btn btn-primary" id="btn-search" type="submit">Search</button>
						</div>
					</form>
					
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>TRF No.</th>
									<th>Buyer</th>
									<th>Category</th>
									<th>Origin</th>
									<th>Lab Location</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
	@include('report.escalation.modal')
@endsection
@section('js')
<script type="text/javascript">

	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                'factory_id' : $('#factory_id').val(),
	                'key' : $('#txkey').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id'},
	        {data: 'buyer', name: 'buyer'},
	        {data: 'category', name: 'category'},
	        {data: 'asal_specimen', name: 'asal_specimen'},
	        {data: 'lab_location', name: 'lab_location'},
	        {data: 'action', name: 'action'}

	    ]
    });

    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#factory_id').change(function(){
		table.clear();
		table.draw();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#table-list').on('click','.escalation',function(){
		var id = $(this).data('id');

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('esc.viewData') }}",
            data:{
                id:id
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                $('#modal_result .modal-body').empty();
                $('#modal_result .modal-body').append(response);
                $('#modal_result .trfid').val(id);
                $('#modal_result').modal('show');
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });
	});


	$('#modal_result').on('click','.btn-save',function(event){
		event.preventDefault();
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : "{{ route('esc.createEscalation')}}",
            data:{
                id:$('#modal_result .trfid').val(),
                escalation:$('input[name=radesc]:checked').val(),
                remark_escalation : $('#modal_result .escremark').val()
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                var notif = response.data_response;
                alert(notif.status,notif.output);
                table.clear();
                table.draw();
                $('#modal_result').modal('hide');

            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });
		
	});

	$('#table-list').on('click','.viewesc',function(){
		var id = $(this).data('id');


		$('#modal_view .txtfid').text('');
        $('#modal_view .txorigin').text('');
        $('#modal_view .txlab').text('');
        $('#modal_view .txcatagory').text('');
        $('#modal_view .txcategoryspc').text('');
        $('#modal_view .txtypespc').text('');

        $('#modal_view .txescalation').val('');
        $('#modal_view .txremark').val('');

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('esc.viewEsc') }}",
            data:{
                id:id
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
            	var data = response.data;

                $('#modal_view .txtfid').text(data.trf_id);
                $('#modal_view .txorigin').text(data.asal_specimen+' - '+data.factory_name);
                $('#modal_view .txlab').text(data.lab_location);
                $('#modal_view .txcatagory').text(data.category);
                $('#modal_view .txcategoryspc').text(data.category_specimen);
                $('#modal_view .txtypespc').text(data.type_specimen);

                $('#modal_view .txescalation').val(data.escalation);
                $('#modal_view .txremark').val(data.remark_escalation);
                $('#modal_view').modal('show');
                 $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });
	});
</script>
@endsection