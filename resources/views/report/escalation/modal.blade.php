<div id="modal_result" class="modal fade" >
    <div class="modal-dialog modal-full">
        <div class="modal-content ">
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>

<div id="modal_view" class="modal fade" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label><b>TRF NO : </b></label><br>
                        <label class="txtfid"></label>
                    </div>
                    <div class="col-md-4">
                        <label><b>ORIGIN : </b></label><br>
                        <label class="txorigin"></label>
                    </div>
                    <div class="col-md-4">
                        <label><b>LAB LOCATION : </b></label><br>
                        <label class="txlab"></label>
                    </div>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label><b>CATEGORY : </b></label><br>
                        <label class="txcatagory"></label>
                    </div>
                    <div class="col-md-4">
                        <label><b>CATEGORY SPECIMEN : </b></label><br>
                        <label class="txcategoryspc"></label>
                    </div>
                    <div class="col-md-4">
                        <label><b>TYPE SPECIMEN : </b></label><br>
                        <label class="txtypespc"></label>
                    </div>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label><b>Escalation Result</b></label>
                        <input type="text" name="txescalation" id="txescalation" class="form-control txescalation" readonly>
                    </div>
                    <div class="col-md-8">
                        <label><b>Escalation Remark</b></label>
                        <input type="text" name="txremark" id="txremark" class="form-control txremark" readonly>
                    </div>
                </div>
                <div class="row">
                    <button class="btn btn-warning" type="button" data-dismiss="modal">CLOSED</button>
                </div>
            </div>
        </div>
    </div>
</div>