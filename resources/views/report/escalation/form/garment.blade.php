	<tr>
		<td><label style="font-weight: bold;">Style</label></td> 
		<td><label>{{$idt->style}}</label></td>

		<td><label style="font-weight: bold;">Embro Desc</label></td> 
		<td><label>{{$idt->description}}</label></td>

		<td><label style="font-weight: bold;">Item Fabric</label></td> 
		<td><label>{{$idt->fabric_item}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Article</label></td> 
		<td><label>{{$idt->article_no}}</label></td>

		<td><label style="font-weight: bold;">Embro Size</label></td> 
		<td><label>{{$idt->size}}</label></td>

		<td><label style="font-weight: bold;">Fabric Type</label></td> 
		<td><label>{{$idt->fabric_type}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Season</label></td> 
		<td><label>{{$idt->season}}</label></td>

		<td><label style="font-weight: bold;">Embro item</label></td> 
		<td><label>{{$idt->item}}</label></td>

		<td><label style="font-weight: bold;">Fabric Color</label></td> 
		<td><label>{{$idt->fabric_color}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style Name</label></td> 
		<td><label>{{$idt->style_name}}</label></td>

		<td><label style="font-weight: bold;">Embro Color</label></td> 
		<td><label>{{$idt->color}}</label></td>

		<td rowspan="2"><label style="font-weight: bold;">Fabric Composition</label></td> 
		<td rowspan="2"><label>{{$idt->fibre_composition}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Garment Size</label></td> 
		<td><label>{{$idt->garment_size}}</label></td>

		<td><label style="font-weight: bold;">Supplier Print</label></td> 
		<td><label>{{$idt->manufacture_name}}</label></td>

	</tr>