<tr>
		<td width="16.5%"><label style="font-weight: bold;">PO Buyer</label></td> 
		<td width="16.5%"><label>{{$idt->document_no}}</label></td>

		<td width="16.5%"><label style="font-weight: bold;">Qty</label></td> 
		<td width="16.5%"><label></label>{{$idt->qty}}</td>

		<td width="16.5%"><label style="font-weight: bold;">Fabric Color</label></td> 
		<td width="16.5%"><label>{{$idt->fabric_color}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Article</label></td> 
		<td><label>{{$idt->article_no}}</label></td>

		<td><label style="font-weight: bold;">Temperature</label></td> 
		<td><label>{{$idt->temperature}}</label></td>

		<td><label style="font-weight: bold;">Interlining Color</label></td> 
		<td><label>{{$idt->interlining_color}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style</label></td> 
		<td><label>{{$idt->style}}</label></td>

		<td><label style="font-weight: bold;">Pressure</label></td> 
		<td><label>{{$idt->pressure}}</label></td>

		<td><label style="font-weight: bold;">Fabric Item</label></td> 
		<td><label> {{$idt->item}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Season</label></td> 
		<td><label>{{$idt->season}}</label></td>

		<td><label style="font-weight: bold;">Duration</label></td> 
		<td><label>{{$idt->duration}}</label></td>

		<td><label style="font-weight: bold;">Fabric Type</label></td> 
		<td><label>{{$idt->fabric_type}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style Name</label></td> 
		<td><label>{{$idt->style_name}}</label></td>

		<td rowspan="2"><label style="font-weight: bold;">Component</label></td> 
		<td rowspan="2"><label>{{$idt->component}}</label></td>

		<td rowspan="2"><label style="font-weight: bold;">Fabric Composition</label></td> 
		<td rowspan="2"><label>{{$idt->fibre_composition}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Machine</label></td> 
		<td><label>{{$idt->machine}}</label></td>
	</tr>