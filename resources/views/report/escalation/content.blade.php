@foreach($data as $dt)

<div class="panel panel-flat">
	<div class="panel-heading">
		<div class="row">
			<div class="col-lg-2">
				<label><b>TRF NO. </b><br> {{$dt->trf_id}}</label>
			</div>

			<div class="col-lg-2">
				<label><b>ORIGIN </b><br> {{$dt->asal_specimen}} - {{$dt->factory_name}}</label>
			</div>

			<div class="col-lg-2">
				<label><b>LAB LOCATION </b><br> {{$dt->lab_location}}</label>
			</div>

			<div class="col-lg-2">
				<label><b>CATEGORY </b><br> {{$dt->category}}</label>
			</div>

			<div class="col-lg-2">
				<label><b>CATEGORY SPECIMEN </b><br> {{$dt->category_specimen}}</label>
			</div>

			<div class="col-lg-2">
				<label><b>TYPE SPECIMEN </b><br> {{$dt->type_specimen}}</label>
			</div>
		</div>

		<div class="row">
			<table style=" width: 100%;">
				<tr>
					<td colspan="6"><label style="font-weight:bold;font-size: 18px;">Specimen Identity </label></td>
				</tr>

				{{-- @if($dt->category_specimen=='FABRIC')
					@include('report.escalation.form.fabric',['idt'=>$dt])
				@elseif($dt->category_specimen=='MOCKUP')
					@include('report.escalation.form.mockup',['idt'=>$dt])
				@elseif($dt->category_specimen=='ACCESORIES/TRIM' )
					@include('report.escalation.form.accesories',['idt'=>$dt])
				@elseif($dt->category_specimen=='GARMENT' )
					@include('report.escalation.form.garment',['idt'=>$dt])
				@elseif($dt->category_specimen=='STRIKE OFF' && $dt->type_specimen=='HEAT TRANSFER' )
					@include('report.escalation.form.heat_transfer',['idt'=>$dt])
				@elseif($dt->category_specimen=='STRIKE OFF' && $dt->type_specimen=='EMBROIDERY' )
					@include('report.escalation.form.embro',['idt'=>$dt])
				@elseif($dt->category_specimen=='STRIKE OFF' && $dt->type_specimen=='PRINTING' )
					@include('report.escalation.form.printing',['idt'=>$dt])
				@elseif($dt->category_specimen=='STRIKE OFF' && $dt->type_specimen=='PAD PRINT' )
					@include('report.escalation.form.pad_print',['idt'=>$dt])
				@elseif($dt->category_specimen=='STRIKE OFF' && $dt->type_specimen=='BADGE' )
					@include('report.escalation.form.badge',['idt'=>$dt])
				@else
					@include('report.escalation.form.error',['idt'=>$dt])
				@endif --}}

				@if($dt->category_specimen=="FABRIC")
					@include('report.export._fabric',['header'=>$dt])
				@elseif($dt->category_specimen=="GARMENT")
					@include('report.export._garment',['header'=>$dt])
				@elseif(($dt->category_specimen=="STRIKE OFF" || $dt->category_specimen=="PANEL") && $dt->type_specimen=="HEAT TRANSFER")
					@include('report.export._heat',['header'=>$dt])
				@elseif(($dt->category_specimen=="STRIKE OFF" || $dt->category_specimen=="PANEL") && $dt->type_specimen=="PAD PRINT")
					@include('report.export._pad',['header'=>$dt])
				@elseif(($dt->category_specimen=="STRIKE OFF" || $dt->category_specimen=="PANEL") && $dt->type_specimen=="PRINTING")
					@include('report.export._printing',['header'=>$dt])
				@elseif(($dt->category_specimen=="STRIKE OFF" || $dt->category_specimen=="PANEL") && $dt->type_specimen=="EMBROIDERY")
					@include('report.export._embro',['header'=>$dt])
				@elseif(($dt->category_specimen=="STRIKE OFF" || $dt->category_specimen=="PANEL") && $dt->type_specimen=="BADGE")
					@include('report.export._badge',['header'=>$dt])
				@elseif(($dt->category_specimen=="STRIKE OFF" || $dt->category_specimen=="PANEL") && $dt->type_specimen=="BONDING")
					@include('report.export._bonding',['header'=>$dt])
				@elseif($dt->category_specimen=="ACCESORIES/TRIM")
					@include('report.export._accesories',['header'=>$dt])
				@elseif($dt->category_specimen=="MOCKUP")
					@include('report.export._panelmock',['header'=>$dt])
				@endif

			</table>
		</div>
	</div>
	<div class="panel-body">
		<table class ="table table-basic table-condensed" id="table-list">
			<thead>
				<tr>
					<th>Methode</th>
					<th>Standart</th>
					<th>Parameter</th>
					<th>Measuring Position (Code)</th>
					<th>Supplier Result</th>
					<th>Result Value</th>
					<th>Pass / Fail</th>
					<th>Remark / Comment</th>
				</tr>
			</thead>
			<tbody>
			

				@foreach(\App\Http\Controllers\Report\EscalationController::getDetail($dt->id,$dt->trf_doc_id) as $dl)


				<tr style="color: {{$dl->result_status=='FAILED' ? 'red' : ''}}">
					<td>{{$dl->method_code}} <br> {{$dl->method_name}}</td>
			
					<td>
						{{\App\Http\Controllers\HelperController::setReqOption($dl->req_id)}}
					</td>

					<td>
						{{$dl->parameter}}
					</td>

					<td>
						{{$dl->measuring_position}} 

						@if(trim($dl->code)!="")
							({{$dl->code}})
						@endif
					</td>

					<td>
						{{$dl->supplier_result}}
					</td>

					<td>
						{{$dl->testing_result}}
					</td>

					<td>
						{{$dl->result_status}}
					</td>

					<td>
						{{$dl->remark_result}}
					</td>
				</tr>

				@endforeach


			</tbody>
		</table>
	</div>
</div>

@endforeach

<div class="panel panel-flat">
	<div class="panel-body">

			<input type="hidden" name="trfid" id="trfid" class="trfid">
            <div class="row">
               <div class="col-md-4 form-group">
                    <label style="font-size: 14px;"><b>Escalation Status : </b></label><br>
                   <label class="radio-inline" style="font-size: 14px;">
                       <input type="radio" name="radesc" value="Re-Test" checked> Re-Test
                   </label>
                   <label class="radio-inline" style="font-size: 14px;">
                       <input type="radio" name="radesc" value="Failed"> Failed
                   </label>
                   <label class="radio-inline" style="font-size: 14px;">
                       <input type="radio" name="radesc" value="Pass"> Pass With Remark
                   </label>
               </div>

               <div class="col-md-8">
               		 <label style="font-size: 14px;"><b>Remark : </b></label>
               		 <input type="text" name="escremark" id="escremark" class="form-control escremark">
               </div>
            </div>
            <div class="row">
            	<center>
            		<button class="btn btn-success btn-save" type="button" id="btn-save">SAVE</button>
            		<button class="btn btn-warning" type="button" data-dismiss="modal">CANCEL</button>
            	</center>
            </div>

	</div>
</div>

