@extends('layouts.app', ['active' => 'menu-report-logbook'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Log Book TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Log Book TRF</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				<div class="row">

					<form action="{{ route('report.logbook.getReportLogbook')}}" id="form-search" method="GET">
						<div class="col-lg-2">
							<label><b>Factory</b></label>
							<select id="factory_id" class="select">
								@foreach($fact as $ft)
									<option value="{{$ft->id}}">{{$ft->factory_name}}</option>
								@endForeach
							</select>
						</div>
						<div class="col-lg-2">
							<label><b>Origin</b></label>
							<select id="origin" class="select">
								<option value="DEVELOPMENT">DEVELOPMENT</option>
								<option value="PRODUCTION">PRODUCTION</option>
							</select>
						</div>
						<div class="col-lg-2">
							<label><b>Buyer</b></label>
							<select id="buyer" class="select">
								<option value="ALL">ALL</option>
								@foreach($buyer as $by)
									<option value="{{$by->buyer}}">{{$by->buyer}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-5">
							<label><b>Submit Date</b></label>
							<input type="text" name="submitdate" id="submitdate" class="form-control daterange-basic">
						</div>
						<div class="col-lg-1">
							<button type="submit" class="btn btn-primary" style="margin-top:27px;">Search</button>
						</div>
					</form>
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>TRF No.</th>
									<th>UserName</th>
									<th>Lab Location</th>
									<th>Specimen Origin</th>
									<th>Buyer</th>
									<th>Status</th>
									<th>Category</th>
									<th>Specimen</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('report.logbook.exportLogBook')}}" id="ExportLogBook"></a>
@endsection


@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
    	buttons:[
    		{
                text: 'Export',
                className: 'btn btn-sm bg-success',
                action: function (e, dt, node, config)
                {
                  	var filter = table.search();

                  	var url = $('#ExportLogBook').attr('href')+'?factory_id='+$('#factory_id').val()+'&origin='+$('#origin').val()+'&buyer='+$('#buyer').val()+'&submitdate='+$('#submitdate').val()+'&filter='+filter;

                  	window.open(url);
                }
            }
    	],
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                'factory_id' : $('#factory_id').val(),
	                'origin' : $('#origin').val(),
	                'buyer'	: $('#buyer').val(),
	                'submitdate' : $('#submitdate').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id'},
	        {data: 'username', name: 'username'},
	        {data: 'lab_location', name: 'lab_location'},
	        {data: 'asal_specimen', name: 'asal_specimen'},
	        {data: 'buyer', name: 'buyer'},
	        {data: 'status', name: 'status'},
	        {data: 'category', name: 'category'},
	        {data: 'specimen', name: 'specimen'},

	    ]
    });


	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#factory_id').change(function(){
		table.clear();
		table.draw();
	});

	$('#origin').change(function(){
		table.clear();
		table.draw();
	});

	$('#buyer').change(function(){
		table.clear();
		table.draw();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});


});






</script>
@endsection