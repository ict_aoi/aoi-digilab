<style type="text/css">
@page{ margin: 10 10 10 10 },

.table{
	border-collapse: collapse;
	width: 100%;
}

.table > thead tr th{
	border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 8px;
    text-align: center;
}

.table > tbody tr td{
	border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 8px;
}

.head{
	border-collapse: collapse;
	width: 100%;
}

.head tr td{
	border: 1px solid black;
	padding-left: 10px;
    padding-right: 10px;
}
</style>
<table class="head">
	<tr>
		<td rowspan="3">
			<center>
				<img src="{{ \App\Http\Controllers\ImageController::getImgSv('bbi')}}" width="125px">
			</center>
		</td>
		<td colspan="4"><center><b>REPORT ALL SPECIMEN</b></center></td>
		<td rowspan="3">
			<center>
				<img src="{{ \App\Http\Controllers\ImageController::getImgSv('aoi')}}" width="125px">
			</center>
		</td>
	</tr>
	<tr>
		
		<td><b>ORIGIN</b></td>
		<td><b>{{$origin}}</b></td>
		<td><b>FACTORY</b></td>
		<td><b>{{$factory}}</b></td>
	
	</tr>
	<tr>
		<td colspan="4"><center><b>SUBMIT DATE {{$date}}</b></center></td>
	</tr>
</table>
<br>
<table class="table" width="100%">
	<thead >
		<tr>
			<th><center><b>No.</b></center></th>
			<th><center><b>DATE SUBMIT TRF</b></center></th>
			<th><center><b>DEPT ORIGIN</b></center></th>
			<th><center><b>FACTORY ORIGIN</b></center></th>
			<th><center><b>CATEGORY SPESIMEN</b></center></th>
			<th><center><b>TYPE SPECIMEN</b></center></th>
			<th><center><b>TRF NUMBER</b></center></th>
			<th><center><b>USER SUBMIT </b></center></th>
			<th><center><b>DOC NUMBER</b></center></th>
			<th><center><b>BUYER / BRAND</b></center></th>
			<th><center><b>STYLE</b></center></th>
			<th><center><b>ARTICLE</b></center></th>
			<th><center><b>USER VERIFY</b></center></th>
			<th><center><b>DATE TRF VERIFY</b></center></th>
			<th><center><b>LEADTIME STD</b></center></th>
			<th><center><b>ESTIMATION DATE RELEASE</b></center></th>
			<th><center><b>ACTUAL REPORT RELEASE</b></center></th>
			<th><center><b>RESULT REPORT</b></center></th>
			<th><center><b>ONTIME TESTING</b></center></th>
			<th><center><b>STATUS</b></center></th>


		</tr>
	</thead>
	<tbody>
		@foreach($list as $ls)
			<tr>
				<td>{{$ls['no']}}</td>
				<td>{{$ls['submit_date']}}</td>
				<td>{{$ls['asal']}}</td>
				<td>{{$ls['factory']}}</td>
				<td>{{$ls['category_specimen']}}</td>
				<td>{{$ls['type_specimen']}}</td>
				<td>{{$ls['trf_no']}}</td>
				<td>{{$ls['user']}}</td>
				<td>{{$ls['doc_no']}}</td>
				<td>{{$ls['buyer']}}</td>
				<td>{{$ls['style']}}</td>
				<td>{{$ls['article']}}</td>
				<td>{{$ls['verif_by']}}</td>
				<td>{{$ls['verif_date']}}</td>
				<td>{{$ls['leadtime']}}</td>
				<td>{{$ls['due']}}</td>
				<td>{{$ls['actual_release']}}</td>
				<td>{{$ls['result']}}</td>
				<td>{{$ls['remOT']}}</td>
				<td>{{$ls['status']}}</td>
			</tr>
		@endforeach
	</tbody>
</table>