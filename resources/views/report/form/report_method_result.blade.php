<style type="text/css">
    @page{ margin: 10 10 10 10 },
    
    .table{
        border-collapse: collapse;
        width: 100%;
    }
    
    .table > thead tr th{
        border: 1px solid black;
        padding-left: 10px;
        padding-right: 10px;
        font-size: 14px;
        text-align: center;
    }
    
    .table > tbody tr td{
        border: 1px solid black;
        padding-left: 10px;
        padding-right: 10px;
        font-size: 12px;
    }
    
    .head{
        border-collapse: collapse;
        width: 100%;
    }
    
    .head tr td{
        border: 1px solid black;
        padding-left: 10px;
        padding-right: 10px;
        text-align: center;
    }
    </style>
<table class="table" width="100%">
	<thead >
        <tr>
            <th colspan="9"><center><b>REPORT METHOD RESULT</b></center></th>
        </tr>
		<tr>
			<th><center><b>No.</b></center></th>
			<th><center><b>Submit Date</b></center></th>
			<th><center><b>TRF No.</b></center></th>
			<th><center><b>Lab Location</b></center></th>
			<th><center><b>Origin</b></center></th>
			<th><center><b>Buyer</b></center></th>
			<th><center><b>Method</b></center></th>
			<th><center><b>Tested By</b></center></th>
			<th><center><b>Status</b></center></th>
		</tr>
	</thead>
	<tbody>
		@foreach($list as $ls)
       
			<tr>
				<td>{{$ls['no']}}</td>
				<td>{{$ls['created_at']}}</td>
				<td>{{$ls['trf_id']}}</td>
				<td>{{$ls['lab_location']}}</td>
				<td>{{$ls['asal_specimen']}}  ({{$ls['factory_name']}})</td>
				<td>{{$ls['buyer']}}</td>
				<td>( {{$ls['method_code']}} ) {{$ls['method_name']}}</td>
				<td>{{$ls['pic']}}</td>
				<td>{{$ls['result_status']}}</td>
			</tr>
		@endforeach
	</tbody>
</table>