<style type="text/css">
@page{ margin: 10 10 10 10 },

.table{
	border-collapse: collapse;
	width: 100%;
}

.table > thead tr th{
	border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 14px;
    text-align: center;
}

.table > tbody tr td{
	border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 12px;
}

.head{
	border-collapse: collapse;
	width: 100%;
}

.head tr td{
	border: 1px solid black;
	padding-left: 10px;
    padding-right: 10px;
}
</style>
<table class="head">
	<tr>
		<td rowspan="3">
			<center>
				<img src="{{ \App\Http\Controllers\ImageController::getImgSv('bbi')}}" width="125px">
				
			</center>
		</td>
		<td colspan="4"><center><b>REPORT SERAH TERIMA SPECIMEN</b></center></td>
		<td rowspan="3">
			<center>
				<img src="{{ \App\Http\Controllers\ImageController::getImgSv('aoi')}}" width="125px">
			</center>
		</td>
	</tr>
	<tr>
		
		<td><b>ORIGIN</b></td>
		<td><b>{{$origin}}</b></td>
		<td><b>FACTORY</b></td>
		<td><b>{{$factory}}</b></td>
	
	</tr>
	<tr>
		<td colspan="4"><center><b>SUBMIT DATE {{$date}}</b></center></td>
	</tr>
</table>
<br>
<table class="table" width="100%">
	<thead >
		<tr>
			<th><center><b>No.</b></center></th>
			<th><center><b>Submit Date</b></center></th>
			<th><center><b>TRF No.</b></center></th>
			<th><center><b>Status</b></center></th>
			<th><center><b>Category</b></center></th>
			<th><center><b>Category Specimen</b></center></th>
			<th><center><b>Type Specimen</b></center></th>
			<th><center><b>User</b></center></th>
			<th><center><b>Style</b></center></th>
			<th><center><b>Article</b></center></th>
			<th><center><b>Verified By</b></center></th>
			<th><center><b>Verified Date</b></center></th>
			<th><center><b>Leadtime STD</b></center></th>
			<th><center><b>Estimation Report Date Release</b></center></th>
		</tr>
	</thead>
	<tbody>
		@foreach($list as $ls)
			<tr>
				<td>{{$ls['no']}}</td>
				<td>{{$ls['submit_date']}}</td>
				<td>{{$ls['trf_no']}}</td>
				<td>{{$ls['status']}}</td>
				<td>{{$ls['category']}}</td>
				<td>{{$ls['category_specimen']}}</td>
				<td>{{$ls['type_specimen']}}</td>
				<td>{{$ls['user']}}</td>
				<td>{{$ls['style']}}</td>
				<td>{{$ls['article']}}</td>
				<td>{{$ls['verif_by']}}</td>
				<td>{{$ls['verif_date']}}</td>
				<td>{{$ls['leadtime']}}</td>
				<td>{{$ls['due']}}</td>
			</tr>
		@endforeach
	</tbody>
</table>