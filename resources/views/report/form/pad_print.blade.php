
<table class="head">
	<tr>
		<td colspan="6"><label style="font-weight:bold;font-size: 18px;">Specimen Identity {{isset($inc) ? $inc : '' }}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style</label></td> 
		<td><label>{{$idt->style}}</label></td>

		<td><label style="font-weight: bold;">Supplier Pad Print</label></td> 
		<td><label>{{$idt->manufacture_name}}</label></td>

		<td><label style="font-weight: bold;">Pad Print Size</label></td> 
		<td><label>{{$idt->size}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Article</label></td> 
		<td><label>{{$idt->article_no}}</label></td>

		<td><label style="font-weight: bold;">Thread</label></td> 
		<td><label>{{$idt->thread}}</label></td>

		<td><label style="font-weight: bold;">Item Fabric</label></td> 
		<td><label>{{$idt->fabric_item}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style Name</label></td> 
		<td><label>{{$idt->style_name}}</label></td>

		<td><label style="font-weight: bold;">Pad Print Item</label></td> 
		<td><label>{{$idt->item}}</label></td>

		<td><label style="font-weight: bold;">Fabric Type</label></td> 
		<td><label>{{$idt->fabric_type}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Season</label></td> 
		<td><label>{{$idt->season}}</label></td>

		<td><label style="font-weight: bold;">Pad Print Color</label></td> 
		<td><label>{{$idt->color}}</label></td>

		<td rowspan="2"><label style="font-weight: bold;">Fabric Composition</label></td> 
		<td rowspan="2"><label>{{$idt->fibre_composition}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Fabric Color</label></td> 
		<td><label>{{$idt->fabric_color}}</label></td>

		<td><label style="font-weight: bold;">Pad Print Desc</label></td> 
		<td><label>{{$idt->description}}</label></td>

	</tr>

</table>