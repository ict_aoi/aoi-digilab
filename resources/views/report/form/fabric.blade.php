
<table class="head">
	<tr>
		<td colspan="6"><label style="font-weight:bold;font-size: 18px;">Specimen Identity {{isset($inc) ? $inc : '' }}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style</label></td> 
		<td><label>{{$idt->style}}</label></td>

		<td><label style="font-weight: bold;">PO Supplier</label></td> 
		<td><label>{{$idt->document_no}}</label></td>

		<td><label style="font-weight: bold;">Roll No</label></td> 
		<td><label>{{$idt->nomor_roll}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Item</label></td> 
		<td><label>{{$idt->item}}</label></td>

		<td><label style="font-weight: bold;">Supplier</label></td> 
		<td><label>{{$idt->manufacture_name}}</label></td>

		<td><label style="font-weight: bold;">Batch No</label></td> 
		<td><label>{{$idt->batch_number}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Color</label></td> 
		<td><label>{{$idt->color}}</label></td>

		<td rowspan="3"><label style="font-weight: bold;">Fibre Composition</label></td> 
		<td rowspan="3"><label>{{$idt->fibre_composition}}</label></td>

		<td><label style="font-weight: bold;">Yards</label></td> 
		<td><label>{{$idt->yds_roll}}</label></td>
	</tr>
	<tr>
		<td><label style="font-weight: bold;">Season</label></td> 
		<td><label>{{$idt->season}}</label></td>

	

		<td><label style="font-weight: bold;">Date Receive</label></td> 
		<td><label>{{isset($idt->verified_lab_date) ? date_format(date_create($idt->verified_lab_date),'d-m-Y') : null}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">PODD</label></td> 
		<td><label>{{date_format(date_create($idt->date_information),'d-m-Y')}}</label></td>

	

		<td><label style="font-weight: bold;">Date Finished</label></td> 
		<td><label>{{isset($idt->closed_at) ? date_format(date_create($idt->closed_at),'d-m-Y') :null}}</label></td>
	</tr>
</table>