
<table class="head">
	<tr>
		<td colspan="6"><label style="font-weight:bold;font-size: 18px;">Specimen Identity {{isset($inc) ? $inc : '' }}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style</label></td> 
		<td><label>{{$idt->style}}</label></td>

		<td><label style="font-weight: bold;">PO Number</label></td> 
		<td><label>{{$idt->document_no}}</label></td>

		<td><label style="font-weight: bold;">Size</label></td> 
		<td><label>{{$idt->size}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Article</label></td> 
		<td><label>{{$idt->article_no}}</label></td>

		<td><label style="font-weight: bold;">Season</label></td> 
		<td><label>{{$idt->season}}</label></td>

		<td><label style="font-weight: bold;">Color</label></td> 
		<td><label>{{$idt->color}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style Name</label></td> 
		<td><label>{{$idt->style_name}}</label></td>

		<td><label style="font-weight: bold;">Temperature</label></td> 
		<td><label>{{$idt->temperature}}</label></td>

		<td><label style="font-weight: bold;">Machine Model</label></td> 
		<td><label>{{$idt->machine}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Drying Process</label></td> 
		<td><label>{{$idt->care_instruction}}</label></td>

		<td><label style="font-weight: bold;">Fabric Properties</label></td> 
		<td><label>{{$idt->fabric_item}} {{$idt->fibre_composition}}</label></td>

		<td><label style="font-weight: bold;">Fibre Composition</label></td> 
		<td><label>{{$idt->fibre_composition}}</label></td>
	</tr>

	
</table>