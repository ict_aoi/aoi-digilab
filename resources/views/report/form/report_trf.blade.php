<script type="text/javascript">
	window.onload = function(){
		window.print();
	}
</script>
<style type="text/css">
@page{ margin: 10 10 10 10 }

.head{
	border-collapse: collapse;
	width: 100%;


}
.head tr td{
	border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 12px;
    max-width: 100%;
}

.head td{
	height: 10px;

}

.testresult{
	border-collapse: collapse;
	width: 100%;

}
.testresult tr td{
	border: 1px solid black;
    padding-left: 10px;
    padding-right: 10px;
     font-size: 12px;
     word-wrap: break-word;
     page-break-before: auto;
     max-width: 100px;
}

.testresult td{
	height: 10px;
	text-align: center;

}
.page-break {
    page-break-after: always;
}
</style>

@foreach($list as $ls)
	@php($headdoc= $ls['head'])
	<table class="head">
		<tr>
			<td colspan="6"><center><h3>INTERNAL TEST REPORT</h3></center></td>
		</tr>
		<tr>
			<td rowspan="4">
				<center>
					<img src="{{url('assets/icon/bbi_logo.png')}}" width="100px">
				</center>
				
			</td>

			<td>
				<label style="font-weight: bold;">TRF NO</label>
			</td>
			<td>
				<label style="font-weight: bold;">{{$headdoc->trf_id}}</label>
			</td>
			<td>
				<label style="font-weight: bold;">DEPT. ORIGIN</label>
			</td>
			<td>
				<label>{{$headdoc->asal_specimen}}</label>
			</td>

			<td rowspan="4">
				<center>
					<img src="{{url('assets/icon/aoi.png')}}" width="100px">
				</center>
			</td>
		</tr>

		<tr>
			<td>
				<label style="font-weight: bold;">BUYER</label>
			</td>
			<td>
				<label>{{$headdoc->buyer}}</label>
			</td>
			<td>
				<label style="font-weight: bold;">CATEGORY</label>
			</td>
			<td>
				<label>{{$headdoc->category}}</label>
			</td>
		</tr>

		<tr>
			<td>
				<label style="font-weight: bold;">FACTORY</label>
			</td>
			<td>
				<label>{{$headdoc->factory_name}}</label>
			</td>
			<td>
				<label style="font-weight: bold;">CATEGORY SPECIMEN</label>
			</td>
			<td>
				<label>{{$headdoc->category_specimen}}</label>
			</td>
		</tr>

		<tr>
			<td>
				<label style="font-weight: bold;">LAB LOCATION</label>
			</td>
			<td>
				<label>{{$headdoc->lab_location}}</label>
			</td>
			<td>
				<label style="font-weight: bold;">TYPE SPECIMEN</label>
			</td>
			<td>
				<label>{{$headdoc->type_specimen}}</label>
			</td>
		</tr>
	</table>
	<br>
	@if($headdoc->category_specimen=='FABRIC')
		@include('report.form.fabric',['idt'=>$headdoc])
	@elseif($headdoc->category_specimen=='MOCKUP')
		@include('report.form.mockup',['idt'=>$headdoc])
	@elseif($headdoc->category_specimen=='ACCESORIES/TRIM' )
		@include('report.form.accesories',['idt'=>$headdoc])
	@elseif($headdoc->category_specimen=='GARMENT' )
		@include('report.form.garment',['idt'=>$headdoc])
		@elseif(($headdoc->category_specimen=='STRIKE OFF' && $headdoc->type_specimen=='HEAT TRANSFER') || $headdoc->category_specimen=='PANEL' ) 
		@include('report.form.heat_transfer',['idt'=>$headdoc])
	@elseif($headdoc->category_specimen=='STRIKE OFF' && $headdoc->type_specimen=='EMBROIDERY' )
		@include('report.form.embro',['idt'=>$headdoc])
	@elseif($headdoc->category_specimen=='STRIKE OFF' && $headdoc->type_specimen=='PRINTING' )
		@include('report.form.printing',['idt'=>$headdoc])
	@elseif($headdoc->category_specimen=='STRIKE OFF' && $headdoc->type_specimen=='PAD PRINT' )
		@include('report.form.pad_print',['idt'=>$headdoc])
	@elseif($headdoc->category_specimen=='STRIKE OFF' && $headdoc->type_specimen=='BADGE' )
		@include('report.form.badge',['idt'=>$headdoc])
	@else
		@include('report.form.error',['idt'=>$headdoc])
	@endif
	<br>

	@php($finRest = 'PASS')
	@foreach($ls['content'] as $rh)

		<table class="testresult" width="100%">
			<tr>
				<td colspan="8">
					<center><label style="font-weight: bold; font-size: 18px;">{{$rh['header']}}</label></center>
				</td>
			</tr>
			<tr>
				<td width="100px">
					<label style="font-weight:bold;">Methode Code <br> (Methode Name)</label>
				</td>
				<td>
					<label style="font-weight:bold;">Standard</label>
				</td>
				<td>
					<label style="font-weight:bold; ">Parameter</label>
				</td>
				<td>
					<label style="font-weight:bold;">Measuring positions</label>
				</td>
				<td>
					<label style="font-weight:bold;">Supplier Result</label>
				</td>
				<td>
					<label style="font-weight:bold;">Result value</label>
				</td>
				<td width="50px">
					<label style="font-weight:bold;">Pass/ Fail</label>
				</td>
				<td>
					<label style="font-weight:bold;">Remark / Comment</label>
				</td>
			</tr>
				
				@php($mthid='')
			
				@for($x=count($rh['result'])-1; $x>=0; $x--)

					<tr style="page-break-before: auto;">
						@if($mthid=='' || $mthid!=$rh['result'][$x]['method_id'])
							<td rowspan="{{$rh['result'][$x]['rowspan']}}" ><label>{{$rh['result'][$x]['method_code']}} <br>{{$rh['result'][$x]['method_name']}}</label></td>
							@php($mthid=$rh['result'][$x]['method_id'])
						@endif
						<td><label >{{$rh['result'][$x]['standart']}} {{$rh['result'][$x]['uom']}}</label></td>
						<td><label >{{$rh['result'][$x]['parameter']}}</label></td>
						<td><label >{{$rh['result'][$x]['measuring_position']}}</label></td>
						<td><label >{{$rh['result'][$x]['supplier_result']}}</label></td>
						<td><label >{{$rh['result'][$x]['result']}} {{$rh['result'][$x]['uom']}}</label></td>
						<td><label style="color: {{$rh['result'][$x]['result_status']=='FAILED' ? '#e60000' : '#000000'  }}" >{{$rh['result'][$x]['result_status']}}</label></td>
						<td><label >{{$rh['result'][$x]['remark_result']}} <br> {{$rh['result'][$x]['comment']}}</label></td>
					</tr>

					@if($finRest=='PASS' && $rh['result'][$x]['result_status']=='FAILED')
						@php($finRest='FAILED')
					@elseif($finRest=='PASS' && $rh['result'][$x]['result_status']=='PASS')
						@php($finRest='PASS')
					@elseif($finRest=='FAILED')
						@php($finRest='FAILED')
					@endif
				@endfor
				
			
		</table>
		<br>
	@endforeach


	<table class="head" style="text-align:center;">
		<tr>
			<td width="50%"><label style="font-weight:bold; font-size:16px;">RESULT</label></td>
			<td width="50%">
				<label style="font-weight:bold; font-size:16px; color: {{$finRest=='FAILED' ? '#e60000' : '#000000'}} ">{{$finRest}}</label>

				@if(isset($escl)!=null)
					<label style="font-weight:bold; font-size:16px; color: {{strtoupper($escl->escalation)=='FAILED' ? '#e60000' : '#000000'}} ">( {{strtoupper($escl->escalation)}} )</label>
				@endif
				
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:left;">
				<label style="font-weight:bold;">Comment :</label><br>
				<label >{{$headdoc->remark_final}}

						@if(isset($escl)!=null)
							{{$escl->remark_escalation}}
						@endif

						@if(isset($remarkesc)!=null)
							{{$remarkesc}}
						@endif


				</label>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:right; height: 20px;">
				<label style="padding-right: 20px;">Semarang, {{date_format(date_create($headdoc->created_at),'d-M-Y')}}</label>
			</td>
		</tr>
		<tr>
			<td>
				<label style="font-weight:bold; font-size:18px;">
					{{$headdoc->posttech}}
				</label>
			</td>
			<td>
				<label style="font-weight:bold; font-size:18px;">
					{{$headdoc->posthead}}
				</label>
			</td>
		</tr>
		<tr>
			<td height="50px">
	            @php($pathtch = 'signature/'.$headdoc->niktech.'.png')

	            @if(file_exists(public_path($pathtch)))
	                <center><img src="{{ url($pathtch) }}" style="height:50px; width: auto;"></center>
	            @endif
				
			</td>
			<td height="50px">
				@php($pathhd = 'signature/'.$headdoc->nikhead.'.png')

	            @if(file_exists(public_path($pathhd)))
	                <center><img src="{{ url($pathhd) }}" style="height:50px; width: auto;"></center>
	            @endif
			</td>
		</tr>
		<tr>
			<td>
				<label style="font-weight:bold;">{{$headdoc->nametech}}</label>
			</td>
			<td>
				<label style="font-weight:bold;">{{$headdoc->namehead}}</label>
			</td>
		</tr>
		
	</table>
	<div class="page-break"></div>
	@php($imgrs = $ls['imageRest'])
	@if(count($imgrs)>0)
		<table class="head">
		@foreach(array_chunk($imgrs->toarray(),2) as $im)
			{{-- <tr>
				<td width="50%">
					<label style="font-weight:bold;">{{isset($im[0]) ? $im[0]->img_number : null}}</label>
				</td>
				<td width="50%">
					<label style="font-weight:bold;">{{isset($im[1]) ? $im[1]->img_number : null}}</label>
				</td>
			</tr> --}}
			<tr>
				<td width="50%">
					<center>
						@if(isset($im[0]))
							@php($img0='imagespc/'.$im[0]->id.'.png')
							<img src="{{ url($img0) }}" width="350px" style="margin:5px;">
						@endif
					</center>
				</td>
				<td width="50%">
					<center>
						@if(isset($im[1]))
							@php($img1='imagespc/'.$im[1]->id.'.png')
							<img src="{{ url($img1) }}" width="350px" style="margin:5px;">
						@endif
					</center>
				</td>
				<tr>
					<td width="50%">
						<label style="font-weight:bold;">{{isset($im[0]) ? $im[0]->remark : null}}</label>
					</td>
					<td width="50%">
						<label style="font-weight:bold;">{{isset($im[1]) ? $im[1]->remark : null}}</label>
					</td>
				</tr>
			</tr>


		@endforeach
		</table>
		<div class="page-break"></div>
	@endif

@endforeach