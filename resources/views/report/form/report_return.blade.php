<style type="text/css">
    .table{
        border-collapse: collapse;
        width: 100%;
    }
    
    .table > thead tr th{
        border: 1px solid black;
        padding-left: 10px;
        padding-right: 10px;
        font-size: 8px;
        text-align: center;
    }
    
    .table > tbody tr td{
        border: 1px solid black;
        padding-left: 10px;
        padding-right: 10px;
        font-size: 8px;
    }
    
    
    </style>
    <table style="width: 100%">
        <tr>
            <td><h3><center>REPORT RETURN SAMPLE</center></h3></td>
        </tr>
        <tr>
            <td><center>{{$factory_name}} - {{$asal}}</center></td>
        </tr>
        <tr>
            <td><center>{{$buyer}} - {{$submitdate[0]}} - {{$submitdate[1]}}</center></td>
        </tr>
    </table>
    <hr>
    <table class="table" width="100%">
        <thead >
            <tr>
                <th><center><b>No.</b></center></th>
                <th><center><b>TRF NO.</b></center></th>
                <th><center><b>CATEGORY</b></center></th>
                <th><center><b>SPECIMEN</b></center></th>
                <th><center><b>BUYER</b></center></th>
                <th><center><b>LAB. LOCATION / ORIGIN</b></center></th>
                <th><center><b>SUBMIT BY</b></center></th>
                <th><center><b>TESTED BY</b></center></th>
                <th><center><b>STATUS </b></center></th>
                <th><center><b>RETURN STATUS</b></center></th>
            </tr>
        </thead>
        <tbody>
            {{-- @php(dd($list)) --}}
            @php($i=1)
            @foreach($list as $ls)
                <tr>
                    <td>{{$i++}}</td>
                    <td>
                        <label style="font-weight: bold;">{{$ls->trf_id}}</label><br>
                        <label>Submit date : {{ date_format(date_create($ls->created_at),'d-M-Y H:i:s')}}</label>
                    </td>
                    <td>
                        <label style="font-weight:bold"> Category : </label> {{$ls->category}}<br>
                        <label style="font-weight:bold"> Category Specimen : </label>{{$ls->category_specimen}} <br>
                        <label style="font-weight:bold"> Type Specimen : </label> {{$ls->type_specimen}}
                    </td>
                    <td>
                        @php($trfdoc = DB::table('trf_testing_document')->where('trf_id',$ls->id)->whereNull('deleted_at')->first())

                        @if($trfdoc->document_type=="MO")
                                <label> <b>No. MO  : </b></label>{{$trfdoc->document_no}} <br>
                                <label> <b>Style : </b></label>{{$trfdoc->style}} <br>
                                <label> <b>Article : </b></label>{{$trfdoc->article_no}} <br>
                                <label> <b>Size : </b></label>{{$trfdoc->size}} <br>
                                <label> <b>Color : </b></label>{{$trfdoc->color}} <br>
                                <label> <b>Item : </b></label>{{$trfdoc->item}} <br>

                        @elseif($trfdoc->document_type=="PO SUPPLIER")
                                <label> <b>PO Supplier : </b></label>{{$trfdoc->document_no}} <br>
                                <label> <b>Item : </b></label>{{$trfdoc->item}} <br>
                                <label> <b>Supplier : </b></label>{{$trfdoc->manufacture_name}} <br>
                                <label> <b>No. Roll : </b></label>{{$trfdoc->nomor_roll}} <br>
                                <label> <b>Bacth No. : </b></label>{{$trfdoc->batch_number}} <br>
                                <label> <b>Barcode Supplier : </b></label>{{$trfdoc->barcode_supplier}} <br>
                        
                        @elseif($trfdoc->document_type=="ITEM")
                                <label> <b>Item : </b></label>{{$trfdoc->document_no}} <br>
                                <label> <b>Style : </b></label>{{$trfdoc->style}} <br>
                                <label> <b>Article : </b></label>{{$trfdoc->article_no}} <br>
                                <label> <b>Size : </b></label>{{$trfdoc->size}} <br>
                                <label> <b>Color : </b></label>{{$trfdoc->size}} <br>


                        @elseif($trfdoc->document_type=="PO BUYER")
                                <label> <b>PO Buyer : </b></label>{{$trfdoc->document_no}} <br>
                                <label> <b>Style : </b></label>{{$trfdoc->style}} <br>
                                <label> <b>Article : </b></label>{{$trfdoc->article_no}} <br>
                                <label> <b>Size : </b></label>{{$trfdoc->size}} <br>
                                <label> <b>Color : </b></label>{{$trfdoc->size}} <br>
                                <label> <b>Item : </b></label>{{$trfdoc->item}} <br>
                                <label> <b>Destination : </b></label>{{$trfdoc->export_to}} <br>
                        
                        @endif
                       
                    </td>
                    <td>{{$ls->buyer}}</td>
                    <td>{{$ls->lab_location}} <br> {{$ls->asal_specimen}}</td>
                    <td>{{$ls->username}}<br> ( {{$ls->nik}} )</td>
                    <td>{{$ls->tested_name}}<br> ( {{$ls->tested_nik}} )</td>
                    <td>
                        <label style="font-weight: bold">{{$ls->status}}</label>
                    </td>
                    <td>
                        @if($ls->return_date==null)
                            <label style="font-weight: bold"> Not Yet Returned</label>
                        @else
                            <label style="font-weight: bold"> Returned at : </label>{{date_format(date_create($ls->return_date),"Y-m-d")}} <br>
                            <label style="font-weight: bold"> By : </label>{{$ls->user_name}} - {{$ls->user_nik}}
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>