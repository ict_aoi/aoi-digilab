
<table class="head">
	<tr>
		<td colspan="6"><label style="font-weight:bold;font-size: 18px;">Specimen Identity {{isset($inc) ? $inc : '' }}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style</label></td> 
		<td><label>{{$idt->style}}</label></td>

		<td><label style="font-weight: bold;">Style Name</label></td> 
		<td><label>{{$idt->style_name}}</label></td>

		<td><label style="font-weight: bold;">Badge Description</label></td> 
		<td><label>{{$idt->description}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Article</label></td> 
		<td><label>{{$idt->article_no}}</label></td>

		<td><label style="font-weight: bold;">Item Badge</label></td> 
		<td><label>{{$idt->item}}</label></td>

		<td><label style="font-weight: bold;">Fabric Color</label></td> 
		<td><label>{{$idt->fabric_color}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Season</label></td> 
		<td><label>{{$idt->season}}</label></td>

		<td><label style="font-weight: bold;">Color Badge</label></td> 
		<td><label>{{$idt->color}}</label></td>

		<td><label style="font-weight: bold;">Fabric Material</label></td> 
		<td><label>{{$idt->fibre_composition}}</label></td>
	</tr>
	
</table>