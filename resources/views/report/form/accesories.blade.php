
<table class="head">
	<tr>
		<td colspan="6"><label style="font-weight:bold;font-size: 18px;">Specimen Identity {{isset($inc) ? $inc : '' }}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">PO Buyer</label></td> 
		<td><label>{{$idt->document_no}}</label></td>

		<td><label style="font-weight: bold;">Color Acc</label></td> 
		<td><label>{{$idt->color}}</label></td>

		<td><label style="font-weight: bold;">Date Finished</label></td> 
		<td><label>{{isset($idt->closed_at) ? date_format(date_create($idt->closed_at),'d-m-Y') :null}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">PO Supplier</label></td> 
		<td><label>{{$idt->barcode_supplier}}</label></td>

		<td><label style="font-weight: bold;">Item Acc</label></td> 
		<td><label>{{$idt->item}}</label></td>

		<td><label style="font-weight: bold;">Date Received</label></td> 
		<td><label>{{date_format(date_create($idt->verified_lab_date),'d-m-Y')}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Style</label></td> 
		<td><label>{{$idt->style}}</label></td>

		<td><label style="font-weight: bold;">Qty</label></td> 
		<td><label>{{$idt->qty}}</label></td>

		<td><label style="font-weight: bold;">Supplier Acc</label></td> 
		<td><label>{{$idt->manufacture_name}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Article</label></td> 
		<td><label>{{$idt->article_no}}</label></td>

		<td rowspan="2" style="font-weight: bold;"><label >Fabric Item</label></td> 
		<td rowspan="2"><label>{{$idt->fabric_item}}</label></td>

		<td rowspan="2"><label style="font-weight: bold;">Fabric Color</label></td> 
		<td rowspan="2"><label>{{$idt->fabric_color}}</label></td>
	</tr>

	<tr>
		<td><label style="font-weight: bold;">Season</label></td> 
		<td><label>{{$idt->season}}</label></td>
	</tr>

</table>