@extends('layouts.app', ['active' => 'menu-method-result'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Method Result</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Method Result</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				<div class="row">

					<form action="{{ route('report.methodresult.getMethodResult') }}" id="form-search" method="GET">
						<div class="col-lg-2">
							<label><b>Factory</b></label>
							<select id="factory_id" class="select">
								<option value="ALL">ALL</option>
								@foreach($factory as $fact)
									<option value="{{$fact->id}}">{{$fact->factory_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2">
							<label><b>Origin</b></label>
							<select id="origin" class="select">
								<option value="ALL">ALL</option>
								<option value="DEVELOPMENT">DEVELOPMENT</option>
								<option value="PRODUCTION">PRODUCTION</option>
							</select>
						</div>
						<div class="col-lg-2">
							<label><b>Lab Location</b></label>
							<select id="labloct" class="select">
								<option value="ALL">ALL</option>
								@foreach($lab as $lb)
									<option value="{{$lb->name}}">{{$lb->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-5">
							<label><b>Submit Date</b></label>
							<input type="text" name="submitdate" id="submitdate" class="form-control daterange-basic">
						</div>
						<div class="col-lg-1">
							<button type="submit" class="btn btn-primary" style="margin-top:27px;">Search</button>
						</div>
					</form>
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>Submit Date</th>
									<th>TRF No.</th>
									<th>Lab Location</th>
									<th>Origin</th>
									<th>Buyer</th>
									<th>Method</th>
									<th>Tested By</th>
									<th>Status</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('report.methodresult.exportResultMethod') }}" id="exportExcel"></a>
@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script>

$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
		buttons:[
			{
				text:'Export',
				className : 'btn btn-success btn-export',
				action : function(e,dt,node,config){
					var filter = table.search();

					var url= $('#exportExcel').attr('href')+'?factory_id='+$('#factory_id').val()+'&origin='+$('#origin').val()+'&labloct='+$('#labloct').val()+'&submitdate='+$('#submitdate').val();

					window.open(url);
				}
			}
		],
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                'factory_id' : $('#factory_id').val(),
	                'origin' : $('#origin').val(),
	                'labloct' : $('#labloct').val(),
	                'submitdate' : $('#submitdate').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'created_at', name: 'created_at'},
	        {data: 'trf_id', name: 'trf_id'},
	        {data: 'lab_location', name: 'lab_location'},
	        {data: 'asal_specimen', name: 'asal_specimen'},
	        {data: 'buyer', name: 'buyer'},
	        {data: 'method_code', name: 'method_code'},
			{data: 'pic', name: 'pic'},
	        {data: 'result_status', name: 'result_status'}
	    ]
    });

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#factory_id').change(function(){
		table.clear();
		table.draw();
	});

	$('#origin').change(function(){
		table.clear();
		table.draw();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#labloct').change(function(){
		table.clear();
		table.draw();
	});


});

</script>
@endsection