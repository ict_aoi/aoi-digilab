@extends('layouts.app', ['active' => 'menu-all-specimen'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">All Specimen</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp All Specimens</span> </h4>
				</div>
			</div>
			<div class="panel-body">
				<form action="{{ route('report.allspecimen.getDataAllspc')}}" id="form-search" method="GET">
				<div class="row">
					<div class="col-lg-2">
						<label><b>Factory</b></label>
						<select id="factory_id" class="select">
							<option value="ALL">ALL</option>
							@foreach($fact as $ft)
							<option value="{{$ft->id}}">{{$ft->factory_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-2">
						<label><b>Origin</b></label>
						<select id="origin" class="select">
							<option value="ALL">ALL</option>
							<option value="DEVELOPMENT">DEVELOPMENT</option>
							<option value="PRODUCTION">PRODUCTION</option>
						</select>
					</div>
					<div class="col-lg-3">
						<label><b>Buyer</b></label>
						<select id="buyer" class="select">
							<option value="ALL">ALL</option>
							@foreach($buyer as $by)
							<option value="{{$by->buyer}}">{{$by->buyer}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-5">
						<label><b>Submit Date</b></label>
						<input type="text" name="submitdate" id="submitdate" class="form-control daterange-basic">
					</div>
					<!-- <div class="col-lg-1">
						<button type="submit" class="btn btn-primary" style="margin-top:27px;">Search</button>
					</div> -->
				</div>
				<div class="row" style="margin-top:18px;">
					<div class="col-lg-4">
						<label style="font-weight: bold;" class="display-block">Category</label>
						<select id="category" class="select" onchange="getCtgSpm(this);">
							<option value="">--Choose Category--</option>
							<option value="HYBRID/PERFORMANCE">HYBRID/PERFORMANCE</option>
							<option value="FASHION/CASUAL">FASHION/CASUAL</option>
							<option value="FASHION/CASUAL - HYBRID/PERFORMANCE">FASHION/CASUAL - HYBRID/PERFORMANCE</option>
						</select>
					</div>
					<div class="col-lg-4">
						<label style="font-weight: bold;" class="display-block">Category Specimen</label>
						<select class="select" id="category_specimen" onchange="getType(this);">
						</select>
					</div>
					<div class="col-lg-4">
						<label style="font-weight: bold;" class="display-block">Type Specimen</label>
						<select class="select" id="type_specimen">
						</select>
					</div>
				</div>
				</form>

				<div class="row form-group">
					<div class="table-responsive">
						<table class="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>TRF No.</th>
									<th>UserName</th>
									<th>Lab Location</th>
									<th>Specimen Origin</th>
									<th>Buyer</th>
									<th>Status</th>
									<th>Category</th>
									<th>Specimen</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('report.allspecimen.exportAllspc') }}" id="ExportExcel"></a>
@endsection


@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$.extend($.fn.dataTable.defaults, {
			stateSave: true,
			autoWidth: false,
			autoLength: false,
			processing: true,
			serverSide: true,
			dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
			language: {
				search: '<span>Filter:</span> _INPUT_',
				searchPlaceholder: 'Type to filter...',
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			}
		});

		var _token = $("input[name='_token']").val();
		// var table = $('#table-list').DataTable({
		// 	buttons: [{
		// 		text: 'Export',
		// 		className: 'btn btn-sm bg-success',
		// 		action: function(e, dt, node, config) {
		// 			var filter = table.search();

		// 			var url = $('#ExportExcel').attr('href') + '?factory_id=' + $('#factory_id').val() + '&origin=' + $('#origin').val() + '&buyer=' + $('#buyer').val() + '&submitdate=' + $('#submitdate').val() + '&filter=' + filter;

		// 			window.open(url);
		// 		}
		// 	}],
		// 	ajax: {
		// 		type: 'GET',
		// 		url: $('#form-search').attr('action'),
		// 		data: function(d) {
		// 			return $.extend({}, d, {
		// 				'factory_id': $('#factory_id').val(),
		// 				'origin': $('#origin').val(),
		// 				'buyer': $('#buyer').val(),
		// 				'submitdate': $('#submitdate').val()
		// 			});
		// 		}
		// 	},
		// 	fnCreatedRow: function(row, data, index) {
		// 		var info = table.page.info();
		// 		var value = index + 1 + info.start;
		// 		$('td', row).eq(0).html(value);
		// 	},
		// 	columnDefs: [{
		// 		className: 'dt-center'
		// 	}],
		// 	columns: [{
		// 			data: null,
		// 			sortable: false,
		// 			orderable: false,
		// 			searchable: false
		// 		},
		// 		{
		// 			data: 'trf_id',
		// 			name: 'trf_id'
		// 		},
		// 		{
		// 			data: 'username',
		// 			name: 'username'
		// 		},
		// 		{
		// 			data: 'lab_location',
		// 			name: 'lab_location'
		// 		},
		// 		{
		// 			data: 'asal_specimen',
		// 			name: 'asal_specimen'
		// 		},
		// 		{
		// 			data: 'buyer',
		// 			name: 'buyer'
		// 		},
		// 		{
		// 			data: 'status',
		// 			name: 'status'
		// 		},
		// 		{
		// 			data: 'category',
		// 			name: 'category'
		// 		},
		// 		{
		// 			data: 'specimen',
		// 			name: 'specimen'
		// 		},

		// 	]
		// });

		var table = $('#table-list').DataTable({
			buttons: [{
				text: 'Export',
				className: 'btn btn-sm bg-success',
				action: function(e, dt, node, config) {
					var filter = table.search();
					var url = $('#ExportExcel').attr('href') + '?factory_id=' + $('#factory_id').val() +
						'&origin=' + $('#origin').val() + 
						'&buyer=' + $('#buyer').val() + 
						'&submitdate=' + $('#submitdate').val() + 
						'&category=' + $('#category').val() + 
						'&category_specimen=' + $('#category_specimen').val() +
						'&type_specimen=' + $('#type_specimen').val() + 
						'&filter=' + filter;

					window.open(url);
				}
			}],

			ajax: {
				type: 'GET',
				url: $('#form-search').attr('action'),
				data: function(d) {
					return $.extend({}, d, {
						'factory_id': $('#factory_id').val(),
						'origin': $('#origin').val(),
						'buyer': $('#buyer').val(),
						'submitdate': $('#submitdate').val(),
						'category': $('#category').val(),
						'category_specimen': $('#category_specimen').val(),
						'type_specimen': $('#type_specimen').val()
					});
				}
			},
			fnCreatedRow: function(row, data, index) {
				var info = table.page.info();
				var value = index + 1 + info.start;
				$('td', row).eq(0).html(value);
			},
			columnDefs: [{
				className: 'dt-center'
			}],
			columns: [
				{ data: null, sortable: false, orderable: false, searchable: false },
				{ data: 'trf_id', name: 'trf_id' },
				{ data: 'username', name: 'username' },
				{ data: 'lab_location', name: 'lab_location' },
				{ data: 'asal_specimen', name: 'asal_specimen' },
				{ data: 'buyer', name: 'buyer' },
				{ data: 'status', name: 'status' },
				{ data: 'category', name: 'category' },
				{ data: 'specimen', name: 'specimen' },
			]
		});

		function reloadTableOnChange() {
			table.clear();
			table.draw();
		}

		$('#factory_id, #origin, #buyer, #submitdate').change(reloadTableOnChange);
		$('#category, #category_specimen, #type_specimen').change(reloadTableOnChange);
		$('#form-search').submit(function(event) {
			event.preventDefault();
			reloadTableOnChange();
		});


		table.on('preDraw', function() {
				loading();
				Pace.start();
			})
			.on('draw.dt', function() {
				$.unblockUI();
				Pace.stop();
			});

		$('#factory_id').change(function() {
			table.clear();
			table.draw();
		});

		$('#origin').change(function() {
			table.clear();
			table.draw();
		});

		$('#form-search').submit(function(event) {
			event.preventDefault();

			table.clear();
			table.draw();
		});

		$('#buyer').change(function() {
			table.clear();
			table.draw();
		});


	});

	function getCtgSpm(e) {
		var category = e.value;
		console.log(category);
		
		var buyer = $('#buyer').val();
		var origin = $('#origin').val();

		if (!category) {
        	alert("Please select a valid category!");
			return;
		}

		$('#category_specimen').empty().append('<option value="">--Choose Category Specimen--</option>');

		$.ajax({
			type: 'get',
			url: "{{ route('report.allspecimen.getCategorySpc') }}",
			data: {
				buyer: buyer,
				category: category,
				origin: origin
			},
			beforeSend: function() {
				loading();
			},
			success: function(response) {
				let data = response.data; 
				for (let i = 0; i < data.length; i++) {
					$('#category_specimen').append('<option value="'+data[i]['category_specimen']+'">'+data[i]['category_specimen']+'</option>');
				}
				$.unblockUI();
			},
			error: function(response) {
				$.unblockUI();
				alert("Error loading category specimens. Please try again.");
			}
		});
	}

	function getType(e) {
    var category_specimen = e.value;
    var category = $('#category').val();
    var buyer = $('#buyer').val();

    if (!category_specimen || !category || !buyer) {
        alert("Please ensure all required fields are filled!");
        return;
    }

    $('#type_specimen').empty().append('<option value="">--Choose Type Specimen--</option>');

    $.ajax({
        type: 'get',
        url: "{{ route('report.allspecimen.getTypeSpc') }}",
        data: {
            buyer: buyer,
            category: category,
            category_specimen: category_specimen
        },
        beforeSend: function() {
            loading();
        },
        success: function(response) {
            if (response && response.length) {
                response.forEach(function(item) {
                    $('#type_specimen').append('<option value="' + item.type_specimen + '">' + item.type_specimen + '</option>');
                });
            } else {
                alert("No data found for the selected category specimen.");
            }

            $.unblockUI();
        },
        error: function(xhr) {
            $.unblockUI();

            let errorMessage = xhr.responseJSON?.message || "An error occurred. Please try again.";
            console.error("Error:", errorMessage);
            alert(errorMessage);
        }
    });
}


</script>
@endsection