@extends('layouts.app', ['active' => 'menu-report-specimen'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Report Specimen</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
            <div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Report Test</span> </h4>
				</div>
				
            </div>
			<div class="panel-body">
				<div class="row">
					<form action="{{ route('report.speciment.getDataReportSpecimen') }}" id="form-search" method="GET">
						<div class="col-md-2">
							<label><b>Factory Origin</b></label>
							<select class="select" id="factory_id" {{ $crl['fact'] }}>
								@foreach($factory as $fc)
									<option value="{{$fc->id}}" {{$fc->id==auth::user()->factory_id ? 'selected' :''}}>{{$fc->factory_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<label><b>Lab Location</b></label>
							<select class="select" id="labloc" {{ $crl['lab'] }}>

								@if(auth::user()->factory_id==1)
									@php($slab='AOI1')
								@elseif(auth::user()->factory_id==2)
									@php($slab='AOI2')
								@elseif(auth::user()->factory_id==3)
									@php($slab='AOI3')
								@elseif(auth::user()->factory_id==4)
									@php($slab='DEVELOPMENT CENTER')
								@else
									@php($slab='')
								@endif

								@foreach($labloc as $lc)
									<option value="{{$lc->name}}" {{$lc->name==$slab ? 'selected' : ''}}>{{$lc->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-7">
							<label><b>Search</b></label>
							<input type="text" name="txkey" id="txkey" class="form-control" placeholder="TRF No. / Buyer/ Document No. / Style / Article / Lab Location">
						</div>
						<div class="col-md-1">
							<button style="margin-top: 30px;" class="btn btn-primary" id="btn-search" type="submit">Search</button>
						</div>
					</form>
					
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>TRF No.</th>
									<th>Specimen</th>
									<th>Method</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_user" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
        	<form action="{{ route('report.specimen.setSignature') }}" id="form-setTech">
            	<div class="modal-header">
	                <center><h4>SET TECHNICIAN PRINT REPORT</h4></center>
	            </div>
	            <div class="modal-body">
	            	
	            	<div class="row">
	            		<div class="col-md-6">
	            			<label><b>Technician</b></label>
	            			<select class="select md-tech" id="md-tech">
	            			</select>
	            		</div>
	            		<div class="col-md-6">
	            			<label><b>Sub. Dept. Head</b></label>
	            			<select class="select md-head" id="md-head">
	            			</select>
	            		</div>
	            	</div>
	            	<div class="row">
	            		<div class="col-md-12">
	            			<label><b>Remark Test</b></label>
	            			<textarea class="form-control md-remark" id="md-remark"></textarea>
	            		</div>
	            	</div>
	            	<input type="hidden" class="txtrfid" name="txtrfid">
	            	<input type="hidden" class="txdocid" name="txdocid">
	            </div>
	            <div class="modal-footer">
	            	<button class="btn btn-success" type="submit">Print</button>
	            </div>
            </form>
            
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">

$(document).ready(function(){
	
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                'factory_id' : $('#factory_id').val(),
	                'lab_location':$('#labloc').val(),
	                'key' : $('#txkey').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id'},
	        {data: 'specimen', name: 'specimen'},
	        {data: 'method', name: 'method'},
	        {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
	    ]
    });


	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#factory_id').change(function(){
		table.clear();
		table.draw();
	});

	$('#labloc').change(function(){
		table.clear();
		table.draw();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#table-list').on('click','.setPrintReportSpc',function(event){
		event.preventDefault();

		var id = $(this).data('id');
		var doc = $(this).data('doc');
		var tech = $(this).data('tech');

		$.ajax({
            type: 'get',
            url : "{{ route('report.specimen.getTech') }}",
            data:{tech:tech},
            success: function(response) {
            	var tech = response.tech;
            	$('#modal_user .md-tech').empty();
            	for (var t = 0; t < tech.length; t++) {
            		$('#modal_user .md-tech').append('<option value="'+tech[t]['id']+'">'+tech[t]['name']+' - '+tech[t]['nik']+'</option>');
            	}

            	var head = response.head;
            	$('#modal_user .md-head').empty();
            	for (var h = 0; h < head.length; h++) {
            		$('#modal_user .md-head').append('<option value="'+head[h]['id']+'">'+head[h]['name']+' - '+head[h]['nik']+' - '+head[h]['position']+'</option>');
            	}

            	$('#modal_user .txtrfid').val(id);
            	$('#modal_user .txdocid').val(doc);

            	$('#modal_user').modal('show');
            },
            error: function(response) {
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });
	});

	$('#form-setTech').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form-setTech').attr('action'),
            data:{
            	type : 'report_specimen',
            	trfid:$('#modal_user .txtrfid').val(),
            	docid:$('#modal_user .txdocid').val(),
            	tech:$('#modal_user .md-tech').val(),
            	head:$('#modal_user .md-head').val(),
            	remark_final:$('#modal_user .md-remark').val()
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                var dresp = response.data_response;

                if (dresp.status==200) {
                	alert(dresp.status,dresp.output);
                	window.open("{{route('report.specimen.printReportSpecimen')}}?id="+$('#modal_user .txtrfid').val()+"&docid="+$('#modal_user .txdocid').val());
                }else{
                	alert(dresp.status,dresp.output);
                }
            },
            error: function(response) {
                $.unblockUI();
                var notif = response.data;
                alert(notif.status,notif.output);

            }
        });

        $('#modal_user').modal('hide');
	});


	$('#table-list').on('click','.PrintReportSpc',function(){
		var trfid = $(this).data('id');
		var docid = $(this).data('doc');
	

		

		window.open("{{route('report.specimen.printReportSpecimen')}}?id="+trfid+"&docid="+docid);
	});
});






</script>
@endsection