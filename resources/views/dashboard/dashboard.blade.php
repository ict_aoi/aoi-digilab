@extends('layouts.app', ['active' => 'dashboard'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>

</div>
@endsection

@section('content')
<div class="content">
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="page-header-content">
			<div class="page-title">
				<h4></i> <span class="text-semibold">&nbsp &nbsp DASHBOARD</span> </h4>
			</div>
        </div>
		<div class="panel-body">
			<div class="row form-group">
				<form id="form_filter" action="{{ route('home.getData') }}">
					@csrf
					<label><b>Search In Here</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control" name="txkey" id="txkey" placeholder="TRF No. / Origin / Buyer / Status / Category / Username / Method Code / Style / Article">
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
				</form>
			</div>
			<div class="row form-group">
				<div class="row  {{Auth::user()->admin === false ? 'hidden' : '' }}">
					<div class="col-md-2">
						<label><b>Lab Location</b></label>
						<select class="select" id="lablocation">
                            @php($userid= Auth::user()->id)

							@foreach($labloc as $fc)
								{{-- <option value="{{$fc->id}}" {{$fc->id==auth::user()->factory_id ? 'selected' : ''}}>{{$fc->factory_name}}</option>
                                 --}}

                                 <option value="{{$fc->name}}" {{  ($fc->id==auth::user()->factory_id) ? 'selected' : ''}}>{{$fc->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row table-responsive">
					<table class="table table-basic table-condensed" id="table-list">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No TRF</th>
                                <th>Status</th>
                                <th>Origin of Specimen</th>
                                <th>Test Required</th>
                                <th>Date Information</th>
                                <th>Buyer</th>
                                <th>Lab Location</th>
                                <th>Category</th>
                                <th>Methods</th>
                                <th>Return Test Sample </th>
                                <th>Due Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){

	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: $('#form_filter').attr('action'),
            data: function (d) {
	            return $.extend({},d,{
	                'lablocation' : $('#lablocation').val(),
	                'key' : $('#txkey').val()
	            });
	        }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id',searchable:true, orderable:true},
            {data: 'status', name: 'status',searchable:true, orderable:true},
            {data: 'asal_specimen', name: 'asal_specimen',searchable:true, orderable:true},
            {data: 'test_required', name: 'test_required',searchable:false, orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false, orderable:true},
            {data: 'buyer', name: 'buyer',searchable:false, orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false, orderable:true},
            {data: 'category', name: 'category',searchable:false, orderable:true},
            {data: 'method_code', name: 'method_code',searchable:false, orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false, orderable:true},
            {data: 'due_date', name: 'due_date',searchable:true, orderable:true},
            {data: 'action', name: 'action',searchable:true, orderable:true},
        ]
    });


    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#lablocation').change(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	})

    $('#form_filter').submit(function(event){
    	event.preventDefault();

    	table.clear();
    	table.draw();
    });

});

</script>
@endsection