@extends('layouts.app', ['active' => 'approval_trf'])
@section('header')
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
            <li class="active">Result Approval TRF</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class=" panel panel-flat">
            <div class="page-header-content">
                <div class="page-title">
                    <h4></i> <span class="text-semibold">&nbsp &nbsp TRF RESULT LIST</span> </h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="table-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No TRF</th>
                                    <th>Origin of Specimen</th>
                                    <th>Test Required</th>
                                    <th>Date Information</th>
                                    <th>Buyer</th>
                                    <th>Lab Location</th>
                                    <th>Category</th>
                                    <th>Return Test Sample </th>
                                    <th>Test PIC</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

@endsection

@section('modal')
<div id="modal_appv" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <center><h4>Release Report</h4></center>
            </div>
            <div class="modal-body">
                <form action="{{ route('trfApp.techRelease') }}" id="form-mdaction">
                    <div class="row">
                        <div class="col-md-4">
                            <label><b>TRF No.</b></label>
                            <input type="text" name="trfno" id="trfno" class="trfno form-control" readonly>
                            <input type="hidden" name="trfid" id="trfid" class="trfid">
                        </div>
                        <div class="col-md-4">
                            <label><b>Origin</b></label>
                            <input type="text" name="asal" id="asal" class="asal form-control" readonly>
                        </div>
                        <div class="col-md-4">
                            <label><b>Buyer</b></label>
                            <input type="text" name="buyer" id="buyer" class="buyer form-control" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label><b>PIC TEST</b></label>
                            <select class="select mdpic" id="mdpic"></select>
                        </div>
                        <div class="col-md-4">
                            <label><b>Remark</b></label>
                            <textarea class="form-control mdremark" id="mdremark"></textarea>
                        </div>
                        <div class="col-md-4">
                            <label><b>Status</b></label>
                            <select class="select mdstatus" id="mdstatus">
                                <option value="CLOSED" data-rest="PASS">PASS</option>
                                <option value="CLOSED" data-rest="FAIL">FAIL</option>
                                <option value="ESCALATION I" data-rest="">ESCALATION</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <center>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </center>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: "{{ route('trfApp.getData') }}",
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id',searchable:true, orderable:true},
            {data: 'asal_specimen', name: 'asal_specimen',searchable:true, orderable:true},
            {data: 'test_required', name: 'test_required',searchable:false, orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false, orderable:true},
            {data: 'buyer', name: 'buyer',searchable:false, orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false, orderable:true},
            {data: 'category', name: 'category',searchable:false, orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false, orderable:true},
            {data: 'pic_test', name: 'pic_test',searchable:true, orderable:true},
            {data: 'action', name: 'action',searchable:true, orderable:true},
        ]
    });

    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });
    
    $('#table-list').on('click','.trfmngapv',function(event){
        event.preventDefault();

        var id  = $(this).data('id');
        var no  = $(this).data('no');
        var asal  = $(this).data('asal');
        var buyer  = $(this).data('buyer');
        getTech(id);
        $('#modal_appv .trfno').val(no);
        $('#modal_appv .asal').val(asal);
        $('#modal_appv .buyer').val(buyer);
        $('#modal_appv .trfid').val(id);
        $('#modal_appv .mdstatus option[data-rest="PASS"]').attr("selected","selected").trigger('change');
        $('#modal_appv').modal('show');

        

    });

    $('#form-mdaction').submit(function(event){
        event.preventDefault();

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url :$('#form-mdaction').attr('action'),
                data:{
                        id:$('#modal_appv .trfid').val(),
                        pic_id:$('#modal_appv .mdpic').val(),
                        remark:$('#modal_appv .mdremark').val(),
                        status:$('#modal_appv .mdstatus').val(),
                        fin_rest:$('#modal_appv .mdstatus').find(':selected').data('rest')
                    },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    $.unblockUI();
                    
                    var notif = response.data;
                    if (notif.status==200) {
                       alert(notif.status,notif.output); 
                       window.location.reload();
                   }else{
                        alert(notif.status,notif.output);
                   }
                    

                },
                error: function(response) {
                 

                   alert(response['status'],response['responseJSON']['message']);

                }
            });
    });
    
});

function getTech(idtrf){
    $.ajax({
        type: 'get',
        url :"{{ route('trfApp.getTech')}}",
        data :{idtrf:idtrf} ,
        success: function(response) {
            var dtx = response.data;

            $('#modal_appv .mdpic').empty();
            // $('#modal_appv .mdpic').append('<option value="36" data-nik="190500113">NAUFAL SETIAWAN</option>');
            for (var i = 0; i < dtx.length; i++) {
                // console.log(dtx[i]['name']);
                $('#modal_appv .mdpic').append('<option value="'+dtx[i]['user_id']+'" data-nik="'+dtx[i]['nik']+'">'+dtx[i]['name']+'</option>');
            }
             
        },
        error: function(response) {
            console.log(response);
        }
            
    });
}

</script>
@endsection
