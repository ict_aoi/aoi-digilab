@extends('layouts.app', ['active' => 'menu-escalation-trf'])
@section('header')
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
            <li class="active">Result Approval TRF</li>
        </ul>
    </div>
</div>
@endsection

@section('content')

<div class="content">
    <div class="row">
        <div class=" panel panel-flat">
            <div class="page-header-content">
                <div class="page-title">
                    <h4></i> <span class="text-semibold">&nbsp &nbsp TRF ESCALATION LIST</span> </h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="row {{$escty}}">
                    <div class="col-md-4">
                        <label><b>Escalation Level</b></label>
                        <select class="select" id="escLavel">
                            <option value="ESCALATION I" {{$esclv=="ESCALATION I" ? 'selected' : ''}}>ESCALATION I</option>
                            <option value="ESCALATION II" {{$esclv=="ESCALATION II" ? 'selected' : ''}}>ESCALATION II</option>
                        </select>
                    </div>
                    
                </div>
                <div class="row form-group">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="table-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No TRF</th>
                                    <th>Origin of Specimen</th>
                                    <th>Test Required</th>
                                    <th>Date Information</th>
                                    <th>Buyer</th>
                                    <th>Lab Location</th>
                                    <th>Category</th>
                                    <th>Methods</th>
                                    <th>Return Test Sample </th>
                                    <th>Test PIC</th>
                                    <th>Remark</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

@endsection

@section('modal')
<div id="modal_appv" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <center><h4>Release Report</h4></center>
            </div>
            <div class="modal-body">
                <form action="{{ route('trfApp.updateEscl') }}" id="form-mdaction">
                    <div class="row">
                        <div class="col-md-4">
                            <label><b>TRF No.</b></label>
                            <input type="text" name="trfno" id="trfno" class="trfno form-control" readonly>
                            <input type="hidden" name="trfid" id="trfid" class="trfid">
                            <input type="hidden" name="lavelesc" id="lavelesc" class="lavelesc">
                        </div>
                        <div class="col-md-4">
                            <label><b>Origin</b></label>
                            <input type="text" name="asal" id="asal" class="asal form-control" readonly>
                        </div>
                        <div class="col-md-4">
                            <label><b>Buyer</b></label>
                            <input type="text" name="buyer" id="buyer" class="buyer form-control" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 techarea" id="techarea">
                            <div class="row">
                                <label><b>Technician</b></label>
                                <input type="text" name="mdtech" id="mdtech" class="form-control mdtech" readonly>
                            </div>
                            <div class="row">
                                <textarea class="form-control mdremtech" readonly></textarea>
                            </div>
                        </div>

                        <div class="col-md-6 labarea" id="labarea">
                            <div class="row">
                                <label><b>Lab Dept. Head</b></label>
                                <input type="text" name="mdlab" id="mdlab" class="form-control mdlab" readonly>
                            </div>
                            <div class="row">
                                <textarea class="form-control mdlabrem" readonly></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><b>Status</b></label>
                            <select id="mdstatus" class="select mdstatus"></select>
                        </div>
                        <div class="col-md-6">
                            <label><b>Remark</b></label>
                            <textarea class="form-control mdRemark" id="mdRemark"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <center>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </center>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
    
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: "{{ route('trfApp.getDataEsc') }}",
            data: function (d) {
                return $.extend({},d,{
                    'level' : $('#escLavel').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'trf_id', name: 'trf_id',searchable:true, orderable:true},
            {data: 'asal_specimen', name: 'asal_specimen',searchable:true, orderable:true},
            {data: 'test_required', name: 'test_required',searchable:false, orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false, orderable:true},
            {data: 'buyer', name: 'buyer',searchable:false, orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false, orderable:true},
            {data: 'category', name: 'category',searchable:false, orderable:true},
            {data: 'method_code', name: 'method_code',searchable:false, orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false, orderable:true},
            {data: 'pic_test', name: 'pic_test',searchable:true, orderable:true},
            {data: 'remark', name: 'remark',searchable:true, orderable:true},
            {data: 'action', name: 'action',searchable:true, orderable:true},
        ]
    });

    table.on('preDraw', function() {
        loading();
        Pace.start();
    })
    .on('draw.dt', function() {
        $.unblockUI();
        Pace.stop();
    });

    $('#escLavel').on('change',function(){
        table.clear();
        table.draw();
    });

    $('#table-list').on('click','.trfesc',function(event){
        event.preventDefault();

        var id  = $(this).data('id');
        var no  = $(this).data('no');
        var asal  = $(this).data('asal');
        var buyer  = $(this).data('buyer');
        var level  = $(this).data('level');

        if (level=="ESCALATION I") {
            $('#labarea').addClass('hidden');
            
            $('#modal_appv .mdstatus').append('<option value="CLOSED" data-rest="PASS">PASS</option>');
            $('#modal_appv .mdstatus').append('<option value="CLOSED" data-rest="FAIL">FAIL</option>');
            $('#modal_appv .mdstatus').append('<option value="ESCALATION" data-rest="">ESCALATION</option>');
            
        }else{
             $('#labarea').remove('hidden');
             $('#modal_appv .mdstatus').append('<option value="CLOSED" data-rest="PASS">PASS</option>');
            $('#modal_appv .mdstatus').append('<option value="CLOSED" data-rest="FAIL">FAIL</option>');
         
        }
        getHisto(id);
        $('#modal_appv .trfno').val(no);
        $('#modal_appv .asal').val(asal);
        $('#modal_appv .buyer').val(buyer);
        $('#modal_appv .lavelesc').val(level);
        $('#modal_appv .trfid').val(id);
        $('#modal_appv').modal('show');

    });

    $('#form-mdaction').submit(function(event){
        event.preventDefault();
        console.log($('#modal_appv .mdstatus').find(':selected').data('rest'));
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url :$('#form-mdaction').attr('action'),
                data:{
                        id:$('#modal_appv .trfid').val(),
                        lavelesc:$('#modal_appv .lavelesc').val(),
                        remark:$('#modal_appv .mdRemark').val(),
                        status:$('#modal_appv .mdstatus').val(),
                        finrest:$('#modal_appv .mdstatus').find(':selected').data('rest')
                    },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    $.unblockUI();
                    
                    var notif = response.data;
                    if (notif.status==200) {
                       alert(notif.status,notif.output); 
                       window.location.reload();
                   }else{
                        alert(notif.status,notif.output);
                   }
                    

                },
                error: function(response) {
                 

                   alert(response['status'],response['responseJSON']['message']);

                }
            });
    });

    $('#modal_appv').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });
    
});

function getHisto(id){
    $.ajax({
        type: 'get',
        url :"{{ route('trfApp.getHisto')}}",
        data :{id:id} ,
        success: function(response) {
            var data = response.data;

            $('#modal_appv .mdtech').val(data['tech']);
            $('#modal_appv .mdremtech').val(data['techrm']);

            $('#modal_appv .mdlab').val(data['labhead']);
            $('#modal_appv .mdlabrem').val(data['labrm']);
             
        },
        error: function(response) {
            console.log(response);
        }
            
    });
}

</script>
@endsection
