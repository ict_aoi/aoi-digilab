<?php

namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MasterHolidayDetail extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['master_holiday_id','date_off'];
    protected $table        = 'holiday_detail';
}
