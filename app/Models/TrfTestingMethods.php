<?php

namespace App\Models;


use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TrfTestingMethods extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['trf_id','master_method_id','created_at','updated_at','deleted_at','created_by','deleted_by'];
    protected $table        = 'trf_testing_methods';
}
