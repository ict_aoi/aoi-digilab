<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Uuids;

class SignatureSet extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $date         = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_id','meth_id','doc_id','tech','head','remark_final','type'];
    protected $table        = 'signature_set';
}
