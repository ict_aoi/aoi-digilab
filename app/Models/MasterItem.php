<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterItem extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['upc','style_name','fabric_composition','weight'];
    protected $table        = 'master_item';
}
