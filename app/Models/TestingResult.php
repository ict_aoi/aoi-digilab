<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class TestingResult extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $date         = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['set_req_id','trf_doc_id','supplier_result','before_test','standart_value','testing_result','result','result_status','number_test','created_by','deleted_by','comment','remark'];
    protected $table        = 'trf_result_test';
}
