<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class EscalationModel extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_id','escalation','remark_escalation','created_by'];
    protected $table        = 'trf_escalation';
}
