<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterMethode extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['method_code' , 'method_name', 'category', 'created_at', 'updated_at', 'deleted_at', 'created_by','sequence','type','avability'];
    protected $table        = 'master_method';
}
