<?php

namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MasterHoliday extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['reason','created_by'];
    protected $table        = 'master_holiday';
}
