<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class TrfReturnSample extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $date         = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_id','user_nik','user_name'];
    protected $table        = 'trf_return_sample';
}
