<?php

namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TrfTestingDocument extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_id',
'document_type','document_no','style','article_no','size','color','fibre_composition','fabric_finish','gauge','fabric_weight','plm_no','care_instruction','manufacture_name','export_to','nomor_roll','batch_number','item','barcode_supplier','additional_information','yds_roll','description','season','deleted_by','invoice','fabric_color','interlining_color','qty','machine','temperature','pressure','duration','style_name','fabric_item','fabric_type','thread','material_shell_panel','product_category','remark','pad','pelling','component','garment_size','sample_type','test_condition','fabric_properties','lot'];
    protected $table        = 'trf_testing_document';
}
