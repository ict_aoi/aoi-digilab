<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterDimenApp extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['buyer','method_id','parameter','measuring_position','code','number_test','created_by','category_specimen','before_test','standart_value','sequence'];
    protected $table        = 'master_specialtest';
}
