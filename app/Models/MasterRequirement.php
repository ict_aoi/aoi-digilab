<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterRequirement extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['master_method_id','master_category_id','komposisi_specimen','parameter','perlakuan_test','operator','uom','value1','value2','value3','value4','value5','value6','value7','remarks','created_by','deleted_by','buyer','sequence','false'];
    protected $table        = 'master_requirements';
}
