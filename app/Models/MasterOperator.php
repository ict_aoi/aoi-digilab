<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterOperator extends Model
{
    public $timestamps      = false;
    // public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at'];
    protected $fillable     = ['operator_name' , 'created_at', 'updated_at'];
    protected $table        = 'master_operators';
}
