<?php

namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TrfTesting extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['trf_id','buyer','lab_location','nik','platform','factory_id','asal_specimen','category_specimen','type_specimen','category','date_information','date_information_remark','test_required','part_of_specimen','created_at','updated_at','deleted_at','return_test_sample','status','verified_lab_date','verified_lab_by','reject_by','reject_date','previous_trf_id','additional_information','id_category','username','final_result','technician','remark_tech','labhead','remark_lab','qchead','remark_qc','closed_at'];
    protected $table        = 'trf_testings';
}
