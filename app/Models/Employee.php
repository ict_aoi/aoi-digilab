<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Employee extends Authenticatable
{
    use Notifiable;
    protected $guard = 'desk';
    protected $connection = 'absen';
    protected $table = 'ns_employee';
    protected $fillable = [
        'id', 'nik', 'name','status','factory'
    ];
}
