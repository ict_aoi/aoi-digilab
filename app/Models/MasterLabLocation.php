<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterLabLocation extends Model
{
    public $incrementing    = true;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['name','address'];
    protected $table        = 'master_lab_location';
}
