<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Uuids;

class SetReqModel extends Model
{
    use Uuids;

    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_meth_id','requirement_id','created_by','parameter','special_id','active'];
    protected $table        = 'trf_requirement_set';
}
