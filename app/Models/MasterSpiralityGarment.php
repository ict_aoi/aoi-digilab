<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterSpiralityGarment extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['master_method_id','parameter','measuring_position','code', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'deleted_by','sequence'];
    protected $table        = 'master_spirality_garments';
}
