<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class ReportAllspc implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $arrays;

    public function __construct($arr){
        $this->arrays = $arr;

        ini_set('max_execution_time', 3800);
    }
    public function view(): View
    {
        return view('report.form.report_allspecimen',[
            'list'=>$this->arrays
        ]);
    }
}
