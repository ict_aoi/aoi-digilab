<?php

namespace App\Http\Middleware;

use Closure;

class DeskMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('nik')) {
            return $next($request);
        }

        return redirect('/desk/login');
    }
}
