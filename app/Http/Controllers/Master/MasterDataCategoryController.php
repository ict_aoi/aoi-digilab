<?php

namespace App\Http\Controllers\Master;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Controllers\Controller;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Models\MasterCategory;
use App\Models\MasterRequirement;
use APP\User;

class MasterDataCategoryController extends Controller
{
    public function index(){
    	return view('master.category.index');
    }

    public function export()
    {
        try {
            $path = storage_path('template/upload_category.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function import(Request $request)
    {
        $array = array();

        if($request->hasFile('file')){

          $extension = \File::extension($request->file->getClientOriginalName());

          if ($extension == "xlsx" || $extension == "xls") {
              // $path = $request->file->Path();

              // $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            $datax = (new FastExcel)->import($request->file);
                
          }

            

            
        }

        if ($datax->count()<=0) {
            return response()->json("File is empty ! ! !",422);
        }

        try {
            DB::beginTransaction();
            foreach ($datax as $dx) {
                // dd($dx['category']);
                $category                    = strtoupper(trim($dx['category']));
                $category_specimen           = strtoupper(trim($dx['category_specimen']));
                $type_specimen               = strtoupper(trim($dx['type_specimen']));
                $sequence                    = isset($dx['sequence']) ? trim($dx['sequence']) : null;

                if ($category!=null && $category_specimen!=null && $type_specimen!=null) {
                    
                       $cek = MasterCategory::where([
                            'category'          => $category,
                            'category_specimen' => $category_specimen,
                            'type_specimen'     => $type_specimen
                        ])->wherenull('deleted_at')->exists();
                       


                    if ($cek==false) {
                        $in = array(
                            'category'          => $category,
                            'category_specimen' => $category_specimen,
                            'type_specimen'     => $type_specimen,
                            'sequence'          => (int)$sequence,
                            'created_by'        => auth::user()->nik,
                            'created_at'        => carbon::now()
                            );

                        MasterCategory::FirstOrCreate($in);
                    }
                }
                
            }
            

            
            DB::commit();
            $data_response = [
                                'status' => 200,
                                'output' => 'Upload Master Category Success'
                            ];
        } catch (Exception $e) {
            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);

                            $data_response = [
                                'status' => 422,
                                'output' => 'Upload Master Category Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function data(Request $request)
    {
        
            $data = MasterCategory::whereNull('deleted_at')
            ->orderby('sequence','asc');

            // dd($data);
            
            return DataTables::of($data)
            ->editColumn('created_at',function ($data)
            {
                $usr = User::where('nik',$data->created_by)->first();
              
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s').'<br> Created By : '.$usr->name;
            })
            ->addColumn('action', function($data) 
            {
                    return view('_action', [
                        'model' => $data,
                        'delctg' => $data->id,
                        // 'edctg' => ['id'=>$data->id,'catg'=>$data->category,'ctgsp'=>$data->category_specimen,'type'=>$data->type_specimen,'sequence'=>$data->sequence],
                    ]);
                
            })
            ->rawColumns(['created_at'])
            ->make(true);
        
    }

    public function delete(Request $request)
    {
        $id = trim($request->id);

        $cekreq = MasterRequirement::where('master_category_id',$id)->exists();

        if ($cekreq) {
            return response()->json('Delete Category from Master Requirement first ! ! ! ',422);
        }
        
        try 
        {
            DB::beginTransaction();
            
    
            $masterCategory = MasterCategory::where('id',$id)->update([
                'deleted_at' => carbon::now(),
                'deleted_by' => auth::user()->id
            ]);
                
            

            
            DB::commit();

            $data_response = [
                                'status' => 200,
                                'output' => 'Upload Master Category Success'
                            ];

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Upload Master Category Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }



    public function update(Request $request)
    {
        $id = $request->txid;
        $category = strtoupper(trim($request->txctg));
        $category_specimen = strtoupper(trim($request->txctgsp));
        $type_specimen = strtoupper(trim($request->txtype));
        $sequence = strtoupper(trim($request->txsquen));


        try 
        {
            DB::beginTransaction();
            
            MasterCategory::where('id', $id)->update([
                'category'=> $category,
                'category_specimen' => $category_specimen,
                'type_specimen'    => $type_specimen,
                'updated_at'    =>carbon::now(),
                'sequence'=>$sequence
            ]);

            DB::commit();

            $data_response = [
                                'status' => 200,
                                'output' => 'Update Master Category Success'
                            ];
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Update Master Category Error'.$message
                            ];
        }

  
        return response()->json(['data'=>$data_response]);
       
    }

    public function excel(Request $request){
        $data = MasterCategory::whereNull('deleted_at');
        $filter = trim($request->filter);
        if (isset($filter)) {
            $data = $data->where('category','LIKE','%'.$filter)
                            ->orwhere('category_specimen','LIKE','%'.$filter)
                            ->orwhere('type_specimen','LIKE','%'.$filter)
                            ->orwhere('sequence','LIKE','%'.$filter);
        }

        $i = 1;
        $filename = "Master_catgory";
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                return 
                    [
                        'Category'=>$row->category,
                        'Category Spesimen'=>$row->category_specimen,
                        'Type Spesimen'=>$row->type_specimen,
                        'Sequence'=>$row->sequence
                    ];
            });

        }else{
            return response()->json([422,"No data ! ! !"]);
        }

    }

    public function addCategory(Request $req){
        $category = strtoupper(trim($req->txctg));
        $category_specimen = strtoupper(trim($req->txctgsp));
        $type_specimen = strtoupper(trim($req->txtype));
        $sequence = strtoupper(trim($req->txsquen));
        try {
            DB::beginTransaction();

                $cek = MasterCategory::where('category',$category)->where('category_specimen',$category_specimen)->where('type_specimen',$type_specimen)->whereNull('deleted_at')->exists();

                if ($cek) {
                    return response()->json("Category was already exists ! ! !",422);
                    
                }

                $new = [
                            'category'=>$category,
                            'category_specimen'=>$category_specimen,
                            'type_specimen'=>$type_specimen,
                            'sequence'=>$sequence,
                            'created_at'=>carbon::now(),
                            'created_by'=>auth::user()->nik
                        ];

                MasterCategory::FirstOrCreate($new);

            DB::commit();
            $data_response = [
                                'status' => 200,
                                'output' => 'Add Master Category Success'
                            ];
        } catch (Exception $e) {
             DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Add Master Category Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

}
