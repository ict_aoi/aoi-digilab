<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use DataTables;
use Rap2hpoutre\FastExcel\FastExcel;
use Carbon\Carbon;

use App\Models\MasterMethode;
use App\Models\MasterCategory;
use App\Models\MasterRequirement;
use App\Models\MasterDimenApp;
use App\User;

class MasterDimenAppController extends Controller
{
    public function index(){
        return view('master.specialtest.index');
    }

    public function getData(){
        // $data = MasterDimenApp::whereNull('deleted_at')->orderby('created_at','asc');
        $data = DB::table('ns_master_specialtest')
                        ->orderBy('sequence','asc');


        return DataTables::of($data)
                            ->editColumn('created_at',function($data){
                                $user = User::where('id',$data->created_by)->first();
                                return Carbon::parse($data->created_at)->format('d/M/Y H:i:s')." <br> By ".$user->name;
                            })
                            ->addColumn('action',function($data){
                                return view('_action',[
                                                'model'=>$data,
                                                'delSpcTest'=>$data->id,
                                                // 'edSpcTest'=>$data->id
                                            ]);


                                // return $data->id;
                            })
                            ->rawColumns(['methods','created_at'])->make(true);
    }

    public function export(){
        try {
            $path = storage_path('template/upload_specialtest.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function import(Request $request){
        if($request->hasFile('file')){
          $extension = \File::extension($request->file->getClientOriginalName());

          if ($extension == "xlsx" || $extension == "xls") {
              $path = $request->file->getRealPath();

              $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
                
          }
        }
        // dd($datax);  
        try {
            DB::beginTransaction();
                foreach ($datax as $dx) {

                    $buyer = strtoupper(trim($dx->buyer));
                    $method_code = strtoupper(trim($dx->method_code));
                    $parameter = ucwords(trim($dx->parameter));
                    $number_test =trim($dx->number_test);
                    $category_specimen = strtoupper(trim($dx->category_specimen));
                    $measuring_position = strtoupper(trim($dx->measuring_position));
                    $code = strtoupper(trim($dx->code));
                    $before_test = strtoupper(trim($dx->before_test));
                    $standart_value = strtoupper(trim($dx->standart_value));
                  

                    if ($buyer!=null && $method_code!=null && $parameter!=null && $category_specimen!=null && $before_test!=null && $standart_value!=null) {
                       
                        $chk = DB::table('get_requirement_testings')->where('buyer',$buyer)->where('method_code',$method_code)->where('parameter',$parameter)->where('category_specimen',$category_specimen)->first();
                      
                        if ($chk!=null) {
                            $chDmn = MasterDimenApp::where('buyer',$buyer)->where('method_id',$chk->method_id)->where('parameter',$parameter)->where('category_specimen',$category_specimen)->where('measuring_position',$measuring_position)->where('code',$code)->exists();

                            if (!$chDmn) {
                                $in = array(
                                            'buyer'=>$buyer,
                                            'method_id'=>$chk->method_id,
                                            'parameter'=>$parameter,
                                            'measuring_position'=>trim($dx->measuring_position),
                                            'code'=>trim($dx->code),
                                            'number_test'=>isset($number_test) ? (int)$number_test : 1,
                                            'created_at'=>carbon::now(),
                                            'created_by'=>auth::user()->id,
                                            'category_specimen'=>$category_specimen,
                                            'before_test'=> $before_test=="YES"  ? true : false,
                                            'standart_value'=> $standart_value=="YES" ? true : false
                                        );
                                MasterDimenApp::firstorcreate($in);
                            }
                        }
                    }
                }
            DB::commit();
            $data_response = [
                                'status' => 200,
                                'output' => 'Upload Master Dimesional Appearence Success'
                            ];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Upload Master Dimesional Appearence Failed'
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function excel(Request $req){

        $filter = trim($req->filter);

        $data = DB::table('master_specialtest')
                            ->join('master_method','master_specialtest.method_id','=','master_method.id')
                            ->whereNull('master_specialtest.deleted_at')
                            ->whereNull('master_method.deleted_at')
                            ->select(
                                'master_method.method_code',
                                'master_method.method_name',
                                'master_specialtest.parameter',
                                'master_specialtest.measuring_position',
                                'master_specialtest.code',
                                'master_specialtest.number_test',
                                'master_specialtest.created_at',
                                'master_specialtest.category_specimen',
                                'master_specialtest.created_by'
                            );
                           

        if (!empty($filter)) {
            $data = $data->where('master_method.method_code',$filter)
                            ->orwhere('master_specialtest.parameter',$filter)
                            ->orwhere('master_specialtest.measuring_position',$filter)
                            ->orwhere('master_specialtest.code',$filter);
        }

        $i = 1;

        $filename = "Master_specialtest";
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        
        if ($data->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                // $uname = User::where('id',$row->created_by)->first();
                return 
                    [
                        '#'=>$row->no,
                        'Methode Code'=>$row->method_code,
                        'Methode Name'=>$row->method_name,
                        'Category Speciment'=>$row->category_specimen,
                        'Parameter'=>$row->parameter,
                        'Measuring Position'=>$row->measuring_position,
                        'Code'=>$row->code,
                        'Number Test'=>$row->number_test,
                        'Created At'=>$row->created_at,
                        // 'Created By'=>$uname->name
                    ];
            });

        }else{
            return response()->json([422,"No data ! ! !"]);
        }

    }


    public function delete(Request $req){
        $id = trim($req->id);

        // dd($id);
        // $cek =DB::table('dimensiapp_testresult')->where('id_specialtest',$id)->whereNull('deleted_at')->exists();

        // if ($cek) {
        //     return response()->json("Have test with the dimensional appearence",422);
        // }

        try {
            DB::beginTransaction();
                MasterDimenApp::where('id',$id)->update(['deleted_at'=>carbon::now()]);

            DB::commit();

             $data_response = [
                                'status' => 200,
                                'output' => 'Upload Master Dimesional Appearence Success'
                            ];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Delete Master Dimesional Appearence Failed'
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function getDataEdit(Request $req){
        $id = trim($req->id);

        $data   = MasterDimenApp::where('id',$id)->first();
        $meth   = MasterMethode::where('id',$data->method_id)->first();

        return response()->json(['data'=>$data,'meth'=>$meth],200);
    }

    public function updateData(Request $req){
        $id = $req->txid;

        try {
            DB::beginTransaction();
                MasterDimenApp::where('id',$id)->update([
                                            'measuring_position'=>trim($req->txmeasur),
                                            'code'=>trim($req->txcode),
                                            'number_test'=>$req->txnum,
                                            'before_test'=>$req->selbefore=="yes" ? true : false,
                                            'standart_value'=>$req->selstand=="yes" ? true : false,
                                            'sequence'=>trim($req->txsque)
                                        ]);
            $data_response = [
                                'status' => 200,
                                'output' => 'Update Master Dimesional Appearence Success'
                            ];
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Update Master Dimesional Appearence Failed'
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function getMeth(){
        $meth = DB::table('ns_req_catg_method')->groupBy('master_method_id','method_code','method_name')->select('master_method_id','method_code','method_name')->get();

        return response()->json(['meth'=>$meth],200);
    }

    public function getCtgParm(Request $req){
        $select = $req->select;
        $where  = $req->where;

        $data = DB::table('ns_req_catg_method')->where($where)->groupBy($select)->select($select)->get();

        return response()->json(['data'=>$data],200);
    }
 
    public function addMapp(Request $req){

        $cek = MasterDimenApp::where([
                                    'method_id'=>$req->selMeth,
                                    'buyer'=>$req->selctbuyer,
                                    'category_specimen'=>$req->selctgspc,
                                    'parameter'=>$req->selctparam,
                                    'measuring_position'=>$req->txmeasur
                                ])->wherenull('deleted_at')->exists();

        if ($cek) {
            return response()->json("Parameter Mapping was Already ! ! !",422);
        }

        try {
            DB::beginTransaction();

                $newDim = [
                                'buyer'=>$req->selctbuyer,
                                'method_id'=>$req->selMeth,
                                'parameter'=>$req->selctparam,
                                'measuring_position'=>trim($req->txmeasur),
                                'code'=>trim($req->code),
                                'number_test'=>trim($req->txnumb),
                                'created_by'=>auth::user()->id,
                                'category_specimen'=>$req->selctgspc,
                                'before_test'=>$req->selctbefore,
                                'standart_value'=>$req->selctstdr,
                                'sequence'=>trim($req->txseq)
                            ];
                MasterDimenApp::firstorcreate($newDim);
            DB::commit();
            $data_response = [
                                'status' => 200,
                                'output' => 'Add Master Dimesional Appearence Success ! ! !'
                            ];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Add Master Dimesional Appearence Failed '.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }
}
