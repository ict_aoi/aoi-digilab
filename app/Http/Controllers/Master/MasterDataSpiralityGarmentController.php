<?php

namespace App\Http\Controllers\Master;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Controllers\Controller;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Models\MasterMethode;
use App\Models\MasterSpiralityGarment;

class MasterDataSpiralityGarmentController extends Controller
{
    public function index(){

       
    	return view('master.spirality_garment.index');
    }

    public function data(Request $request)
    {
        


            $data = DB::table('master_spirality_garments')
                        ->leftjoin('master_method','master_spirality_garments.master_method_id','=','master_method.id')
                        ->select('master_method.method_code','master_spirality_garments.id','master_spirality_garments.parameter','master_spirality_garments.measuring_position','master_spirality_garments.code','master_spirality_garments.created_at')
                        ->whereNull('master_spirality_garments.deleted_at')
                        ->orderby('master_spirality_garments.sequence','asc');

            // dd($data->get());
            
            return DataTables::of($data)
                    ->editColumn('created_at',function ($data){
                        return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s');
                    })
                    ->addColumn('action', function($data) 
                    {
                            return view('master._action', [
                                'model' => $data,
                                'delete' => route('spirality_garment.delete',$data->id),
                                'edit' => route('spirality_garment.edit',$data->id),
                            ]);
                        
                    })
                    ->rawColumns(['created_at','action'])
                    ->make(true);
        
    }

    public function export()
    {
         try {
            $path = storage_path('template/upload_spirality.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function import(Request $request)
    {
        $array = array();


        if($request->hasFile('file')){
          $extension = \File::extension($request->file->getClientOriginalName());

          if ($extension == "xlsx" || $extension == "xls") {
              $path = $request->file->getRealPath();

              $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
                
          }
        }

        try {
            DB::beginTransaction();
                foreach ($datax as $dx) {
                    $method_code                 = strtoupper(trim($dx->method_code));
                    $parameter                   = strtoupper(trim($dx->parameter));
                    $measuring_position          = strtoupper(trim($dx->measuring_position));
                    $code                        = strtoupper(trim($dx->code));
                    $sequence                    = trim($dx->sequence);


                    $cekmth = MasterMethod::where('method_code',$method_code)->whereNull('deleted_at')->first();
                    if ($cekmth!=null) {
                        $ceksprt= MasterSpiralityGarment::where(['master_method_id'=>$cekmth->id,'parameter'=>$parameter,'measuring_position'=>$measuring_position,'code'=>$code])->whereNull('deleted_at')->exists();

                        if ($ceksprt==false) {
                            $in = array(
                                        'master_method_id'=>$cekmth->id,
                                        'parameter'=>$parameter,
                                        'measuring_position'=>$measuring_position,
                                        'code'=>$code,
                                        'sequence'=>$sequence,
                                        'created_by'=>auth::user()->nik,
                                        'created_at'=>carbon::now()

                                    );
                        }
                    }
                }
                 $data_response = [
                                'status' => 200,
                                'output' => 'Upload Master Method Success'
                            ];
            DB::commit();
        } catch (Exception $e) {
             DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);

                            $data_response = [
                                'status' => 422,
                                'output' => 'Upload Master Method Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function delete($id)
    {
        try 
        {
            DB::beginTransaction();
            
            $masterSpirality = MasterSpiralityGarment::find($id);
            $masterSpirality->update([
                'deleted_at' => carbon::now()
            ]);
        

            $data_response = [
                'status' => 200,
                'output' => 'Delete Master Spirality Garment Success'
            ];


            
            DB::commit();

            return redirect()->route('spirality_garment.index');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                'status' => 500,
                'output' => 'Delete Master Spirality Garment Failed'
            ];
            return response()->json(['data'=>$data_response]);
        }
    }


    public function edit($id)
    {
        $spirality = MasterSpiralityGarment::where('master_spirality_garments.id', $id)
        ->leftjoin('master_method', 'master_method.id', '=','master_spirality_garments.master_method_id')
        ->select('master_method.method_code','master_spirality_garments.id','master_spirality_garments.parameter','master_spirality_garments.measuring_position','master_spirality_garments.code','master_spirality_garments.master_method_id')
        ->get();



        return view('master.spirality_garment.edit',compact('spirality', $spirality));
        // return view('master.method.edit');
    }

    public function update(Request $request)
    {
        
        $id                  = $request->id;
        $method_code         = $request->method_code;
        $parameter           = $request->parameter;
        $measuring_position  = $request->measuring_position;
        $code                = $request->code;
        $date                = carbon::now()->toDateTimeString();


        try 
        {
            DB::beginTransaction();
            
            $methods = MasterMethod::where('method_code', $method_code)->whereNull('deleted_at')->first();
            // dd($methods);

            $spirality = MasterSpiralityGarment::where('id', $id)
            ->update([
                'parameter'             => $parameter,
                'measuring_position'    => $measuring_position,
                'code'                  => $code,
                'updated_at'            =>$date
            ]);
            // dd($spirality);

            $data_response = [
                'status' => 200,
                'output' => 'Edit Master Spirality Garment Success'
            ];
            

            
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 500,
                'output' => 'Edit Master Spirality Garment Failed'
            ];
        }
        return redirect()->route('spirality_garment.index');


       
    }


    public function excel(Request $request){
        $data = DB::table('master_spirality_garments')
                        ->leftjoin('master_method','master_spirality_garments.master_method_id','=','master_method.id')
                        ->select('master_method.method_code','master_spirality_garments.id','master_spirality_garments.parameter','master_spirality_garments.measuring_position','master_spirality_garments.code','master_spirality_garments.created_at')
                        ->whereNull('master_spirality_garments.deleted_at')
                        ->orderby('master_spirality_garments.sequence','asc');
        $filter = trim($request->filter);
        if (isset($filter)) {
            $data = $data->where('master_method.method_code','LIKE','%'.$filter)
                            ->orwhere('master_spirality_garments.parameter','LIKE','%'.$filter)
                            ->orwhere('master_spirality_garments.measuring_position','LIKE','%'.$filter)
                            ->orwhere('master_spirality_garments.code','LIKE','%'.$filter)
                            ->orwhere('master_spirality_garments.sequence','LIKE','%'.$filter);
        }

        $i = 1;
        $filename = "Master_sprirality";
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                return 
                    [
                        'Created At'=>carbon::parse($row->created_at)->format('d-m-Y H:i:s'),
                        'Methode Code'=>$row->method_code,
                        'Parameter'=>$row->parameter,
                        'Measuring Position'=>$row->measuring_position,
                        'Code'=>$row->code
                    ];
            });

        }else{
            return response()->json([422,"No data ! ! !"]);
        }

    }

}
