<?php

namespace App\Http\Controllers\Master;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Controllers\Controller;

use Rap2hpoutre\FastExcel\FastExcel;


use App\Models\MasterMethode;
use App\Models\MasterRequirement;
use App\User;

class MasterDataMethodController extends Controller
{
    
    public function index(){
    	return view('master.method.index');
    }

    
    public function data()
    {
         
            $data = MasterMethode::whereNull('deleted_at')
            ->orderby('sequence','asc');

            // dd($data->get());
            
            return DataTables::of($data)
                    ->editColumn('created_at',function ($data){
                        $usr = User::where('nik',$data->created_by)->first();


                        return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s').'<br> Created By '.$usr->name;
                    })
                    ->addColumn('action', function($data) 
                    {
                            return view('_action', [
                                'model' => $data,
                                'delmeth' => $data->id,
                                // 'edmeth'=>[
                                //         'id'=>$data->id,
                                //         'code'=>$data->method_code,
                                //         'name'=>$data->method_name,
                                //         'category'=>$data->category,
                                //         'type'=>$data->type,
                                //     ]
                            ]);
                        
                    })
                    ->rawColumns(['created_at','action'])
                    ->make(true);
        
    }

    public function export(){
         try {
            $path = storage_path('template/upload_method.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
     

    public function import(Request $request)
    {
        $array = array();

        if($request->hasFile('file')){
          $extension = \File::extension($request->file->getClientOriginalName());

          if ($extension == "xlsx" || $extension == "xls") {
              // $path = $request->file->getRealPath();

              // $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
            $datax = (new FastExcel)->import($request->file);
                
          }
        }
        
        if ($datax->count()<=0) {
            return response()->json("File is empty ! ! !",422);
        }


        try {
            DB::beginTransaction();
                foreach ($datax as $dt) {


                    $method_code = strtoupper(trim($dt['method_code']));
                    $method_name = strtoupper(trim($dt['method_name']));
                    $category = strtoupper(trim($dt['category']));
                    $type = strtoupper(trim($dt['type']));
                    $sequence = strtoupper(trim($dt['sequence']));

                    

                    if ($method_code!=null && $method_name!=null && $category!=null && $type!=null) {
                        $cek = MasterMethode::where('method_code',$method_code)->wherenull('deleted_at')->exists();

                        if ($cek==false) {
                            $in = array(
                                'method_code'=>$method_code,
                                'method_name'=>$method_name,
                                'category'=>$category,
                                'type'=>$type,
                                'sequence'=>$sequence,
                                'created_at'=>carbon::now(),
                                'created_by'=>auth::user()->nik
                            );

                            MasterMethode::firstorcreate($in);
                        }
                    }
                    
                }

                 
            DB::commit();
            $data_response = [
                                'status' => 200,
                                'output' => 'Upload Master Method Success'
                            ];
        } catch (Exception $e) {
            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);

                            $data_response = [
                                'status' => 422,
                                'output' => 'Upload Master Method Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function excel(Request $request){
        $data = MasterMethode::whereNull('deleted_at');
        $filter = trim($request->filter);
        if (isset($filter)) {
            $data = $data->where('method_code','LIKE','%'.$filter)
                            ->orwhere('method_name','LIKE','%'.$filter)
                            ->orwhere('category','LIKE','%'.$filter)
                            ->orwhere('type','LIKE','%'.$filter)
                            ->orwhere('sequence','LIKE','%'.$filter);
        }

        $i = 1;
        $filename = "Master_methode";
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                return 
                    [
                        'Methode Code'=>$row->method_code,
                        'Methode Name'=>$row->method_name,
                        'Category'=>$row->category,
                        'Sequence'=>$row->sequence,
                        'Type'=>$row->type
                    ];
            });

        }else{
            return response()->json([422,"No data ! ! !"]);
        }

    }

    public function delete(Request $req)
    {
        $id = trim($req->id);

        // $cekreq = MasterRequirement::where('master_method_id',$id)->exists();

        // if ($cekreq) {
        //    return response()->json('Delete Category from Master Requirement first ! ! ! ',422);
        // }

        try 
        {
            DB::beginTransaction();
                
                MasterMethode::where('id',$id)->update(['deleted_at'=>carbon::now()]);            
            DB::commit();

            $data_response = [
                                'status' => 200,
                                'output' => 'Delete Master Method Success'
                            ];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

             $data_response = [
                                'status' => 422,
                                'output' => 'Upload Master Method Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }


 

    public function update(Request $request)
    {
        $id = $request->id;
        $method_code = strtoupper(trim($request->code));
        $method_name = strtoupper(trim($request->name));
        $category = strtoupper(trim($request->category));
        $type = strtoupper(trim($request->type));

        try 
        {
            DB::beginTransaction();
            
           MasterMethode::where('id', $id)->update([
                'method_code'=> $method_code,
                'method_name' => $method_name,
                'category'    => $category,
                'type' =>$type,
                'updated_at'    =>carbon::now()
            ]);

            DB::commit();
            $data_response = [
                                'status' => 200,
                                'output' => 'Update Master Method Success'
                            ];
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Delete Master Method Failed'
                            ];
        }

        return response()->json(['data'=>$data_response]);

       
    }

    public function addMethod(Request $req){

       $code = strtoupper(trim($req->txcode));

       try {
           DB::beginTransaction();
                $cek = MasterMethode::where('method_code',$code)->wherenull('deleted_at')->exists();

                if ($cek) {
                    return response()->json("Method was already exists  ! ! ! ",422);
                }

                $newin = [
                            'method_code'=>$code,
                            'method_name'=>strtoupper(trim($req->txname)),
                            'category'=>strtoupper(trim($req->txctg)),
                            'type'=>strtoupper(trim($req->txtype)),
                            'sequence'=>(int)$req->txsquen,
                            'avability'=>!empty($req->txavab) ? implode(',',$req->txavab) : null,
                            'created_at'=>carbon::now(),
                            'created_by'=>auth::user()->nik
                        ];

                MasterMethode::firstorcreate($newin);

           DB::commit();
           $data_response = [
                                'status' => 200,
                                'output' => 'Add Master Method Success ! ! !'
                            ];
       } catch (Exception $e) {
           DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Add Master Method Failed'.$message
                            ];
       }

       return response()->json(['data'=>$data_response]);
    }
}
