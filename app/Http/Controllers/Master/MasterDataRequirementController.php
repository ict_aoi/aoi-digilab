<?php

namespace App\Http\Controllers\Master;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Controllers\Controller;

use Rap2hpoutre\FastExcel\FastExcel;


use App\Models\MasterMethode;
use App\Models\MasterOperator;
use App\Models\MasterCategory;
use App\Models\MasterRequirement;
use App\Models\TestingResult;
use App\user;

class MasterDataRequirementController extends Controller
{
    public function index(){
        return view('master.requirements.index');
    }





    public function data()
    {

            $data = DB::table('get_master_requirements')->orderby('sequence','asc');

            // dd($data);

            return DataTables::of($data)
            ->editColumn('created_at',function ($data)
            {
                $usr = User::where('nik',$data->created_by)->first();
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s').'<br> Created By  '.$usr->name;
            })
            ->addColumn('action', function($data)
            {
               
                return view('_action', [
                    'model' => $data,
                    'delRequ'=>$data->id,
                    // 'editRequ'=>$data->id
                ]);

            })
            ->rawColumns(['created_at','action'])
            ->make(true);

    }



    public function delete(Request $req)
    {
        $id = trim($req->id);

 
        // $cekTest = TestingResult::where('requirement_id',$id)->whereNull('deleted_at')->count();

        // if ($cekTest>0) {
        //     return response()->json($cekTest." Test Result have the requirment ! ! !",422);
        // }

        try
        {
            DB::beginTransaction();

            MasterRequirement::where('id', $id)
                            ->update([
                                'deleted_at' => carbon::now(),
                                'deleted_by'=>auth::user()->nik
                            ]);


            DB::commit();

           $data_response = [
                                'status' => 200,
                                'output' => 'Delete Master Requirement Success'
                            ];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Delete Master Requirement Failed'
                            ];
        }

         return response()->json(['data'=>$data_response]);
    }

    public function export()
    {
        try {
            $path = storage_path('template/upload_requirements.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }




    public function import(Request $request)
    {
        $array = array();

        if($request->hasFile('file')){
          $extension = \File::extension($request->file->getClientOriginalName());

          if ($extension == "xlsx" || $extension == "xls") {
              // $path = $request->file->getRealPath();

              // $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();

            $datax = (new FastExcel)->import($request->file);

          }
        }

        try {

            DB::beginTransaction();
            // foreach ($datax as $dx) {

            //     if ($dx['methode_code']=='PHX-AP0451' || $dx['methode_code']=='PHX-AP0514' || $dx['methode_code']=='PHM-AP0419') {
            //         dd($dx['methode_code']);
            //     }
            // }


            foreach ($datax as $dx) {

                $method_code                    = strtoupper(trim($dx['methode_code']));
                $category                       = strtoupper(trim($dx['category']));
                $category_specimen              = strtoupper(trim($dx['category_specimen']));
                $type_specimen                  = strtoupper(trim($dx['type_specimen']));
                $operator                       = trim($dx['operator']);
                $buyer                          = strtoupper(trim($dx['buyer']));
                $parameter                      = ucwords(trim($dx['parameter']));
                $komposisi_specimen             = !empty(trim($dx['komposisi_specimen'])) ? ucwords(trim($dx['komposisi_specimen'])) : "-";
                $perlakuan_test             = !empty(trim($dx['perlakuan_test'])) ? ucwords(trim($dx['perlakuan_test'])) : "-";


                if ($method_code!=null && $category!=null && $category_specimen!=null && $type_specimen!=null && $operator!=null && $buyer!=null && $parameter!=null) {

                    $mbuyer = DB::table('master_buyer')->where('buyer',$buyer)->wherenull('deleted_at')->first();
                    $mmeth  = MasterMethode::where('method_code',$method_code)->whereNull('deleted_at')->first();
                    $mctgry = MasterCategory::where('category',$category)
                                                ->where('category_specimen',$category_specimen)
                                                ->where('type_specimen',$type_specimen)
                                                ->whereNull('deleted_at')->first();
                    $moprt  = MasterOperator::where('operator_name',$operator)->first();

                    // dd($mbuyer,$mmeth,$mctgry);

                    if ($mbuyer!=null && $mmeth!=null && $mctgry!=null && $moprt!=null) {
                            $mreqie = MasterRequirement::where('master_method_id',$mmeth->id)
                                                    ->where('master_category_id',$mctgry->id)
                                                    ->where('buyer',$buyer)
                                                    ->where('parameter',$parameter)
                                                    ->where('komposisi_specimen',$komposisi_specimen)
                                                    ->where('perlakuan_test',$perlakuan_test)
                                                    ->whereNull('deleted_at')->exists();

                            if ($mreqie==false) {
                               $in = array(
                                    'master_method_id'=>$mmeth->id,
                                    'master_category_id'=>$mctgry->id,
                                    'komposisi_specimen'=>$komposisi_specimen,
                                    'parameter'=>$parameter,
                                    'perlakuan_test'=>$perlakuan_test,
                                    'operator'=>$moprt->id,
                                    'uom'=>strtoupper(trim($dx['uom'])),
                                    'value1'=>trim($dx['value_1']),
                                    'value2'=>trim($dx['value_2']),
                                    'value3'=>trim($dx['value_3']),
                                    'value4'=>trim($dx['value_4']),
                                    'value5'=>trim($dx['value_5']),
                                    'value6'=>trim($dx['value_6']),
                                    'value7'=>trim($dx['value_7']),
                                    'false'=>!empty(trim($dx['fail'])) ? ucwords(trim($dx['fail'])) : "-",
                                    'remarks'=>trim($dx['remark']),
                                    'created_by'=>auth::user()->nik,
                                    'buyer'=>strtoupper(trim($dx['buyer'])),
                                    'sequence'=>trim($dx['sequence'])
                                    );

                                MasterRequirement::firstorcreate($in);
                            }

                            continue;


                    }


                }
            }

            $data_response = [
                                'status' => 200,
                                'output' => 'Upload  Success'
                            ];
            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);

                            $data_response = [
                                'status' => 422,
                                'output' => 'Upload  Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

     public function excel(Request $request){
        $data = DB::table('ns_req_catg_method')->orderby('sequence','asc');
        $filter = trim($request->filter);


        if (!empty($filter)) {
            $data = $data->where('method_code','LIKE','%'.$filter)
                            ->orwhere('method_name','LIKE','%'.$filter)
                            ->orwhere('category','LIKE','%'.$filter)
                            ->orwhere('type_specimen','LIKE','%'.$filter)
                            ->orwhere('operator_name','LIKE','%'.$filter);
        }

        $i = 1;
        $filename = "Master_requirements";
        $data_result = $data->get();


        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        if ($data->count()>0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){
                return
                    [
                        'Created at'=>carbon::parse($row->created_at)->format('d-m-Y H:i'),
                        'Buyer'=>$row->buyer,
                        'Methode Code'=>$row->method_code,
                        'Methode Name'=>$row->method_name,
                        'Category'=>$row->category,
                        'Category Specimen'=>$row->category_specimen,
                        'Type Specimen'=>$row->type_specimen,
                        'Komposisi'=>$row->komposisi_specimen,
                        'Perlakuan Test'=>$row->perlakuan_test,
                        'Parameter'=>$row->parameter,
                        'Operator'=>$row->operator_name,
                        'UOM'=>$row->uom,
                        'Value 1'=>$row->value1,
                        'Value 2'=>$row->value2,
                        'Value 3'=>$row->value3,
                        'Value 4'=>$row->value4,
                        'Value 5'=>$row->value5,
                        'Value 6'=>$row->value6,
                        'Value 7'=>$row->value7,
                        'False Value'=>$row->false,
                        'Remark'=>$row->remarks
                    ];
            });

        }else{
            return response()->json([422,"No data ! ! !"]);
        }

    }

    public function getDataDetail(Request $request){
        $id = trim($request->id);

        $dataReq    = DB::table('ns_req_catg_method')->where('id',$id)->first();

        $lMeth      = MasterMethode::whereNull('deleted_at')->get();
        $lCatg      = MasterCategory::whereNull('deleted_at')->groupBy('category')->select('category')->get();

        $opration   = DB::table('master_operators')->get();
        return response()->json(['dataReq'=>$dataReq,'lMeth'=>$lMeth,'lCatg'=>$lCatg,'oprt'=>$opration],200);



    }

    

    public function update(Request $req){
        $id = trim($req->txid);

        try {
            DB::beginTransaction();

                MasterRequirement::where('id',$id)
                                    ->update( 
                                        [
                                            'komposisi_specimen'=>!empty(trim($req->txcomps)) ? ucwords(trim($req->txcomps)) : '-',
                                            'parameter'=>!empty(trim($req->txparam)) ? ucwords(trim($req->txparam)) : '-',
                                            'perlakuan_test'=>!empty(trim($req->txtreat)) ? ucwords(trim($req->txtreat)) : '-',
                                            'operator'=>trim($req->txopert),
                                            'uom'=>trim($req->txuom),
                                            'value1'=>trim($req->txval1),
                                            'value2'=>trim($req->txval2),
                                            'value3'=>trim($req->txval3),
                                            'value4'=>trim($req->txval4),
                                            'value5'=>trim($req->txval5),
                                            'value6'=>trim($req->txval6),
                                            'value7'=>trim($req->txval7),
                                            'false'=>!empty(trim($req->txfalse)) ? ucwords(trim($req->txfalse)) : '-',
                                            'remarks'=>trim($req->txremark)
                                        ]
                                    );

                $data_response = [
                                'status' => 200,
                                'output' => 'Update  Success ! ! !'
                            ];
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);

                            $data_response = [
                                'status' => 422,
                                'output' => 'Update  Error'.$message
                            ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function getMethCtg(){
        $byr = DB::table('master_buyer')->whereNull('deleted_at')->select('buyer')->get();
        $meth = MasterMethode::whereNull('deleted_at')->groupBy('id','method_code','method_name')->select('id','method_code','method_name')->get();

        $ctg = MasterCategory::whereNull('deleted_at')->groupBy('category')->select('category')->get();

        $opt = DB::table('master_operators')->get();

        return response()->json(['meth'=>$meth,'ctg'=>$ctg,'opt'=>$opt,'byr'=>$byr],200);
    }

    public function getCtgSpcType(Request $req){
        $select = $req->select;
        $where  = $req->where;

        $data = MasterCategory::whereNull('deleted_at')->where($where)->groupBy($select)->select($select)->get();

        return response()->json(['data'=>$data],200);
    }

    public function addRequirment (Request $req){
        $compos = !empty(trim($req->txcomps)) ? ucwords(trim($req->txcomps)) : "-";
        $treat = !empty(trim($req->txtreat)) ? ucwords(trim($req->txtreat)) : "-";
        $param = !empty(trim($req->txparam)) ? ucwords(trim($req->txparam)) : "-";


        $cek = DB::table('ns_req_catg_method')
                            ->where([
                                'master_method_id'=>$req->selMeth,
                                'category'=>$req->selCtg,
                                'category_specimen'=>$req->selctgspc,
                                'type_specimen'=>$req->selTypespc,
                                'komposisi_specimen'=>$compos,
                                'perlakuan_test'=>$treat,
                                'parameter'=>$param,
                                'buyer'=>strtoupper(trim($req->txbuyer))
                            ])
                            ->exists();

        if ($cek) {
           return response()->json("Requirement was already ! ! ! ",422);
        }

        try {
            DB::beginTransaction();
                $mctg = MasterCategory::where('category',$req->selCtg)->where('category_specimen',$req->selctgspc)->where('type_specimen',$req->selTypespc)->wherenull('deleted_at')->first();

                $newReq = [
                            'master_method_id'=>$req->selMeth,
                            'master_category_id'=>$mctg->id,
                            'komposisi_specimen'=>$compos,
                            'parameter'=>$param,
                            'perlakuan_test'=>$treat,
                            'operator'=>$req->selOpt,
                            'uom'=>trim($req->txuom),
                            'value1'=>trim($req->val1),
                            'value2'=>trim($req->val2),
                            'value3'=>trim($req->val3),
                            'value4'=>trim($req->val4),
                            'value5'=>trim($req->val5),
                            'value6'=>trim($req->val6),
                            'value7'=>trim($req->val7),
                            'remarks'=>trim($req->txremark),
                            'created_by'=>auth::user()->nik,
                            'buyer'=>strtoupper(trim($req->txbuyer)),
                            'false'=>ucwords(trim($req->txfail)),
                            'created_at'=>carbon::now()
                        ];
                MasterRequirement::firstorcreate($newReq);

            DB::commit();
            $data_response = [
                'status' => 200,
                'output' => 'Add Master Requirement Success ! ! !'
            ];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 422,
                'output' => 'Add Master Requirement Failed ! ! !'.$message
            ];
        }

        return response()->json(['data'=>$data_response]);
    }
}
