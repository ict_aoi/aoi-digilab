<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use DataTables;
use Carbon\Carbon;

use App\Models\MasterHoliday;
use App\Models\MasterHolidayDetail;
use App\User;


class MasterHolidayController extends Controller
{
    public function index(){
        return view('master.holiday.index');
    }

   public function getData(){
        $data = MasterHoliday::whereNull('deleted_at')->orderby('created_at','desc');

        return DataTables::of($data)
                            ->editColumn('created_at',function($data){
                                $user = User::where('nik',$data->created_by)->first();

                                return "<b>Created By</b> : ".$user->name." (".$user->nik.") <br> <b>Created At</b> : ".carbon::parse($data->created_at)->format('d-m-Y H:i:s');
                            })
                            ->editColumn('off_date',function($data){
                                $get = MasterHolidayDetail::where('master_holiday_id',$data->id)
                                                            ->whereNull('deleted_at')
                                                            ->groupBy('master_holiday_id')
                                                            ->select('master_holiday_id',db::raw("string_agg(to_char(date_off,'Day, dd Monthyyyy'), '<br>') as date_off"))
                                                            ->first();
                                return $get->date_off;
                            })
                            ->addColumn('action',function($data){
                                return view('_action',[
                                                // 'editHoliday'=>['id'=>$data->id,'off_date'=>$data->off_date,'reason'=>$data->reason],
                                                'deleteHoliday'=>['id'=>$data->id]
                                            ]);
                            })
                            ->rawColumns(['created_at','off_date','action'])
                            ->make(true);
   }

   public function add(Request $req){
        
        $from =carbon::parse(explode(" - ",$req->off_date)[0])->format('Y-m-d');
        $to =carbon::parse(explode(" - ",$req->off_date)[1])->format('Y-m-d');
        $reason =trim($req->reason);

        try {
            DB::beginTransaction();

                $head = [
                            'reason'=>$reason,
                            'created_at'=>carbon::now(),
                            'created_by'=>auth::user()->nik
                        ];

                $inhead = MasterHoliday::firstorcreate($head);

                $diff = date_diff(date_create($from),date_create($to))->d;


                for ($i=0; $i <=$diff ; $i++) { 
                    $dates = carbon::parse($from)->addDays($i)->format('Y-m-d');
                    $cek =(int) carbon::parse($dates)->format('w');
                    
                    if ($cek>0 && $cek<6) {

                    
                        $detail = [
                                'master_holiday_id'=>$inhead->id,
                                'date_off'=>$dates,
                                'created_at'=>carbon::now()
                            ];

                        MasterHolidayDetail::firstorcreate($detail);
                    }
                    
                }

                $data_response = [
                                'status' => 200,
                                'output' =>'Add Master Holiday Success'
                            ];
                
            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Add Master Holiday Failed'
                            ];
        }

        return response()->json(['data'=>$data_response]);
   }


   public function delete(Request $req){
        $id =trim($req->id);
        // dd($id);
        try {
            DB::beginTransaction();

                MasterHoliday::where('id',$id)->update([
                                        'deleted_at'=>carbon::now()
                                    ]);

                MasterHolidayDetail::where('master_holiday_id',$id)->update([
                                        'deleted_at'=>carbon::now()
                                    ]);
                
                $data_response = [
                                'status' => 200,
                                'output' => 'Delete Master Holiday Success'
                            ];
            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Delete Master Holiday Failed'
                            ];
        }

        return response()->json(['data'=>$data_response]);
   }
}
