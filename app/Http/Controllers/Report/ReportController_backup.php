<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\File;
use Uuid;

use App\Exports\ReportLogBook;
use App\Exports\ReportAllspc;

use App\User;
use App\Models\SignatureSet;

use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterLabLocation;
use App\Models\EscalationModel;

use App\Http\Controllers\HelperController;

class ReportController extends Controller
{
    //report test
    public function reportTest(){
        $fact = DB::table('factory')->whereNull('deleted_at')->get();
        $labloc = MasterLabLocation::whereNull('deleted_at')->orderby('id','asc')->get();
        $crl = HelperController::cekRoleReport();

    

        return view('report.report_test')->with('factory',$fact)
                                        ->with('labloc',$labloc)
                                        ->with('crl',$crl);
    }

    public function getDataReportTest(Request $req){
        $factory_id = $req->factory_id;
        $lab_location = $req->lab_location;
        $key = trim($req->key);

        $data= DB::table('ns_list_report_test')
                    ->where('factory_id',$factory_id)
                    ->where('lab_location',$lab_location);

        if (!empty($key)) {
            
            $data = $data->where(function($query) use ($key){
                $query->where('trf_id','like','%'.$key.'%')
                            ->orwhere('buyer','like','%'.$key.'%')
                            ->orwhere('method_code','like','%'.$key.'%')
                            ->orwhere('method_name','like','%'.$key.'%')
                            ->orwhere('document_no','like','%'.$key.'%')
                            ->orwhere('style','like','%'.$key.'%')
                            ->orwhere('article_no','like','%'.$key.'%')
                            ->orwhere('lab_location','like','%'.$key.'%');
            });
            
        }else{
            $from = carbon::now()->subday(7)->format('Y-m-d H:i:s');
            $to = carbon::now()->format('Y-m-d H:i:s');

            $data = $data->wherebetween('finish_date',[$from,$to]);
        }

        return DataTables::of($data->orderby('finish_date','desc'))
                        ->addColumn('specimen',function($data){
                            return HelperController::setDoc($data);
                        })
                        ->addColumn('method',function($data){
                            return '<b>'.$data->method_code.'</b><br>'.$data->method_name;
                        })
                        ->addColumn('action',function($data){
                            $labrole = auth::user()->hasRole(['ICT','ME','LAB','LAB TECHNICIAN','LAB ADMIN','LAB SECTION HEAD']);
                            $cekSign = SignatureSet::where('type','report_test')
                                                        ->where('trf_id',$data->id)
                                                        ->where('meth_id',$data->trf_meth_id)
                                                        ->where('doc_id',$data->trf_doc_id)
                                                        ->exists();

                            if ($cekSign==false && $labrole==true) {
                               return view('_action',[
                                        'setPrintReportTest'=>[
                                                        'id'=>$data->id,
                                                        'meth'=>$data->trf_meth_id,
                                                        'doc'=>$data->trf_doc_id,
                                                        'tech'=>$data->tech_id
                                                    ]
                                    ]);
                            }else if($cekSign==false && $labrole==false){
                                return '<span class="label label-warning">On Progress Release Report</span>';
                            }else{
                                return view('_action',[
                                        'PrintReportTest'=>[
                                                        'id'=>$data->id,
                                                        'meth'=>$data->trf_meth_id,
                                                        'doc'=>$data->trf_doc_id
                                                    ]
                                    ]);
                            }
                            
                            
                        })
                        ->rawColumns(['specimen','method','action'])
                        ->make(true);
    }

    public function printReportTest(Request $req){


        $headdoc = DB::select("SELECT * FROM ns_headdoc('".$req->id."','".$req->docid."')");

      
        $TestGroup = DB::table('ns_report_test')
                            ->where('id',$req->id)
                            ->where('trf_meth_id',$req->methid)
                            ->where('trf_doc_id',$req->docid)
                            ->groupBy('method_category')->select('method_category',db::raw('max(created_at) as test_finish'))->get();
        $headdoc[0]->finish = $TestGroup[0]->test_finish;
        $headdoc[0]->dry_process = "";

        $signSet = DB::select("SELECT * FROM ns_set_signature('".$req->id."','".$req->methid."','".$req->docid."','report_test')")[0];

        $dataRest = [];
        $finRest = 'PASS';
        foreach ($TestGroup as $tg) {
            $dx = DB::table('ns_report_test')
                            ->where('id',$req->id)
                            ->where('trf_meth_id',$req->methid)
                            ->where('trf_doc_id',$req->docid)
                            ->where('method_category',$tg->method_category)
                            ->orderby('sequence','desc')->get();
                $dataRest[$tg->method_category]['header']=$tg->method_category;
            $i=1;
            $methid='';
            
              foreach ($dx as $d) {
                
                if ($d->method_code=='PHX-AP0701' && $headdoc[0]->dry_process=="") {
                    $headdoc[0]->dry_process = $d->perlakuan_test;
                }else if ( $d->method_code!='PHX-AP0701'){
                    $headdoc[0]->dry_process = "";
                }

                if ($methid=='' || $methid!=$d->trf_meth_id) {
                    $methid = $d->trf_meth_id;
                    $i=1;
                    
                    
                }else{
                    $i = $i+1;
                }

                if ($finRest=='PASS' && $d->result_status=='FAILED') {
                    $finRest = 'FAILED';
                }else if ($finRest=='PASS' && $d->result_status=='PASS') {
                    $finRest = 'PASS';
                }else if($finRest=='FAILED'){
                    $finRest = 'FAILED';
                }

                $dtcont['method_id']=$d->trf_meth_id;
                $dtcont['method_code']=$d->method_code;
                $dtcont['method_name']=$d->method_name;
                $dtcont['standart']=HelperController::setReqOption($d->req_id);
                $dtcont['parameter']=$d->parameter;
                $dtcont['measuring_position']=$d->measuring_position;
                $dtcont['code']=$d->code;
                $dtcont['number_test']=$d->number_test;
                $dtcont['supplier_result']=$d->supplier_result;
                $dtcont['result']=$d->result;
                $dtcont['uom']=$d->uom;
                $dtcont['result_status']=$d->result_status;
                $dtcont['remark_result']=$d->remark_result;
                $dtcont['comment']=$d->comment;
                $dtcont['rowspan']=$i;

                $dataRest[$tg->method_category]['content'][]=$dtcont;
            }
            
            
        }
   
        $getImg = DB::table('trf_testing_methods')
                        ->join('trf_requirement_set','trf_testing_methods.id','=','trf_requirement_set.trf_meth_id')
                        ->join('result_image','trf_requirement_set.id','=','result_image.set_req_id')
                        ->where('trf_testing_methods.id',$req->methid)
                        ->where('result_image.trf_doc_id',$req->docid)
                        ->whereNull('trf_requirement_set.deleted_at')
                        ->whereNull('result_image.deleted_at')
                        ->where('trf_requirement_set.active',true)
                        ->orderby('trf_requirement_set.id','asc')
                        ->orderby('result_image.img_number','asc')
                        ->select(
                            'result_image.id',
                            'result_image.img_number',
                            'result_image.remark'
                        )->get();


        // return view('report.form.index',['headdoc'=>$headdoc[0],'result'=>$dataRest,'sign'=>$signSet]);
             
        // $pdf = \PDF::loadView('report.form.index',['headdoc'=>$headdoc[0],'result'=>$dataRest,'sign'=>$signSet,'image'=>$getImg,'result_status'=>$finRest])->setPaper('A4','Potrait');
        // return $pdf->stream($headdoc[0]->trf_id);

        return View('report.form.index',['headdoc'=>$headdoc[0],'result'=>$dataRest,'sign'=>$signSet,'image'=>$getImg,'result_status'=>$finRest]);
    }

    public function getTech(Request $req){
        $tech = explode(',',$req->tech);

        $tech = User::whereIn(DB::raw('id::varchar'),$tech)->select('id','name','nik')->get();
        $head = User::whereIn('position',['LAB SUB. DEPT. HEAD','LAB DEPT. HEAD'])->wherenull('deleted_at')->select('id','name','nik','position')->get();
        
        return response()->json(['tech'=>$tech,'head'=>$head],200);
    }

    public function setSignature(Request $req){
        $techid = $req->tech;
        $headid = $req->head;
        $remark = trim($req->remark_final);

  
        try {
            DB::beginTransaction();
                if ($req->type=='report_test') {
                   $arin = array(
                        'trf_id'=>$req->trfid,
                        'meth_id'=>$req->methid,
                        'doc_id'=>$req->docid,
                        'tech'=>$techid,
                        'head'=>$headid,
                        'remark_final'=>$remark,
                        'type'=>'report_test'
                    );
                }elseif ($req->type=='report_specimen') {
                   $arin = array(
                        'trf_id'=>$req->trfid,
                        'meth_id'=>$req->methid,
                        'doc_id'=>$req->docid,
                        'tech'=>$techid,
                        'head'=>$headid,
                        'remark_final'=>$remark,
                        'type'=>'report_specimen'
                    );
                }else if($req->type=='report_trf'){
                    $arin = array(
                        'trf_id'=>$req->trfid,
                        'tech'=>$techid,
                        'head'=>$headid,
                        'remark_final'=>$remark,
                        'type'=>'report_trf'
                    );
                }
                    


                SignatureSet::firstorcreate($arin);
            DB::commit();
            $data_response = ['status'=>200,"output"=>"Set Report Success ! ! !"];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>"Set Report Failed ! ! ! ".$message];
        }

        return response()->json(['data_response'=>$data_response]);
    }
    //report test

    //report speciment
    public function reportSpecimen(){
        $fact = DB::table('factory')->whereNull('deleted_at')->get();
        $labloc = MasterLabLocation::whereNull('deleted_at')->orderby('id','asc')->get();
        $crl = HelperController::cekRoleReport();

    

        return view('report.report_specimen')->with('factory',$fact)
                                        ->with('labloc',$labloc)
                                        ->with('crl',$crl);

    }

    public function getDataReportSpecimen(Request $req){
        $factory_id = $req->factory_id;
        $lab_location = $req->lab_location;
        $key = trim($req->factory_id);

        $data = DB::table('ns_list_test_specimen')
                    ->where('factory_id',$factory_id)
                    ->where('lab_location',$lab_location);

        if (!empty($key)) {
            
            $data = $data->where(function($query) use ($key){
                $query->where('trf_id','like','%'.$key.'%')
                            ->orwhere('buyer','like','%'.$key.'%')
                            ->orwhere('document_no','like','%'.$key.'%')
                            ->orwhere('style','like','%'.$key.'%')
                            ->orwhere('article_no','like','%'.$key.'%')
                            ->orwhere('lab_location','like','%'.$key.'%');
            });
            
        }else{
            $from = carbon::now()->subday(7)->format('Y-m-d H:i:s');
            $to = carbon::now()->format('Y-m-d H:i:s');

            $data = $data->wherebetween('finish_date',[$from,$to]);
        }
        return DataTables::of($data->orderby('finish_date','desc'))
                            ->addColumn('specimen',function($data){
                                return HelperController::setDoc($data);
                            })
                            ->editColumn('method',function($data){
                                return $data->method;
                            })
                            ->addColumn('action',function($data){
                                $labrole = auth::user()->hasRole(['ICT','ME','LAB','LAB TECHNICIAN','LAB ADMIN','LAB SECTION HEAD']);

                                $cekSign = SignatureSet::where('type','report_specimen')
                                                            ->where('trf_id',$data->id)
                                                            ->where('doc_id',$data->trf_doc_id)
                                                            ->whereNull('deleted_at')->exists();
                                if ($cekSign==false && $labrole==true) {
                                    return view('_action',[
                                                'setPrintReportSpc'=>[
                                                            'id'=>$data->id,
                                                            'doc'=>$data->trf_doc_id,
                                                            'tech'=>$data->techid
                                                        ]
                                            ]);
                                }else if($cekSign==false && $labrole==false){
                                    return '<span class="label label-warning">On Progress Release Report</span>';
                                }else{
                                    return view('_action',[
                                                'PrintReportSpc'=>[
                                                            'id'=>$data->id,
                                                            'doc'=>$data->trf_doc_id
                                                        ]
                                            ]);
                                }
                                
                            })
                            ->rawColumns(['specimen','method','action'])
                            ->make(true);
    }


    public function printReportSpecimen(Request $req){


        $headdoc = DB::select("SELECT * FROM ns_headdoc('".$req->id."','".$req->docid."')");

      
        $TestGroup = DB::table('ns_report_test')
                            ->where('id',$req->id)
                            ->where('trf_doc_id',$req->docid)
                            ->groupBy('method_category')->select('method_category',db::raw('max(created_at) as test_finish'))->get();

        $headdoc[0]->finish = $TestGroup[0]->test_finish;
        $headdoc[0]->dry_process = "";

        $signSet = DB::select("SELECT * FROM ns_set_signature2('".$req->id."','".$req->docid."','report_specimen')")[0];

     
        $dataRest = [];
        $finRest = 'PASS';
        foreach ($TestGroup as $tg) {
            $dx = DB::table('ns_report_test')
                            ->where('id',$req->id)
                            ->where('trf_doc_id',$req->docid)
                            ->where('method_category',$tg->method_category)
                            ->orderby('sequence','desc')->get();
                $dataRest[$tg->method_category]['header']=$tg->method_category;
            $i=1;
            $methid='';
            
              foreach ($dx as $d) {

                if ($d->method_code=='PHX-AP0701' && $headdoc[0]->dry_process=="") {
                    $headdoc[0]->dry_process = $d->perlakuan_test;
                }else if ( $d->method_code!='PHX-AP0701'){
                    $headdoc[0]->dry_process = "";
                }
                
                if ($methid=='' || $methid!=$d->trf_meth_id) {
                    $methid = $d->trf_meth_id;
                    $i=1;
                    
                    
                }else{
                    $i = $i+1;
                }

                if ($finRest=='PASS' && $d->result_status=='FAILED') {
                    $finRest = 'FAILED';
                }else if ($finRest=='PASS' && $d->result_status=='PASS') {
                    $finRest = 'PASS';
                }else if($finRest=='FAILED'){
                    $finRest = 'FAILED';
                }

                $dtcont['method_id']=$d->trf_meth_id;
                $dtcont['method_code']=$d->method_code;
                $dtcont['method_name']=$d->method_name;
                $dtcont['standart']=HelperController::setReqOption($d->req_id);
                $dtcont['parameter']=$d->parameter;
                $dtcont['measuring_position']=$d->measuring_position;
                $dtcont['code']=$d->code;
                $dtcont['number_test']=$d->number_test;
                $dtcont['supplier_result']=$d->supplier_result;
                $dtcont['result']=$d->result;
                $dtcont['uom']=$d->uom;
                $dtcont['result_status']=$d->result_status;
                $dtcont['remark_result']=$d->remark_result;
                $dtcont['comment']=$d->comment;
                $dtcont['rowspan']=$i;

                $dataRest[$tg->method_category]['content'][]=$dtcont;
            }
            
            
        }
   
        $getImg = DB::table('trf_testing_methods')
                        ->join('trf_requirement_set','trf_testing_methods.id','=','trf_requirement_set.trf_meth_id')
                        ->join('result_image','trf_requirement_set.id','=','result_image.set_req_id')
                        ->where('result_image.trf_doc_id',$req->docid)
                        ->whereNull('trf_requirement_set.deleted_at')
                        ->whereNull('result_image.deleted_at')
                        ->where('trf_requirement_set.active',true)
                        ->orderby('trf_requirement_set.id','asc')
                        ->orderby('result_image.img_number','asc')
                        ->select(
                            'result_image.id',
                            'result_image.img_number',
                            'result_image.remark'
                        )->get();
        // return view('report.form.index',['headdoc'=>$headdoc[0],'result'=>$dataRest,'sign'=>$signSet]);

        // $pdf = \PDF::loadView('report.form.index',['headdoc'=>$headdoc[0],'result'=>$dataRest,'sign'=>$signSet,'image'=>$getImg,'result_status'=>$finRest])->setPaper('A4','Potrait');
        // return $pdf->stream($headdoc[0]->trf_id);

        return View('report.form.index',['headdoc'=>$headdoc[0],'result'=>$dataRest,'sign'=>$signSet,'image'=>$getImg,'result_status'=>$finRest]);
    }

    //report speciment


    //report trf
    public function reportTrf(){
        $fact = DB::table('factory')->whereNull('deleted_at')->get();
        $labloc = MasterLabLocation::whereNull('deleted_at')->orderby('id','asc')->get();
        $crl = HelperController::cekRoleReport();

    

        return view('report.report_trf')->with('factory',$fact)
                                        ->with('labloc',$labloc)
                                        ->with('crl',$crl);
    }

    public function getDataReportTrf(Request $req){
        $key = trim($req->key);
        $factory_id = $req->factory_id;
        $lab_location = $req->lab_location;
  
        $data = DB::table('ns_report_test')
                            ->where('factory_id',$factory_id)
                            ->where('lab_location',$lab_location)
                            ->where('status','CLOSED')
                            ->whereNotNull('created_at')
                            ->groupBy([
                                'id',
                                'trf_id',
                                'buyer',
                                'lab_location',
                                'platform',
                                'factory_id',
                                'asal_specimen',
                                'date_information',
                                'date_information_remark',
                                'test_required',
                                'part_of_specimen',
                                'return_test_sample',
                                'status',
                                'category',
                                'category_specimen',
                                'type_specimen'
                            ])
                            ->select(
                                'id',
                                'trf_id',
                                'buyer',
                                'lab_location',
                                'platform',
                                'factory_id',
                                'asal_specimen',
                                'date_information',
                                'date_information_remark',
                                'test_required',
                                'part_of_specimen',
                                'return_test_sample',
                                'status',
                                'category',
                                'category_specimen',
                                'type_specimen',
                                db::raw("string_agg(created_by::varchar,',') as techid")
                            )
                            // ->having(DB::raw("string_agg(COALESCE(created_by,0)::VARCHAR,',')"),"not like","%0%")
                            ->orderby('trf_id','desc');
            // dd($data->get());
        if ($key) {
            $data = $data->where('style','LIKE','%'.$key.'%')
                            ->orwhere('article_no','LIKE','%'.$key.'%')
                            ->orwhere('method_code','LIKE','%'.$key.'%')
                            ->orwhere('method_name','LIKE','%'.$key.'%')
                            ->orwhere('trf_id','LIKE','%'.$key.'%')
                            ->orwhere('lab_location','LIKE','%'.$key.'%')
                            ->orwhere('document_no','LIKE','%'.$key.'%')
                            ->orwhere('buyer','LIKE','%'.$key.'%')
                            ->orwhere('nik','LIKE','%'.$key.'%');
        }else{
            $from = carbon::now()->subday(14)->format('Y-m-d 00:00:00');
            $to = carbon::now()->format('Y-m-d H:i:s');

            // dd($from,$to);
            $data = $data->wherebetween('created_at',[$from,$to]);
        }
        // dd($data->get());
        return DataTables::of($data->get())
                                ->addColumn('specimen',function($data){
                                    $doc = TrfTestingDocument::where('trf_id',$data->id)->get();
                                    $ret = "";
                                    foreach ($doc as $dc) {
                                        $ret = $ret." ".HelperController::setDoc($dc)." <hr>";
                                    }

                                    return $ret;
                                })
                                ->addColumn('method',function($data){
                                    $meth = DB::table('trf_testing_methods')->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                                            ->where('trf_testing_methods.trf_id',$data->id)
                                            ->whereNull('trf_testing_methods.deleted_at')
                                            ->groupBy('trf_testing_methods.trf_id')
                                            ->select(db::raw("string_agg(concat('<b>',master_method.method_code,'</b>',master_method.method_name,' '), '<br>') as methods"))->first();
                                    return $meth->methods;
                                })
                                ->addColumn('action',function($data){
                                    

                                    $labrole = auth::user()->hasRole(['ICT','ME','LAB','LAB TECHNICIAN','LAB ADMIN','LAB SECTION HEAD']);

                                    $cekSign = SignatureSet::where('type','report_trf')
                                                                ->where('trf_id',$data->id)
                                                                ->whereNull('deleted_at')->first();
                                
                                    if ($cekSign==null && $labrole==true) {
                                        return view('_action',[
                                                    'setPrintReportTrf'=>[
                                                                'id'=>$data->id,
                                                                'tech'=>$data->techid
                                                            ]
                                                ]);
                                    }else if($cekSign==null && $labrole==false){
                                        return '<span class="label label-warning">On Progress Release Report</span>';
                                    }else{
                                        return view('_action',[
                                                    'PrintReportTrf'=>[
                                                                'id'=>$data->id,
                                                                'sign'=>json_encode($cekSign)
                                                            ],
                                                    'PrintReportTrf2'=>[
                                                                'id'=>$data->id,
                                                                'sign'=>json_encode($cekSign)
                                                            ],
                                                ]);
                                    }
                                    
                                })
                                ->rawColumns(['specimen','method','action'])
                                ->make(true);

    }

    public function printReportTrf(Request $req){
        $id = trim($req->id);
        $list = [];
        $dataHead = DB::select("SELECT * FROM ns_get_header('".$id."');");
        $doci = 1;
        $sing = DB::select("SELECT * FROM ns_set_singnature3('".$id."')")[0];


        $escl = EscalationModel::where('trf_id',$id)->whereNull('deleted_at')->first();

        foreach ($dataHead as $dh) {
            $finRest = "PASS";
            $TestGroup = DB::table('ns_report_test')
                            ->where('id',$dh->id)
                            ->where('trf_doc_id',$dh->doc_id)
                            ->groupBy('method_category')->select('method_category',db::raw('max(created_at) as test_finish'))->get();

            $dh->finish = $TestGroup[0]->test_finish;
            $dh->dry_process = "";
            $dh->niktech = explode("|", $sing->tech)[0];
            $dh->nametech = explode("|", $sing->tech)[1];
            $dh->posttech = explode("|", $sing->tech)[2];
            $dh->nikhead = explode("|", $sing->head)[0];
            $dh->namehead = explode("|", $sing->head)[1];
            $dh->posthead = explode("|", $sing->head)[2];
            $dh->remark_final = $sing->remark_final;
            $dh->created_at = $sing->created_at;
            $list[$doci]['head']=$dh;
            foreach ($TestGroup as $tg) {
                $methid='';
                $content = [];
                $drest = DB::table('ns_report_test')
                            ->where('id',$dh->id)
                            ->where('trf_doc_id',$dh->doc_id)
                            ->where('method_category',$tg->method_category)
                            ->orderby('sequence','desc')->get();

                $content['header']=$tg->method_category;

                foreach ($drest as $d) {

                    if ($d->method_code=='PHX-AP0701' && $list[$doci]['head']->dry_process=="") {
                        $dh->dry_process = $d->perlakuan_test;
                    }else if ( $d->method_code!='PHX-AP0701'){
                        $dh->dry_process = "";
                    }

                    if ($methid=='' || $methid!=$d->trf_meth_id) {
                        $methid = $d->trf_meth_id;
                        $i=1; 
                    }else{
                        $i = $i+1;
                    }

                    if ($finRest=='PASS' && $d->result_status=='FAILED') {
                        $finRest = 'FAILED';
                    }else if ($finRest=='PASS' && $d->result_status=='PASS') {
                        $finRest = 'PASS';
                    }else if($finRest=='FAILED'){
                        $finRest = 'FAILED';
                    }
                    $dtcont['method_id']=$d->trf_meth_id;
                    $dtcont['method_code']=$d->method_code;
                    $dtcont['method_name']=$d->method_name;
                    $dtcont['standart']=HelperController::setReqOption($d->req_id);
                    $dtcont['parameter']=$d->parameter;
                    $dtcont['measuring_position']=$d->measuring_position;
                    $dtcont['code']=$d->code;
                    $dtcont['number_test']=$d->number_test;
                    $dtcont['supplier_result']=$d->supplier_result;
                    $dtcont['result']=$d->result;
                    $dtcont['uom']=$d->uom;
                    $dtcont['result_status']=$d->result_status;
                    $dtcont['remark_result']=$d->remark_result;
                    $dtcont['comment']=$d->comment;
                    $dtcont['rowspan']=$i;

                    $content['result'][]=$dtcont;
                }//drest

                $list[$doci]['content'][]=$content;

            }//TestGroup

            

            $getImg = DB::table('trf_testing_methods')
                                ->join('trf_requirement_set','trf_testing_methods.id','trf_requirement_set.trf_meth_id')
                                ->join('result_image','trf_requirement_set.id','result_image.set_req_id')
                                ->where('trf_testing_methods.trf_id',$dh->id)
                                ->where('result_image.trf_doc_id',$dh->doc_id)
                                ->whereNull('trf_testing_methods.deleted_at')
                                ->whereNull('trf_requirement_set.deleted_at')
                                ->whereNull('result_image.deleted_at')
                                ->select('result_image.id','result_image.img_number','result_image.remark')
                                ->get();

            $list[$doci]['imageRest']=$getImg; 
            $doci++;
        }//dataHead

        return View('report.form.report_trf',['list'=>$list,'escl'=>$escl]);
        // $pdf = \PDF::loadView('report.form.report_trf',['list'=>$list])->setPaper('A4','Potrait');
        // return $pdf->stream($id);

    }

    public function printReportTrfExt(Request $req){
        $id = trim($req->id);
        $list = [];

        $dataHead = DB::select("SELECT * FROM ns_get_header('".$id."');");
        $doci = 1;
        $sing = DB::select("SELECT * FROM ns_set_singnature3('".$id."')")[0];

        $escl = EscalationModel::where('trf_id',$id)->whereNull('deleted_at')->first();

        foreach ($dataHead as $dh) {
            $finRest = "PASS";
            $TestGroup = DB::table('ns_report_test')
                            ->where('id',$dh->id)
                            ->where('trf_doc_id',$dh->doc_id)
                            ->groupBy('method_category')->select('method_category',db::raw('max(created_at) as test_finish'))->get();

            $dh->finish = $TestGroup[0]->test_finish;
            $dh->dry_process = "";
            $dh->niktech = explode("|", $sing->tech)[0];
            $dh->nametech = explode("|", $sing->tech)[1];
            $dh->posttech = explode("|", $sing->tech)[2];
            $dh->nikhead = explode("|", $sing->head)[0];
            $dh->namehead = explode("|", $sing->head)[1];
            $dh->posthead = explode("|", $sing->head)[2];
            $dh->remark_final = $sing->remark_final;
            $dh->created_at = $sing->created_at;
            $list[$doci]['head']=$dh;

            
            foreach ($TestGroup as $tg) {
                $methid='';
                $content = [];
                $drest = DB::table('ns_report_test')
                            ->where('id',$dh->id)
                            ->where('trf_doc_id',$dh->doc_id)
                            ->where('method_category',$tg->method_category)
                            ->orderby('method_code','desc')
                            ->orderby('sequence','asc')
                            ->orderBy('code','asc')
                            ->orderby('remark_result','desc')
                            ->get();

                $content['header']=$tg->method_category;

                foreach ($drest as $d) {

                    if ($d->method_code=='PHX-AP0701' && $list[$doci]['head']->dry_process=="") {
                        $dh->dry_process = $d->perlakuan_test;
                    }else if ( $d->method_code!='PHX-AP0701'){
                        $dh->dry_process = "";
                    }

                    if ($methid=='' || $methid!=$d->trf_meth_id) {
                        $methid = $d->trf_meth_id;
                        $i=1; 
                    }else{
                        $i = $i+1;
                    }

                    if ($finRest=='PASS' &&  self::resultEscal($d->result_status,$escl->escalation)=='FAILED') {
                        $finRest = 'FAILED';
                    }else if ($finRest=='PASS' && self::resultEscal($d->result_status,$escl->escalation)=='PASS') {
                        $finRest = 'PASS';
                    }else if($finRest=='FAILED'){
                        $finRest = 'FAILED';
                    }
                    $dtcont['method_id']=$d->trf_meth_id;
                    $dtcont['method_code']=$d->method_code;
                    $dtcont['method_name']=$d->method_name;
                    $dtcont['standart']=HelperController::setReqOption($d->req_id);
                    $dtcont['parameter']=$d->parameter;
                    $dtcont['measuring_position']=$d->measuring_position;
                    $dtcont['code']=$d->code;
                    $dtcont['number_test']=$d->number_test;
                    $dtcont['supplier_result']=$d->supplier_result;
                    $dtcont['result']=$d->result;
                    $dtcont['uom']=$d->uom;
                    $dtcont['result_status']=self::resultEscal($d->result_status,$escl->escalation);
                    $dtcont['remark_result']=$d->remark_result;
                    $dtcont['comment']=$d->comment;
                    $dtcont['rowspan']=$i;

                    $content['result'][]=$dtcont;
                }//drest

                $list[$doci]['content'][]=$content;

            }//TestGroup

            

            $getImg = DB::table('trf_testing_methods')
                                ->join('trf_requirement_set','trf_testing_methods.id','trf_requirement_set.trf_meth_id')
                                ->join('result_image','trf_requirement_set.id','result_image.set_req_id')
                                ->where('trf_testing_methods.trf_id',$dh->id)
                                ->where('result_image.trf_doc_id',$dh->doc_id)
                                ->whereNull('trf_testing_methods.deleted_at')
                                ->whereNull('trf_requirement_set.deleted_at')
                                ->whereNull('result_image.deleted_at')
                                ->select('result_image.id','result_image.img_number','result_image.remark')
                                ->get();

            $list[$doci]['imageRest']=$getImg; 
            $doci++;
        }//dataHead

        return View('report.form.report_trf',['list'=>$list,'remarkesc'=>$escl->remark_escalation]);
        // $pdf = \PDF::loadView('report.form.report_trf',['list'=>$list])->setPaper('A4','Potrait');
        // return $pdf->stream($id);

    }

    private function resultEscal($result,$escal){
        if ($escal!=null) {
            if ($result=="PASS" && strtoupper($escal)=="PASS") {
               $ret = "PASS";
            }else if($result=="FAILED" && strtoupper($escal)=="PASS") {
               $ret = "PASS";
            }else if($result=="FAILED" && strtoupper($escal)=="FAILED") {
               $ret = "FAILED";
            }
        }else{
            $ret = $result;
        }

        return $ret;
    }
    //report trf

    //report log book
    public function reportlogbook(){
        $fact = DB::table('factory')
                    ->whereNull('deleted_at')->get();
        $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();
        return view('report.report_logbook')->with('fact',$fact)->with('buyer',$buyer);
    }

     public function getReportLogbook(Request $req){
        $factory_id = $req->factory_id;
        $origin = $req->origin;
        $buyer = $req->buyer;
        $from = carbon::parse(explode(" - ",$req->submitdate)[0])->format('Y-m-d 00:00:00');
        $to = carbon::parse(explode(" - ",$req->submitdate)[1])->format('Y-m-d 23:59:59');



        $data = DB::table('ns_report_logbook')
                        ->wherebetween('created_at',[$from,$to]);
                        // ->where('factory_id',$factory_id)
                        // ->where('asal_specimen',$origin);

        if ($factory_id!='ALL') {
           $data = $data->where('factory_id',$factory_id);
        }

        if ($origin!='ALL') {
           $data = $data->where('asal_specimen',$origin);
        }

        if ($buyer!='ALL') {
           $data = $data->where('buyer',$buyer);
        }

        return DataTables::of($data)
                            ->editColumn('trf_id',function($data){
                                return '<b>'.$data->trf_id.'</b><br>Submit date : '.carbon::parse($data->created_at)->format('d-m-Y H:i:s');
                            })
                            ->editColumn('username',function($data){
                                return $data->username." (".$data->nik.")";
                            })
                            ->editColumn('status',function($data){
                                return HelperController::statusCek($data->id);
                            })
                            ->editColumn('category',function($data){
                                return '<b>Category </b> : '.$data->category.'<br><b>Category Specimen</b> : '.$data->category_specimen.'<br><b>Type Specimen</b> : '.$data->type_specimen;
                            })
                            ->addColumn('specimen',function($data){
                                return HelperController::setDoc($data);
                            })
                            ->rawColumns(['username','status','category','trf_id','specimen'])
                            ->make(true);
    }

    public function exportLogBook(Request $req){
        $factory_id =$req->factory_id;
        $origin =$req->origin;
        $buyer =$req->buyer;
        $from = carbon::parse(explode(" - ",$req->submitdate)[0])->format('Y-m-d');
        $to = carbon::parse(explode(" - ",$req->submitdate)[1])->format('Y-m-d');

        $filter =trim($req->filter);

        $fact = DB::table('factory')->where('id',$factory_id)->first();

        $data = DB::table('ns_report_logbook')
                        ->wherebetween('created_at',[$from,$to])
                        // ->where('factory_id',$factory_id)
                        // ->where('asal_specimen',$origin)
                        ->orderby('created_at','asc');

        if ($factory_id!='ALL') {
           $data = $data->where('factory_id',$factory_id);
        }

        if ($origin!='ALL') {
           $data = $data->where('asal_specimen',$origin);
        }

        if ($buyer!='ALL') {
           $data = $data->where('buyer',$buyer);
        }

        if ($filter!=null) {
            $data = $data->where(function($query) use ($filter){
                $query->where('username','LIKE','%'.$filter.'%')
                            ->orwhere('nik','LIKE','%'.$filter.'%')
                            ->orwhere('trf_id','LIKE','%'.$filter.'%')
                            ->orwhere('lab_location','LIKE','%'.$filter.'%')
                            ->orwhere('buyer','LIKE','%'.$filter.'%')
                            ->orwhere('status','LIKE','%'.$filter.'%')
                            ->orwhere('category_specimen','LIKE','%'.$filter.'%')
                            ->orwhere('category','LIKE','%'.$filter.'%')
                            ->orwhere('type_specimen','LIKE','%'.$filter.'%');
            });
        }

        $filename = "Report Serah Terima ".$origin."_".$fact->factory_name." Submit Date".$from." - ".$to.".xlsx";
        $list = [];
        $no = 1;
        foreach ($data->get() as $dt) {

            if ($dt->verified_lab_date!=null) {
                $userVefif = User::where('nik',$dt->verified_lab_by)->whereNull('deleted_at')->first();
                $verif_by = $userVefif->name." (".$dt->lab_location.")";
                $verif_date = carbon::parse($dt->verified_lab_date)->format('d M Y H:i:s');
                $due = carbon::parse(HelperController::getDue($dt->verified_lab_date))->format('d M Y');
            }else{
                $verif_by ="";
                $verif_date = "";
                $due = "";
            }
            


            $dx['no']=$no;
            $dx['submit_date']=carbon::parse($dt->created_at)->format('d M Y H:i:s');
            $dx['trf_no']=$dt->trf_id;
            $dx['status']=$dt->status;
            $dx['category']=$dt->category;
            $dx['category_specimen']=$dt->category_specimen;
            $dx['type_specimen']=$dt->type_specimen;
            $dx['user']=$dt->username." (".$dt->nik.")\r \n".$dt->asal_specimen;
            $dx['style']=$dt->style;
            $dx['article']=$dt->article_no;
            $dx['verif_by']=$verif_by;
            $dx['verif_date']=$verif_date;
            $dx['leadtime']="3";
            $dx['due']=$due;

            $list[]=$dx;

            $no++;
        }

         // return Excel::download(new ReportLogBook($list), $filename);
        $legal = array(0, 0, 612.00, 1008.00);
        $pdf = \PDF::loadView('report.form.report_logbook',[
                                                            'list'=>$list,
                                                            'origin'=>$origin,
                                                            'factory'=>$fact->factory_name,
                                                            'date'=>carbon::parse($from)->format('d M Y')." - ".carbon::parse($to)->format('d M Y')
                                                        ])->setPaper($legal,'landscape');
        return $pdf->stream($filename);
    }
    //report log book

    //report all specimen
    public function reportAllspc(){
        $fact = DB::table('factory')
                    ->whereNull('deleted_at')->get();

        $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();

        return view('report.report_allspecimen')->with('fact',$fact)->with('buyer',$buyer);
    }

    public function getDataAllspc(Request $req){
        $factory_id = $req->factory_id;
        $origin = $req->origin;
        $buyer = $req->buyer;
        $from = carbon::parse(explode(" - ",$req->submitdate)[0])->format('Y-m-d 00:00:00');
        $to = carbon::parse(explode(" - ",$req->submitdate)[1])->format('Y-m-d 23:59:59');


        $data = DB::table('ns_report_logbook')
                        // ->wherebetween('created_at',[$from,$to])
                        // ->Orwherebetween('closed_at',[$from,$to])
                        ->where(function ($query) use ($from,$to){
                            $query->whereBetween('created_at',[$from,$to])
                            ->orwhereBetween('closed_at',[$from,$to]);
                        })
                        ->orderBy('created_at','asc');
                        // ->where('factory_id',$factory_id)
                        // ->where('asal_specimen',$origin);

        if ($buyer!="ALL") {
            $data = $data->where('buyer',$buyer);
        }

        if ($origin!="ALL") {
            $data = $data->where('asal_specimen',$origin);
        }

        if ($factory_id!="ALL") {
            $data = $data->where('factory_id',$factory_id);
        }

        return DataTables::of($data)
                            ->editColumn('trf_id',function($data){

                                if ($data->status=='CLOSED') {
                                    $dtclose = Carbon::parse($data->closed_at)->format('d-m-Y H:i:s');
                                }else{
                                    $dtclose = "";
                                }
                                return '<b>'.$data->trf_id.'</b><br>Submit date : '.carbon::parse($data->created_at)->format('d-m-Y H:i:s').'<br> Closed Date  : '.$dtclose;
                            })
                            ->editColumn('username',function($data){
                                return $data->username." (".$data->nik.")";
                            })
                            ->editColumn('status',function($data){
                                return HelperController::statusCek($data->id);
                            })
                            ->editColumn('category',function($data){
                                return '<b>Category </b> : '.$data->category.'<br><b>Category Specimen</b> : '.$data->category_specimen.'<br><b>Type Specimen</b> : '.$data->type_specimen;
                            })
                            ->addColumn('specimen',function($data){
                                return HelperController::setDoc($data);
                            })
                            ->rawColumns(['username','status','category','trf_id','specimen'])
                            ->make(true);
    }

    public function exportAllspc(Request $req){
        $factory_id =$req->factory_id;
        $origin =$req->origin;
        $buyer = $req->buyer;
        $from = carbon::parse(explode(" - ",$req->submitdate)[0])->format('Y-m-d 00:00:00');
        $to = carbon::parse(explode(" - ",$req->submitdate)[1])->format('Y-m-d 23:59:59');

        $filter =trim($req->filter);

        // $fact = DB::table('factory')->where('id',$factory_id)->first();

        $data = DB::table('ns_report_logbook')
                        // ->wherebetween('created_at',[$from,$to])
                        // ->Orwherebetween('closed_at',[$from,$to])
                        ->where(function ($query) use ($from,$to){
                            $query->whereBetween('created_at',[$from,$to])
                            ->orwhereBetween('closed_at',[$from,$to]);
                        })
                        // ->where('factory_id',$factory_id)
                        // ->where('asal_specimen',$origin)
                        ->orderby('created_at','asc');
       
        if ($buyer!="ALL") {

            
            $data = $data->where('buyer',$buyer);
        }

        if ($origin!="ALL") {
            
            $data = $data->where('asal_specimen',$origin);
        }

        if ($factory_id!="ALL") {
         
            $data = $data->where('factory_id',$factory_id);
        }
        // dd($buyer,$origin,$factory_id,$data->get());
        if ($filter!=null) {
            $data = $data->where(function($query) use ($filter){
                $query->where('username','LIKE','%'.$filter.'%')
                            ->orwhere('nik','LIKE','%'.$filter.'%')
                            ->orwhere('trf_id','LIKE','%'.$filter.'%')
                            ->orwhere('lab_location','LIKE','%'.$filter.'%')
                            ->orwhere('buyer','LIKE','%'.$filter.'%')
                            ->orwhere('status','LIKE','%'.$filter.'%')
                            ->orwhere('category_specimen','LIKE','%'.$filter.'%')
                            ->orwhere('category','LIKE','%'.$filter.'%')
                            ->orwhere('type_specimen','LIKE','%'.$filter.'%');
            });
        }

        $filename = "Report Specimen ".$origin."_".$factory_id." Submit Date".$from." - ".$to.".xlsx";
        $list = [];
        $no = 1;
        foreach ($data->get() as $dt) {

            if ($dt->verified_lab_date!=null) {
                $userVefif = User::where('nik',$dt->verified_lab_by)->whereNull('deleted_at')->first();
                $verif_by = $userVefif->name." (".$dt->lab_location.")";
                $verif_date = carbon::parse($dt->verified_lab_date)->format('d M Y H:i:s');
                $due = carbon::parse(HelperController::getDue($dt->verified_lab_date))->format('Y-m-d');
            }else{
                $verif_by ="";
                $verif_date = "";
                $due = "";
            }

            $release = SignatureSet::where('trf_id',$dt->id)->where('type','report_trf')->wherenull('deleted_at')->first();

            if ($release!=null) {
                $act = carbon::parse($release->created_at)->format('Y-m-d');

                if ($due>=$act) {
                    $remOT= "ONTIME";
                }else{
                    $remOT= "LATE";
                }
            }else{
                $act = "";
                $remOT = "";
            }
            
            $dx['no']=$no;
            $dx['submit_date']=carbon::parse($dt->created_at)->format('d M Y H:i:s');
            $dx['asal']=$dt->asal_specimen;
            $dx['factory']=$dt->factory_name;
            $dx['category']=$dt->category;
            $dx['category_specimen']=$dt->category_specimen;
            $dx['type_specimen']=$dt->type_specimen;
            $dx['trf_no']=$dt->trf_id;
            $dx['user']=$dt->username." (".$dt->nik.")\r \n".$dt->asal_specimen;
            $dx['doc_no']=$dt->document_no;
            $dx['buyer']=$dt->buyer;
            $dx['style']=$dt->style;
            $dx['article']=$dt->article_no;
            $dx['manufacture_name']=!empty(trim($dt->manufacture_name)) ? trim($dt->manufacture_name) : "-";
            $dx['verif_by']=$verif_by;
            $dx['verif_date']=$verif_date;
            $dx['leadtime']="3";
            $dx['due']=Carbon::parse($due)->format('d M Y');
            $dx['actual_release']=Carbon::parse($act)->format('d M Y');
            $dx['result']=HelperController::checkSpcResult($dt->docid);
            $dx['remOT']=$remOT;
            $dx['status']=$dt->status;
            $dx['closed_at']=($dt->status=='CLOSED') ? Carbon::parse($dt->closed_at) : null;
           
            
            
            
            
            

            $list[]=$dx;

            $no++;
        } 

         return Excel::download(new ReportAllspc($list), $filename);
        // $legal = array(0, 0, 612.00, 1008.00);
        // $pdf = \PDF::loadView('report.form.report_allspecimen',[
        //                                                     'list'=>$list,
        //                                                     'origin'=>$origin,
        //                                                     'factory'=>$fact->factory_name,
        //                                                     'date'=>carbon::parse($from)->format('d M Y')." - ".carbon::parse($to)->format('d M Y')
        //                                                 ])->setPaper($legal,'landscape');
        // return $pdf->stream($filename);
    }
    //report all specimen

}
