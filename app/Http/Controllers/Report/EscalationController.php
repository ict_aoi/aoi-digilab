<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\File;
use Uuid;

use App\User;
use App\Models\SignatureSet;

use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\EscalationModel;

use App\Http\Controllers\HelperController;

class EscalationController extends Controller
{
    public function index(){
        $fact = DB::table('factory')->wherenull('deleted_at')->get();

        return view('report.escalation.index')->with('fact',$fact);
    }

    public function getDataEcl(Request $req){
        $factory_id = $req->factory_id;
        $key        = trim($req->key);


        $data = DB::table('ns_report_test')
                            ->where('factory_id',$factory_id)
                            ->select(
                                    'id',
                                    'trf_id',
                                    'buyer',
                                    'lab_location',
                                    'platform',
                                    'factory_id',
                                    'asal_specimen',
                                    'category',
                                    'category_specimen',
                                    'type_specimen',
                                    DB::raw("string_agg(result_status,',') as result")
                                )
                            ->groupBy(
                                    'id',
                                    'trf_id',
                                    'buyer',
                                    'lab_location',
                                    'platform',
                                    'factory_id',
                                    'asal_specimen',
                                    'category',
                                    'category_specimen',
                                    'type_specimen'
                                )
                            ->having(DB::raw("string_agg(result_status,',')"),'LIKE','%FAILED%')
                            ->get();
        return DataTables::of($data)
                                ->editColumn('category',function($data){
                                    return "<b>Category : </b> ".$data->category."
                                            <br><b>Category Specimen : </b> ".$data->category_specimen."
                                            <br><b>Type Specimen : </b> ".$data->type_specimen;
                                })
                                ->addColumn('action',function($data){
                                    $cekEsc = EscalationModel::where('trf_id',$data->id)->whereNull('deleted_at')->exists();

                                    if (!$cekEsc) {
                                       return view('_action',[
                                                            'escalation'=>$data->id
                                                        ]);
                                    }else{
                                        return view('_action',[
                                                            'viewesc'=>$data->id
                                                        ]);
                                    }
                                    
                                })
                                ->setRowAttr([
                                    'style'=>function($data){

                                        $colEsc = EscalationModel::where('trf_id',$data->id)->whereNull('deleted_at')->exists();
                                        if ($colEsc) {
                                            return 'background-color:#ffa64d';
                                        }else{
                                            return 'background-color:#ffffff';
                                        }
                                    }
                                ])
                                ->rawColumns(['action','category'])->make(true);
    }

    public function viewData(Request $req){
        $id = $req->id;

        $data = DB::table('ns_report_test')
                        ->join('factory','ns_report_test.factory_id','factory.id')
                        ->where('ns_report_test.id',$id)
                        ->select(
                            'ns_report_test.id',
                            'ns_report_test.trf_id',
                            'ns_report_test.buyer',
                            'ns_report_test.lab_location',
                            'ns_report_test.platform',
                            'ns_report_test.factory_id',
                            'ns_report_test.asal_specimen',
                            'ns_report_test.category',
                            'ns_report_test.category_specimen',
                            'ns_report_test.type_specimen',
                            'ns_report_test.trf_doc_id',
                            'ns_report_test.document_type',
                            'ns_report_test.document_no',
                            'ns_report_test.style',
                            'ns_report_test.article_no',
                            'ns_report_test.size',
                            'ns_report_test.color',
                            'ns_report_test.fibre_composition',
                            'ns_report_test.fabric_finish',
                            'ns_report_test.gauge',
                            'ns_report_test.fabric_weight',
                            'ns_report_test.plm_no',
                            'ns_report_test.care_instruction',
                            'ns_report_test.manufacture_name',
                            'ns_report_test.export_to',
                            'ns_report_test.nomor_roll',
                            'ns_report_test.batch_number',
                            'ns_report_test.is_repetation',
                            'ns_report_test.item',
                            'ns_report_test.barcode_supplier',
                            'ns_report_test.additional_information',
                            'ns_report_test.yds_roll',
                            'ns_report_test.description',
                            'ns_report_test.season',
                            'ns_report_test.invoice',
                            'ns_report_test.fabric_color',
                            'ns_report_test.interlining_color',
                            'ns_report_test.qty',
                            'ns_report_test.machine',
                            'ns_report_test.temperature',
                            'ns_report_test.pressure',
                            'ns_report_test.duration',
                            'ns_report_test.style_name',
                            'ns_report_test.fabric_item',
                            'ns_report_test.fabric_type',
                            'ns_report_test.thread',
                            'ns_report_test.material_shell_panel',
                            'ns_report_test.product_category',
                            'ns_report_test.remark',
                            'ns_report_test.pad',
                            'ns_report_test.pelling',
                            'ns_report_test.component',
                            'ns_report_test.garment_size',
                            'ns_report_test.sample_type',
                            'ns_report_test.test_condition',
                            'ns_report_test.fabric_properties',
                            'ns_report_test.date_information',
                            'ns_report_test.date_information_remark',
                            'ns_report_test.test_required',
                            'ns_report_test.po_buyer',
                            'factory.factory_name'
                        )
                        ->groupBy(
                            'ns_report_test.id',
                            'ns_report_test.trf_id',
                            'ns_report_test.buyer',
                            'ns_report_test.lab_location',
                            'ns_report_test.platform',
                            'ns_report_test.factory_id',
                            'ns_report_test.asal_specimen',
                            'ns_report_test.category',
                            'ns_report_test.category_specimen',
                            'ns_report_test.type_specimen',
                            'ns_report_test.trf_doc_id',
                            'ns_report_test.document_type',
                            'ns_report_test.document_no',
                            'ns_report_test.style',
                            'ns_report_test.article_no',
                            'ns_report_test.size',
                            'ns_report_test.color',
                            'ns_report_test.fibre_composition',
                            'ns_report_test.fabric_finish',
                            'ns_report_test.gauge',
                            'ns_report_test.fabric_weight',
                            'ns_report_test.plm_no',
                            'ns_report_test.care_instruction',
                            'ns_report_test.manufacture_name',
                            'ns_report_test.export_to',
                            'ns_report_test.nomor_roll',
                            'ns_report_test.batch_number',
                            'ns_report_test.is_repetation',
                            'ns_report_test.item',
                            'ns_report_test.barcode_supplier',
                            'ns_report_test.additional_information',
                            'ns_report_test.yds_roll',
                            'ns_report_test.description',
                            'ns_report_test.season',
                            'ns_report_test.invoice',
                            'ns_report_test.fabric_color',
                            'ns_report_test.interlining_color',
                            'ns_report_test.qty',
                            'ns_report_test.machine',
                            'ns_report_test.temperature',
                            'ns_report_test.pressure',
                            'ns_report_test.duration',
                            'ns_report_test.style_name',
                            'ns_report_test.fabric_item',
                            'ns_report_test.fabric_type',
                            'ns_report_test.thread',
                            'ns_report_test.material_shell_panel',
                            'ns_report_test.product_category',
                            'ns_report_test.remark',
                            'ns_report_test.pad',
                            'ns_report_test.pelling',
                            'ns_report_test.component',
                            'ns_report_test.garment_size',
                            'ns_report_test.sample_type',
                            'ns_report_test.test_condition',
                            'ns_report_test.fabric_properties',
                            'ns_report_test.date_information',
                            'ns_report_test.date_information_remark',
                            'ns_report_test.test_required',
                            'ns_report_test.po_buyer',
                            'factory.factory_name'
                        )
                        ->get();

        return view('report.escalation.content')->with('data',$data);
    }


    static function getDetail($trfid,$docid){
        $data = DB::table('ns_report_test')
                        ->where('id',$trfid)
                        ->where('trf_doc_id',$docid)
                        ->whereNotNull('created_at')
                        ->orderby('method_code','asc')
                        ->orderby('code','asc')
                        ->orderby('measuring_position','asc')
                        ->orderby('parameter','asc')
                        ->get();

        return $data;
    }


    public function createEscalation(Request $req){
        $trfid = $req->id;
        $escalation = $req->escalation;
        $remark_escalation = trim($req->remark_escalation);

        try {
            DB::beginTransaction();
                $cek = EscalationModel::where('trf_id',$trfid)->whereNull('deleted_at')->exists();

                if ($cek) {
                     return response()->json("TRF was escalation ! ! !",422);
                }

                EscalationModel::firstorcreate([
                                                'trf_id'=>$trfid,
                                                'escalation'=>strtoupper($escalation),
                                                'remark_escalation'=>$remark_escalation,
                                                'created_at'=>carbon::now(),
                                                'created_by'=>auth::user()->id
                                                ]);



            DB::commit();
            $data_response = ['status'=>200,"output"=>"Escalation Success ! ! ! "];
        } catch (Exception $e) {
            DB::rollBack();
            $data_response = ['status'=>422,"output"=>"Escalation Failed ! ! ! ".$e->getMessage()];
        }

        return response()->json(['data_response'=>$data_response]);
    }

    public function viewEsc(Request $req){
        $id =$req->id;

        $data = DB::table('trf_escalation')
                    ->join('trf_testings','trf_escalation.trf_id','trf_testings.id')
                    ->join('master_category','trf_testings.id_category','master_category.id')
                    ->join('factory','trf_testings.factory_id','factory.id')
                    ->whereNull('trf_escalation.deleted_at')
                    ->whereNull('trf_testings.deleted_at')
                    ->where('trf_escalation.trf_id',$id)
                    ->select(
                            'trf_testings.trf_id',
                            'trf_testings.asal_specimen',
                            'trf_testings.lab_location',
                            'trf_testings.buyer',
                            'factory.factory_name',
                            'master_category.category',
                            'master_category.category_specimen',
                            'master_category.type_specimen',
                            'trf_escalation.escalation',
                            'trf_escalation.remark_escalation'
                    )
                    ->first();
                
        return response()->json(['data'=>$data],200);
    }

}
