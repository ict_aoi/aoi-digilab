<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\File;
use Uuid;

use Maatwebsite\Excel\Excel as ExcelExcel;

use App\Exports\ReportLogBook;
use App\Exports\ReportAllspc;
use App\Exports\ReportMethodResult;

use App\User;
use App\Models\SignatureSet;

use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterLabLocation;
use App\Models\EscalationModel;
use App\Models\TrfReturnSample;

use App\Http\Controllers\HelperController;
use Illuminate\Support\Facades\DB as FacadesDB;
use Users;

class ReportController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time',3800);
    }

    public function getTech(Request $req){
        $tech = explode(',',$req->tech);

        $tech = User::whereIn(DB::raw('id::varchar'),$tech)->select('id','name','nik')->get();
        $head = User::whereIn('position',['LAB SUB. DEPT. HEAD','LAB DEPT. HEAD'])->wherenull('deleted_at')->select('id','name','nik','position')->get();
        
        return response()->json(['tech'=>$tech,'head'=>$head],200);
    }

    public function releaseReport(Request $req){
        try {
            DB::beginTransaction();

                $tech = DB::table('trf_testings')
                                    ->join('users','trf_testings.verified_lab_by','users.nik')
                                    ->where('trf_testings.id',$req->id)
                                    ->select('users.id')->first();
                $labhead = User::whereIn('position',['LAB SUB. DEPT. HEAD','DEPT. HEAD'])
                                    ->whereNull('deleted_at')->orderBy('created_at','desc')->first();

                $arin = [
                    'trf_id'=>$req->id,
                    'tech'=>$tech->id,
                    'head'=>$labhead->id,
                    'type'=>'report_trf'
                ];
               
                SignatureSet::firstorcreate($arin);
            DB::commit();
            $data_response = ['status'=>200,"output"=>"Set Report Success ! ! !"];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>"Set Report Failed ! ! ! ".$message];
        }

        return response()->json(['data_response'=>$data_response]);
    }

    //report trf
    public function reportTrf(){
        $fact = DB::table('factory')->whereNull('deleted_at')->get();
        $labloc = MasterLabLocation::whereNull('deleted_at')->orderby('id','asc')->get();
        $crl = HelperController::cekRoleReport();

        return view('report.report_trf')->with('factory',$fact)
                                        ->with('labloc',$labloc)
                                        ->with('crl',$crl);
    }

    public function getDataReportTrf(Request $req){
        $key = trim($req->key);
        $fil = trim($req->search['value']);
        $factory_id = $req->factory_id;
        $lab_location = $req->lab_location;
        
        $data= DB::table('ns_report_test')
                                // ->where('factory_id',$factory_id)
                                ->where('lab_location',$lab_location)
                                ->whereNotNull('created_at')
                                ->groupBy([
                                    'id',
                                    'trf_id',
                                    'buyer',
                                    'lab_location',
                                    'platform',
                                    'factory_id',
                                    'asal_specimen',
                                    'date_information',
                                    'date_information_remark',
                                    'test_required',
                                    'part_of_specimen',
                                    'return_test_sample',
                                    'status',
                                    'category',
                                    'category_specimen',
                                    'type_specimen'
                                ])
                                ->select(
                                    'id',
                                    'trf_id',
                                    'buyer',
                                    'lab_location',
                                    'platform',
                                    'factory_id',
                                    'asal_specimen',
                                    'date_information',
                                    'date_information_remark',
                                    'test_required',
                                    'part_of_specimen',
                                    'return_test_sample',
                                    'status',
                                    'category',
                                    'category_specimen',
                                    'type_specimen',
                                    db::raw("string_agg(created_by::varchar,',') as techid")
                                )
                                // ->where('status','CLOSED')
                                // ->having(DB::raw("string_agg(COALESCE(created_by,0)::VARCHAR,',')"),"not like","%0%")
                                ->orderby('trf_id','desc');
                                    


            
        if (isset($key)) {

            $data = $data->where(function($query) use ($key){
                $query->where('style','LIKE','%'.$key.'%')
                            ->orwhere('article_no','LIKE','%'.$key.'%')
                            ->orwhere('method_code','LIKE','%'.$key.'%')
                            ->orwhere('method_name','LIKE','%'.$key.'%')
                            ->orwhere('trf_id','LIKE','%'.$key.'%')
                            ->orwhere('lab_location','LIKE','%'.$key.'%')
                            ->orwhere('document_no','LIKE','%'.$key.'%')
                            ->orwhere('buyer','LIKE','%'.$key.'%')
                            ->orwhere('nik','LIKE','%'.$key.'%');

            });
            
                           
        }else{
            $from = carbon::now()->subday(14)->format('Y-m-d 00:00:00');
            $to = carbon::now()->format('Y-m-d H:i:s');

            // dd($from,$to);
            $data = $data->wherebetween('created_at',[$from,$to]);
        }

      
        if (isset($fil)) {
            $data = $data->where(function($query) use ($fil){
                $query->where('style','LIKE','%'.$fil.'%')
                ->orwhere('article_no','LIKE','%'.$fil.'%')
                ->orwhere('method_code','LIKE','%'.$fil.'%')
                ->orwhere('method_name','LIKE','%'.$fil.'%')
                ->orwhere('trf_id','LIKE','%'.$fil.'%')
                ->orwhere('lab_location','LIKE','%'.$fil.'%')
                ->orwhere('document_no','LIKE','%'.$fil.'%')
                ->orwhere('buyer','LIKE','%'.$fil.'%')
                ->orwhere('nik','LIKE','%'.$fil.'%');
            });
            
        }
   
        return DataTables::of($data)
                                ->editColumn('trf_id',function($data){
                                    switch ($data->status) {
                                        case 'CLOSED':
                                            $status = '<span class="label label-success">CLOSED</span>';
                                            break;
                                        
                                        case 'ONPROGRESS':
                                            $status = '<span class="label label-warning">ONPROGRESS</span>';
                                            break;
                                        
                                        default:
                                        $status = '<span class="label label-danger">'.$data->status.'</span>';
                                            break;
                                    }

                                    return $data->trf_id."<br>".$status;
                                })
                                ->addColumn('specimen',function($data){
                                    $doc = TrfTestingDocument::where('trf_id',$data->id)->get();
                                    $ret = "";
                                    foreach ($doc as $dc) {
                                        $ret = $ret." ".HelperController::setDoc($dc)." <hr>";
                                    }

                                    return $ret;
                                })
                                ->editColumn('category',function($data){
                                    return '<b>Category </b> : '.$data->category.'
                                    <br><b>Category Specimen </b> : '.$data->category_specimen.'
                                    <br><b>Type Specimen </b> : '.$data->type_specimen;
                                })
                                ->addColumn('method',function($data){
                                    $meth = DB::table('trf_testing_methods')->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                                            ->where('trf_testing_methods.trf_id',$data->id)
                                            ->whereNull('trf_testing_methods.deleted_at')
                                            ->groupBy('trf_testing_methods.trf_id')
                                            ->select(db::raw("string_agg(concat('<b>',master_method.method_code,'</b>',master_method.method_name,' '), '<br>') as methods"))->first();
                                    return $meth->methods;
                                })
                                ->addColumn('action',function($data){
                                


                                    $labrole = auth::user()->hasRole(['ICT','ME','LAB','LAB TECHNICIAN','LAB ADMIN','LAB SECTION HEAD']);

                                    $cekSign = SignatureSet::where('type','report_trf')
                                                                ->where('trf_id',$data->id)
                                                                ->whereNull('deleted_at')->first();
                                
                                    if ($cekSign==null && $labrole==true) {
                                        if ($data->status=="CLOSED") {
                            
                                            return view('_action',[
                                                        'releaseTrf'=>[
                                                                    'id'=>$data->id
                                                                ]
                                                    ]);
                                        }else{
                                            return view('_action',[
                                                'PrintReportTrf'=>[
                                                            'id'=>$data->id,
                                                            'sign'=>null
                                                        ],
                                            ]);
                                        }
                                    }else if($cekSign==null && $labrole==false){
                                        return '<span class="label label-warning">On Progress Release Report</span>';
                                    }else{
                                        return view('_action',[
                                                    'PrintReportTrf'=>[
                                                                'id'=>$data->id,
                                                                'sign'=>json_encode($cekSign)
                                                            ],
                                                    // 'PrintReportTrf2'=>[
                                                    //             'id'=>$data->id,
                                                    //             'sign'=>json_encode($cekSign)
                                                    //         ],
                                                ]);
                                    }
                                    
                                })
                                ->rawColumns(['trf_id','specimen','method','action','category'])
                                ->make(true);

    }

    public function printReportTrf(Request $req){
        $id = trim($req->id);
        $list = [];
        $final_result = "PASS";
        $dataHead = DB::select("SELECT * FROM ns_get_header('".$id."');");

        $data = DB::table('trf_testing_methods')
                                ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                                ->where('trf_testing_methods.trf_id',$id)
                                ->whereNull('trf_testing_methods.deleted_at')
                                ->groupBy('master_method.category')
                                ->select(
                                    'master_method.category',
                                    DB::raw("string_agg(concat(trf_testing_methods.id,'|',master_method.method_code,'|',method_name),'+') as method")
                                )->get();

            
        foreach ($data as $dt) {
            $x = [];    
           
            $x['header'] = $dt->category;

            foreach (explode('+',$dt->method) as $lm) {
          
                $expmtd = explode('|',$lm);

                $content = [];
        
                $content['category']   =$dt->category;
                $content['method_code']=$expmtd[1];
                $content['method_name']=$expmtd[2];

                $standart = null;
                $parameter = null;
                $treatment = null;
                $cycle = null;
    
                $rowmeth =0;
                $rowstd =0;
                $rowparam =0;
                $rowcycle = 0;
                $rowtreat = 0;
                $i =1;

                $rest = DB::select("SELECT * FROM ns_method_result('".$expmtd[0]."')");

                foreach ($rest as $rs) {

                    $std = HelperController::operatorVal($rs->id);
    
                    if($standart!=$std){
                        $standart = $std;
                        $rowstd = 1;

                        $treatment = $rs->perlakuan_test;
                        $rowtreat=1;
                    }else{
                        $rowstd++;

                        if($treatment!=$rs->perlakuan_test){
                            $treatment = $rs->perlakuan_test;
                            $rowtreat=1;
                            
                        }else{
                            $rowtreat++;
                        }

                    }
    
                    if($parameter!=$rs->parameter){
                        $parameter = $rs->parameter;
                        $rowparam = 1;
                    }else{
                        $rowparam++;
                    }
    
                    if($cycle!=$rs->remark || $rs->remark==""){
                        $cycle = $rs->remark;
                        $rowcycle = 1;
                    }else{
                        $rowcycle++;
                    }
    
                    $rowmeth++;
                    $rs->standart = $std;
                    // $rs->rowmeth = $rowmeth;
                    $rs->rowstd = $rowstd;
                    $rs->rowparam = $rowparam;
                    $rs->rowcycle = $rowcycle;
                    $rs->rowtreat = $rowtreat;
                    $rs->i = $i++;
    
                    $content['list'][]=$rs;
                    $content['rowmeth']=$rowmeth;
    
                    
                }
                $x['content'][$expmtd[0]]=$content;

            }
            $list[]=$x;
        }
        
        $images = DB::select("SELECT * FROM ns_get_images('".$id."')");

        return view('report.export.print')->with('header',$dataHead[0])
                                            ->with('list',$list)
                                            ->with('images',$images);
    }

    // public function printReportTrfExt(Request $req){
    //     $id = trim($req->id);
    //     $list = [];

    //     $dataHead = DB::select("SELECT * FROM ns_get_header('".$id."');");
    //     $doci = 1;
    //     $sing = DB::select("SELECT * FROM ns_set_singnature3('".$id."')")[0];

    //     $escl = EscalationModel::where('trf_id',$id)->whereNull('deleted_at')->first();

    //     foreach ($dataHead as $dh) {
    //         $finRest = "PASS";
    //         $TestGroup = DB::table('ns_report_test')
    //                         ->where('id',$dh->id)
    //                         ->where('trf_doc_id',$dh->doc_id)
    //                         ->groupBy('method_category')->select('method_category',db::raw('max(created_at) as test_finish'))->get();

    //         $dh->finish = $TestGroup[0]->test_finish;
    //         $dh->dry_process = "";
    //         $dh->niktech = explode("|", $sing->tech)[0];
    //         $dh->nametech = explode("|", $sing->tech)[1];
    //         $dh->posttech = explode("|", $sing->tech)[2];
    //         $dh->nikhead = explode("|", $sing->head)[0];
    //         $dh->namehead = explode("|", $sing->head)[1];
    //         $dh->posthead = explode("|", $sing->head)[2];
    //         $dh->remark_final = $sing->remark_final;
    //         $dh->created_at = $sing->created_at;
    //         $list[$doci]['head']=$dh;

            
    //         foreach ($TestGroup as $tg) {
    //             $i = 0;
    //             $methid='';
    //             $content = [];
    //             $drest = DB::table('ns_report_test')
    //                         ->where('id',$dh->id)
    //                         ->where('trf_doc_id',$dh->doc_id)
    //                         ->where('method_category',$tg->method_category)
    //                         ->orderby('method_code','desc')
    //                         ->orderby('sequence','asc')
    //                         ->orderBy('code','asc')
    //                         ->orderby('remark_result','desc')
    //                         ->get();

    //             $content['header']=$tg->method_category;

    //             foreach ($drest as $d) {

    //                 if ($d->method_code=='PHX-AP0701' && $list[$doci]['head']->dry_process=="") {
    //                     $dh->dry_process = $d->perlakuan_test;
    //                 }else if ( $d->method_code!='PHX-AP0701'){
    //                     $dh->dry_process = "";
    //                 }

    //                 if ($methid=='' || $methid!=$d->trf_meth_id) {
    //                     $methid = $d->trf_meth_id;
    //                     $i=1; 
    //                 }else{
    //                     $i = $i+1;
    //                 }

    //                 if ($finRest=='PASS' &&  self::resultEscal($d->result_status,$escl->escalation)=='FAILED') {
    //                     $finRest = 'FAILED';
    //                 }else if ($finRest=='PASS' && self::resultEscal($d->result_status,$escl->escalation)=='PASS') {
    //                     $finRest = 'PASS';
    //                 }else if($finRest=='FAILED'){
    //                     $finRest = 'FAILED';
    //                 }
    //                 $dtcont['method_id']=$d->trf_meth_id;
    //                 $dtcont['method_code']=$d->method_code;
    //                 $dtcont['method_name']=$d->method_name;
    //                 $dtcont['standart']=HelperController::setReqOption($d->req_id);
    //                 $dtcont['parameter']=$d->parameter;
    //                 $dtcont['measuring_position']=$d->measuring_position;
    //                 $dtcont['code']=$d->code;
    //                 $dtcont['number_test']=$d->number_test;
    //                 $dtcont['supplier_result']=$d->supplier_result;
    //                 $dtcont['result']=$d->result;
    //                 $dtcont['uom']=$d->uom;
    //                 $dtcont['result_status']=self::resultEscal($d->result_status,$escl->escalation);
    //                 $dtcont['remark_result']=$d->remark_result;
    //                 $dtcont['comment']=$d->comment;
    //                 $dtcont['rowspan']=$i;

    //                 $content['result'][]=$dtcont;
    //             }//drest

    //             $list[$doci]['content'][]=$content;

    //         }//TestGroup

            

    //         $getImg = DB::table('trf_testing_methods')
    //                     ->join('trf_requirement_set','trf_testing_methods.id','=','trf_requirement_set.trf_meth_id')
    //                     ->join('result_image','trf_requirement_set.id','=','result_image.set_req_id')
    //                     ->where('result_image.trf_doc_id',$req->docid)
    //                     ->whereNull('trf_requirement_set.deleted_at')
    //                     ->whereNull('result_image.deleted_at')
    //                     ->where('trf_requirement_set.active',true)
    //                     ->select(
    //                         'result_image.id',
    //                         'result_image.img_number',
    //                         'result_image.remark'
    //                     )->get();

    //         $list[$doci]['imageRest']=$getImg; 
    //         $doci++;
    //     }//dataHead

    //     return View('report.form.report_trf',['list'=>$list,'remarkesc'=>$escl->remark_escalation]);
    //     // $pdf = \PDF::loadView('report.form.report_trf',['list'=>$list])->setPaper('A4','Potrait');
    //     // return $pdf->stream($id);

    // }

    private function resultEscal($result,$escal){
        if ($escal!=null) {
            if ($result=="PASS" && strtoupper($escal)=="PASS") {
               $ret = "PASS";
            }else if($result=="FAILED" && strtoupper($escal)=="PASS") {
               $ret = "PASS";
            }else if($result=="FAILED" && strtoupper($escal)=="FAILED") {
               $ret = "FAILED";
            }
        }else{
            $ret = $result;
        }

        return $ret;
    }
    //report trf

    //report log book
    public function reportlogbook(){
        $fact = DB::table('factory')
                    ->whereNull('deleted_at')->get();
        $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();
        return view('report.report_logbook')->with('fact',$fact)->with('buyer',$buyer);
    }

     public function getReportLogbook(Request $req){
        $factory_id = $req->factory_id;
        $origin = $req->origin;
        $buyer = $req->buyer;
        $from = carbon::parse(explode(" - ",$req->submitdate)[0])->format('Y-m-d 00:00:00');
        $to = carbon::parse(explode(" - ",$req->submitdate)[1])->format('Y-m-d 23:59:59');

        $data = DB::table('ns_report_logbook')
                        ->wherebetween('created_at',[$from,$to]);
                        // ->where('factory_id',$factory_id)
                        // ->where('asal_specimen',$origin);

        if ($factory_id!='ALL') {
           $data = $data->where('factory_id',$factory_id);
        }

        if ($origin!='ALL') {
           $data = $data->where('asal_specimen',$origin);
        }

        if ($buyer!='ALL') {
           $data = $data->where('buyer',$buyer);
        }

        return DataTables::of($data)
                            ->editColumn('trf_id',function($data){
                                return '<b>'.$data->trf_id.'</b><br>Submit date : '.carbon::parse($data->created_at)->format('d-m-Y H:i:s');
                            })
                            ->editColumn('username',function($data){
                                return $data->username." (".$data->nik.")";
                            })
                            ->editColumn('status',function($data){
                                return HelperController::statusCek($data->id);
                            })
                            ->editColumn('category',function($data){
                                return '<b>Category </b> : '.$data->category.'<br><b>Category Specimen</b> : '.$data->category_specimen.'<br><b>Type Specimen</b> : '.$data->type_specimen;
                            })
                            ->addColumn('specimen',function($data){
                                return HelperController::setDoc($data);
                            })
                            ->rawColumns(['username','status','category','trf_id','specimen'])
                            ->make(true);
    }

    public function exportLogBook(Request $req){
        $factory_id =$req->factory_id;
        $origin =$req->origin;
        $buyer =$req->buyer;
        $from = carbon::parse(explode(" - ",$req->submitdate)[0])->format('Y-m-d 00:00:00');
        $to = carbon::parse(explode(" - ",$req->submitdate)[1])->format('Y-m-d 23:59:59');

        $filter =trim($req->filter);

        $fact = DB::table('factory')->where('id',$factory_id)->first();

        $data = DB::table('ns_report_logbook')
                        ->wherebetween('created_at',[$from,$to])
                        // ->where('factory_id',$factory_id)
                        // ->where('asal_specimen',$origin)
                        ->orderby('created_at','asc');

        if ($factory_id!='ALL') {
           $data = $data->where('factory_id',$factory_id);
        }

        if ($origin!='ALL') {
           $data = $data->where('asal_specimen',$origin);
        }

        if ($buyer!='ALL') {
           $data = $data->where('buyer',$buyer);
        }

        if ($filter!=null) {
            $data = $data->where(function($query) use ($filter){
                $query->where('username','LIKE','%'.$filter.'%')
                            ->orwhere('nik','LIKE','%'.$filter.'%')
                            ->orwhere('trf_id','LIKE','%'.$filter.'%')
                            ->orwhere('lab_location','LIKE','%'.$filter.'%')
                            ->orwhere('buyer','LIKE','%'.$filter.'%')
                            ->orwhere('status','LIKE','%'.$filter.'%')
                            ->orwhere('category_specimen','LIKE','%'.$filter.'%')
                            ->orwhere('category','LIKE','%'.$filter.'%')
                            ->orwhere('type_specimen','LIKE','%'.$filter.'%');
            });
        }

        $filename = "Report Serah Terima ".$origin."_".$fact->factory_name." Submit Date".$from." - ".$to.".xlsx";
        $list = [];
        $no = 1;
        foreach ($data->get() as $dt) {

            if ($dt->verified_lab_date!=null) {
                $userVefif = User::where('nik',$dt->verified_lab_by)->first();
                $verif_by = $userVefif->name." (".$dt->lab_location.")";
                $verif_date = carbon::parse($dt->verified_lab_date)->format('d M Y H:i:s');
                $due = carbon::parse(HelperController::getDue($dt->verified_lab_date))->format('d M Y');
            }else{
                $verif_by ="";
                $verif_date = "";
                $due = "";
            }
            


            $dx['no']=$no;
            $dx['submit_date']=carbon::parse($dt->created_at)->format('d M Y H:i:s');
            $dx['trf_no']=$dt->trf_id;
            $dx['status']=$dt->status;
            $dx['category']=$dt->category;
            $dx['category_specimen']=$dt->category_specimen;
            $dx['type_specimen']=$dt->type_specimen;
            $dx['user']=$dt->username." (".$dt->nik.")\r \n".$dt->asal_specimen;
            $dx['style']=$dt->style;
            $dx['article']=$dt->article_no;
            $dx['verif_by']=$verif_by;
            $dx['verif_date']=$verif_date;
            $dx['leadtime']="3";
            $dx['due']=$due;

            $list[]=$dx;

            $no++;
        }

         // return Excel::download(new ReportLogBook($list), $filename);
        $legal = array(0, 0, 612.00, 1008.00);
        $pdf = \PDF::loadView('report.form.report_logbook',[
                                                            'list'=>$list,
                                                            'origin'=>$origin,
                                                            'factory'=>$fact->factory_name,
                                                            'date'=>carbon::parse($from)->format('d M Y')." - ".carbon::parse($to)->format('d M Y')
                                                        ])->setPaper($legal,'landscape');
        return $pdf->stream($filename);
    }
    //report log book

    //report all specimen
    public function getCategorySpc(Request $req) {
        $spc = ['STRIKE OFF', 'GARMENT', 'FABRIC', 'MOCKUP', 'ACCESORIES/TRIM', 'HIJAB', 'PANEL'];
    
        if (!empty($req->origin)) {
            switch ($req->origin) {
                case 'DEVELOPMENT':
                    $spc = ['STRIKE OFF', 'GARMENT', 'FABRIC', 'MOCKUP', 'ACCESORIES/TRIM', 'HIJAB'];
                    break;
    
                case 'PRODUCTION':
                    $spc = ['FABRIC', 'ACCESORIES/TRIM', 'GARMENT', 'MOCKUP', 'STRIKE OFF', 'PANEL', 'HIJAB'];
                    break;
    
                default:
                    $spc = ['STRIKE OFF', 'GARMENT', 'FABRIC', 'MOCKUP', 'ACCESORIES/TRIM', 'HIJAB', 'PANEL'];
            }
        }

        $query = DB::table('ns_req_catg_method')
                    ->select('category_specimen')
                    ->distinct()
                    ->where('category', trim($req->category));
    
        
        if (!empty($req->buyer) && $req->buyer != 'ALL') {
            $query->where('buyer', trim($req->buyer));
        }
    
        if (!empty($spc)) {
            $query->whereIn('category_specimen', $spc);
        }
    
        $data = $query->get();
    
        return response()->json(['data' => $data], 200);
    }
    
    public function getTypeSpc(Request $req) {
        
        $buyer = $req->buyer;  
        $category = $req->category;  
        $category_specimen = $req->category_specimen;  
    
        $query = DB::table('ns_req_catg_method')
                    ->where('category', $category)
                    ->where('category_specimen', $category_specimen);
    
        if (!empty($buyer)&& $req->buyer != 'ALL') {
            $query->where('buyer', $buyer);
        }
    
        $data = $query->groupBy('type_specimen')
                      ->select('type_specimen')
                      ->distinct()
                      ->get();
    
        return response()->json($data);
    }    

    public function reportAllspc(){
        $fact = DB::table('factory')
                    ->whereNull('deleted_at')->get();

        $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();

        return view('report.report_allspecimen')->with('fact',$fact)->with('buyer',$buyer);
    }

    public function getDataAllspc(Request $req)
    {
        $factory_id = $req->factory_id;
        $origin = $req->origin;
        $buyer = $req->buyer;
        $from = Carbon::parse(explode(" - ", $req->submitdate)[0])->format('Y-m-d 00:00:00');
        $to = Carbon::parse(explode(" - ", $req->submitdate)[1])->format('Y-m-d 23:59:59');

        $data = DB::table('ns_report_logbook')
                    ->whereBetween('created_at', [$from, $to]);

        if ($buyer != "ALL") {
            $data = $data->where('buyer', $buyer);
        }

        if ($origin != "ALL") {
            $data = $data->where('asal_specimen', $origin);
        }

        if ($factory_id != "ALL") {
            $data = $data->where('factory_id', $factory_id);
        }

        if ($req->filled('category')) {
            $data = $data->where('category', $req->category);
        }

        if ($req->filled('category_specimen')) {
            $data = $data->where('category_specimen', $req->category_specimen);
        }

        if ($req->filled('type_specimen')) {
            $data = $data->where('type_specimen', $req->type_specimen);
        }

        return DataTables::of($data)
            ->editColumn('trf_id', function ($data) {
                return '<b>' . $data->trf_id . '</b><br>Submit date : ' . Carbon::parse($data->created_at)->format('d-m-Y H:i:s');
            })
            ->editColumn('username', function ($data) {
                return $data->username . " (" . $data->nik . ")";
            })
            ->editColumn('status', function ($data) {
                return HelperController::statusCek($data->id);
            })
            ->editColumn('category', function ($data) {
                return '<b>Category </b> : ' . $data->category . '<br><b>Category Specimen</b> : ' . $data->category_specimen . '<br><b>Type Specimen</b> : ' . $data->type_specimen;
            })
            ->addColumn('specimen', function ($data) {
                return HelperController::setDoc($data);
            })
            ->rawColumns(['username', 'status', 'category', 'trf_id', 'specimen'])
            ->make(true);
    }

    public function exportAllspc(Request $req){
        
        $factory_id =$req->factory_id;
        $origin =$req->origin;
        $buyer = $req->buyer;
        $from = carbon::parse(explode(" - ",$req->submitdate)[0])->format('Y-m-d 00:00:00');
        $to = carbon::parse(explode(" - ",$req->submitdate)[1])->format('Y-m-d 23:59:59');
       
        $filter =trim($req->filter);
        
        $data = DB::table('ns_report_logbook')
        ->whereBetween('created_at', [$from, $to])
        ->orderBy('created_at', 'asc')
        ->when($buyer && $buyer !== "ALL", function ($query) use ($buyer) {
            $query->where('buyer', $buyer);
        })
        ->when($origin && $origin !== "ALL", function ($query) use ($origin) {
            $query->where('asal_specimen', $origin);
        })
        ->when($factory_id && $factory_id !== "ALL", function ($query) use ($factory_id) {
            $query->where('factory_id', $factory_id);
        })
        ->when($req->filled('category') && $req->category !== "null", function ($query) use ($req) {
            $query->where('category', $req->category);
        })
        ->when($req->filled('category_specimen') && $req->category_specimen !== "null", function ($query) use ($req) {
            $query->where('category_specimen', $req->category_specimen);
        })
        ->when($req->filled('type_specimen') && $req->type_specimen !== "null", function ($query) use ($req) {
            $query->where('type_specimen', $req->type_specimen);
        })
        ->when($filter, function ($query) use ($filter) {
            $query->where(function ($subQuery) use ($filter) {
                $subQuery->where('username', 'LIKE', '%' . $filter . '%')
                    ->orWhere('nik', 'LIKE', '%' . $filter . '%')
                    ->orWhere('trf_id', 'LIKE', '%' . $filter . '%')
                    ->orWhere('lab_location', 'LIKE', '%' . $filter . '%')
                    ->orWhere('buyer', 'LIKE', '%' . $filter . '%')
                    ->orWhere('status', 'LIKE', '%' . $filter . '%')
                    ->orWhere('category_specimen', 'LIKE', '%' . $filter . '%')
                    ->orWhere('category', 'LIKE', '%' . $filter . '%')
                    ->orWhere('type_specimen', 'LIKE', '%' . $filter . '%');
            });
        })
        ->get();


        $filename = "Report Specimen ".$origin."_".$factory_id." Submit Date".$from." - ".$to.".xlsx";
        $list = [];
        $no = 1;

        foreach ($data as $dt) {

            if ($dt->verified_lab_date!=null) {
                $userVefif = User::where('nik',$dt->verified_lab_by)->first();
                $verif_by = $userVefif->name." (".$dt->lab_location.")";
                $verif_date = carbon::parse($dt->verified_lab_date)->format('d M Y H:i:s');
                $due = carbon::parse(HelperController::getDue($dt->verified_lab_date))->format('Y-m-d');
            }else{
                $verif_by ="";
                $verif_date = "";
                $due = "";
            }

            $release = SignatureSet::where('trf_id',$dt->id)->where('type','report_trf')->wherenull('deleted_at')->first();

            if ($release!=null) {
                $act = carbon::parse($release->created_at)->format('Y-m-d');

                if ($due>=$act) {
                    $remOT= "ONTIME";
                }else{
                    $remOT= "LATE";
                }
            }else{
                $act = "";
                $remOT = "";
            }
            
            $dx['no']=$no;
            $dx['submit_date']=carbon::parse($dt->created_at)->format('d M Y H:i:s');
            $dx['asal']=$dt->asal_specimen;
            $dx['factory']=$dt->factory_name;
            $dx['category']=$dt->category;
            $dx['category_specimen']=$dt->category_specimen;
            $dx['type_specimen']=$dt->type_specimen;
            $dx['trf_no']=$dt->trf_id;
            $dx['user']=$dt->username." (".$dt->nik.")\r \n".$dt->asal_specimen;
            $dx['doc_no']=$dt->document_no;
            $dx['buyer']=$dt->buyer;
            $dx['style']=$dt->style;
            $dx['article']=$dt->article_no;
            $dx['manufacture_name']=$dt->manufacture_name;
            $dx['verif_by']=$verif_by;
            $dx['verif_date']=$verif_date;
            $dx['leadtime']="3";
            $dx['due']=$due;
            $dx['actual_release']=$act;
            $dx['result']=HelperController::checkSpcResult($dt->docid);
            $dx['remOT']=$remOT;
            $dx['status']=$dt->status;
            $dx['closed_at']=$dt->closed_at;

            $list[]=$dx;

            $no++;
        } 

         return Excel::download(new ReportAllspc($list), $filename,ExcelExcel::XLSX);

        // $legal = array(0, 0, 612.00, 1008.00);
        // $pdf = \PDF::loadView('report.form.report_allspecimen',[
        //                                                     'list'=>$list,
        //                                                     'origin'=>$origin,
        //                                                     'factory'=>$fact->factory_name,
        //                                                     'date'=>carbon::parse($from)->format('d M Y')." - ".carbon::parse($to)->format('d M Y')
        //                                                 ])->setPaper($legal,'landscape');
        // return $pdf->stream($filename);
    }
    //report all specimen

    //report return sample
    public function reportReturn(){
        $factory = DB::table('factory')->whereNull('deleted_at')->get();
        $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();
        
        return view('report.report_return')->with('factory',$factory)->with('buyer',$buyer);
    }

    public function getDataReturn(Request $req){
        $submit = $req->submitdate;

        $from = date_format(date_create(trim(explode("-",$submit)[0])),'Y-m-d 00:00:00');
        $to = date_format(date_create(trim(explode("-",$submit)[1])),'Y-m-d 23:59:59');

        // dd($from,$to);
        $data = DB::table('trf_testings')
                            ->join('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                            ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->join('master_category','trf_testings.id_category','master_category.id')
                            ->whereBetween('trf_testings.created_at',[$from,$to])
                            ->where([
                                'trf_testings.factory_id'=>$req->factory_id,
                                'trf_testings.buyer'=>$req->buyer,
                                'trf_testings.asal_specimen'=>$req->asal,
                                'trf_testings.return_test_sample'=>true
                            ])
                            // ->where('trf_testings.factory_id',$req->factory_id)
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->select(
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.id',
                                'trf_testings.result_t2',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                DB::raw("string_agg(concat(master_method.method_code,'-',master_method.method_name), '<br>') as methods")
                            )->groupBy([
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.id',
                                'trf_testings.result_t2',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen'
                            ])
                            ->orderBy('created_at','desc');
                            // ->get();
                                // dd($data,(int)$req->factory_id,$req->asal,$req->buyer);
        return DataTables::of($data)
                                ->editColumn('trf_id',function($data){
                                    return '<label style="font-weight:bold">'.$data->trf_id.'</label> <br>
                                    <label>Submit Date : </label> '.date_format(date_create($data->created_at),'d-M-Y H:i:s');
                                })
                                ->editColumn('category',function($data){
                                    return '<label style="font-weight:bold"> Category : </label> '.$data->category.'<br>
                                    <label style="font-weight:bold"> Category Specimen : </label> '.$data->category_specimen.'<br>
                                    <label style="font-weight:bold"> Type Specimen : </label>'.$data->type_specimen;
                                })
                                ->addColumn('specimen',function($data){
                                    $spc = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->first();

                                    return HelperController::setDoc($spc);
                                })
                                ->editColumn('lab_location',function($data){
                                    return $data->lab_location.'<br>'.$data->asal_specimen;
                                })
                                ->editColumn('nik',function($data){
                                    return $data->username.'<br> ('.$data->nik.')';
                                })
                                ->addColumn('tested',function($data){
                                    $testby = DB::select("SELECT * FROM ns_getTestedBy('".$data->id."');");
                                    // $testby = DB::select("SELECT * FROM ns_getTestedBy('0929c570-8504-11ed-8ea6-d7d4c8f363d5');");

                                    // dd($testby[0]);
                                    if (!empty($testby)) {
                                        return $testby[0]->name." <br>(".$testby[0]->nik.")";
                                    }else{
                                        return "";
                                    }
                                })
                                ->editColumn('status',function($data){
                                    return HelperController::statusCek($data->id);
                                })
                                ->addColumn('return_status',function($data){
                                    $checkret = TrfReturnSample::where('trf_id',$data->id)->whereNull('deleted_at')->first();

                                    if($checkret!=null){
                                        return '<span class="label bg-primary">Returned at '.date_format(date_create($checkret->created_at),'Y-m-d').'<br> By '.$checkret->user_name.' ('.$checkret->user_nik.')</span>';
                                    }else{
                                        return '<span class="label bg-warning">Not Yet Returned </span>';
                                    }
                                })
                                ->rawColumns(['trf_id','category','specimen','lab_location','nik','tested','status','return_status'])
                                ->make(true);
    }

    public function exportReturnSample(Request $req){
        $submit = $req->submitdate;

        $from = date_format(date_create(trim(explode("-",$submit)[0])),'Y-m-d 00:00:00');
        $to = date_format(date_create(trim(explode("-",$submit)[1])),'Y-m-d 23:59:59');

        // dd($from,$to);
        $data = DB::table('trf_testings')
                            ->join('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                            ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->join('master_category','trf_testings.id_category','master_category.id')
                            ->leftJoin('trf_return_sample','trf_testings.id','trf_return_sample.trf_id')
                            ->whereBetween('trf_testings.created_at',[$from,$to])
                            ->where([
                                'trf_testings.factory_id'=>$req->factory_id,
                                'trf_testings.buyer'=>$req->buyer,
                                'trf_testings.asal_specimen'=>$req->asal,
                                'trf_testings.return_test_sample'=>true
                            ])
                            // ->where('trf_testings.factory_id',$req->factory_id)
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->select(
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.id',
                                'trf_testings.result_t2',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                'trf_return_sample.user_nik',
                                'trf_return_sample.user_name',
                                DB::raw("string_agg(concat(master_method.method_code,'-',master_method.method_name), '<br>') as methods"),
                                DB::raw("max(trf_return_sample.created_at) as return_date")
                            )->groupBy([
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.id',
                                'trf_testings.result_t2',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                'trf_return_sample.user_nik',
                                'trf_return_sample.user_name'
                            ])
                            ->orderBy('created_at','desc')->get();
                            
        foreach($data as $d){
            $testby = DB::select("SELECT * FROM ns_getTestedBy('".$d->id."');");

            if(!empty($testby)){
                $d->tested_nik = $testby[0]->nik;
                $d->tested_name = $testby[0]->name;
            }else{
                $d->tested_nik = "-";
                $d->tested_name = "-";
            }
        }

        $fact = DB::table('factory')->where('id',$req->factory_id)->first();
        
        $filename = $fact->factory_name."_".$req->asal."_".$req->buyer."_".$from."-".$to;

        $legal = array(0, 0, 612.00, 1008.00);
        $pdf = \PDF::loadView('report.form.report_return',[
                                                            'list'=>$data,
                                                            'factory_name'=>$fact->factory_name,
                                                            'submitdate'=>[$from,$to],
                                                            'buyer'=>$req->buyer,
                                                            'asal'=>$req->asal
                                                        ])->setPaper($legal,'landscape');
        return $pdf->stream($filename);

        // return view('report.form.report_return')->with('list',$data);
    }
    //report return sample

    //report method result
    public function methodResult(){
        $factory = DB::table('factory')->whereNull('deleted_at')->get();
        $lab = DB::table('master_lab_location')->whereNull('deleted_at')->get();

        return view('report.report_method_result')->with('factory',$factory)->with('lab',$lab);
    }

    public function getMethodResult(Request $request){
        
        $submit = $request->submitdate;

        $from = date_format(date_create(trim(explode("-",$submit)[0])),'Y-m-d 00:00:00');
        $to = date_format(date_create(trim(explode("-",$submit)[1])),'Y-m-d 23:59:59');

        $data = DB::table('trf_testings')
                            ->join('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                            ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->join('factory','trf_testings.factory_id','factory.id')
                            ->leftJoin('trf_requirement_set','trf_testing_methods.id','trf_requirement_set.trf_meth_id')
                            ->leftJoin('trf_result_test','trf_requirement_set.id','trf_result_test.set_req_id')
                            ->whereBetween('trf_testings.created_at',[$from,$to])
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->whereNull('trf_requirement_set.deleted_at')
                            ->whereNull('trf_result_test.deleted_at')
                            ->where('trf_requirement_set.active',true)
                            ->groupBy([
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.created_at',
                                'trf_testings.factory_id',
                                'factory.factory_name',
                                'trf_testings.buyer',
                                'trf_testings.asal_specimen',
                                'trf_testings.lab_location',
                                'master_method.method_code',
                                'master_method.method_name'
                            ])
                            ->select(
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.created_at',
                                'trf_testings.factory_id',
                                'factory.factory_name',
                                'trf_testings.buyer',
                                'trf_testings.asal_specimen',
                                'trf_testings.lab_location',
                                'master_method.method_code',
                                'master_method.method_name',
                                DB::raw("
                                    string_agg(trf_result_test.result_status, '|') as result_status
                                ")
                            );

        if ($request->factory_id!="ALL") {
           $data = $data->where('trf_testings.factory_id',$request->factory_id);
        }

        if ($request->origin!="ALL") {
            $data = $data->where('trf_testings.asal_specimen',$request->origin);
        }

        if ($request->labloct!="ALL") {
            $data = $data->where('trf_testings.lab_location',$request->labloct);
        }

        return datatables::of($data)
                            ->editColumn('created_at',function($data){
                                return date_format(date_create($data->created_at),'d-M-Y H:i:s');
                            })
                            ->editColumn('asal_specimen',function($data){
                                return $data->asal_specimen.'<br> ('.$data->factory_name.')';
                            })
                            ->editColumn('method_code',function($data){
                                return '('.$data->method_code.') '.$data->method_name;
                            })
                            ->editColumn('result_status',function($data){

                                $resarr = isset($data->result_status) ? explode("|",$data->result_status) : null;
                                if ($resarr!=null && in_array("FAILED",$resarr)) {
                                    return '<span class="label bg-danger">FAILED</span>';
                                }else if ($resarr!=null && !in_array("FAILED",$resarr) && in_array("PASS",$resarr)){
                                    return '<span class="label bg-success">PASS</span>';
                                }else{
                                    return '<span class="label bg-warning">NOT YET TESTED</span>';
                                }

                                return $data->result_status;
                            })
                            ->addColumn('pic',function($data){
                                $cek = isset($data->result_status) ? DB::select("SELECT * FROM ns_gettestedby('".$data->id."') WHERE nik is not null") : null;


                                if ($cek!=null) {
                                    $cek = $cek[0];
                                   return $cek->nik." - ".$cek->name;
                                }else{
                                    return null;
                                }
                            })
                            ->rawColumns(['asal_specimen','method_code','result_status','pic'])
                            ->make(true);
    }

    public function exportResultMethod(Request $request){
        $submit = $request->submitdate;

        $from = date_format(date_create(trim(explode("-",$submit)[0])),'Y-m-d 00:00:00');
        $to = date_format(date_create(trim(explode("-",$submit)[1])),'Y-m-d 23:59:59');

        $data = DB::table('trf_testings')
                            ->join('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                            ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->join('factory','trf_testings.factory_id','factory.id')
                            ->leftJoin('trf_requirement_set','trf_testing_methods.id','trf_requirement_set.trf_meth_id')
                            ->leftJoin('trf_result_test','trf_requirement_set.id','trf_result_test.set_req_id')
                            ->whereBetween('trf_testings.created_at',[$from,$to])
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->whereNull('trf_requirement_set.deleted_at')
                            ->whereNull('trf_result_test.deleted_at')
                            ->where('trf_requirement_set.active',true)
                            ->groupBy([
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.created_at',
                                'trf_testings.factory_id',
                                'factory.factory_name',
                                'trf_testings.buyer',
                                'trf_testings.asal_specimen',
                                'trf_testings.lab_location',
                                'master_method.method_code',
                                'master_method.method_name'
                            ])
                            ->select(
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.created_at',
                                'trf_testings.factory_id',
                                'factory.factory_name',
                                'trf_testings.buyer',
                                'trf_testings.asal_specimen',
                                'trf_testings.lab_location',
                                'master_method.method_code',
                                'master_method.method_name',
                                DB::raw("
                                    string_agg(trf_result_test.result_status, '|') as result_status
                                ")
                            );

        if ($request->factory_id!="ALL") {
           $data = $data->where('trf_testings.factory_id',$request->factory_id);
        }

        if ($request->origin!="ALL") {
            $data = $data->where('trf_testings.asal_specimen',$request->origin);
        }

        if ($request->labloct!="ALL") {
            $data = $data->where('trf_testings.lab_location',$request->labloct);
        }

        $filename = "Report Method Result ".$request->origin."_".$request->factory_id." Submit Date".$from." - ".$to.".xlsx";
        $list = [];
        $no = 1;

        foreach ($data->get() as $key => $value) {
            $x['no']=$no++;
            $x['created_at']=date_format(date_create($value->created_at),'d-M-Y H:i:s');
            $x['trf_id']=$value->trf_id;
            $x['lab_location']=$value->lab_location;
            $x['asal_specimen']=$value->asal_specimen;
            $x['factory_name']=$value->factory_name;
            $x['buyer']=$value->buyer;
            $x['method_code']=$value->method_code;
            $x['method_name']=$value->method_name;

            $resarr = isset($value->result_status) ? explode("|",$value->result_status) : null;

            if ($resarr!=null && in_array("FAILED",$resarr)) {
                $x['result_status']="FAILED";
            }elseif ($resarr!=null && !in_array("FAILED",$resarr) && in_array("PASS",$resarr)){
                $x['result_status']="PASS";
            }else{
                $x['result_status']="NOT YET TESTED";
            }
            
            $cekpic = isset($value->result_status) ? DB::select("SELECT * FROM ns_gettestedby('".$value->id."') WHERE nik is not null") : null;

            if ($cekpic!=null) {
                $x['pic'] = $cekpic[0]->nik." - ".$cekpic[0]->name;
             }else{
                $x['pic'] = null;
             }

             $list[]=$x;
        }

        return Excel::download(new ReportMethodResult($list), $filename,ExcelExcel::XLSX);

    }
    //report method result

}
