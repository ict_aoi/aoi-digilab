<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class erpLoadData extends Controller
{   
   

    
    static function getDocErp($docno,$type,$origin){
        $return = [];

        $i=1;
        

        switch ($type) {
            case 'PO BUYER':
                $data = DB::connection('erp')->select("select * From digilab_trf_so('".strtoupper($docno)."')");

                foreach ($data as $key => $value) {

                    $x['document_type']="PO BUYER";
                    $x['origin']=$origin;
                    $x['document_no']= isset($value->documentno)  ? strtoupper( $value->documentno ) : null;
                    $x['style']= isset($value->style)  ? strtoupper( $value->style ) : null;
                    $x['article_no']= isset($value->article)  ? strtoupper( $value->article ) : null;
                    $x['size']= isset($value->size)  ? strtoupper( $value->size ) : null;
                    $x['color']= isset($value->color)  ? strtoupper( $value->color ) : null;
                    $x['fibre_composition']=null;
                    $x['fabric_finish']=null;
                    $x['gauge']=null;
                    $x['fabric_weight']=null;
                    $x['plm_no']=null;
                    $x['care_instruction']=null;
                    $x['manufacture_name']=null;
                    $x['export_to']= isset($value->export_to)  ? strtoupper( $value->export_to ) : null;
                    $x['nomor_roll']=null;
                    $x['batch_number']=null;
                    $x['item']=null;
                    $x['barcode_supplier']=null;
                    $x['additional_information']=null;
                    $x['yds_roll']=null;
                    $x['description']=null;
                    $x['season']= isset($value->season)  ? strtoupper( $value->season ) : null;
                    $x['invoice']=null;
                    $x['fabric_color']=null;
                    $x['interlining_color']=null;
                    $x['qty']=null;
                    $x['machine']=null;
                    $x['temperature']=null;
                    $x['pressure']=null;
                    $x['duration']=null;
                    $x['style_name']=null;
                    $x['fabric_item']=null;
                    $x['fabric_type']=null;
                    $x['thread']=null;
                    $x['material_shell_panel']=null;
                    $x['product_category']=null;
                    $x['remark']=null;
                    $x['pad']=null;
                    $x['pelling']=null;
                    $x['component']=null;
                    $x['garment_size']=null;
                    $x['sample_type']=null;
                    $x['test_condition']=null;
                    $x['fabric_properties']=null;
                    $x['lot']=null;
                    $x['po_buyer']=null;
                    $x['arrival_date']=  null;      
                    


                    $return[] = $x;
                }

                break;

            case 'MO':
                // $data = DB::connection('ssm')->table('sample_request')
                //                                         ->where('documentno','like','%'.strtoupper($docno).'%')
                //                                         ->whereNull('deleted_at')
                //                                         ->get();
                $data = DB::connection('erp')->table('bw_dss_mo_development')
                                                        ->where('documentno','like','%'.strtoupper($docno).'%')
                                                        ->whereNotIn('docstatus',['VO'])
                                                        ->get();

                foreach ($data as $key => $value) {

                    $x['document_type']="MO";
                    $x['origin']=$origin;
                    $x['document_no']= isset($value->documentno)  ? strtoupper( $value->documentno ) : null;
                    $x['style']= isset($value->style)  ? strtoupper( $value->style ) : null;
                    $x['article_no']= isset($value->article)  ? strtoupper( $value->article ) : null;
                    $x['size']= isset($value->size)  ? strtoupper( $value->size ) : null;
                    // $x['color']= isset($value->color)  ? strtoupper( $value->color ) : null;
                    $x['color']= isset($value->colorway)  ? strtoupper( $value->colorway ) : null;
                    $x['fibre_composition']=null;
                    $x['fabric_finish']=null;
                    $x['gauge']=null;
                    $x['fabric_weight']=null;
                    $x['plm_no']=null;
                    $x['care_instruction']=null;
                    $x['manufacture_name']=null;
                    $x['export_to']=null;
                    $x['nomor_roll']=null;
                    $x['batch_number']=null;
                    $x['item']=null;
                    $x['barcode_supplier']=null;
                    $x['additional_information']=null;
                    $x['yds_roll']=null;
                    $x['description']= isset($value->description)  ? strtoupper( $value->description ) : null;
                    $x['season']= isset($value->season)  ? strtoupper( $value->season ) : null;
                    $x['invoice']=null;
                    $x['fabric_color']=null;
                    $x['interlining_color']=null;
                    $x['qty']= isset($value->qty)  ? (int)strtoupper( $value->qty ) : null;
                    $x['machine']=null;
                    $x['temperature']=null;
                    $x['pressure']=null;
                    $x['duration']=null;
                    $x['style_name']=null;
                    $x['fabric_item']=null;
                    $x['fabric_type']=null;
                    $x['thread']=null;
                    $x['material_shell_panel']=null;
                    $x['product_category']=null;
                    $x['remark']=null;
                    $x['pad']=null;
                    $x['pelling']=null;
                    $x['component']=null;
                    $x['garment_size']=null;
                    $x['sample_type']=null;
                    $x['test_condition']=null;
                    $x['fabric_properties']=null;
                    $x['lot']=null;     
                    $x['po_buyer']=null;        
                    $x['arrival_date']=  null;             


                    $return[]=$x;
                }

                break;

            case 'BARCODE BUNDLE' : 
                // $data = DB::connection('cdms')->table('v_supply_line')
                //                                 ->where('barcode_id','LIKE','%'.strtoupper($docno).'%')
                //                                 ->orwhere('poreference','LIKE','%'.strtoupper($docno).'%')->get();

                $data = DB::connection('cdms')->table('bundle_detail')
                                                ->join('bundle_header','bundle_detail.bundle_header_id','bundle_header.id')
                                                ->where(function($query) use ($docno){
                                                    $query->where('bundle_detail.barcode_group','LIKE','%'.$docno.'%')
                                                            ->orWhere('bundle_header.poreference','LIKE','%'.$docno.'%');
                                                })
                                                ->whereNull('bundle_detail.deleted_at')
                                                // ->whereNull('bundle_header.deleted_at')
                                                ->groupBy([
                                                    'bundle_header.poreference',
                                                    'bundle_header.style',
                                                    'bundle_detail.qty',
                                                    'bundle_header.season',
                                                    'bundle_header.article',
                                                    'bundle_header.cut_num',
                                                    'bundle_header.size',
                                                    'bundle_header.factory_id',
                                                    'bundle_header.color_name',
                                                    'bundle_header.destination',
                                                    'bundle_detail.barcode_group',
                                                    'bundle_detail.current_locator_id',
                                                    'bundle_detail.komponen_name',
                                                    'bundle_detail.no_bundle',
                                                    'bundle_detail.start_no',
                                                    'bundle_detail.end_no',
                                                    'bundle_detail.set_type'
                                                ])
                                                ->select(
                                                    'bundle_header.poreference',
                                                    'bundle_header.style',
                                                    'bundle_detail.qty',
                                                    'bundle_header.season',
                                                    'bundle_header.article',
                                                    'bundle_header.cut_num',
                                                    'bundle_header.size',
                                                    'bundle_header.factory_id',
                                                    'bundle_header.color_name',
                                                    'bundle_header.destination',
                                                    'bundle_detail.barcode_group',
                                                    'bundle_detail.current_locator_id',
                                                    'bundle_detail.komponen_name',
                                                    'bundle_detail.no_bundle',
                                                    'bundle_detail.start_no',
                                                    'bundle_detail.end_no',
                                                    'bundle_detail.set_type',
                                                    DB::raw("bundle_detail.barcode_group as barcode_id")
                                                )->get();
                
                foreach ($data as $key => $value) {
                    
                    $x['document_type']="PO BUYER";
                    $x['origin']=$origin;
                    $x['document_no']= isset($value->poreference)  ? strtoupper( $value->poreference ) : null;
                    $x['style']= isset($value->style)  ? strtoupper( $value->style ) : null;
                    $x['article_no']= isset($value->article)  ? strtoupper( $value->article ) : null;
                    $x['size']= isset($value->size)  ? strtoupper( $value->size ) : null;
                    $x['color']= isset($value->color_name)  ? strtoupper( $value->color_name ) : null;
                    $x['fibre_composition']=null;
                    $x['fabric_finish']=null;
                    $x['gauge']=null;
                    $x['fabric_weight']=null;
                    $x['plm_no']=null;
                    $x['care_instruction']=null;
                    $x['manufacture_name']=null;
                    $x['export_to']= isset($value->destination)  ? strtoupper( $value->destination ) : null;
                    $x['nomor_roll']= isset($value->barcode_id)  ? strtoupper( $value->barcode_id ) : null;
                    $x['batch_number']=null;
                    $x['item']=null;
                    $x['barcode_supplier']=null;
                    $x['additional_information']=null;
                    $x['yds_roll']=null;
                    $x['description']=null;
                    $x['season']= isset($value->season)  ? strtoupper( $value->season ) : null;
                    $x['invoice']=null;
                    $x['fabric_color']=null;
                    $x['interlining_color']=null;
                    $x['qty']=null;
                    $x['machine']=null;
                    $x['temperature']=null;
                    $x['pressure']=null;
                    $x['duration']=null;
                    $x['style_name']=null;
                    $x['fabric_item']=null;
                    $x['fabric_type']=null;
                    $x['thread']=null;
                    $x['material_shell_panel']=null;
                    $x['product_category']=null;
                    $x['remark']=null;
                    $x['pad']=null;
                    $x['pelling']=null;
                    $x['component']= isset($value->komponen_name)  ? strtoupper( $value->komponen_name ) : null;
                    $x['garment_size']=null;
                    $x['sample_type']=null;
                    $x['test_condition']=null;
                    $x['fabric_properties']=null;
                    $x['lot']=null;
                    $x['po_buyer']=null;
                    $x['arrival_date']=  null;      
                    


                    $return[]=$x;
                }

                break;


            case 'BARCODE ROLL':
                $data = DB::connection('wms')->table('get_fg_fabric_v')
                                                            // ->where(DB::raw('upper("barcode")'),strtoupper($docno))
                                                            ->where(DB::raw('lower("barcode")'),strtolower($docno))
                                                            ->get();
            
                    foreach ($data as $key => $value) {
                        
                        $x['document_type']="PO SUPPLIER";
                        $x['origin']=$origin;
                        $x['document_no']= isset($value->purchase_number)  ? strtoupper( $value->purchase_number ) : null;
                        // $x['style']= isset($value->upc)  ? strtoupper( $value->upc ) : null;
                        $x['style']= isset($value->planning_allocating_style)  ? strtoupper( $value->planning_allocating_style ) : null;
                        $x['article_no']=null;
                        $x['size']= isset($value->actual_width)  ? strtoupper( $value->actual_width ) : null;
                        $x['color']= isset($value->color)  ? strtoupper( $value->color ) : null;
                        $x['fibre_composition']=null;
                        $x['fabric_finish']=null;
                        $x['gauge']=null;
                        $x['fabric_weight']=null;
                        $x['plm_no']=null;
                        $x['care_instruction']=null;
                        $x['manufacture_name']=isset($value->supplier_name)  ? strtoupper( $value->supplier_name ) : null;
                        $x['export_to']=null;
                        $x['nomor_roll']= isset($value->nomor_roll)  ? strtoupper( $value->nomor_roll ) : null;
                        $x['batch_number']= isset($value->batch_number)  ? strtoupper( $value->batch_number ) : null;
                        $x['item']= isset($value->item_code)  ? strtoupper( $value->item_code ) : null;
                        $x['barcode_supplier']= isset($value->barcode)  ? strtoupper( $value->barcode ) : null;
                        $x['additional_information']=null;
                        $x['yds_roll']= isset($value->total_stock_arrival_roll)  ? strtoupper( $value->total_stock_arrival_roll ) : null;
                        $x['description']= isset($value->item_description)  ? strtoupper( $value->item_description ) : null;
                        $x['season']= isset($value->season)  ? strtoupper( $value->season ) : null;
                        $x['invoice']= isset($value->no_invoice)  ? strtoupper( $value->no_invoice ) : null;
                        $x['fabric_color']=null;
                        $x['interlining_color']=null;
                        $x['qty']=null;
                        $x['machine']=null;
                        $x['temperature']=null;
                        $x['pressure']=null;
                        $x['duration']=null;
                        $x['style_name']=null;
                        $x['fabric_item']=null;
                        $x['fabric_type']=null;
                        $x['thread']=null;
                        $x['material_shell_panel']=null;
                        $x['product_category']=null;
                        $x['remark']=null;
                        $x['pad']=null;
                        $x['pelling']=null;
                        $x['component']=null;
                        $x['garment_size']=null;
                        $x['sample_type']=null;
                        $x['test_condition']=null;
                        $x['fabric_properties']=null;
                        $x['lot']= isset($value->lot)  ? strtoupper( $value->lot ) : null;    
                        $x['po_buyer']=  null;        
                        $x['arrival_date']= isset($value->arrival_date)  ? strtoupper( $value->arrival_date ) : null;    

                        $return[]=$x;
                    }
                break;
            
            case 'BARCODE ACCESORIES':
                // $data = DB::connection('wms')->table('get_fg_acc_v2')
                //                                             ->where('barcode_stock',strtolower($docno))
                //                                             ->where('po_buyer',strtolower($docno))
                //                                             ->get();
                $data = DB::connection('wms')->table('get_fg_acc_v2')
                                                            ->where(DB::raw('lower("barcode_stock")'),strtolower($docno))
                                                            ->orWhere(DB::raw('lower("po_buyer")'),strtolower($docno))
                                                            ->get();
                              
                foreach ($data as $key => $value) {
            
                    
                    $x['document_type']="PO SUPPLIER";
                    $x['origin']=$origin;
                    $x['document_no']= isset($value->purchase_number)  ? strtoupper( $value->purchase_number ) : null;
                    $x['style']= isset($value->style)  ? strtoupper( $value->style ) : null;
                    $x['article_no']= isset($value->article_no)  ? strtoupper( $value->article_no ) : null;
                    $x['size']=null;
                    $x['color']= isset($value->color)  ? strtoupper( $value->color ) : null;
                    $x['fibre_composition']=null;
                    $x['fabric_finish']=null;
                    $x['gauge']=null;
                    $x['fabric_weight']=null;
                    $x['plm_no']=null;
                    $x['care_instruction']=null;
                    $x['manufacture_name']= isset($value->supplier_name)  ? strtoupper( $value->supplier_name ) : null;
                    $x['export_to']=null;
                    $x['nomor_roll']=null;
                    $x['batch_number']=null;
                    $x['item']= isset($value->item_code)  ? strtoupper( $value->item_code ) : null;
                    $x['barcode_supplier']= isset($value->barcode)  ? strtoupper( $value->barcode ) : null;
                    $x['additional_information']=null;
                    $x['yds_roll']=null;
                    $x['description']= isset($value->item_desc)  ? strtoupper( $value->item_desc ) : null;
                    $x['season']= isset($value->season)  ? strtoupper( $value->season ) : null;
                    $x['invoice']= isset($value->no_invoice)  ? strtoupper( $value->no_invoice ) : null;
                    $x['fabric_color']=null;
                    $x['interlining_color']=null;
                    $x['qty']= isset($value->total_stock_inventory)  ? strtoupper( $value->total_stock_inventory ) : null;
                    $x['machine']=null;
                    $x['temperature']=null;
                    $x['pressure']=null;
                    $x['duration']=null;
                    $x['style_name']=null;
                    $x['fabric_item']=null;
                    $x['fabric_type']=null;
                    $x['thread']=null;
                    $x['material_shell_panel']=null;
                    $x['product_category']=null;
                    $x['remark']=null;
                    $x['pad']=null;
                    $x['pelling']=null;
                    $x['component']=null;
                    $x['garment_size']=null;
                    $x['sample_type']=null;
                    $x['test_condition']=null;
                    $x['fabric_properties']=null;
                    $x['lot']= isset($value->upc)  ? strtoupper( $value->upc ) : null;                
                    $x['po_buyer']=isset($value->po_buyer)  ? strtoupper( $value->po_buyer ) : null;    
                    $x['arrival_date']=  null;          

                    $return[]=$x;

                }
                
                break;
            
            case 'BARCODE FABRIC DEV':
                    $data = DB::connection('wms_sample')->table('nit_test_lab')
                                                                ->where(function ($query) use ($docno){
                                                                    $query->where(DB::raw('upper("purchase_number")'),'LIKE','%'.strtoupper($docno).'%')
                                                                    ->orWhere(DB::raw('upper("barcode")'),'LIKE','%'.strtoupper($docno).'%');
                                                                })
                                                                ->where('category',"FB")
                                                                ->get();
                
                    foreach ($data as $key => $value) {
                
                        
                        $x['document_type']="PO SUPPLIER";
                        $x['origin']=$origin;
                        $x['document_no']= isset($value->purchase_number) ? strtoupper($value->purchase_number) : null;
                        $x['style']=null;
                        $x['article_no']=null;
                        $x['size']=null;
                        $x['color']= isset($value->color) ? strtoupper($value->color) : null;
                        $x['fibre_composition']= isset($value->item_desc) ? strtoupper($value->item_desc) : null;
                        $x['fabric_finish']=null;
                        $x['gauge']=null;
                        $x['fabric_weight']=null;
                        $x['plm_no']=null;
                        $x['care_instruction']=null;
                        $x['manufacture_name']= isset($value->supplier_name) ? strtoupper($value->supplier_name) : null;
                        $x['export_to']=null;
                        $x['nomor_roll']= isset($value->roll_number) ? strtoupper($value->roll_number) : null;
                        $x['batch_number']= isset($value->batch_number) ? strtoupper($value->batch_number) : null;
                        $x['item']= isset($value->item_code) ? strtoupper($value->item_code) : null;
                        $x['barcode_supplier']= isset($value->barcode) ? strtoupper($value->barcode) : null;
                        $x['additional_information']=null;
                        $x['yds_roll']=null;
                        $x['description']= isset($value->item_desc) ? strtoupper($value->item_desc) : null;
                        $x['season']= isset($value->season) ? strtoupper($value->season) : null;
                        $x['invoice']= isset($value->no_invoice) ? strtoupper($value->no_invoice) : null;
                        $x['fabric_color']=null;
                        $x['interlining_color']=null;
                        $x['qty']= isset($value->total_qty_arrival) ? strtoupper($value->total_qty_arrival) : null;
                        $x['machine']=null;
                        $x['temperature']=null;
                        $x['pressure']=null;
                        $x['duration']=null;
                        $x['style_name']=null;
                        $x['fabric_item']=null;
                        $x['fabric_type']=null;
                        $x['thread']=null;
                        $x['material_shell_panel']=null;
                        $x['product_category']=null;
                        $x['remark']=null;
                        $x['pad']=null;
                        $x['pelling']=null;
                        $x['component']=null;
                        $x['garment_size']=null;
                        $x['sample_type']=null;
                        $x['test_condition']=null;
                        $x['fabric_properties']=null;
                        $x['lot']=null;
                        $x['po_buyer']=null;
                        $x['arrival_date']= isset($value->received_at) ? date_format(date_create(strtoupper($value->received_at)),'d-M-Y H:i:s') : null;
     
    
                        $return[]=$x;
    
                    }
                    
                    break;

            case 'BARCODE ACC DEV':
                        $data = DB::connection('wms_sample')->table('nit_test_lab')
                                                                    ->where(function ($query) use ($docno){
                                                                        $query->where(DB::raw('upper("purchase_number")'),'LIKE','%'.strtoupper($docno).'%')
                                                                        ->orWhere(DB::raw('upper("barcode")'),'LIKE','%'.strtoupper($docno).'%');
                                                                    })
                                                                    ->whereNotIn('category',["FB"])
                                                                    ->get();
                    
                        foreach ($data as $key => $value) {
                    
                            
                            $x['document_type']="PO SUPPLIER";
                            $x['origin']=$origin;
                            $x['document_no']= isset($value->purchase_number) ? strtoupper($value->purchase_number) : null;
                            $x['style']=null;
                            $x['article_no']=null;
                            $x['size']=null;
                            $x['color']= isset($value->color) ? strtoupper($value->color) : null;
                            $x['fibre_composition']=null;
                            $x['fabric_finish']=null;
                            $x['gauge']=null;
                            $x['fabric_weight']=null;
                            $x['plm_no']=null;
                            $x['care_instruction']=null;
                            $x['manufacture_name']= isset($value->supplier_name) ? strtoupper($value->supplier_name) : null;
                            $x['export_to']=null;
                            $x['nomor_roll']= isset($value->roll_number) ? strtoupper($value->roll_number) : null;
                            $x['batch_number']= isset($value->batch_number) ? strtoupper($value->batch_number) : null;
                            $x['item']= isset($value->item_code) ? strtoupper($value->item_code) : null;
                            $x['barcode_supplier']= isset($value->barcode) ? strtoupper($value->barcode) : null;
                            $x['additional_information']=null;
                            $x['yds_roll']=null;
                            $x['description']= isset($value->item_desc) ? strtoupper($value->item_desc) : null;
                            $x['season']= isset($value->season) ? strtoupper($value->season) : null;
                            $x['invoice']= isset($value->no_invoice) ? strtoupper($value->no_invoice) : null;
                            $x['fabric_color']=null;
                            $x['interlining_color']=null;
                            $x['qty']= isset($value->total_qty_arrival) ? strtoupper($value->total_qty_arrival) : null;
                            $x['machine']=null;
                            $x['temperature']=null;
                            $x['pressure']=null;
                            $x['duration']=null;
                            $x['style_name']=null;
                            $x['fabric_item']=null;
                            $x['fabric_type']=null;
                            $x['thread']=null;
                            $x['material_shell_panel']=null;
                            $x['product_category']=null;
                            $x['remark']=null;
                            $x['pad']=null;
                            $x['pelling']=null;
                            $x['component']=null;
                            $x['garment_size']=null;
                            $x['sample_type']=null;
                            $x['test_condition']=null;
                            $x['fabric_properties']=null;
                            $x['lot']=null;
                            $x['po_buyer']=null;
                            $x['arrival_date']= isset($value->received_at) ? date_format(date_create(strtoupper($value->received_at)),'d-M-Y H:i:s') : null;
         
        
                            $return[]=$x;
        
                        }
                        
                        break;
        }

       
        return $return;
    }
}
