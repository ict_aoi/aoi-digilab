<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterMethode;
use App\Models\MasterCategory;
use App\Models\Employee;
use App\User;

use App\Http\Controllers\HelperController;
class ApiMiddleController extends Controller
{
    public function getDetailTrf(Request $req){
        $id= trim($req->id);

        $data = DB::table('trf_testings')
                            ->join('master_category','trf_testings.id_category','=','master_category.id')
                            ->join('factory','trf_testings.factory_id','=','factory.id')
                            ->where('trf_testings.id',$id)
                            ->whereNull('trf_testings.deleted_at')
                            ->select(
                                    'trf_testings.id',
                                    'trf_testings.trf_id',
                                    'trf_testings.lab_location',
                                    'trf_testings.buyer',
                                    'trf_testings.nik',
                                    'trf_testings.platform',
                                    'trf_testings.asal_specimen',
                                    'trf_testings.date_information',
                                    'trf_testings.date_information_remark',
                                    'trf_testings.test_required',
                                    'trf_testings.part_of_specimen',
                                    'trf_testings.created_at',
                                    'trf_testings.return_test_sample',
                                    'trf_testings.status',
                                    'trf_testings.verified_lab_date',
                                    'trf_testings.verified_lab_by',
                                    'trf_testings.reject_by',
                                    'trf_testings.reject_date',
                                    'trf_testings.additional_information',
                                    'trf_testings.previous_trf_id',
                                    'master_category.category',
                                    'master_category.category_specimen',
                                    'master_category.type_specimen',
                                    'factory.factory_name'
                                )->first();



        $api = 'http://'.$req->getHttpHost().'/api/print/report-specimen?id='.$data->id.'&notrf='.$data->trf_id;
        $qrcode =  HelperController::generateQr($api);

        $testreq = HelperController::testRequired($data->test_required,$data->previous_trf_id);
        $dateinfo = HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
        $meth = DB::table('trf_testing_methods')
                        ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                        ->whereNull('trf_testing_methods.deleted_at')
                        ->where('trf_testing_methods.trf_id',$data->id)
                        ->select('master_method.id','master_method.method_code','master_method.method_name','master_method.category','master_method.type')->get()->toArray();

        $docm = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->get();

        $users = Employee::where('nik', $data->nik)->first();




        $vefpiclab = isset($data->verified_lab_by) ?  User::where('id', $data->verified_lab_by)->first()['name'] : "";
        $vefdatelab = isset($data->verified_lab_date) ? carbon::parse($data->verified_lab_date)->format('d-m-Y H:i:s') : "";
        $filename = "TRF_DETAIL_".$data->trf_id;

        $pdf = \PDF::loadView('create_trf.print_detail',['data'=>$data,'method'=>$meth,'docm'=>$docm,'submit_by'=>isset($users) ? $users->name : $data->nik,'test_required'=>$testreq,'dateinfo'=>$dateinfo,'labdate'=>$vefdatelab,'labpic'=>$vefpiclab,'qrcode'=>base64_encode($qrcode)])->setPaper('A4','Potrait');
        return $pdf->stream($filename);

    }

    public function printBarcode(Request $req){
        $id= trim($req->id);

        $data = TrfTesting::where('id',$id)->first();

        return view('create_trf.print_barcode',['data'=>$data]);
    }

    

    public function reportSpecimentResult(Request $req){
        $id = trim($req->id);
        $docid = trim($req->docid);

        $trfdoc = [];
        $filename="TRF_REPORT_SPECIMEN".$docid;
               foreach (HelperController::getTrfDoc($id,null) as $tdc) {
                    $api = 'http://'.$req->getHttpHost().'/api/print/report-specimen?id='.$tdc->id."&docid=".$tdc->doc_id;
                    $qrcode =  HelperController::generateQr($api);  
                    
                    $fac = DB::table('factory')->where('id',$tdc->factory_id)->first();
                    $dx['trfid']= $tdc->id;
                    $dx['notrf']= $tdc->trf_id;
                    $dx['buyer']= $tdc->buyer;
                    $dx['lab_location']= $tdc->lab_location;
                    $dx['nik']= $tdc->nik;
                    $dx['platform']= $tdc->platform;
                    $dx['factory_id']= $tdc->factory_id;
                    $dx['fact']= $fac->factory_name;
                    $dx['asal_specimen']= $tdc->asal_specimen;
                    $dx['date_information']= $tdc->date_information;
                    $dx['date_information_remark']= $tdc->date_information_remark;
                    $dx['set_date_info']= HelperController::dateInfoSet($tdc->date_information,$tdc->date_information_remark);
                    $dx['test_required']= $tdc->test_required;
                    $dx['set_testreq']= HelperController::testRequired($tdc->test_required,$tdc->previous_trf_id);
                    $dx['part_of_specimen']= $tdc->part_of_specimen;
                    $dx['created_at']= $tdc->created_at;
                    $dx['updated_at']= $tdc->updated_at;
                    $dx['return_test_sample']= $tdc->return_test_sample;
                    $dx['status']= $tdc->status;
                    $dx['previous_trf_id']= $tdc->previous_trf_id;
                    $dx['id_category']= $tdc->id_category;
                    $dx['doc_id']= $tdc->doc_id;
                    $dx['document_type']= $tdc->document_type;
                    $dx['document_no']= $tdc->document_no;
                    $dx['season']= $tdc->season;
                    $dx['style']= $tdc->style;
                    $dx['article_no']= $tdc->article_no;
                    $dx['size']= $tdc->size;
                    $dx['color']= $tdc->color;
                    $dx['fibre_composition']= $tdc->fibre_composition;
                    $dx['fabric_finish']= $tdc->fabric_finish;
                    $dx['gauge']= $tdc->gauge;
                    $dx['fabric_weight']= $tdc->fabric_weight;
                    $dx['plm_no']= $tdc->plm_no;
                    $dx['care_instruction']= $tdc->care_instruction;
                    $dx['manufacture_name']= $tdc->manufacture_name;
                    $dx['export_to']= $tdc->export_to;
                    $dx['nomor_roll']= $tdc->nomor_roll;
                    $dx['batch_number']= $tdc->batch_number;
                    $dx['item']= $tdc->item;
                    $dx['barcode_supplier']= $tdc->barcode_supplier;
                    $dx['additional_information']= $tdc->additional_information;
                    $dx['yds_roll']= $tdc->yds_roll;
                    $dx['description']= $tdc->description;
                    $dx['category']= $tdc->category;
                    $dx['category_specimen']= $tdc->category_specimen;
                    $dx['type_specimen']= $tdc->type_specimen;
                    $dx['qrcode']= base64_encode($qrcode);
                    $dx['result_test']=$this->SetRest($tdc->doc_id);
                    $dx['technician']= isset($tdc->techincian) ?  HelperController::GetUser($tdc->techincian) : null;
                    $dx['labhead']= isset($tdc->labhead) ?  HelperController::GetUser($tdc->labhead) : null;
                    $dx['qchead']= isset($tdc->qchead) ?  HelperController::GetUser($tdc->qchead) : null

                    $trfdoc[] = $dx;
               }


       $pdf = \PDF::loadView('dashboard.report',['data'=>$trfdoc])->setPaper('A4','Potrait');
       return $pdf->stream($filename);
    }

    public function report(Request $req){
        $id = trim($req->id);
        $notrf = trim($req->notrf);

        $trfdoc = [];
        $filename="TRF_REPORT_".$notrf;
               foreach (HelperController::getTrfDoc($id,null) as $tdc) {
                    $api = 'http://'.$req->getHttpHost().'/api/print/report-specimen?id='.$tdc->id."&docid=".$tdc->doc_id;
                    $qrcode =  HelperController::generateQr($api);               

                    $fac = DB::table('factory')->where('id',$tdc->factory_id)->first();
                    $dx['trfid']= $tdc->id;
                    $dx['notrf']= $tdc->trf_id;
                    $dx['buyer']= $tdc->buyer;
                    $dx['lab_location']= $tdc->lab_location;
                    $dx['nik']= $tdc->nik;
                    $dx['platform']= $tdc->platform;
                    $dx['factory_id']= $tdc->factory_id;
                    $dx['fact']= $fac->factory_name;
                    $dx['asal_specimen']= $tdc->asal_specimen;
                    $dx['date_information']= $tdc->date_information;
                    $dx['date_information_remark']= $tdc->date_information_remark;
                    $dx['set_date_info']= HelperController::dateInfoSet($tdc->date_information,$tdc->date_information_remark);
                    $dx['test_required']= $tdc->test_required;
                    $dx['set_testreq']= HelperController::testRequired($tdc->test_required,$tdc->previous_trf_id);
                    $dx['part_of_specimen']= $tdc->part_of_specimen;
                    $dx['created_at']= $tdc->created_at;
                    $dx['updated_at']= $tdc->updated_at;
                    $dx['return_test_sample']= $tdc->return_test_sample;
                    $dx['status']= $tdc->status;
                    $dx['remarks']= $tdc->remarks;
                    $dx['previous_trf_id']= $tdc->previous_trf_id;
                    $dx['id_category']= $tdc->id_category;
                    $dx['pic_test']= $tdc->pic_test;
                    $dx['approval_test']= $tdc->approval_test;
                    $dx['doc_id']= $tdc->doc_id;
                    $dx['document_type']= $tdc->document_type;
                    $dx['document_no']= $tdc->document_no;
                    $dx['season']= $tdc->season;
                    $dx['style']= $tdc->style;
                    $dx['article_no']= $tdc->article_no;
                    $dx['size']= $tdc->size;
                    $dx['color']= $tdc->color;
                    $dx['fibre_composition']= $tdc->fibre_composition;
                    $dx['fabric_finish']= $tdc->fabric_finish;
                    $dx['gauge']= $tdc->gauge;
                    $dx['fabric_weight']= $tdc->fabric_weight;
                    $dx['plm_no']= $tdc->plm_no;
                    $dx['care_instruction']= $tdc->care_instruction;
                    $dx['manufacture_name']= $tdc->manufacture_name;
                    $dx['export_to']= $tdc->export_to;
                    $dx['nomor_roll']= $tdc->nomor_roll;
                    $dx['batch_number']= $tdc->batch_number;
                    $dx['item']= $tdc->item;
                    $dx['barcode_supplier']= $tdc->barcode_supplier;
                    $dx['additional_information']= $tdc->additional_information;
                    $dx['yds_roll']= $tdc->yds_roll;
                    $dx['description']= $tdc->description;
                    $dx['category']= $tdc->category;
                    $dx['category_specimen']= $tdc->category_specimen;
                    $dx['type_specimen']= $tdc->type_specimen;
                    $dx['result_test']=$this->SetRest($tdc->doc_id);
                    $dx['technician']= isset($tdc->techincian) ?  HelperController::GetUser($tdc->techincian) : null;
                    $dx['labhead']= isset($tdc->labhead) ?  HelperController::GetUser($tdc->labhead) : null;
                    $dx['qchead']= isset($tdc->qchead) ?  HelperController::GetUser($tdc->qchead) : null

                    $trfdoc[] = $dx;
               }


       $pdf = \PDF::loadView('dashboard.report',['data'=>$trfdoc])->setPaper('A4','Potrait');
       return $pdf->stream($filename);
    }


    private function SetRest($doc_id){
        $data = HelperController::getTrfRest($doc_id);
        $i =0;
        $lst =[];
        foreach ($data as $dt) {
            $tr['no']=++$i;
            $tr['imgdir']='imagespc/'.$dt->id.'.png';
            $tr['requirment']=HelperController::operatorVal($dt->requirement_id);
            $tr['before_test']=$dt->before_test;
            $tr['supplier_result']=$dt->supplier_result;
            $tr['result']=$dt->result;
            $tr['method_code']=$dt->method_code;
            $tr['method_name']=$dt->method_name;
            $tr['set_result']=HelperController::ReportResult($dt->requirement_id,$dt->result);
            $lst[]=$tr;
        }

        return $lst;
    }

}
