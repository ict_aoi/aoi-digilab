<?php

namespace App\Http\Controllers\Trf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use DataTables;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterMethode;
use App\Models\MasterCategory;
use App\Models\Employee;
use App\User;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Http\Controllers\HelperController;

class ApprovalController extends Controller
{
    public function index(){

        // dd(auth::user()->hasRole('ICT'));
        return view('approval.index');
    }

    public function getData(){
        DB::statement('REFRESH MATERIALIZED VIEW ns_list_closed_trf');

        $data = DB::table('ns_list_closed_trf')->where('status','ONPROGRESS');

        return DataTables::of($data)
                    ->editColumn('test_required',function($data){
                        return HelperController::testRequired($data->test_required,$data->previous_trf_id);
                    })
                    ->editColumn('date_information_remark',function($data){
                        return HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
                    })
                    ->editColumn('return_test_sample',function($data){
                        switch ($data->return_test_sample) {
                            case true:
                                return '<span class="label label-success">YES</span>';
                                break;

                            case false:
                                return '<span class="label label-warning">NO</span>';
                                break;

                            default:
                                return '<span class="label label-primary">ERROR</span>';
                                break;
                        }
                    })
                    ->editColumn('category',function($data){
                        return '<b>Category</b> : '.$data->category.'<br> <b>Category Specimen</b> : '.$data->category_specimen.'<br> <b>Type Specimen</b> : '.$data->type_specimen;
                    })
                    ->addColumn('pic_test',function($data){
                        if (isset($data->technician)) {
                            $pic = User::where('id',$data->technician)->first();

                            return $pic->name." ".$pic->nik;
                        }else{
                            return '';
                        }
                    })
                    ->addColumn('action',function($data){
               
                        return view('_action',[
                                    'model'=>$data,
                                    'trfmngapv'=>['id'=>$data->id,'notrf'=>$data->trf_id,'asal'=>$data->asal_specimen,'buyer'=>$data->buyer]
                                ]);
                    })
                    ->rawColumns(['test_required','date_information_remark','return_test_sample','category','pic_test','remarks'])
                    ->make(true);
    }

    public function getTech(Request $req){
        $id = $req->idtrf;

        $list = DB::select("SELECT * FROM ns_gettech('".$id."')");

        return response()->json(['data'=>$list],200);
    }

    public function techRelease(Request $req){
        $id = $req->id;
        $pic_id = $req->pic_id;
        $remark = $req->remark;
        $status = $req->status;
        $final  = $req->fin_rest;

        if ($status=="CLOSED") {
            $labhead = DB::table('mapping_report_sign')->wherenull('deleted_at')->first()->labhead;
        }else{
            $labhead = null;
        }

        try {
            DB::beginTransaction();
                $update = array(
                            'status'=>$status,
                            'final_result'=>$final,
                            'technician'=>$pic_id,
                            'remark_tech'=>$remark,
                            'updated_at'=>carbon::now(),
                            'labhead'=>$labhead
                        );

               TrfTesting::where('id',$id)->update($update);
            DB::commit();
            $data_response = ['status'=>200,"output"=>$status." TRF Success ! ! ! "];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>$status." TRF Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }

    
    public function ecalation(){

        if (auth::user()->hasRole('ICT') || auth::user()->hasRole('ME') ) {
            $esclv = '';
            $escty = '';
        }else if (auth::user()->hasRole('LAB DEPT. HEAD') || auth::user()->hasRole('LAB SUB. DEPT. HEAD') ) {
            $esclv = 'ESCALATION I';
            $escty = 'hidden';
        }else if (auth::user()->hasRole('QUALITY HEAD')) {
            $esclv = 'ESCALATION II';
            $escty = 'hidden';
        }

        return view('approval.escalation')->with('esclv',$esclv)->with('escty',$escty);
    }

    public function getDataEsc(Request $req){
        DB::statement('REFRESH MATERIALIZED VIEW ns_list_closed_trf');
        $lev = $req->level;


        $data = DB::table('ns_list_closed_trf')->where('status',$lev);

        return DataTables::of($data)
                    ->editColumn('test_required',function($data){
                        return HelperController::testRequired($data->test_required,$data->previous_trf_id);
                    })
                    ->editColumn('date_information_remark',function($data){
                        return HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
                    })
                    ->editColumn('return_test_sample',function($data){
                        switch ($data->return_test_sample) {
                            case true:
                                return '<span class="label label-success">YES</span>';
                                break;

                            case false:
                                return '<span class="label label-warning">NO</span>';
                                break;

                            default:
                                return '<span class="label label-primary">ERROR</span>';
                                break;
                        }
                    })
                    ->editColumn('category',function($data){
                        return '<b>Category</b> : '.$data->category.'<br> <b>Category Specimen</b> : '.$data->category_specimen.'<br> <b>Type Specimen</b> : '.$data->type_specimen;
                    })
                    ->addColumn('pic_test',function($data){
                        $pic = User::where('id',$data->technician)->first();

                        return $pic->name." ".$pic->nik;
                    })
                    ->addColumn('action',function($data) use($lev){
               
                        return view('_action',[
                                            'trfesc'=>[
                                                        'id'=>$data->id,
                                                        'notrf'=>$data->trf_id,
                                                        'asal'=>$data->asal_specimen,
                                                        'buyer'=>$data->buyer,
                                                        'level'=>$lev]
                                    ]);
                    })
                    ->rawColumns(['test_required','date_information_remark','return_test_sample','category','pic_test'])
                    ->make(true);
    }

    public function updateEscl(Request $req){
        $id = $req->id;
        $status = ($req->status=="ESCALATION") ? "ESCALATION II" : $req->status;
        $final = $req->finrest;
        $remark = $req->remark;
        $lavelesc = $req->lavelesc;


        try {
            DB::beginTransaction();
                switch ($lavelesc) {
                    case 'ESCALATION I':
                        $update = array(
                                    'final_result'=>$final,
                                    'status'=>$status,
                                    'labhead'=>auth::user()->id,
                                    'remark_lab'=>$remark,
                                    'updated_at'=>carbon::now()
                                );
                    break;

                    case 'ESCALATION II':
                        $update = array(
                                    'final_result'=>$final,
                                    'status'=>$status,
                                    'qchead'=>auth::user()->id,
                                    'remark_qc'=>$remark,
                                    'updated_at'=>carbon::now()
                                );
                    break;
                }

                TrfTesting::where('id',$id)->update($update);
            DB::commit();
            $data_response = ['status'=>200,"output"=>$status." TRF Success ! ! ! "];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>$status." TRF Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }


    public function getHisto(Request $req){
        $trf = TrfTesting::where('id',$req->id)->first();

       $data = [
                    'tech'=>user::where('id',$trf->technician)->first()->name,
                    'techrm'=>$trf->remark_tech,
                    'labhead'=>isset($trf->labhead) ? user::where('id',$trf->labhead)->first()->name : null,
                    'labrm'=>$trf->remark_lab
                ];
        
       
        return response()->json(['data'=>$data],200);
    }
}
