<?php

namespace App\Http\Controllers\Trf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


use App\Models\MasterItem;

class GetSpecimenController extends Controller
{
    public function getData(Request $req){
        $data = [];
        if ($req->ctgspc=="FABRIC") {
            $datas = self::getBarcodeRoll($req->key);
            
            foreach ($datas as $dt) {
                $cekitem = MasterItem::where('upc',$dt->upc)->whereNull('deleted_at')->first();

                $dx['doctype']="PO SUPPLIER";
                $dx['specimen']=$req->ctgspc;
                $dx['type']=$req->typespc;

                $dx['docno']=$dt->purchase_number;
                $dx['style']=$dt->planning_allocating_style;
                $dx['article']=null;
                $dx['size']=$dt->actual_width;
                $dx['color']=$dt->color;
                $dx['item']=$dt->item_code;
                $dx['season']=$dt->season;
                $dx['barcode_roll']=$dt->barcode;
                $dx['supplier_name']=$dt->supplier_name;
                $dx['batch_no']=$dt->batch_number;
                $dx['qty']=$dt->total_stock_arrival_roll;
                $dx['invoice']=$dt->no_invoice;
                $dx['country']=null;
                $dx['purchase_no']=null;
                $dx['style_name']=isset($cekitem) ? $cekitem->style_name : null;
                $dx['fabric_composition']=isset($cekitem) ? $cekitem->fabric_composition : null;
                $dx['weight']=isset($cekitem) ? $cekitem->weight : null;

                $data[]=$dx;
            }
        }else if($req->ctgspc=="ACCESORIES/TRIM"){
            $datas = self::getBarcodeAcc($req->key);

            foreach ($datas as $dt) {
                $cekitem = MasterItem::where('upc',$dt->upc)->whereNull('deleted_at')->first();

                $dx['doctype']="PO SUPPLIER";
                $dx['specimen']=$req->ctgspc;
                $dx['type']=$req->typespc;

                $dx['docno']=$dt->purchase_number;
                $dx['style']=$dt->style;
                $dx['article']=$dt->article_no;
                $dx['size']=null;
                $dx['color']=$dt->color;
                $dx['item']=$dt->item_code;
                $dx['season']=$dt->season;
                $dx['barcode_roll']=$dt->barcode;
                $dx['supplier_name']=$dt->supplier_name;
                $dx['batch_no']=null;
                $dx['qty']=$dt->total_stock_inventory;
                $dx['invoice']=$dt->no_invoice;
                $dx['country']=null;
                $dx['purchase_no']=$dt->po_buyer;
                $dx['style_name']=isset($cekitem) ? $cekitem->style_name : null;
                $dx['fabric_composition']=isset($cekitem) ? $cekitem->fabric_composition : null;
                $dx['weight']=isset($cekitem) ? $cekitem->weight : null;

                $data[]=$dx;
            }
        }else if(($req->ctgspc=="STRIKE OFF" && $req->typespc=="HEAT TRANSFER") || $req->ctgspc=="PANEL"){

            switch ($req->origin) {
                case 'DEVELOPMENT':
                        $datas = self::getManufOrder($req->key,true);
                    break;

                case 'PRODUCTION':
                        $datas = self::getSalesOrder($req->key,true);
                    break;
            }

            foreach ($datas as $ds) {
                $cekitem = MasterItem::where('upc',$ds->style)->whereNull('deleted_at')->first();

                $dx['doctype']="PO BUYER";
                $dx['specimen']=$req->ctgspc;
                $dx['type']=$req->typespc;

                $dx['docno']=$ds->documentno;
                $dx['style']=$ds->style;
                $dx['article']=$ds->article;
                $dx['size']=null;
                $dx['color']=$ds->color;
                $dx['item']=null;
                $dx['season']=$ds->season;
                $dx['barcode_roll']=null;
                $dx['supplier_name']=null;
                $dx['batch_no']=null;
                $dx['qty']=null;
                $dx['invoice']=null;
                $dx['country']=isset($ds->export_to) ? $ds->export_to :null;
                $dx['purchase_no']=null;
                $dx['style_name']=isset($cekitem) ? $cekitem->style_name : null;
                $dx['fabric_composition']=isset($cekitem) ? $cekitem->fabric_composition : null;
                $dx['weight']=isset($cekitem) ? $cekitem->weight : null;

                $data[]=$dx;
            }

            

        }else if($req->ctgspc=="GARMENT" || ($req->ctgspc=="STRIKE OFF" && ($req->typespc=="EMBROIDERY" || $req->typespc=="PRINTING" || $req->typespc=="PAD PRINT" || $req->typespc=="BADGE"))){

            switch ($req->origin) {
                case 'DEVELOPMENT':
                        $datas = self::getManufOrder($req->key,false);
                    break;

                case 'PRODUCTION':
                        $datas = self::getSalesOrder($req->key,false);
                    break;
            }

            foreach ($datas as $ds) {

                $cekitem = MasterItem::where('upc',$ds->style)->whereNull('deleted_at')->first();

                $dx['doctype']="PO BUYER";
                $dx['specimen']=$req->ctgspc;
                $dx['type']=$req->typespc;

                $dx['docno']=$ds->documentno;
                $dx['style']=$ds->style;
                $dx['article']=$ds->article;
                $dx['size']=$ds->size;
                $dx['color']=$ds->color;
                $dx['item']=$ds->item;
                $dx['season']=$ds->season;
                $dx['barcode_roll']=null;
                $dx['supplier_name']=null;
                $dx['batch_no']=null;
                $dx['qty']=null;
                $dx['invoice']=null;
                $dx['country']=isset($ds->export_to) ? $ds->export_to :null;
                $dx['purchase_no']=null;
                $dx['style_name']=isset($cekitem) ? $cekitem->style_name : null;
                $dx['fabric_composition']=isset($cekitem) ? $cekitem->fabric_composition : null;
                $dx['weight']=isset($cekitem) ? $cekitem->weight : null;

                $data[]=$dx;
            }

       
        }


     
        return response()->json(['data'=>$data],200);
    }

    static function getBarcodeRoll($key){
        $dBrc = DB::connection('wms')
                        ->table('get_fg_fabric_v')
                        ->where('barcode','LIKE','%'.strtolower($key).'%')
                        ->select(
                            db::raw('UPPER(barcode) as barcode'),
                            db::raw('UPPER(purchase_number) as purchase_number'),
                            db::raw('UPPER(supplier_name) as supplier_name'),
                            db::raw('UPPER(color) as color'),
                            db::raw('UPPER(item_code) as item_code'),
                            db::raw('UPPER(item_desc) as item_desc'),
                            db::raw('UPPER(season) as season'),
                            db::raw('UPPER(no_invoice) as no_invoice'),
                            db::raw('UPPER(upc) as upc'),
                            'batch_number',
                            'planning_allocating_style',
                            'total_stock_arrival_roll',
                            'actual_width'
                        )
                        ->get();
        
        return $dBrc;
    }

    static function getBarcodeAcc($key){
        $dBa =DB::connection('wms')
                        ->table('get_fg_acc_v')
                        ->where('barcode','LIKE','%'.strtolower($key).'%')
                        ->select(
                            db::raw('UPPER(barcode) as barcode'),
                            db::raw('UPPER(purchase_number) as purchase_number'),
                            db::raw('UPPER(item_code) as item_code'),
                            db::raw('UPPER(item_desc) as item_desc'),
                            db::raw('UPPER(season) as season'),
                            db::raw('UPPER(style) as style'),
                            db::raw('UPPER(article_no) as article_no'),
                            db::raw('UPPER(color) as color'),
                            db::raw('UPPER(po_buyer) as po_buyer'),
                            db::raw('UPPER(upc) as upc'),
                            'supplier_name',
                            'no_invoice',
                            'total_stock_inventory'
                        )->get();

        return $dBa;
    }

    static function getSalesOrder($key,$group){
        if ($group==true) {
            $dSo = DB::connection('erp')->select("SELECT documentno,style,article,color,season,export_to FROM digilab_trf_so('".$key."') GROUP BY documentno,style,article,color,season,export_to");
        }else{
            $dSo = DB::connection('erp')->select("SELECT * FROM digilab_trf_so('".$key."')");
        }

        return $dSo;
    }

    static function getManufOrder($key,$group){
        
        if ($group==true) {
            $dMn = DB::connection('erp')->select("SELECT documentno,style,article,season,color FROM digilab_trf_mo('".$key."') GROUP BY documentno,style,article,season,color");
        }else{
            $dMn = DB::connection('erp')->select("SELECT * FROM digilab_trf_mo('".$key."')");
        }
     
        return $dMn;
    }
}
