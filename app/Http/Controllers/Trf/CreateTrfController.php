<?php

namespace App\Http\Controllers\Trf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use DataTables;


use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterMethode;
use App\Models\MasterCategory;
use App\Models\Employee;
use App\User;
use App\Models\MasterLabLocation;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Http\Controllers\HelperController;
use App\Http\Controllers\ERP\erpLoadData;

class CreateTrfController extends Controller
{

    public function index(){
        $fact = DB::table('factory')->whereNull('deleted_at')->get();

        return view('create_trf.index')->with('fact',$fact);
    }

    public function getData(Request $req){
        // DB::statement('REFRESH MATERIALIZED VIEW ns_list_active_trf');

        
        $data = DB::table('ns_list_active_trf')->where('factory_id',$req->factory_id);

        return DataTables::of($data)
                        ->editColumn('asal_specimen',function($data){
                            return strtoupper($data->asal_specimen);
                        })
                        ->editColumn('test_required',function($data){
                            if (isset($data->test_required)) {
                                return HelperController::testRequired($data->test_required,$data->previous_trf_id);
                            }else{
                                return '';
                            }
                            
                        })
                        ->editColumn('status',function($data){
                            return HelperController::statusCek($data->id);
                        })
                        ->editColumn('date_information_remark',function($data){
                            return HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
                        })
                        ->editColumn('return_test_sample',function($data){
                            if ($data->return_test_sample==true) {
                                return '<label class="label label-success ">YES</label>';
                            }else{
                                return '<label class="label label-warning ">NO</label>';
                            }
                        })
                        ->addColumn('action',function($data){
                            return view('_action',[
                                        'model'=>$data,
                                        'printBcd'=>route('trfcreate.printBarcode',['id'=>$data->id]),
                                        'detailBcd'=>route('trfcreate.printDetail',['id'=>$data->id])
                                    ]);
                        })
                        ->editColumn('nik',function($data){
                            $emp = Employee::where('nik',$data->nik)->first();
                            $ret = isset($emp) ? $emp->name." (".$emp->nik.")" : $data->nik;
                            return $ret;
                        })
                        ->rawcolumns(['asal_specimen','test_required','status','date_information_remark','return_test_sample','method_code'])->make(true);
    }

    public function getCtg(Request $req){
        $data = DB::table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->groupBy('category')
                    ->select('category')
                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function getCtgSpc(Request $req){
        $origin = $req->origin;
   
        switch ($origin) {
            case 'SSM':
                $spc = ['STRIKE OFF','GARMENT','FABRIC','MOCKUP','ACCESORIES/TRIM','HIJAB'];
                break;

            case 'WMS':
                $spc = ['FABRIC','ACCESORIES/TRIM'];
                break;

            case 'FGMS':
                $spc = ['GARMENT'];
                break;

            case 'CDMS':
                $spc = ['MOCKUP','STRIKE OFF','PANEL'];
                break;
            
            default:
                $spc = ['STRIKE OFF','GARMENT','FABRIC','MOCKUP','ACCESORIES/TRIM','HIJAB'];
                break;
                break;
        }
        $data = DB::table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->where('category',trim($req->category))
                    ->whereIn('category_specimen',$spc)
                    ->groupBy('category_specimen')
                    ->select('category_specimen')
                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function getTypeSpc(Request $req){
        
        $data = DB::table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->where('category',trim($req->category))
                    ->where('category_specimen',trim($req->category_specimen))
                    ->groupBy('type_specimen')
                    ->select('type_specimen')
                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function getMeth(Request $req){
        $data = DB::table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->where('category',trim($req->category))
                    ->where('category_specimen',trim($req->category_specimen))
                    ->where('type_specimen',trim($req->type_specimen))
                    ->groupBy('master_method_id','method_code','method_name')
                    ->select('master_method_id','method_code','method_name')
                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function formCreate(){
        $buyer = DB::table('master_buyer')->wherenull('deleted_at')->get();
        // $fact = DB::table('factory')->wherenull('deleted_at')->get();
        $labloc = MasterLabLocation::whereNull('deleted_at')->get();

        return view('create_trf.create')->with('buyer',$buyer)->with('labloc',$labloc);
    }
    public function createTrf(Request $req){


        $methods = $req->test_methods;
        $datas = $req->datas;

       

        $origin = $req->origin;
        $asal = $req->asal;
        $buyer = $req->buyer;
        $category_specimen = $req->category_specimen;
        $category = $req->category;
        $type_specimen = $req->type_specimen;
        $test_req = $req->test_req;
        $radio_date_info = $req->radio_date_info;
        $date_info = date_format(date_create($req->date_info),'Y-m-d');
        $part_test = $req->part_test;
        $return_test = $req->returntest;
        $no_trf = trim($req->no_trf);
        // $addInfo = trim($req->addInfo);
        // dd($category_specimen,$asal);
        $trf_id = $this->getCode($category_specimen,$asal);
        $getCtg = MasterCategory::where('category',$category)->where('category_specimen',$category_specimen)->where('type_specimen',$type_specimen)->whereNull('deleted_at')->first();
        $labloction = $req->labloction;
        $chkTrf = TrfTesting::where('trf_id',$trf_id)->whereNull('deleted_at')->exists();

        if ($chkTrf) {
            return response()->json('TRF Number was already ! ! !',422);
        }

        try {
            DB::beginTransaction();
                $in = array(
                            'trf_id'=>$trf_id,
                            'buyer'=>$buyer,
                            'lab_location'=>$labloction,
                            'nik'=>auth::user()->nik,
                            'platform'=>$origin,
                            'factory_id'=>auth::user()->factory_id,
                            'asal_specimen'=>$asal,
                            'category_specimen'=>$category_specimen,
                            'category'=>$category,
                            'type_specimen'=>$type_specimen,
                            'date_information'=>$date_info,
                            'date_information_remark'=>$radio_date_info,
                            'test_required'=>$test_req,
                            'part_of_specimen'=>$part_test,
                            'created_at'=>carbon::now(),
                            'return_test_sample'=>$return_test,
                            'status'=>'OPEN',
                            'previous_trf_id'=>isset($no_trf)  ? $no_trf : null,
                            'id_category'=>$getCtg->id,
                            'username'=>auth::user()->name
                        );

                $inTrf = TrfTesting::firstorcreate($in);

                if ($inTrf) {
                    foreach ($methods as $mtd) {
                       TrfTestingMethods::firstorcreate(['trf_id'=>$inTrf->id,'master_method_id'=>$mtd,'created_at'=>carbon::now(),'created_by'=>auth::user()->nik]);
                    }

                    foreach ($datas as $ds) {
                        
                    // dd($ds,isset($ds['qty']));
                        $docin = array(
                                    'trf_id'=>$inTrf->id,
                                    'document_type'=>trim($ds['document_type']),
                                    'document_no'=>trim($ds['document_no']),
                                    'style'=>trim($ds['style']),
                                    'article_no'=>trim($ds['article_no']),
                                    'size'=>trim($ds['size']),
                                    'color'=>trim($ds['color']),
                                    'fibre_composition'=>trim($ds['fibre_composition']),
                                    'manufacture_name'=>trim($ds['manufacture_name']),
                                    'export_to'=>trim($ds['export_to']),
                                    'nomor_roll'=>trim($ds['nomor_roll']),
                                    'batch_number'=>trim($ds['batch_number']),
                                    'item'=>trim($ds['item']),
                                    'barcode_supplier'=>trim($ds['barcode_supplier']),
                                    'yds_roll'=>trim($ds['yds_roll']),
                                    'season'=>trim($ds['season']),
                                    'invoice'=>trim($ds['invoice']),
                                    'fabric_color'=>trim($ds['fabric_color']),
                                    'interlining_color'=>trim($ds['interlining_color']),
                                    'qty'=>trim($ds['qty']) ? trim($ds['qty']) :0,
                                    'machine'=>trim($ds['machine']),
                                    'temperature'=>trim($ds['temperature']),
                                    'pressure'=>trim($ds['pressure']),
                                    'duration'=>trim($ds['duration']),
                                    'style_name'=>trim($ds['style_name']),
                                    'fabric_item'=>trim($ds['fabric_item']),
                                    'fabric_type'=>trim($ds['fabric_type']),
                                    'thread'=>trim($ds['thread']),
                                    'material_shell_panel'=>trim($ds['material_shell_panel']),
                                    'product_category'=>trim($ds['product_category']),
                                    'remark'=>trim($ds['remark']),
                                    'pad'=>trim($ds['pad']),
                                    'pelling'=>trim($ds['pelling']),
                                    'component'=>trim($ds['component']),
                                    'garment_size'=>trim($ds['garment_size']),
                                    'sample_type'=>trim($ds['sample_type']),
                                    'test_condition'=>trim($ds['test_condition']),
                                    'fabric_properties'=>trim($ds['fabric_properties']),
                                    'description'=>trim($ds['description']),
                                    'created_at'=>carbon::now()
                                );

                        TrfTestingDocument::firstorcreate($docin);
                    }
                }
            DB::commit();
            $data_response = ['status'=>200,"output"=>"Create TRF Success ! ! ! ". $trf_id];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>"Create TRF Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }

    private function getCode($spcode,$asal){

       

        $gefac = DB::table('factory')->where('id',Auth::user()->factory_id)->first();
        $sp = DB::table('master_spesiment_code')->where('platform',$asal)->where('spesimen',$spcode)->whereNull('deleted_at')->first();
        $date = Carbon::now()->format('ymd');
        $last = TrfTesting::where('factory_id',auth::user()->factory_id)->where('asal_specimen',$asal)->whereDate('created_at',Carbon::now()->format('Y-m-d'))->orderby('created_at','desc')->first();

        if ($last==null) {
            $x = (int)0;
        }else{
            $x = (int)substr($last->trf_id, -3);

        }

        $rcode = "TRF/".$gefac->factory_name."/".$date."-".$sp->code.sprintf("%03s", $x+1);

        return $rcode;
    }

    public function printBarcode(Request $req){
        $id= trim($req->id);

        $data = TrfTesting::where('id',$id)->first();

        return view('create_trf.print_barcode',['data'=>$data]);
    }

    public function printDetail(Request $req){
        $id= trim($req->id);

        $data = DB::table('trf_testings')
                            ->join('master_category','trf_testings.id_category','=','master_category.id')
                            ->join('factory','trf_testings.factory_id','=','factory.id')
                            ->where('trf_testings.id',$id)
                            ->whereNull('trf_testings.deleted_at')
                            ->select(
                                    'trf_testings.id',
                                    'trf_testings.trf_id',
                                    'trf_testings.lab_location',
                                    'trf_testings.buyer',
                                    'trf_testings.nik',
                                    'trf_testings.platform',
                                    'trf_testings.asal_specimen',
                                    'trf_testings.date_information',
                                    'trf_testings.date_information_remark',
                                    'trf_testings.test_required',
                                    'trf_testings.part_of_specimen',
                                    'trf_testings.created_at',
                                    'trf_testings.return_test_sample',
                                    'trf_testings.status',
                                    'trf_testings.verified_lab_date',
                                    'trf_testings.verified_lab_by',
                                    'trf_testings.reject_by',
                                    'trf_testings.reject_date',
                                    'trf_testings.additional_information',
                                    'trf_testings.previous_trf_id',
                                    'master_category.category',
                                    'master_category.category_specimen',
                                    'master_category.type_specimen',
                                    'factory.factory_name'
                                )->first();


        $currentURL = 'http://'.$req->getHttpHost().'/api/print/report-specimen?id='.$data->id.'&notrf='.$data->trf_id;
        $qrcode =  HelperController::generateQr($currentURL);

        

        $testreq = isset($data->test_required) ? HelperController::testRequired($data->test_required,$data->previous_trf_id) : '';
        $dateinfo = HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
        $meth = DB::table('trf_testing_methods')
                        ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                        ->whereNull('trf_testing_methods.deleted_at')
                        ->where('trf_testing_methods.trf_id',$data->id)
                        ->select('master_method.id','master_method.method_code','master_method.method_name','master_method.category','master_method.type')->get()->toArray();

        $docm = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->get();

        $users = Employee::where('nik', $data->nik)->first();




        $vefpiclab = isset($data->verified_lab_by) ?  User::where('nik', $data->verified_lab_by)->first()['name'] : "";
        $vefdatelab = isset($data->verified_lab_date) ? carbon::parse($data->verified_lab_date)->format('d-m-Y H:i:s') : "";
        $filename = "TRF_DETAIL_".$data->trf_id;

        $pdf = \PDF::loadView('create_trf.print_detail',['data'=>$data,'method'=>$meth,'docm'=>$docm,'submit_by'=>isset($users) ? $users->name : $data->nik,'test_required'=>$testreq,'dateinfo'=>$dateinfo,'labdate'=>$vefdatelab,'labpic'=>$vefpiclab,'qrcode'=>base64_encode($qrcode)])->setPaper('A4','Potrait');
        return $pdf->stream($filename);
    }

    public function getDocument(Request $req){
        $type = trim($req->type);
        $docno = trim($req->docno);

        $load = erpLoadData::getDocErp($docno,$type);

        return response()->json(['data'=>$load],200);
    }

    public function mapsModalDoc(Request $req){

        return HelperController::mappingSpc($req->ctgspc,$req->typespc);
    }
}
