<?php

namespace App\Http\Controllers\Trf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use DataTables;


use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterMethode;
use App\Models\MasterCategory;
use App\Models\Employee;
use App\User;
use App\Models\MasterLabLocation;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Http\Controllers\HelperController;
use App\Http\Controllers\ERP\erpLoadData;
use Exception;

class CreateController extends Controller
{
    public function index(){

        $lab = MasterLabLocation::whereNull('deleted_at')->get();

        return view('create_trf.index',compact('lab'));
    }


    public function getData(Request $req){
        // DB::statement('REFRESH MATERIALIZED VIEW ns_list_active_trf');
        $fil = trim($req->search['value']);

   
        
        $data = DB::table('ns_list_active_trf')->where('lab_location',$req->lablocation);

        if (Auth::user()->admin==false) {
           $data = $data->where('factory_id',auth::user()->factory_id);
        }else{
            $data = $data;
        }

        if(isset($fil)){
            $data = $data->where(function($query) use ($fil){
                $query->where('trf_id','Like','%'.$fil.'%')
                ->orwhere('asal_specimen','Like','%'.$fil.'%')
                ->orwhere('test_required','Like','%'.$fil.'%')
                ->orwhere('date_information_remark','Like','%'.$fil.'%')
                ->orwhere('buyer','Like','%'.$fil.'%')
                ->orwhere('nik','Like','%'.$fil.'%')
                ->orwhere('category','Like','%'.$fil.'%')
                ->orwhere('category_specimen','Like','%'.$fil.'%')
                ->orwhere('type_specimen','Like','%'.$fil.'%')
                ->orwhere('method_code','Like','%'.$fil.'%')
                ->orwhere('return_test_sample','Like','%'.$fil.'%');
            });
        }



        return DataTables::of($data)
                        ->editColumn('asal_specimen',function($data){
                            return strtoupper($data->asal_specimen);
                        })
                        ->editColumn('test_required',function($data){
                            if (isset($data->test_required)) {
                                return HelperController::testRequired($data->test_required,$data->previous_trf_id);
                            }else{
                                return '';
                            }
                            
                        })
                        ->editColumn('status',function($data){
                            return HelperController::statusCek($data->id);
                        })
                        ->editColumn('date_information_remark',function($data){
                            return HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
                        })
                        ->editColumn('return_test_sample',function($data){
                            if ($data->return_test_sample==true) {
                                return '<label class="label label-success ">YES</label>';
                            }else{
                                return '<label class="label label-warning ">NO</label>';
                            }
                        })
                        ->addColumn('action',function($data){
                            // return view('_action',[
                            //             'model'=>$data,
                            //             'printBcd'=>route('trf.create.printBarcode',['data'=>json_encode([$data->id])]),
                            //             'detailBcd'=>route('trf.create.detailTrf',['data'=>json_encode([$data->id])])
                            //         ]);

                            if ($data->status=="OPEN") {
                                return view('_action',[
                                    'model'=>$data,
                                    'printBcd'=>route('trf.create.printBarcode',['data'=>json_encode([$data->id])]),
                                    'detailBcd'=>route('trf.create.detailTrf',['data'=>json_encode([$data->id])]),
                                    'editrf'     =>route('dashboardTrf.editTrf',['id'=>$data->id,'return'=>route('trf.create.index')])
                                ]);
                            }else{
                                return view('_action',[
                                    'model'=>$data,
                                    'printBcd'=>route('trf.create.printBarcode',['data'=>json_encode([$data->id])]),
                                    'detailBcd'=>route('trf.create.detailTrf',['data'=>json_encode([$data->id])])
                                ]);
                            }
                        })
                        ->editColumn('nik',function($data){
                            $emp = Employee::where('nik',$data->nik)->first();
                            $ret = isset($emp) ? $emp->name." (".$emp->nik.")" : $data->nik;
                            return $ret;
                        })
                        ->rawcolumns(['asal_specimen','test_required','status','date_information_remark','return_test_sample','method_code'])->make(true);
    }


    public function createTrf(){
        $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();
        $labloc = DB::table('master_lab_location')->whereNull('deleted_at')->get();
        return view('create_trf.create',compact('buyer','labloc'));
    }

    public function getCategory(Request $req){
        $buyer = $req->buyer;

        $data = DB::table('ns_req_catg_method')->where('buyer',$buyer)->groupBy('category')->select('category')->get();

        return response()->json($data);
    }

    public function getCategorySpc(Request $req){
        $buyer = $req->buyer;
        $category = $req->category;

        switch ($req->origin) {
            case 'SSM':
                $spc = ['STRIKE OFF','GARMENT','FABRIC','MOCKUP','ACCESORIES/TRIM','HIJAB'];
                break;

            case 'WMS':
                $spc = ['FABRIC','ACCESORIES/TRIM'];
                break;

            case 'FGMS':
                $spc = ['GARMENT'];
                break;

            case 'CDMS':
                $spc = ['MOCKUP','STRIKE OFF','PANEL'];
                break;
            
            default:
                $spc = ['STRIKE OFF','GARMENT','FABRIC','MOCKUP','ACCESORIES/TRIM','HIJAB'];
                break;
                break;
        }

        $data = DB::table('ns_req_catg_method')
                        ->where([
                            'buyer'=>$buyer,
                            'category'=>$category
                        ])
                        ->whereIn('category_specimen',$spc)
                        ->groupBy('category_specimen')
                        ->select('category_specimen')->get();

        return response()->json($data);
    }

    public function getTypeSpc(Request $req){
        $buyer = $req->buyer;
        $category = $req->category;
        $category_specimen = $req->category_specimen;

        $data = DB::table('ns_req_catg_method')
                        ->where([
                            'buyer'=>$buyer,
                            'category'=>$category,
                            'category_specimen'=>$category_specimen
                        ])
                        ->groupBy('type_specimen')
                        ->select('type_specimen')->get();

        return response()->json($data);
    }

    public function getMethod(Request $req){
        $buyer = $req->buyer;
        $category = $req->category;
        $category_specimen = $req->category_specimen;
        $type_specimen = $req->type_specimen;

        $data = DB::table('ns_req_catg_method')
                        ->where([
                            'buyer'=>$buyer,
                            'category'=>$category,
                            'category_specimen'=>$category_specimen,
                            'type_specimen'=>$type_specimen
                        ])
                        ->groupBy([
                            'master_method_id',
                            'method_code',
                            'method_name'
                        ])
                        ->select(
                            'master_method_id',
                            'method_code',
                            'method_name'
                        )->get();

        return response()->json($data);
    }

    public function getDataSpecimen(Request $req){
        $type = $req->type;
        // $category_specimen = $req->category_specimen;
        // $type_specimen = $req->type_specimen;
        $origin = $req->origin;
        $key = $req->key;
    
        $data = erpLoadData::getDocErp($key,$type,$origin);
        // dd($data);
        return response()->json(['data'=>$data],200);
    }

    public function submitTrf(Request $req){
        $dataSpec =$req->data;

        try {
           DB::beginTransaction();
                $list =[];
                $getcategory = MasterCategory::where([
                                                        'category'=>$req->category,
                                                        'category_specimen'=>$req->category_specimen,
                                                        'type_specimen'=>$req->type_specimen
                                                    ])->whereNull('deleted_at')->first();
                foreach ($dataSpec as $key => $value) {

                    // dd($value);
                   $trfid = self::getCode($req->category_specimen,$req->asal);
                    
                //    $cekTrf = TrfTesting::where('trf_id',$trfid)->whereNull('deleted_at')->exists();

                //    if ($cekTrf) {
                //         return response()->json('TRF is Already exists ! ! !',422);
                //    }

                   
     
                   $trftesting = TrfTesting::firstOrCreate([
                    'trf_id'=>$trfid,
                    'buyer'=>$req->buyer,
                    'lab_location'=>$req->lab_location,
                    'nik'=>auth::user()->nik,
                    'platform'=>$req->origin,
                    'factory_id'=>auth::user()->factory_id,
                    'asal_specimen'=>$req->asal,
                    'category_specimen'=>$req->category_specimen,
                    'category'=>$req->category,
                    'type_specimen'=>$req->type_specimen,
                    'date_information'=>date_format(date_create($req->date_info),'Y-m-d'),
                    'date_information_remark'=>$req->radio_date_info,
                    'test_required'=>$req->test_req,
                    'part_of_specimen'=>trim($req->part_to_test),
                    'created_at'=>Carbon::now(),
                    'return_test_sample'=>$req->return_test,
                    'status'=>"OPEN",
                    'previous_trf_id'=>trim($req->no_trf),
                    'id_category'=>$getcategory->id,
                    'username'=>auth::user()->name
                   ]);
                
                    

              
                   foreach ($req->method as $mth) {
                     TrfTestingMethods::firstOrCreate([
                        'trf_id'=>$trftesting->id,
                        'master_method_id'=>$mth,
                        'created_at'=>Carbon::now(),
                        'created_by'=>auth::user()->nik
                     ]);
                   }

                    $docid = array(
                        'trf_id'=>$trftesting->id,
                        'document_type'=>trim($value['document_type']),
                        'document_no'=>trim($value['document_no']),
                        'style'=>trim($value['style']),
                        'article_no'=>trim($value['article_no']),
                        'size'=>trim($value['size']),
                        'color'=>trim($value['color']),
                        'fibre_composition'=>trim($value['fibre_composition']),
                        'fabric_finish'=>trim($value['fabric_finish']),
                        'gauge'=>trim($value['gauge']),
                        'fabric_weight'=>trim($value['fabric_weight']),
                        'plm_no'=>trim($value['plm_no']),
                        'care_instruction'=>trim($value['care_instruction']),
                        'manufacture_name'=>trim($value['manufacture_name']),
                        'export_to'=>trim($value['export_to']),
                        'nomor_roll'=>trim($value['nomor_roll']),
                        'batch_number'=>trim($value['batch_number']),
                        'item'=>trim($value['item']),
                        'barcode_supplier'=>trim($value['barcode_supplier']),
                        'additional_information'=>trim($value['additional_information']),
                        'yds_roll'=>trim($value['yds_roll']),
                        'description'=>trim($value['description']),
                        'season'=>trim($value['season']),
                        'invoice'=>trim($value['invoice']),
                        'fabric_color'=>trim($value['fabric_color']),
                        'interlining_color'=>trim($value['interlining_color']),
                        'qty'=>(int)trim($value['qty']),
                        'machine'=>trim($value['machine']),
                        'temperature'=>trim($value['temperature']),
                        'pressure'=>trim($value['pressure']),
                        'duration'=>trim($value['duration']),
                        'style_name'=>trim($value['style_name']),
                        'fabric_item'=>trim($value['fabric_item']),
                        'fabric_type'=>trim($value['fabric_type']),
                        'thread'=>trim($value['thread']),
                        'material_shell_panel'=>trim($value['material_shell_panel']),
                        'product_category'=>trim($value['product_category']),
                        'remark'=>trim($value['remark']),
                        'pad'=>trim($value['pad']),
                        'pelling'=>trim($value['pelling']),
                        'component'=>trim($value['component']),
                        'garment_size'=>trim($value['garment_size']),
                        'sample_type'=>trim($value['sample_type']),
                        'test_condition'=>trim($value['test_condition']),
                        'fabric_properties'=>trim($value['fabric_properties']),
                        'lot'=>trim($value['lot']),
                        'po_buyer'=>trim($value['po_buyer']),
                        'arrival_date'=>trim($value['arrival_date'])!=null ? trim($value['arrival_date']) : null,
                        'created_at'=>Carbon::now()
                    );

                    TrfTestingDocument::firstOrCreate($docid);

                    $list[]=$trftesting->id;
                }
                
           DB::commit();
           $data_response = [
                'status'=>200,
                'output'=>"Create Trf Success ! ! !",
                'list'=>json_encode($list)
            ];
        } catch (Exception $th) {
            DB::rollback();
            $data_response = [
                'status'=>422,
                'output'=>"Create Trf Failed ! ! !
                ".$th
            ];
        }

        return response()->json(['data_response'=>$data_response]);
    }

    private function getCode($spcode,$asal){

       

        $gefac = DB::table('factory')->where('id',Auth::user()->factory_id)->first();
        $sp = DB::table('master_spesiment_code')->where('platform',$asal)->where('spesimen',$spcode)->whereNull('deleted_at')->first();
        $date = Carbon::now()->format('ymd');
        $last = TrfTesting::where('factory_id',auth::user()->factory_id)->where('asal_specimen',$asal)->whereDate('created_at',Carbon::now()->format('Y-m-d'))->orderby('created_at','desc')->orderby('trf_id','desc')->first();

        if ($last==null) {
            $x = (int)0;
        }else{
            $x = (int)substr($last->trf_id, -3);

        }

        $rcode = "TRF/".$gefac->factory_name."/".$date."-".$sp->code.sprintf("%03s", $x+1);

        return $rcode;
    }

    public function printBarcode(Request $req){
        $data = json_decode($req->data);

        $list = TrfTesting::whereIn('id',$data)->whereNull('deleted_at')->get();

        return view('create_trf.barcode_trf',['list'=>$list]);
    }

    public function detailTrf(Request $req){
        $data = json_decode($req->data);
        // dd($data);
        $sub = DB::table('trf_testing_methods')
                            ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->groupBy('trf_testing_methods.trf_id')
                            ->select('trf_testing_methods.trf_id',DB::raw("string_agg(concat(master_method.method_code,' (',master_method.method_name,')'),'|') as methods"));

        $list = DB::table('trf_testings')
                            ->join('trf_testing_document','trf_testings.id','trf_testing_document.trf_id')
                            ->join('master_category','trf_testings.id_category','master_category.id')
                            ->join('factory','trf_testings.factory_id','factory.id')
                            ->leftjoinSub($sub,'sub','trf_testings.id','sub.trf_id')
                            ->whereIn('trf_testings.id',$data)
                            ->whereNull('trf_testing_document.deleted_at')
                            ->whereNull('trf_testings.deleted_at')
                            ->select(
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.verified_lab_date',
                                'trf_testings.verified_lab_by',
                                'trf_testings.reject_by',
                                'trf_testings.reject_date',
                                'trf_testings.previous_trf_id',
                                'trf_testings.id',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'trf_testing_document.document_type',
                                'trf_testing_document.document_no',
                                'trf_testing_document.style',
                                'trf_testing_document.article_no',
                                'trf_testing_document.size',
                                'trf_testing_document.color',
                                'trf_testing_document.fibre_composition',
                                'trf_testing_document.fabric_finish',
                                'trf_testing_document.gauge',
                                'trf_testing_document.fabric_weight',
                                'trf_testing_document.plm_no',
                                'trf_testing_document.care_instruction',
                                'trf_testing_document.manufacture_name',
                                'trf_testing_document.export_to',
                                'trf_testing_document.nomor_roll',
                                'trf_testing_document.batch_number',
                                'trf_testing_document.item',
                                'trf_testing_document.barcode_supplier',
                                'trf_testing_document.additional_information',
                                'trf_testing_document.yds_roll',
                                'trf_testing_document.description',
                                'trf_testing_document.season',
                                'trf_testing_document.invoice',
                                'trf_testing_document.fabric_color',
                                'trf_testing_document.interlining_color',
                                'trf_testing_document.qty',
                                'trf_testing_document.machine',
                                'trf_testing_document.temperature',
                                'trf_testing_document.pressure',
                                'trf_testing_document.duration',
                                'trf_testing_document.style_name',
                                'trf_testing_document.fabric_item',
                                'trf_testing_document.fabric_type',
                                'trf_testing_document.thread',
                                'trf_testing_document.material_shell_panel',
                                'trf_testing_document.product_category',
                                'trf_testing_document.remark',
                                'trf_testing_document.pad',
                                'trf_testing_document.pelling',
                                'trf_testing_document.component',
                                'trf_testing_document.garment_size',
                                'trf_testing_document.sample_type',
                                'trf_testing_document.test_condition',
                                'trf_testing_document.fabric_properties',
                                'trf_testing_document.lot',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                'sub.methods',
                                'factory.factory_name'
                            )->get();
        
        $pdf = \PDF::loadview('create_trf.detail_trf',['list'=>$list])->setPaper('A4','potrait');

        return $pdf->stream('test.pdf');
    }
}
