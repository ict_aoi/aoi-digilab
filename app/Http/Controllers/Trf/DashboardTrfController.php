<?php

namespace App\Http\Controllers\Trf;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Controllers\Controller;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Models\MasterTrfTestingModel;
use App\Models\TestingResult;
use App\Models\MasterRequirement;
use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterMethode;
use App\Models\supplierResult;
use App\Models\Employee;
use App\Models\MasterCategory;
use App\Models\MasterLabLocation;

use App\Http\Controllers\HelperController;

class DashboardTrfController extends Controller
{


    public function index()
    {
        // $fact = DB::table('factory')->wherenull('deleted_at')->get();
        // return view('dashboard_trf.index')->with('fact',$fact);
        $labloc = DB::table('master_lab_location')->wherenull('deleted_at')->orderBy('id','asc')->get();
        return view('dashboard_trf.index')->with('labloct',$labloc);
    }

    public function data(Request $req)
    {

            

            $data = DB::table('ns_trf_waitinglist')->where('lab_location',$req->labloct);
            // dd($data->get());
            return DataTables::of($data)
                        ->editColumn('created_at',function($data){
                            return Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s')." <br> By : ".$data->username;
                        })
                        ->editColumn('trf_id',function($data){
                            return '<b>'.$data->trf_id.'</b><br>
                                    ('.$data->buyer.' - '.$data->asal_specimen.')';
                        })
                        ->addColumn('specimen',function($data){
                                $doc = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->get();
                                $ret = '';
                                foreach ($doc as $dc) {
                                    $x = HelperController::setDoc($dc);

                                    $ret = $ret.' '.$x.'<hr>';
                                }

                                return $ret;
                        })
                        ->editColumn('test_required',function($data){
                            return HelperController::testRequired($data->test_required,$data->previous_trf_id);
                        })
                        ->editColumn('date_information_remark',function($data){
                            return HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
                        })
                        ->editColumn('status',function($data){
                            return HelperController::statusCek($data->id);
                        })
                        ->editColumn('nik',function($data){
                            return $data->username."<br>".$data->nik;
                        })
                        ->editColumn('category',function($data){
                            return '<b>Category </b> : '.$data->category.'<br>
                                    <b>Category Specimen </b> : '.$data->category_specimen.'<br>
                                    <b>Type Specimen </b> : '.$data->type_specimen;
                        })
                        ->editColumn('return_test_sample',function($data){
                            if($data->return_test_sample == 't')
                            {
                                return '<span class="label bg-success">YES</span>';
                            }else {
                                return '<span class="label bg-danger">NO</span>';

                            }
                        })
                        ->addColumn('action',function($data){
                            if($data->status == 'OPEN' && $data->methods!="")
                            {
                                return view('_action', [
                                    'model'  => $data,
                                    'approval'   =>['id'=>$data->id,'trf'=>$data->trf_id],
                                    'reject'     =>['id'=>$data->id,'trf'=>$data->trf_id],
                                    'setMeth'    =>['id'=>$data->id,'trf'=>$data->trf_id,'id_category'=>$data->id_category,'buyer'=>$data->buyer],
                                    'editrf'     =>route('dashboardTrf.editTrf',['id'=>$data->id,'return'=>route('dashboardTrf.index')])
                                ]);
                            }else if($data->status == 'OPEN' && $data->methods=="")
                            {
                                return view('_action', [
                                    'model'  => $data,
                                    'reject'     =>['id'=>$data->id,'trf'=>$data->trf_id],
                                    'setMeth'    =>['id'=>$data->id,'trf'=>$data->trf_id,'id_category'=>$data->id_category,'buyer'=>$data->buyer],
                                    'editrf'     =>route('dashboardTrf.editTrf',['id'=>$data->id,'return'=>route('dashboardTrf.index')])
                                ]);
                            } else{
                                return view('dashboard_trf._action', [
                                    'detail'     => route('dashboardTrf.detail', $data->id),
                                ]);
                            }
                        })
                        ->rawColumns(['specimen','category','trf_id','created_at','test_required','date_information_remark','status','nik','return_test_sample','action'])
                        ->make(true);

    }


    public function approval(Request $request)
    {
        try
        {
            DB::beginTransaction();
                    switch ($request->status) {
                        case 'VERIFIED':
                            $up = array(
                                        'verified_lab_date' => carbon::now(),
                                        'status'       => 'VERIFIED',
                                        'verified_lab_by'   => auth::user()->nik,
                                        'reject_date'=>null,
                                        'reject_by'=>null,
                                        'updated_at'        => carbon::now()
                                    );
                        break;


                        case 'REJECT':
                            $up = array(
                                        'reject_date' => carbon::now(),
                                        'status'       => 'REJECT',
                                        'reject_by'   => auth::user()->nik,
                                        'updated_at'        => carbon::now()
                                    );
                        break;

                    }

                    $masterTrf = TrfTesting::where('id', $request->id)->update($up);

            DB::commit();
            $data_response = ['status'=>200,"output"=>$request->status." TRF ".$request->trf." Success ! ! !"];

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = ['status'=>422,"output"=>"Approve TRF ".$request->trf." Field ! ! !".$message];
        }

        return response()->json(['data'=>$data_response]);
    }


    // public function setMethode(){
    //     return view('dashboard_trf.set_method');
    // }

    // public function dataSetMeth()
    // {

    //         switch (auth::user()->factory_id) {
    //             case '1':
    //                 $factory = "AOI1";
    //                 break;

    //             case '2':
    //                 $factory = "AOI2";
    //                 break;
    //         }

    //         $fabric_testing = TrfTesting::whereNull('deleted_at')
    //         ->whereIn('status',['VERIFIED'])
    //         ->where('lab_location', $factory)
    //         ->orderBy('created_at','desc');
    //         // dd($factory);


    //         return DataTables::of($fabric_testing)
    //         ->editColumn('nik',function ($fabric_testing)
    //         {
    //             $users = Employee::where('nik', $fabric_testing->nik)->first();

    //             return isset($users->name) ? $users->name : $fabric_testing->nik;
    //         })
    //         ->editColumn('lab_location',function ($fabric_testing)
    //         {
    //             if($fabric_testing->lab_location == 'AOI1') return 'AOI 1';
    //             elseif($fabric_testing->lab_location == 'AOI2') return 'AOI 2';
    //             else return '-';
    //         })
    //         ->editColumn('date_information_remark',function ($fabric_testing)
    //         {
    //             if($fabric_testing->date_information_remark == 'buy_ready') return 'Buy Ready - '. $fabric_testing->date_information ;
    //             elseif($fabric_testing->date_information_remark == 'output_sewing') return '1st Output Sewing - '. $fabric_testing->date_information ;
    //             else return 'PODD - '. $fabric_testing->date_information ;
    //         })
    //         ->editColumn('test_required',function ($fabric_testing)
    //         {
    //             return HelperController::testRequired($fabric_testing->test_required,$fabric_testing->previous_trf_id);
    //         })
    //         ->editColumn('status',function ($fabric_testing)
    //         {

    //             HelperController::statusCek($fabric_testing->id);
    //         })
    //         ->editColumn('return_test_sample',function ($fabric_testing)
    //         {
    //             if($fabric_testing->return_test_sample == 't')
    //             {
    //                 return '<span class="label bg-success">YES</span>';
    //             }else {
    //                 return '<span class="label bg-danger">NO</span>';

    //             }

    //         })

    //         ->addColumn('action', function($fabric_testing) {

    //           return view('_action', [
    //                     'model'  => $fabric_testing,
    //                     'setMeth'=>[
    //                                 'id'=>$fabric_testing->id,
    //                                 'trf'=>$fabric_testing->trf_id,
    //                                 'id_category'=>$fabric_testing->id_category
    //                             ]
    //                 ]);
    //         })
    //         ->rawColumns(['status','action','orderno','return_test_sample'])
    //         ->make(true);

    // }

    public function getMethode(Request $req){

        // $data=MasterMethode::whereNull('deleted_at')->get();
        $data = DB::table('ns_req_catg_method')->where('master_category_id',$req->idctg)->where('buyer',$req->buyer)->groupBy('master_method_id','method_code','method_name')->select('master_method_id','method_code','method_name')->get();

        return response()->json(['data'=>$data],200);
    }

    public function getTrfMEth(Request $request){

        $data = TrfTestingMethods::where('trf_id',$request->id)->whereNull('deleted_at')->select('master_method_id')->get()->toArray();

        // dd(array_column($data,'master_method_id'));

        return response()->json(['data'=>array_column($data,'master_method_id')],200);
    }


    public function updateMth(Request $request){
        $id = $request->id;
        $data = $request->data;

        if (count($data)<=0) {
            return response()->json("Methode not set !!!",422);
        }

        $cekReslt = DB::table('trf_testing_methods')
                                ->join('trf_requirement_set','trf_testing_methods.id','=','trf_requirement_set.trf_meth_id')
                                ->whereNull('trf_testing_methods.deleted_at')
                                ->whereNull('trf_requirement_set.deleted_at')
                                ->where('trf_testing_methods.trf_id',trim($id))
                                ->count();

        if ($cekReslt>0) {
            return response()->json('TRF Methode has set requirement ! ! ! ',422);
        }

        try {
            DB::beginTransaction();
            $upDel = array('deleted_at'=>carbon::now(),'deleted_by'=>auth::user()->nik);
            $cek = TrfTestingMethods::where('trf_id',$id)->whereNotIn('master_method_id',$data)->update($upDel);

          

            foreach ($data as $dt) {

                $cek = TrfTestingMethods::where('trf_id',$id)->where('master_method_id',$dt)->exists();
                if ($cek==false) {
                       $inmeth = array(
                                    'trf_id'=>$id,
                                    'master_method_id'=>$dt,
                                    'created_by'=>auth::user()->nik
                                );
                   TrfTestingMethods::firstorcreate($inmeth);
                }

            }
            $data_response = ['status'=>200,"output"=>"Update methode  success ! ! !"];
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = ['status'=>422,"output"=>"Update methode  Field ! ! !".$message];
        }

        return response()->json(['data'=>$data_response]);
    }


    public function editTrf(Request $req){
        $trfid = trim($req->id);
        // $fact  = DB::table('factory')->wherenull('deleted_at')->get();
        $buyer = DB::table('master_buyer')->wherenull('deleted_at')->get();
        $labloc = MasterLabLocation::whereNull('deleted_at')->get();

        $dataTrf = DB::table('trf_testings')
                            ->leftjoin('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                            ->leftjoin('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->join('master_category','trf_testings.id_category','master_category.id')
                            ->join('master_buyer','trf_testings.buyer','master_buyer.buyer')
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->where('trf_testings.id',$trfid)
                            ->groupBy(
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.previous_trf_id',
                                'trf_testings.username',
                                'master_buyer.buyer',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen'
                                // 'master_method.id'
                            )
                            ->select(
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.previous_trf_id',
                                'trf_testings.username',
                                'master_buyer.buyer',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                DB::raw("string_agg(master_method.id,',') as method_id")
                                // 'master_method.id'
                            )->first();
    
        $doc   =  TrfTestingDocument::where('trf_id',$trfid)
                                ->whereNull('deleted_at')
                                ->select(
                                        'id',
                                        'trf_id',
                                        'document_type',
                                        'document_no',
                                        'style',
                                        'article_no',
                                        'size',
                                        'color',
                                        'fibre_composition',
                                        'fabric_finish',
                                        'gauge',
                                        'fabric_weight',
                                        'plm_no',
                                        'care_instruction',
                                        'manufacture_name',
                                        'export_to',
                                        'nomor_roll',
                                        'batch_number',
                                        'item',
                                        'barcode_supplier',
                                        'additional_information',
                                        'yds_roll',
                                        'description',
                                        'season',
                                        'invoice',
                                        'fabric_color',
                                        'interlining_color',
                                        'qty',
                                        'machine',
                                        'temperature',
                                        'pressure',
                                        'duration',
                                        'style_name',
                                        'fabric_item',
                                        'fabric_type',
                                        'thread',
                                        'material_shell_panel',
                                        'product_category',
                                        'remark',
                                        'pad',
                                        'pelling',
                                        'component',
                                        'garment_size',
                                        'sample_type',
                                        'test_condition',
                                        'fabric_properties',
                                        'lot',
                                        'po_buyer',
                                        'arrival_date'
                                    )
                                ->get()->toArray();

        $meth = TrfTestingMethods::where('trf_id',$trfid)->wherenull('deleted_at')->select('master_method_id')->get()->toArray();
        

        return view('dashboard_trf.editrf')
                                        ->with('buyer',$buyer)
                                        ->with('trf',$dataTrf)
                                        ->with('labloc',$labloc)
                                        ->with('docl',$doc)
                                        ->with('meth',$meth)
                                        ->with('returnpage',$req->return);
    }


    public function getCtgMethReq(Request $req){
        // dd($req->category_specimen);
        $type = $req->type;

        $data = DB::table('ns_req_catg_method');

        switch ($type) {
            case 'category':
                $data = $data->where('buyer',$req->buyer)->groupBy('category')->select('category')->orderby('category','asc');
                break;

            case 'category_specimen':
                $data = $data->where('buyer',$req->buyer)->where('category',$req->category)->groupBy('category_specimen')->select('category_specimen')->orderby('category_specimen','asc');
                break;

            case 'type_specimen':
                $data = $data->where('buyer',$req->buyer)->where('category',$req->category)->where('category_specimen',$req->category_specimen)->groupBy('type_specimen')->select('type_specimen')->orderby('type_specimen','asc');
                break;

            case 'method':
                $data = $data->where([
                                ['buyer', $req->buyer],
                                ['category', $req->category],
                                ['category_specimen', $req->category_specimen],
                                ['type_specimen', $req->type_specimen],
                            ])
                            ->groupBy(['master_method_id', 'method_code', 'method_name'])
                            ->select('master_method_id', 'method_code', 'method_name')
                            ->orderBy('method_code', 'asc');
                break;  
        }

        return ['data'=>$data->get()];
    }


    public function mapsModalDoc(Request $req){

        return HelperController::mappingSpc($req->ctgspc,$req->typespc);
    }

    public function updateTrf(Request $req)
    {
        $id = $req->id;

        if (!$id) {
            return response()->json(['status' => 422, 'output' => 'ID TRF tidak valid!']);
        }

        $category_id = MasterCategory::where('category', $req->category)
                                    ->where('category_specimen', $req->category_specimen)
                                    ->where('type_specimen', $req->type_specimen)
                                    ->whereNull('deleted_at')
                                    ->first();

       
        if (!$category_id) {
            return response()->json(['status' => 422, 'output' => 'Kategori tidak ditemukan!']);
        }

        $platform = $req->origin;
        $asal_speciment = $req->asal;
        $buyer = $req->buyer;
        $returntest = $req->return_test;
        $labloction = $req->lab_location;
        $testreq = $req->test_req;
        $no_trf = trim($req->no_trf);
        $date_information_remark = $req->radio_date_info;
        $methods = $req->method;

        try {
            $date_information = Carbon::parse($req->date_info)->format('Y-m-d');
        } catch (\Exception $e) {
            return response()->json(['status' => 422, 'output' => 'Format tanggal tidak valid!']);
        }

        $part_test = $req->part_to_test;
        $datas = $req->data;

        try {
            DB::beginTransaction();

            $trfTesting = TrfTesting::findOrFail($id); 
            $isCategoryChanged = $req->category_specimen !== $trfTesting->category_specimen ||
                                $req->category !== $trfTesting->category ||
                                $req->type_specimen !== $trfTesting->type_specimen;

            $updateData = [
                'buyer' => $buyer,
                'lab_location' => $labloction,
                'platform' => $platform,
                'asal_specimen' => $asal_speciment,
                'date_information' => $date_information,
                'date_information_remark' => $date_information_remark,
                'test_required' => $testreq,
                'part_of_specimen' => $part_test,
                'updated_at' => Carbon::now(),
                'return_test_sample' => $returntest,
                'id_category' => $category_id->id
            ];

            if ($isCategoryChanged) {
                $updateData['category_specimen'] = $req->category_specimen;
                $updateData['category'] = $req->category;
                $updateData['type_specimen'] = $req->type_specimen;
            }

            $trfTesting->update($updateData);

            if (!empty($methods)) {

                TrfTestingMethods::where('trf_id',$id)->whereNotIn('master_method_id',$methods)->update(['deleted_at'=>carbon::now()]);

                foreach ($methods as $mt) {
                    $cekTm = TrfTestingMethods::where('trf_id',$id)->where('master_method_id',$mt)->whereNull('deleted_at')->exists();

                    if (!$cekTm) {
                        TrfTestingMethods::firstorcreate(['trf_id'=>$id,'master_method_id'=>$mt,'created_at'=>carbon::now()]);
                    }
                }
            }

            foreach ($datas as $key => $value) {
                $docin = [
                    'document_type' => trim($value['document_type']),
                    'document_no' => trim($value['document_no']),
                    'style' => trim($value['style']),
                    'article_no' => trim($value['article_no']),
                    'size' => trim($value['size']),
                    'color' => trim($value['color']),
                    'fibre_composition' => trim($value['fibre_composition']),
                    'fabric_finish' => trim($value['fabric_finish']),
                    'gauge' => trim($value['gauge']),
                    'fabric_weight' => trim($value['fabric_weight']),
                    'plm_no' => trim($value['plm_no']),
                    'care_instruction' => trim($value['care_instruction']),
                    'manufacture_name' => trim($value['manufacture_name']),
                    'export_to' => trim($value['export_to']),
                    'nomor_roll' => trim($value['nomor_roll']),
                    'batch_number' => trim($value['batch_number']),
                    'item' => trim($value['item']),
                    'barcode_supplier' => trim($value['barcode_supplier']),
                    'additional_information' => trim($value['additional_information']),
                    'yds_roll' => trim($value['yds_roll']),
                    'description' => trim($value['description']),
                    'season' => trim($value['season']),
                    'invoice' => trim($value['invoice']),
                    'fabric_color' => trim($value['fabric_color']),
                    'interlining_color' => trim($value['interlining_color']),
                    'qty' => (int) trim($value['qty']),
                    'machine' => trim($value['machine']),
                    'temperature' => trim($value['temperature']),
                    'pressure' => trim($value['pressure']),
                    'duration' => trim($value['duration']),
                    'style_name' => trim($value['style_name']),
                    'fabric_item' => trim($value['fabric_item']),
                    'fabric_type' => trim($value['fabric_type']),
                    'thread' => trim($value['thread']),
                    'material_shell_panel' => trim($value['material_shell_panel']),
                    'product_category' => trim($value['product_category']),
                    'remark' => trim($value['remark']),
                    'pad' => trim($value['pad']),
                    'pelling' => trim($value['pelling']),
                    'component' => trim($value['component']),
                    'garment_size' => trim($value['garment_size']),
                    'sample_type' => trim($value['sample_type']),
                    'test_condition' => trim($value['test_condition']),
                    'fabric_properties' => trim($value['fabric_properties']),
                    'lot' => trim($value['lot']),
                    'po_buyer' => trim($value['po_buyer']),
                    'arrival_date' => trim($value['arrival_date']) != null ? trim($value['arrival_date']) : null
                ];

                TrfTestingDocument::where('id', $value['id'])->update($docin);
            }

            DB::commit();
            $data_response = ['status' => 200, "output" => "Update TRF Success!"];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status' => 422, "output" => "Update TRF Failed! " . $message];
        }
    
        return response()->json(['data' => $data_response]);
    }

}
