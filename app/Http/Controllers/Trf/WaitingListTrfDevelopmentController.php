<?php

namespace App\Http\Controllers\Trf;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Controllers\Controller;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Models\MasterTrfTestingModel;
use App\Models\TestingResult;
use App\Models\MasterRequirement;
use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;

use App\Http\Controllers\HelperController;

class WaitingListTrfDevelopmentController extends Controller
{
    public function index()
    {
        return view('waiting_list_trf_development.index');
    }

    public function data(Request $request)
    {
        
            // dd(auth::user()->factory_id);
            if(auth::user()->factory_id == '1')
            {
                $factory = 'AOI1';
            } elseif(auth::user()->factory_id == '2'){
                $factory = 'AOI2';
            } else{
                $factory = null;
            }

            $fabric_testing = TrfTesting::whereNull('deleted_at')
            ->whereIn('status',['OPEN','REJECT'])
            ->where('lab_location', $factory)
            ->where('asal_specimen', 'DEVELOPMENT')
            ->orderBy('created_at','desc');
            // dd($fabric_testing);

            
            return DataTables::of($fabric_testing)
            ->editColumn('nik',function ($fabric_testing)
            {

                $absensi = DB::connection('absence_aoi');
                $users = $absensi->table('adt_bbigroup_emp_new')->where('nik', $fabric_testing->nik)->first();
                $user = null;
                if($users != null)
                {
                    $user =  $users->name;
                }else{
                    $user =  $fabric_testing->nik;
                }
                return $user;
                
            })
            ->editColumn('lab_location',function ($fabric_testing)
            {
                if($fabric_testing->lab_location == 'AOI1') return 'AOI 1';
                elseif($fabric_testing->lab_location == 'AOI2') return 'AOI 2';
                else return '-';
            })
            ->editColumn('date_information_remark',function ($fabric_testing)
            {
                if($fabric_testing->date_information_remark == 'buy_ready') return 'Buy Ready - '. $fabric_testing->date_information ;
                elseif($fabric_testing->date_information_remark == 'output_sewing') return '1st Output Sewing - '. $fabric_testing->date_information ;
                else return 'PODD - '. $fabric_testing->date_information ;
            })
            ->editColumn('test_required',function ($fabric_testing)
            {
                if($fabric_testing->test_required == 'developtesting_t1') return 'Develop Testing T1';
                elseif($fabric_testing->test_required == 'developtesting_t2') return 'Develop Testing T2';
                elseif($fabric_testing->test_required == 'developtesting_selective') return 'Develop Testing Selective';
                elseif($fabric_testing->test_required == 'bulktesting_m') return '1st Bulk Testing M (Model Level)';
                elseif($fabric_testing->test_required == 'bulktesting_a') return '1st Bulk Testing A (Article Level)';
                elseif($fabric_testing->test_required == 'bulktesting_selective') return '1st Bulk Testing Selective';
                elseif($fabric_testing->test_required == 'reordertesting_m') return 'Re-Order Testing M (Model Level)';
                elseif($fabric_testing->test_required == 'reordertesting_a') return 'Re-Order Testing A (Article Level)';
                elseif($fabric_testing->test_required == 'reordertesting_selective') return 'Re-Order Testing Selective';
                else return 'Re-Test';
            })
            ->editColumn('status',function ($fabric_testing)
            {
              
                if($fabric_testing->status == 'OPEN'){
                    return '<span class="label bg-grey-400">open</span>';
                }elseif($fabric_testing->status == 'VERIFIED') {
                    $user_verified = DB::table('users')->where('nik', $fabric_testing->verified_lab_by)->first();
                    return '<span class="label bg-warning">verified by '.$user_verified->name.'</span>';
                }elseif($fabric_testing->status == 'REJECT' && $fabric_testing->ishandover == false){
                    $user_rejected =  DB::table('users')->where('nik', $fabric_testing->reject_by)->first();
                    return '<span class="label bg-danger">reject by '.$user_rejected->name.'</span>';
                }elseif($fabric_testing->status == 'REJECT' && $fabric_testing->ishandover == true){
                    $user_rejected =  DB::table('users')->where('nik', $fabric_testing->reject_by)->first();
                    return '<span class="label bg-danger">reject handover by '.$user_rejected->name.'</span>';
                } 
                elseif($fabric_testing->status == 'ONPROGRESS'){
                    return '<span class="label bg-blue">onprogress</span>';
                } 
                elseif($fabric_testing->status == 'CLOSED'){
                    return '<span class="label bg-success">closed</span>';
                } 
                else return 'eror';
            })
            ->editColumn('return_test_sample',function ($fabric_testing)
            {
                if($fabric_testing->return_test_sample == 't')
                {
                    return '<span class="label bg-success">YES</span>';
                }else {
                    return '<span class="label bg-danger">NO</span>';
              
                }
            
            })
          
            ->addColumn('orderno',function ($fabric_testing)
            {
                $trf_document = TrfTestingDocument::where('trf_id', $fabric_testing->id)->first();

                return HelperController::setDoc($trf_document);
                
            })
            ->addColumn('action', function($fabric_testing) {
                if($fabric_testing->status == 'OPEN')
                {
                    return view('waiting_list_trf_development._action', [
                        // 'approval'   => route('waitinglistTrfDevelopment.approval',$fabric_testing->id),
                        // 'detail'     => route('waitinglistTrfDevelopment.detail', $fabric_testing->id),
                        'chooseTesting'=> $fabric_testing->id ,
                        'reject'       => route('waitinglistTrfDevelopment.reject', $fabric_testing->id),
                        'handover'     => route('waitinglistTrfDevelopment.handover', $fabric_testing->id),
                    ]);
                }
                //  else{
                //     return view('waiting_list_trf_development._action', [
                //         'detail'     => route('waitinglistTrfDevelopment.detail', $fabric_testing->id),
                //     ]);
                // }
                
            })
            ->rawColumns(['action','orderno','status','return_test_sample'])
            ->make(true);
        
    }

    
    public function approval($id)
    {
        try 
        {
            DB::beginTransaction();
            
        
                    $masterTrf = TrfTesting::where('id', $id)
                            ->update([
                                'verified_lab_date' => carbon::now(),
                                'status'       => 'VERIFIED',
                                'verified_lab_by'   => auth::user()->id,
                                'updated_at'        => carbon::now(),
                            ]);
            
            DB::commit();

            return redirect()->route('waitinglistTrfDevelopment.index');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }


    public function reject($id)
    {

            $obj                = new stdClass();
            $obj->id            = $id;

      

        return response()->json($obj,200);
       
    }

    public function sendReject(Request $request)
    {
        $id     = $request->id;
        $remark = $request->remark;

        try 
        {
            DB::beginTransaction();
        
                    $masterTrf = TrfTesting::where('id', $id)
                    ->update([
                        'updated_at'        => carbon::now(),
                        'status'       => 'REJECT',
                        'reject_by'         => auth::user()->id,
                        'reject_date'         => carbon::now(),
                        'remarks'            => $remark
                    ]);

                    $data_response = [
                        'status' => 200,
                        'output' => 'Successfull Reject!!'
                    ];
                   
            
            DB::commit();

            return redirect()->route('waitinglistTrfDevelopment.index');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 422,
                'output' => 'update error '.$message
            ];
        }
    }

    public function handover($id)
    {
        try 
        {
            DB::beginTransaction();
            
                    // dd($id);
                    $masterTrf = TrfTesting::where('id', $id)
                    ->update([
                        'is_handover'        => true,
                        'updated_at'        => carbon::now(),
                        'status_handover'   => 'waiting approval'
                    ]);
        

            
            DB::commit();

            return redirect()->route('waitinglistTrfDevelopment.index');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function saveDataDevreq(Request $request)
    {
        $data =  $request->data;
        $id   = $request->id;
        // dd($data);
        foreach ($data as $key => $dt) {
            try 
            {
                
                // dd($dt['req_id']);
                DB::beginTransaction();

                $method = DB::table('get_master_requirements')->where('id', $dt['req_id'])->first();

                $masterTrf = TrfTesting::where('id', $id)
                ->update([
                    'verified_lab_date' => carbon::now(),
                    'status'       => 'VERIFIED',
                    'verified_lab_by'   => auth::user()->nik,
                    'updated_at'        => carbon::now(),
                    'type_specimen'     => $method->type_specimen
                ]);

                TrfTestingMethods::firstorcreate([
                    'trf_id' => $id,
                    'master_method_id' => $method->master_method_id,
                    'created_at'    => carbon::now(),
                    'updated_at'    => carbon::now(),
                    'created_by'    => auth::user()->nik
                ]);

                TestingResult::FirstOrCreate([
                    'master_requirement_id' => $dt['req_id'],
                    'master_trf_testing_id' => $id,
                    'created_at'            => carbon::now(),
                    'updated_at'            => carbon::now(),
                    'nik'                   => Auth::user()->nik,
                    // 'value_testing'         => $input_testing,
                    // 'testing_result'        => $result
    
                ]);
                

                $data_response = [
                    'status' => 200,
                    'output' => 'Insert Success',
                    'ids'=>$id
                    ];

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);

                $data_response = [
                    'status' => 422,
                    'output' => 'insert error '.$message
                ];
            }
           

        }

        return response()->json(['data'=>$data_response]);
    }

    public function getDataDevreq(Request $request)
    {
        $data   =   DB::table('trf_testings')->where('id', $request->id)->first();
        

        $getChooseRequirement   =  DB::table('get_requirement_testings')
                                    ->join('trf_testing_methods','trf_testing_methods.master_method_id', '=','get_requirement_testings.method_id')
                                    ->where('get_requirement_testings.category', $data->category)->where('get_requirement_testings.category_specimen', $data->category_specimen)
                                    ->where('get_requirement_testings.buyer', $data->buyer)
                                    ->get();
        // dd($getChooseRequirement);
        
        return DataTables::of($getChooseRequirement)
        ->addColumn('selector', function($getChooseRequirement) {
            return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$getChooseRequirement->id.'">';
        })
        ->rawColumns(['selector'])
        ->make(true);
    }

    public function chooseTesting(Request $request)
    {
        $testing_id = $request->id;
        $cek_testing = TestingResult::where('master_trf_testing_id', $testing_id)->count();
        $checkAsal = TrfTesting::where('id', $testing_id)->first();
        // dd($checkAsal);
        
        if($cek_testing > 0)
        {
            // $obj                        = new stdClass();
            // $obj->id                    = $testing_id;
            // $obj->asal                  = $checkAsal->asal_specimen;
            // $obj->check                 = true;

            $obj = array(
                        'id'=>$testing_id,
                        'asal'=>$checkAsal->asal_specimen,
                        'check'=>true
                    );

        }else
        {
            // $obj                        = new stdClass();
            // $obj->id                    = $testing_id;
            // $obj->asal                  = $checkAsal->asal_specimen;
            // $obj->check                 = false;

            $obj = array(
                'id'=>$testing_id,
                'asal'=>$checkAsal->asal_specimen,
                'check'=>false
            );
        }


        return response()->json($obj,200);
    }

}
