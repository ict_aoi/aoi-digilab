<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;


use App\User;
use App\Models\MasterMO;
use App\Models\GarmentId;
use App\Models\Locator;
use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterMethode;
use App\Models\MasterCategory;
use App\Models\Employee;
use App\Models\EscalationModel;

use App\Http\Controllers\HelperController;

class DashboardController extends Controller
{
    public function index(){
        // $factory = DB::table('factory')->wherenull('deleted_at')->orderby('factory_name')->get();
        // return view('dashboard.dashboard')->with('factory',$factory);
        $labloc = DB::table('master_lab_location')->whereNull('deleted_at')->orderBy('id','asc')->get();
        return view('dashboard.dashboard')->with('labloc',$labloc);
    }
 
    public function getData(Request $req){
        // switch ($req->factory) {
        //     case '1':
        //         $loclab = "AOI1";
        //     break;

        //     case '2':
        //         $loclab = "AOI2";
        //     break;

        //     case '3':
        //         $loclab = "BBIS";
        //     break;
        // }

        $key = trim($req->key);
        $data= DB::table('ns_dashboard_trf')->where('lab_location',$req->lablocation);

        if (!empty($key)) {

            $data = $data->where(function($query) use ($key){
                                $query->where('trf_id','LIKE','%'.$key)
                                        ->orwhere('asal_specimen','LIKE','%'.$key)
                                        ->orwhere('buyer','LIKE','%'.$key)
                                        ->orwhere('status','LIKE','%'.$key)
                                        ->orwhere('category','LIKE','%'.$key)
                                        ->orwhere('category_specimen','LIKE','%'.$key)
                                        ->orwhere('type_specimen','LIKE','%'.$key)
                                        ->orwhere('nik','LIKE','%'.$key)
                                        ->orwhere('username','LIKE','%'.$key)
                                        ->orwhere('method_code','LIKE','%'.$key.'%')
                                        ->orwhere('styleart','LIKE','%'.$key.'%');
                            });

        }else{
 
            $now = carbon::now()->format('Y-m-d');
            $data = $data->where(function($query) use ($now){
                                $query->whereIn('status',['OPEN','VERIFIED','ONPROGRESS'])
                                        ->orwhere('created_at',$now)
                                        ->orwhere('updated_at',$now);
                            });

        }
        // dd($req->factory,$loclab);
        return DataTables::of($data)
                            ->editColumn('status',function($data){
                                return HelperController::statusCek($data->id);
                            })
                            ->editColumn('test_required',function($data){
                                return HelperController::testRequired($data->test_required,$data->previous_trf_id);
                            })
                            ->editColumn('date_information_remark',function($data){
                                return HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
                            })
                            ->editColumn('category',function($data){
                                return '<b>Category </b> : '.$data->category.'<br> <b>Category Specimen </b> : '.$data->category_specimen.'<br> <b>Type Specimen </b> : '.$data->type_specimen;
                            })
                            ->editColumn('return_test_sample',function($data){
                                switch ($data->return_test_sample) {
                                    case true:
                                        return '<span class="label label-success">YES</span>';
                                        break;

                                    case false:
                                        return '<span class="label label-warning">NO</span>';
                                        break;

                                    default:
                                        return '<span class="label label-primary">ERROR</span>';
                                        break;
                                }
                            })
                            // ->addColumn('pic_test',function($data){
                            //     if (isset($data->technician)) {
                            //         $usr = User::where('id',$data->technician)->first();
                            //         return $usr->name;
                            //     }else{
                            //         return '';
                            //     }
                            // })
                            ->addColumn('action',function($data){
                                return view('_action',[
                                            'model'=>$data,
                                            'dashbarcode'=>route('home.printBarcode',['id'=>$data->id]),
                                            'dashdetail'=>route('home.printDetail',['id'=>$data->id])
                                            // 'dashreport'=>route('home.report',['id'=>$data->id,'notrf'=>$data->trf_id])
                                        ]);
                            })
                            ->addColumn('due_date',function($data){
                                if ($data->verified_lab_date!=null) {
                                    $due = HelperController::getDue($data->verified_lab_date);
                                    return '<b>Verified Date </b> : '.carbon::parse($data->verified_lab_date)->format('l, d M Y').'<br><b>Due Date </b> : '.carbon::parse($due)->format('l, d M Y');
                                }else{
                                    return '';
                                }
                            })
                            ->rawColumns(['status','testRequired','date_information_remark','category','return_test_sample','action','method_code','due_date'])
                            ->make(true);
    }

    public function printBarcode(Request $req){
        $id = trim($req->id);

        // $data = TrfTesting::where('id',$id)->first();

        // return view('create_trf.barcode_trf',['list'=>$data]);

        $list = TrfTesting::whereIn('id',[$id])->whereNull('deleted_at')->get();

        return view('create_trf.barcode_trf',['list'=>$list]);

    }

    public function printDetail(Request $req){
        $id = $req->id;

        $list = DB::table('trf_testings')
                            ->join('trf_testing_document','trf_testings.id','trf_testing_document.trf_id')
                            ->join('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                            ->join('factory','trf_testings.factory_id','factory.id')
                            ->join('master_category','trf_testings.id_category','master_category.id')
                            ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->whereIn('trf_testings.id',[$id])
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_document.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->groupBy([
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.verified_lab_date',
                                'trf_testings.verified_lab_by',
                                'trf_testings.previous_trf_id',
                                'trf_testings.id',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'trf_testing_document.document_type',
                                'trf_testing_document.document_no',
                                'trf_testing_document.style',
                                'trf_testing_document.article_no',
                                'trf_testing_document.size',
                                'trf_testing_document.color',
                                'trf_testing_document.fibre_composition',
                                'trf_testing_document.fabric_finish',
                                'trf_testing_document.gauge',
                                'trf_testing_document.fabric_weight',
                                'trf_testing_document.plm_no',
                                'trf_testing_document.care_instruction',
                                'trf_testing_document.manufacture_name',
                                'trf_testing_document.export_to',
                                'trf_testing_document.nomor_roll',
                                'trf_testing_document.batch_number',
                                'trf_testing_document.item',
                                'trf_testing_document.barcode_supplier',
                                'trf_testing_document.additional_information',
                                'trf_testing_document.yds_roll',
                                'trf_testing_document.description',
                                'trf_testing_document.season',
                                'trf_testing_document.invoice',
                                'trf_testing_document.fabric_color',
                                'trf_testing_document.interlining_color',
                                'trf_testing_document.qty',
                                'trf_testing_document.machine',
                                'trf_testing_document.temperature',
                                'trf_testing_document.pressure',
                                'trf_testing_document.duration',
                                'trf_testing_document.style_name',
                                'trf_testing_document.fabric_item',
                                'trf_testing_document.fabric_type',
                                'trf_testing_document.thread',
                                'trf_testing_document.material_shell_panel',
                                'trf_testing_document.product_category',
                                'trf_testing_document.remark',
                                'trf_testing_document.pad',
                                'trf_testing_document.pelling',
                                'trf_testing_document.component',
                                'trf_testing_document.garment_size',
                                'trf_testing_document.sample_type',
                                'trf_testing_document.test_condition',
                                'trf_testing_document.fabric_properties',
                                'trf_testing_document.lot',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                'factory.factory_name',
                                'factory.address'
                            ])
                            ->select(
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.verified_lab_date',
                                'trf_testings.verified_lab_by',
                                'trf_testings.previous_trf_id',
                                'trf_testings.id',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'trf_testing_document.document_type',
                                'trf_testing_document.document_no',
                                'trf_testing_document.style',
                                'trf_testing_document.article_no',
                                'trf_testing_document.size',
                                'trf_testing_document.color',
                                'trf_testing_document.fibre_composition',
                                'trf_testing_document.fabric_finish',
                                'trf_testing_document.gauge',
                                'trf_testing_document.fabric_weight',
                                'trf_testing_document.plm_no',
                                'trf_testing_document.care_instruction',
                                'trf_testing_document.manufacture_name',
                                'trf_testing_document.export_to',
                                'trf_testing_document.nomor_roll',
                                'trf_testing_document.batch_number',
                                'trf_testing_document.item',
                                'trf_testing_document.barcode_supplier',
                                'trf_testing_document.additional_information',
                                'trf_testing_document.yds_roll',
                                'trf_testing_document.description',
                                'trf_testing_document.season',
                                'trf_testing_document.invoice',
                                'trf_testing_document.fabric_color',
                                'trf_testing_document.interlining_color',
                                'trf_testing_document.qty',
                                'trf_testing_document.machine',
                                'trf_testing_document.temperature',
                                'trf_testing_document.pressure',
                                'trf_testing_document.duration',
                                'trf_testing_document.style_name',
                                'trf_testing_document.fabric_item',
                                'trf_testing_document.fabric_type',
                                'trf_testing_document.thread',
                                'trf_testing_document.material_shell_panel',
                                'trf_testing_document.product_category',
                                'trf_testing_document.remark',
                                'trf_testing_document.pad',
                                'trf_testing_document.pelling',
                                'trf_testing_document.component',
                                'trf_testing_document.garment_size',
                                'trf_testing_document.sample_type',
                                'trf_testing_document.test_condition',
                                'trf_testing_document.fabric_properties',
                                'trf_testing_document.lot',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                'factory.factory_name',
                                'factory.address',
                                DB::raw("string_agg(concat(master_method.method_code,'-',master_method.method_name),'|') as methods")
                            )->get();
        // return view('create_trf.detail_trf',compact('list'));
        $filename = 'TRF-DETAIL';
        $pdf =  \PDF::loadView('create_trf.detail_trf',['list'=>$list])->setPaper('A4','Potrait');
        return $pdf->stream($filename);
    }

    // public function printDetail(Request $req){
    //     $id= trim($req->id);

    //     $data = DB::table('trf_testings')
    //                         ->join('master_category','trf_testings.id_category','=','master_category.id')
    //                         ->join('factory','trf_testings.factory_id','=','factory.id')
    //                         ->where('trf_testings.id',$id)
    //                         ->whereNull('trf_testings.deleted_at')
    //                         ->select(
    //                                 'trf_testings.id',
    //                                 'trf_testings.trf_id',
    //                                 'trf_testings.lab_location',
    //                                 'trf_testings.buyer',
    //                                 'trf_testings.nik',
    //                                 'trf_testings.platform',
    //                                 'trf_testings.asal_specimen',
    //                                 'trf_testings.date_information',
    //                                 'trf_testings.date_information_remark',
    //                                 'trf_testings.test_required',
    //                                 'trf_testings.part_of_specimen',
    //                                 'trf_testings.created_at',
    //                                 'trf_testings.return_test_sample',
    //                                 'trf_testings.status',
    //                                 'trf_testings.verified_lab_date',
    //                                 'trf_testings.verified_lab_by',
    //                                 'trf_testings.reject_by',
    //                                 'trf_testings.reject_date',
    //                                 'trf_testings.additional_information',
    //                                 'trf_testings.previous_trf_id',
    //                                 'master_category.category',
    //                                 'master_category.category_specimen',
    //                                 'master_category.type_specimen',
    //                                 'factory.factory_name'
    //                             )->first();



    //     $api = 'http://'.$req->getHttpHost().'/api/print/report-specimen?id='.$data->id.'&notrf='.$data->trf_id;
    //     $qrcode =  HelperController::generateQr($api);

    //     $testreq = HelperController::testRequired($data->test_required,$data->previous_trf_id);
    //     $dateinfo = HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
    //     $meth = DB::table('trf_testing_methods')
    //                     ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
    //                     ->whereNull('trf_testing_methods.deleted_at')
    //                     ->where('trf_testing_methods.trf_id',$data->id)
    //                     ->select('master_method.id','master_method.method_code','master_method.method_name','master_method.category','master_method.type')->get()->toArray();

    //     $docm = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->get();

    //     $users = Employee::where('nik', $data->nik)->first();




    //     $vefpiclab = isset($data->verified_lab_by) ?  User::where('nik', $data->verified_lab_by)->first()['name'] : "";
    //     $vefdatelab = isset($data->verified_lab_date) ? carbon::parse($data->verified_lab_date)->format('d-m-Y H:i:s') : "";
    //     $filename = "TRF_DETAIL_".$data->trf_id;

    //     $pdf = \PDF::loadView('create_trf.detail_trf',['data'=>$data,'method'=>$meth,'docm'=>$docm,'submit_by'=>isset($users) ? $users->name : $data->nik,'test_required'=>$testreq,'dateinfo'=>$dateinfo,'labdate'=>$vefdatelab,'labpic'=>$vefpiclab,'qrcode'=>base64_encode($qrcode)])->setPaper('A4','Potrait');
    //     return $pdf->stream($filename);
    // }

    public function report(Request $req){
        $id = trim($req->id);
        $notrf = trim($req->notrf);

        $trfdoc = [];
    
        $filename="TRF_REPORT_".$notrf;
               foreach (HelperController::getTrfDoc($id,null) as $tdc) {
                    $api = 'http://'.$req->getHttpHost().'/api/print/report-specimen?id='.$tdc->id."&docid=".$tdc->doc_id;
                    $qrcode = HelperController::generateQr($api);

                    $fac = DB::table('factory')->where('id',$tdc->factory_id)->first();
                    $dx['trfid']= $tdc->id;
                    $dx['notrf']= $tdc->trf_id;
                    $dx['buyer']= $tdc->buyer;
                    $dx['lab_location']= $tdc->lab_location;
                    $dx['nik']= $tdc->nik;
                    $dx['platform']= $tdc->platform;
                    $dx['factory_id']= $tdc->factory_id;
                    $dx['fact']= $fac->factory_name;
                    $dx['asal_specimen']= $tdc->asal_specimen;
                    $dx['date_information']= $tdc->date_information;
                    $dx['date_information_remark']= $tdc->date_information_remark;
                    $dx['set_date_info']= HelperController::dateInfoSet($tdc->date_information,$tdc->date_information_remark);
                    $dx['test_required']= $tdc->test_required;
                    $dx['set_testreq']= HelperController::testRequired($tdc->test_required,$tdc->previous_trf_id);
                    $dx['part_of_specimen']= $tdc->part_of_specimen;
                    $dx['created_at']= $tdc->created_at;
                    $dx['updated_at']= $tdc->updated_at;
                    $dx['return_test_sample']= $tdc->return_test_sample;
                    $dx['status']= $tdc->status;
                    $dx['previous_trf_id']= $tdc->previous_trf_id;
                    $dx['id_category']= $tdc->id_category;
                    $dx['doc_id']= $tdc->doc_id;
                    $dx['document_type']= $tdc->document_type;
                    $dx['document_no']= $tdc->document_no;
                    $dx['season']= $tdc->season;
                    $dx['style']= $tdc->style;
                    $dx['article_no']= $tdc->article_no;
                    $dx['size']= $tdc->size;
                    $dx['color']= $tdc->color;
                    $dx['fibre_composition']= $tdc->fibre_composition;
                    $dx['fabric_finish']= $tdc->fabric_finish;
                    $dx['gauge']= $tdc->gauge;
                    $dx['fabric_weight']= $tdc->fabric_weight;
                    $dx['plm_no']= $tdc->plm_no;
                    $dx['care_instruction']= $tdc->care_instruction;
                    $dx['manufacture_name']= $tdc->manufacture_name;
                    $dx['export_to']= $tdc->export_to;
                    $dx['nomor_roll']= $tdc->nomor_roll;
                    $dx['batch_number']= $tdc->batch_number;
                    $dx['item']= $tdc->item;
                    $dx['barcode_supplier']= $tdc->barcode_supplier;
                    $dx['additional_information']= $tdc->additional_information;
                    $dx['yds_roll']= $tdc->yds_roll;
                    $dx['description']= $tdc->description;
                    $dx['category']= $tdc->category;
                    $dx['category_specimen']= $tdc->category_specimen;
                    $dx['type_specimen']= $tdc->type_specimen;
                    $dx['qrcode']= base64_encode($qrcode);
                    $dx['result_test']=$this->SetRest($tdc->doc_id);
                    $dx['technician']= isset($tdc->technician) ?  HelperController::GetUser($tdc->technician) : null;
                    $dx['labhead']= isset($tdc->labhead) ?  HelperController::GetUser($tdc->labhead) : null;
                    $dx['qchead']= isset($tdc->qchead) ?  HelperController::GetUser($tdc->qchead) : null;
                    $trfdoc[] = $dx;
               }


       $pdf = \PDF::loadView('dashboard.report',['data'=>$trfdoc])->setPaper('A4','Potrait');
       return $pdf->stream($filename);
    }

    private function SetRest($doc_id){
        $data = HelperController::getTrfRest($doc_id);
        $i =0;
        $lst =[];
        foreach ($data as $dt) {
            $tr['no']=++$i;
            $tr['imgdir']='imagespc/'.$dt->id.'.png';
            $tr['requirment']=HelperController::operatorVal($dt->requirement_id);
            $tr['before_test']=$dt->before_test;
            $tr['supplier_result']=$dt->supplier_result;
            $tr['result']=$dt->result;
            $tr['method_code']=$dt->method_code;
            $tr['method_name']=$dt->method_name;
            $tr['set_result']=HelperController::ReportResult($dt->requirement_id,$dt->result);
            $lst[]=$tr;
        }

        return $lst;
    }

}
