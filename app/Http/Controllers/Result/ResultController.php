<?php

namespace App\Http\Controllers\Result;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\File;
use Uuid;

use App\Models\TrfTesting;
use App\Models\TrfTestingMethods;
use App\Models\TrfTestingDocument;
use App\Models\TestingResult;
use App\Models\supplierResult;
use App\Models\MasterDimenApp;
use App\Models\MasterRequirement;
use App\Models\SetReqModel;
// use App\Models\EscalationReq;

use App\Http\Controllers\HelperController;

class ResultController extends Controller
{
    public function index(){
        // $fact = DB::table('factory')->whereNull('deleted_at')->orderby('factory_name','asc')->get();
        // return view('result.index')->with('fact',$fact);

        $labloc = DB::table('master_lab_location')->wherenull('deleted_at')->orderBy('id','asc')->get();
        return view('result.index')->with('labloct',$labloc);
    }

    public function getDataIndex(Request $req ){
        
        $locLab = trim($req->labloct);

        // $data = DB::table('trf_testings')
        //                     ->join('master_category','trf_testings.id_category','=','master_category.id')
        //                     ->where('trf_testings.lab_location',$locLab)->whereIn('trf_testings.status',['VERIFIED','ONPROGRESS'])
        //                     ->select('trf_testings.id','trf_testings.trf_id','master_category.category','master_category.category_specimen','master_category.type_specimen')
        //                     ->orderBy('trf_testings.created_at','desc');

        $data = DB::table('ns_trf_result_list')->where('lab_location',$locLab)->orderBy('created_at','desc');
        // dd($data->get());
        return DataTables::of($data)
                            ->editColumn('trf_id',function($data){
                                return "<b>".$data->trf_id."</b><br>
                                        ".HelperController::statusCek($data->id);
                            })
                            ->addColumn('specimen',function($data){
                                $doc = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->get();
                                $ret = '';
                                foreach ($doc as $dc) {
                                    $x = HelperController::setDoc($dc);

                                    $ret = $ret.' '.$x.'<hr>';
                                }

                                return $ret;
                            })
                            ->editColumn('category',function($data){
                                return '<b>Category </b> : '.$data->category.'<br>
                                    <b>Category Specimen </b> : '.$data->category_specimen.'<br>
                                    <b>Type Specimen </b> : '.$data->type_specimen;
                            })
                            ->addColumn('action',function($data){
                                $cek = DB::table('ns_trf_set_req')->where('id',$data->id)->whereNotNull('set_req_id')->groupBy('id','trf_id')->select('id','trf_id')->exists();
                                
                                if ($cek==false && $data->status!='CLOSED') {
                                    return view('_action',[
                                                'setReq'=>route('result.setRequirement',['id'=>$data->id])
                                            ]);
                                }
                                elseif($cek==true && $data->status!='CLOSED')
                                {
                                    
                                    return view('_action',[
                                                'delReq'=>$data->id,
                                                'result'=>route('result.setResult',['id'=>$data->id])
                                            ]);
                                }elseif($data->status=='CLOSED'){
                                    return view('_action',[
                                                'result'=>route('result.setResult',['id'=>$data->id]),
                                                'reopen'=>$data->id
                                            ]);
                                }
                                
                            })  
                            ->rawColumns(['trf_id','specimen','methods','category'])
                            ->make(true);

    }

    public function setRequirement(Request $req){
        $id = trim($req->id);
        $trf = DB::table('trf_testings')
                    ->join('master_category','trf_testings.id_category','=','master_category.id')
                    ->where('trf_testings.id',$id)
                    ->whereNull('trf_testings.deleted_at')
                    ->select(
                        'trf_testings.id',
                        'trf_testings.trf_id',
                        'trf_testings.buyer',
                        'trf_testings.asal_specimen',
                        'master_category.category',
                        'master_category.category_specimen',
                        'master_category.type_specimen'
                    )->first();
        $data = DB::table('ns_trf_req_rest')->where('id',$id)->get();
        return view('result.new_requirement')->with('trf',$trf)->with('data',$data);
    }


    public function getTestTreat(Request $req){
        $data = MasterRequirement::where('master_method_id',$req->idmth)->where('master_category_id',$req->idctg)->where('komposisi_specimen',$req->comps)->whereNull('deleted_at')->groupBy('perlakuan_test')->select('perlakuan_test')->get();

        return response()->json(['data'=>$data],200);
    }



    public function getParmater(Request $req){

        $dmApp = MasterDimenApp::where('method_id',$req->idmth)->where('category_specimen',$req->ctgspc)->whereNull('deleted_at')->select('parameter')->get()->toArray();

        $data = MasterRequirement::where('master_method_id',$req->idmth)->where('master_category_id',$req->idctg)->where('komposisi_specimen',$req->vComp)->whereIn('perlakuan_test',$req->treat)->groupBy('parameter')->select('parameter')->get();


        return response()->json(['data'=>$data,'dimApp'=>array_column($dmApp,'parameter')],200);
    }

    public function createRequirement(Request $req){
        $data = $req->data;
        $id   = $req->trfid;

  
    
        try {
            DB::beginTransaction();

                foreach ($data as $dt) {
                  
                    $gReqId = DB::table('ns_req_catg_method')->where([
                                                    'master_method_id'=>$dt['idmethod'],
                                                    'master_category_id'=>$dt['idctg'],
                                                    'komposisi_specimen'=>$dt['composisi']
                                                ])
                                                ->whereIn('perlakuan_test',$dt['treatment'])
                                                ->whereIn('parameter',$dt['parameter'])
                                                ->get();

                    foreach ($gReqId as $gr) {
                        $cekSpc = MasterDimenApp::where('method_id',$dt['idmethod'])->where('category_specimen',$gr->category_specimen)->where('parameter',$gr->parameter)->whereNull('deleted_at');

                        if ($cekSpc->count()>0) {
                            foreach ($cekSpc->get() as $cspc) {
                                $in = array(
                                    'trf_meth_id'=>$dt['trfidmeth'],
                                    'requirement_id'=>$gr->id,
                                    'created_by'=>auth::user()->id,
                                    'created_at'=>carbon::now(),
                                    'special_id'=>$cspc->id
                                );
                            
                                SetReqModel::firstorcreate($in);
                            }
                        }else{
                            $in = array(
                                'trf_meth_id'=>$dt['trfidmeth'],
                                'requirement_id'=>$gr->id,
                                'created_by'=>auth::user()->id,
                                'created_at'=>carbon::now()
                            );
                        
                            SetReqModel::firstorcreate($in);
                        }
                        
                    }
                }

          
                TrfTesting::where('id',$id)->update(['status'=>"ONPROGRESS",'updated_at'=>carbon::now()]);
                $data_response = ['status'=>200,"output"=>"Set Requirement Success ! ! !"];
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>"Set Requirement Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function delRequirement(Request $req){
        $id = trim($req->id);

        $data = DB::table('ns_trf_set_req')->where('id',$id)->groupBy('id')->select(DB::raw("string_agg(trf_meth_id,',') as trfmethid"),DB::raw("string_agg(set_req_id,',') as set_req_id"))->first();

        $cek = TestingResult::whereIn('set_req_id',explode(",",$data->set_req_id))->whereNull('deleted_at')->exists();

        if ($cek) {
            return response()->json("Requirement use for Result Test, cancel result test first ! ! !",422);
        }
        
        try {
            DB::beginTransaction();
                SetReqModel::whereIn('trf_meth_id',explode(',',$data->trfmethid))->update(['deleted_at'=>carbon::now(),'active'=>false]);
            DB::commit();
             $data_response = ['status'=>200,"output"=>"Delete Requirement Success ! ! !"];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>"Delete Requirement Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);

    }

    public function inactiveRequirement(Request $req){
        $setreqId = trim($req->setId); 
        $trfid = trim($req->trfid);
        $type   = $req->typePow;

       

        try {
            DB::beginTransaction();
                if ($type=="on") {
                    $active = true;
                    $alert = "Active ";
                }elseif ($type=="off"){
                    $active = false;
                    $alert = "Inactive ";
                }

                SetReqModel::where('id',$setreqId)->update(['active'=>$active]);
                HelperController::cekAutoClosed($trfid);
            DB::commit();
             $data_response = ['status'=>200,"output"=>$alert." Requirement Success ! ! !"];
        } catch (Exception $e) {
             DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>$alert." Requirement Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }
    
    public function setResult(Request $req){
        $id = trim($req->id);
        $data = DB::table('trf_testings')
                    ->join('master_category','trf_testings.id_category','=','master_category.id')
                    ->where('trf_testings.id',$id)
                    ->whereNull('trf_testings.deleted_at')
                    ->select(
                        'trf_testings.id',
                        'trf_testings.trf_id',
                        'trf_testings.buyer',
                        'trf_testings.asal_specimen',
                        'master_category.category',
                        'master_category.category_specimen',
                        'master_category.type_specimen')
                    ->first();

        return view('result.new_result')->with('data',$data);

        

    }

    static function getMethSpc(){
        $data = DB::table('master_specialtest')->wherenull('deleted_at')->groupBy('method_id')->select(DB::raw("string_agg(method_id,',') as method_id"))->first();

        return isset($data->method_id) ? explode(",", $data->method_id) : array();
    }
    public function getResultReg(Request $req){
        $id = trim($req->id);

        $data = DB::table('ns_trf_listresult')->where('id',$id);
        // dd($data->get());
        
        return DataTables::of($data)
                               ->editColumn('method_code',function($data){
                                    return $data->method_code."<br>".$data->method_name;
                               })
                               ->addColumn('specimen',function($data){
                                    return HelperController::setDoc($data);
                               })
                               ->addColumn('status',function($data){
                                    $sts = HelperController::CalcSatusMethSpc($data->trfmehtid,$data->trfdocid);

                                    switch ($sts) {
                                        case 'PASS':
                                            return '<span class="label bg-success">PASS</span>';
                                            break;

                                        case 'FAILED':
                                            return '<span class="label bg-danger">FAILED</span>';
                                            break;
                                        
                                        default:
                                            return '<span class="label bg-grey-400">'.$sts.'</span>';
                                            break;
                                    }
                               })
                               ->addColumn('action',function($data){
                                    $acsts = HelperController::CalcSatusMethSpc($data->trfmehtid,$data->trfdocid);

                                    if ($acsts=='FAILED') {
                                        return view('_action',[
                                                    'trflist'=>route('result.formInput',[
                                                                    'trfid'=>$data->id,
                                                                    'trfmethid'=>$data->trfmehtid,
                                                                    'trfdocid'=>$data->trfdocid
                                                                ])
                                                ]);
                                    }else{
                                        return view('_action',[
                                                    'trflist'=>route('result.formInput',[
                                                                    'trfid'=>$data->id,
                                                                    'trfmethid'=>$data->trfmehtid,
                                                                    'trfdocid'=>$data->trfdocid
                                                                ])
                                                ]);
                                    }
                                    
                               })
                                ->rawColumns(['method_code','specimen','status','action'])->make(true);
    }

    
    public function formInput(Request $req){
        $trfid = trim($req->trfid);
        $trfmethid = trim($req->trfmethid);
        $trfdocid = trim($req->trfdocid);
        $doc = TrfTestingDocument::where('id',$trfdocid)->whereNull('deleted_at')->first();
        $inptrest = DB::table('ns_trfmeth_setreq')->where('id',$trfid)->where('trfmethid',$trfmethid)->orderBy('active','desc')->orderby('parameter','asc')->orderBy('code','asc')->orderby('measuring_position','asc')->get();

        $trf_docmeth = DB::select("SELECT * FROM get_trf_doc_meth('".$trfid."','".$trfmethid."','".$trfdocid."')")[0];
        // dd($trf_docmeth);
        return view('result.new_input')->with('doc',$doc)
                                        ->with('trfid',$trfid)
                                        ->with('trfmethid',$trfmethid)
                                        ->with('input',$inptrest)
                                        ->with('trfinfo',$trf_docmeth);
    }
 
    public function getDataTrfSet(Request $req){

    }

    public function saveResultReg(Request $req){
            $trfid              = trim($req->trfid);
            $trfdocid           = trim($req->trfdocid);
            $trfmehtid          = trim($req->trfmehtid);
            $setreqid           = trim($req->setreqid);
            $measuring_poss     = trim($req->measuring_poss);
            $supplier_result    = trim($req->supplier_result);
            $before_test        = trim($req->before_test);
            $standart_val       = trim($req->standart_val);
            $result_test        = $req->result_test;
            $images             = $req->images;
            $imginc             = 1;



            try {
                DB::beginTransaction();
                    $reqid = SetReqModel::where('id',$setreqid)->whereNull('deleted_at')->first();
                    foreach ($result_test as $rts) {
                        if (trim($rts['texresult'])==null) {
                            throw new Exception("Result Required ! ! !", 422);
                            
                        }

                        $calRest = HelperController::calculateResult($reqid->requirement_id,['before'=>$before_test,'standart'=>$standart_val,'result'=>$rts['texresult']],$measuring_poss);

                        $inRest = array(
                            'set_req_id'        =>$setreqid,
                            'trf_doc_id'        =>$trfdocid,
                            'supplier_result'   =>$supplier_result,
                            'before_test'       =>$before_test,
                            'standart_value'    =>$standart_val,
                            'testing_result'    =>$rts['texresult'],
                            'result'            =>$calRest,
                            'result_status'     =>HelperController::ReportResult($reqid->requirement_id,strval($calRest)),
                            'number_test'       =>$rts['increment'],
                            'comment'           =>$rts['texcomment'],
                            'remark'            =>$rts['texremark'],
                            'created_by'        =>auth::user()->id
                        );

                        TestingResult::FirstOrCreate($inRest);

                    }

                    if (!empty($images)) {
                        foreach ($images as $img) {
                            if ($img['image']!="" || $img['image']!='' || $img['image']!=null ) {
                                $imgid = Uuid::generate()->string;
                                $imgs = $img['image'];
                                $filename = "imagespc/".$imgid.".png";

                                $image_parts = explode(";base64,", $imgs);
                                file_put_contents($filename,base64_decode($image_parts[1]));

                                DB::table('result_image')->insert(['id'=>$imgid,'set_req_id'=>$setreqid,'remark'=>$img['imgremark'],'img_number'=>$imginc++,'created_at'=>carbon::now(),'trf_doc_id'=>$trfdocid]);
                            }
                        }
                    }


                    HelperController::cekAutoClosed($trfid);
                $data_response = ['status'=>200,"output"=>"Save Result Success ! ! ! "];
                DB::commit();
                
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
                $data_response = ['status'=>422,"output"=>"Save Result Failed ! ! ! ".$message];
            }
        return response()->json(['data'=>$data_response]);
    }

    public function getImage(Request $req){
        $setreqid = $req->setreqid;

        $data = DB::table('result_image')->where('set_req_id',$setreqid)->get()->toArray();

        return view('result.md_image')->with('data',array_chunk($data,3));
    }

    public function delResultReg(Request $req){
        $setreqid = trim($req->setreqid);
        $docid = trim($req->docid);

        try {
            DB::beginTransaction();
                TestingResult::where('set_req_id',$setreqid)->where('trf_doc_id',$docid)->update(['deleted_at'=>carbon::now(),'deleted_by'=>auth::user()->id]);

                DB::table('result_image')->where('set_req_id',$setreqid)->where('trf_doc_id',$docid)->update(['deleted_at'=>carbon::now()]);
                $data_response = ['status'=>200,"output"=>"Delete Result Success ! ! ! "];
            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>"Delete Result Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function inputRest(Request $req){
        $totin = $req->totin;
        $reqid = $req->reqid;
        $trfid = $req->trfid;

        $reqTest = DB::table('ns_req_catg_method')->where('id',$reqid)->select('method_name','method_code','category_specimen','type_specimen','category','parameter','perlakuan_test')->first();
        
        $form = [];
        for ($i=1; $i <=$totin ; $i++) { 
            if (($reqTest->method_code=="PHM-AP0405" ||$reqTest->method_code=="PHX-AP0701") && $reqTest->category_specimen=="FABRIC") {
                $setWash = $totin>1 ? $reqTest->parameter." ".$i : "";
            }else if($reqTest->method_code=="PHX-AP0451"){
                $setWash = $totin>1 ? "Inspector Odour ".$i : "";
            }else{
                $setWash = $totin>1 ? "Wash cycle ".$i : "";
            }
            
            // $inp ='<tr><td width="30px"><b>'.$i.'</b></td ><td width="300px"><input type="text" class="form-control txremark_'.$i.'" value="'.$setWash.'"></td><td>'.HelperController::setInputResult($reqid,$i).'</td><td><input type="text" class="form-control comment_'.$i.'"></td></tr>';
            $cf['i'] = $i;
            $cf['setwash'] = $setWash;
            $cf['finput'] = HelperController::setInputResult($reqid,$i); 

            $form[]=$cf;
        }

        $trfDoc = DB::table('trf_testing_document')->where('trf_id', $trfid)->first();
        $fibCom = ($trfDoc && !empty($trfDoc->fibre_composition)) ? $trfDoc->fibre_composition : '-';

        return response()->json(['form'=>$form,'spcreq'=>$reqTest,'trfDoc'=>$fibCom],200);

    }

    public function reOpen(Request $req){
        try {
            DB::beginTransaction();
                TrfTesting::where('id',$req->trfid)->update(['status'=>'ONPROGRESS','updated_at'=>carbon::now(),'closed_at'=>null]);
               
            DB::commit();
             $data_response = ['status'=>200,"output"=>"Open TRF Success ! ! ! "];
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = ['status'=>422,"output"=>"Open TRF Failed ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }


}
