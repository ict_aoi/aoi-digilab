<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use App\Models\TrfTestingDocument;
use App\Models\MasterRequirement;
use App\Models\TrfTesting;
use App\Models\EscalationReq;
use App\Models\TestingResult;
use App\Models\MasterHoliday;
use App\User;

class HelperController extends Controller
{
    

    static function setDoc($data){
        $text = "";
        switch ($data->document_type) {
            case 'MO':
                $text = '<table>
                        <tr><td width="75px"><b>No. MO </b></td><td>'.$data->document_no.'</td></tr>
                        <tr><td width="75px"><b>Style</b></td><td>'.$data->style.'</td></tr>
                        <tr><td width="75px"><b>Article</b></td><td>'.$data->article_no.'</td></tr>
                        <tr><td width="75px"><b>Size</b></td><td>'.$data->size.'</td></tr>
                        <tr><td width="75px"><b>Color</b></td><td>'.$data->color.'</td></tr>
                        <tr><td width="75px"><b>Item</b></td><td>'.$data->item.'</td></tr>
                    </table>';

                  
            break;

            case 'PO SUPPLIER':
                $text = '<table>
                        <tr><td width="75px"><b>PO Supplier</b></td><td>'.$data->document_no.'</td></tr>
                        <tr><td width="75px"><b>Item</b></td><td>'.$data->item.'</td></tr>
                        <tr><td width="75px"><b>Supplier</b></td><td>'.$data->manufacture_name.'</td></tr>
                        <tr><td width="75px"><b>No. Roll</b></td><td>'.$data->nomor_roll.'</td></tr>
                        <tr><td width="75px"><b>Bacth No.</b></td><td>'.$data->batch_number.'</td></tr>
                        <tr><td width="75px"><b>Barcode Supplier</b></td><td>'.$data->barcode_supplier.'</td></tr>
                    </table>';

            break;

            case 'ITEM':
                $text = '<table>
                            <tr><td width="75px"><b>Item</b></td><td>'.$data->document_no.'</td></tr>
                            <tr><td width="75px"><b>Style</b></td><td>'.$data->style.'</td></tr>
                            <tr><td width="75px"><b>Article</b></td><td>'.$data->article_no.'</td></tr>
                            <tr><td width="75px"><b>Size</b></td><td>'.$data->size.'</td></tr>
                            <tr><td width="75px"><b>Color</b></td><td>'.$data->size.'</td></tr>
                        </table>';

            break;

            case 'PO BUYER':
                $text = '<table>
                            <tr><td width="75px"><b>PO Buyer</b></td><td>'.$data->document_no.'</td></tr>
                            <tr><td width="75px"><b>Style</b></td><td>'.$data->style.'</td></tr>
                            <tr><td width="75px"><b>Article</b></td><td>'.$data->article_no.'</td></tr>
                            <tr><td width="75px"><b>Size</b></td><td>'.$data->size.'</td></tr>
                            <tr><td width="75px"><b>Color</b></td><td>'.$data->size.'</td></tr>
                            <tr><td width="75px"><b>Item</b></td><td>'.$data->item.'</td></tr>
                            <tr><td width="75px"><b>Destination</b></td><td>'.$data->export_to.'</td></tr>
                        </table>';

            break;
            
        
        }


        return $text;
    }


    static function operatorVal($req_id){
        $masreq = db::table('master_requirements')
                            ->join('master_operators','master_requirements.operator','=','master_operators.id')
                            ->where('master_requirements.id',$req_id)
                            // ->whereNull('master_requirements.deleted_at')
                            ->select(
                                'master_requirements.id',
                                'master_requirements.operator',
                                'master_requirements.value1',
                                'master_requirements.value2',
                                'master_requirements.value3',
                                'master_requirements.value4',
                                'master_requirements.value5',
                                'master_requirements.value6',
                                'master_requirements.value7',
                                'master_requirements.false',
                                'master_operators.operator_name',
                                'master_operators.symbol'
                            )->first();
                            
        switch ($masreq->operator) {
            case '1':
                return $masreq->value1;
                break;

            case '2':
                return $masreq->symbol." ".$masreq->value1;
                break;
            case '3':
                return $masreq->symbol." ".$masreq->value1;
                break;

            case '4':

                $arr = array($masreq->value1,$masreq->value2,$masreq->value3,$masreq->value4,$masreq->value5,$masreq->value6,$masreq->value7);
                return implode(",",array_filter($arr));
                break;

            case '5':
                return $masreq->value1." ".$masreq->symbol." ".$masreq->value2;
                break;
            
        }

        // return implode(",",array_filter($arr));
    }

    static function setInputResult($req_id,$inc){
        
        $req = MasterRequirement::where('id',$req_id)->first();


        switch ($req->operator) {
            case '1':
                
                $retn = '<select class="form-control select txresult_'.$inc.'" id="txresult" name="txresult" required>

                            <option value="" >--Choose Result--</option>
                            <option value="'.$req->value1.'" >'.$req->value1.' '.$req->uom.'</option>
                            <option value="'.$req->false.'" >'.$req->false.' '.$req->uom.'</option>
                        </select>';
                break;

            
            case '4':
                

                $retn = '<select class="form-control select txresult_'.$inc.'"  id="txresult" name="txresult" required>
                             <option value="" >--Choose Result--</option>
                            <option value="1" >1</option>
                            <option value="1-2" >1-2</option>
                            <option value="2" >2</option>
                            <option value="2 (Evenly)" >2 (Evenly)</option>
                            <option value="2 (Unevenly)" >2 (Unevenly)</option>
                            <option value="2-3" >2-3</option>
                            <option value="2-3 (Evenly)" >2-3 (Evenly)</option>
                            <option value="2-3 (Unevenly)" >2-3 (Unevenly)</option>
                            <option value="3" >3</option>
                            <option value="3-4" >3-4</option>
                            <option value="4" >4</option>
                            <option value="4-5" >4-5</option>
                            <option value="5" >5</option>
                        </select>';
                break;

            case '2' || '3' || '5':

                $retn = '<input type="text" class="form-control txresult_'.$inc.'" id="txresult" name="txresult" required onkeypress="return comma(event);">';
                break;
        
        }

        return $retn;
    }

    static function setResult($req_id,$rest_val){
        $mreq = MasterRequirement::where('id',$req_id)->first();

        if ($rest_val!='null' || $rest_val!=null || $rest_val!='') {
            switch ($mreq->operator) {
                case '1':
                        if ($rest_val== strval($mreq->value1)) {
                            $return ='<label class="label label-success label-rounded ">PASS</label>';
                        }else{
                            $return ='<label class="label label-danger label-rounded ">FAILED</label>';
                        }
                    break;

                case '2':
                        if (floatval($rest_val)<= floatval($mreq->value1)) {
                           $return ='<label class="label label-success label-rounded ">PASS</label>';
                        }else{
                            $return ='<label class="label label-danger label-rounded ">FAILED</label>';
                        }
                    break;

                case '3':
                        if (floatval($rest_val)>= floatval($mreq->value1)) {
                            $return ='<label class="label label-success label-rounded ">PASS</label>';
                        }else{
                            $return ='<label class="label label-danger label-rounded ">FAILED</label>';
                        }
                    break;


                case '4':
                        $requ = array($mreq->value1,$mreq->value2,$mreq->value2,$mreq->value3,$mreq->value4,$mreq->value5,$mreq->value6,$mreq->value7);
            
                        $tes = in_array($rest_val,$requ);
                
                        if ($tes==true) {
                          $return ='<label class="label label-success label-rounded ">PASS</label>';
                        }else{
                            $return ='<label class="label label-danger label-rounded ">FAILED</label>';
                        }
                    break;



                case '5':
                        if ( ( floatval($rest_val)>= floatval($mreq->value1)) && ( floatval($rest_val)<= floatval($mreq->value2))) {
                            $return ='<label class="label label-success label-rounded ">PASS</label>';
                        }else{
                           $return ='<label class="label label-danger label-rounded ">FAILED</label>';
                        }
                    break;

                default : 
                    return 'ERROR';
                break;
                
                
            }
        }else{
            $return = "";
        }

        return $return;
    }


    static function setReqOption($req_id){

        $req =  DB::table('master_requirements')
                            ->join('master_operators','master_requirements.operator','=','master_operators.id')
                            ->where('master_requirements.id',$req_id)
                            ->select(
                                'master_requirements.id',
                                'master_requirements.operator',
                                'master_requirements.value1',
                                'master_requirements.value2',
                                'master_requirements.value3',
                                'master_requirements.value4',
                                'master_requirements.value5',
                                'master_requirements.value6',
                                'master_requirements.value7',
                                'master_operators.operator_name',
                                'master_operators.symbol'
                            )->first();

       switch ($req->operator) {
           

            case '3' || '2' || '1':
                return $req->symbol." ".$req->value1;
                break;
            case '4':
               $arr = array($masreq->value1,$masreq->value2,$masreq->value3,$masreq->value4,$masreq->value5,$masreq->value6,$masreq->value7);
                return implode(",",array_filter($arr));
               break;
            case '5':
               return $req->value1." ".$req->symbol." ".$req->value2;
               break;
           
            default:
               return 'ERROR';
               break;
       }
    }

    static function statusCek($id){
        $trf = TrfTesting::where('id',$id)->first();

        switch ($trf->status) {
            case 'OPEN':
                return '<span class="label bg-grey-400">OPEN</span>';
                break;

            case 'ONPROGRESS':
                return '<span class="label bg-blue">ONPROGRESS</span>';
                break;

            case 'CLOSED':
                return '<span class="label bg-success">CLOSED</span>';
                break;

            case 'REJECT':
                $user = DB::table('users')->where('nik',$trf->reject_by)->wherenull('deleted_at')->first();
                return '<span class="label bg-danger">REJECT </span>' ;
                break;

            case 'VERIFIED':
                $user = DB::table('users')->where('nik',$trf->verified_lab_by)->wherenull('deleted_at')->first();
                return '<span class="label bg-warning">VERIFIED </span>';
                break;

            case 'ESCALATION I':
                return '<span class="label bg-info">ESCALTION I</span>';
                break;

            case 'ESCALATION I':
                return '<span class="label bg-purple">ESCALTION II</span>';
                break;

            default : 
                return 'ERROR';
            break;
        }
    }

    // static function getComposition($method,$category){
    //     $data = MasterRequirement::where('master_method_id',$method)->where('master_category_id',$category)->groupBy('komposisi_specimen')->select('komposisi_specimen')->get();

    //     return $data;
    // }

    static function testRequired($tesreq,$prvtrf){
        

        switch ($tesreq) {
            case 'developtesting_t1':
                return "Develop Testing T1";
            break;

            case 'developtesting_t2':
                return "Develop Testing T2";
            break;

            case 'developtesting_selective':
                return "Develop Testing Selective";
            break;

            case 'bulktesting_m':
                return "1st Bulk Testing M (Model Level)";
            break;


            case 'bulktesting_a':
                return "1st Bulk Testing A (Article Level)";
            break;


            case 'bulktesting_selective':
                return "1st Bulk Testing Selective";
            break;

            case 'reordertesting_m':
                return "Re-Order Testing M (Model Level)";
            break;

            case 'reordertesting_a':
                return "Re-Order Testing A (Article Level)";
            break;

            case 'reordertesting_selective':
                return "Re-Order Testing Selective";
            break;

            case 'preproductiontesting_m':
                return "Pre-Production Testing M (Model Level)";
            break;

            case 'preproductiontesting_a':
                return "Pre-Production Testing A (Article Level)";
            break;

            case 'preproductiontesting_selective':
                return "Pre-Productionr Testing Selective";
            break;

            case 'retest':
                return "Re-Test ".isset($prvtrf);
            break;

            default : 
                return '';
            break;
            
          
        }
    }

    static function dateInfoSet($date,$dateinforemrk){
        switch ($dateinforemrk) {
            case 'buy_ready':
                return 'BUY READY  <br>'.date_format(date_create($date),'d-m-Y');
                break;

            case 'podd':
                return 'PODD <br>'.date_format(date_create($date),'d-m-Y');
                break;

            case 'output_sewing':
                return 'Output Sewing <br>'.date_format(date_create($date),'d-m-Y');
                break;
            
            default:
                return 'ERROR';
                break;
        }
    }


    static function ReportResult($req_id,$rest_val){
        $mreq = MasterRequirement::where('id',$req_id)->first();

        if ($rest_val!='null' || $rest_val!=null || $rest_val!='') {
            switch ($mreq->operator) {
                case '1':
                        if ($rest_val== strval($mreq->value1)) {
                            $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;

                case '2':
                        if (floatval($rest_val)<= floatval($mreq->value1)) {
                          $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;

                case '3':
                        if (floatval($rest_val)>= floatval($mreq->value1)) {
                           $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;


                case '4':
                        $requ = array($mreq->value1,$mreq->value2,$mreq->value2,$mreq->value3,$mreq->value4,$mreq->value5,$mreq->value6,$mreq->value7);
            
                        $tes = in_array($rest_val,$requ);
                
                        if ($tes==true) {
                          $return ="PASS";
                        }else{
                             $return ="FAILED";
                        }
                    break;



                case '5':
                        if ( ( floatval($rest_val)>= floatval($mreq->value1)) && ( floatval($rest_val)<= floatval($mreq->value2))) {
                            $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;

                default : 
                    return 'ERROR';
                break;
                
                
            }
        }else{
            $return = "";
        }

        return $return;
    }



    static function getTrfDoc($id,$docid){
        $gtrdoc = DB::table('trf_testings')
                            ->join('trf_testing_document','trf_testings.id','=','trf_testing_document.trf_id')
                            ->join('master_category','trf_testings.id_category','=','master_category.id')
                            ->where('trf_testings.id',$id)
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_document.deleted_at')
                            ->select(
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.updated_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.previous_trf_id',
                                'trf_testings.id_category',
                                'trf_testings.technician',
                                'trf_testings.labhead',
                                'trf_testings.qchead',
                                DB::raw('trf_testing_document.id as doc_id'),
                                'trf_testing_document.document_type',
                                'trf_testing_document.document_no',
                                'trf_testing_document.season',
                                'trf_testing_document.style',
                                'trf_testing_document.article_no',
                                'trf_testing_document.size',
                                'trf_testing_document.color',
                                'trf_testing_document.fibre_composition',
                                'trf_testing_document.fabric_finish',
                                'trf_testing_document.gauge',
                                'trf_testing_document.fabric_weight',
                                'trf_testing_document.plm_no',
                                'trf_testing_document.care_instruction',
                                'trf_testing_document.manufacture_name',
                                'trf_testing_document.export_to',
                                'trf_testing_document.nomor_roll',
                                'trf_testing_document.batch_number',
                                'trf_testing_document.item',
                                'trf_testing_document.barcode_supplier',
                                'trf_testing_document.additional_information',
                                'trf_testing_document.yds_roll',
                                'trf_testing_document.description',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen'
                                );
        if (isset($docid)) {
            $gtrdoc = $gtrdoc->where('trf_testing_document.id',$docid);
        }
        return $gtrdoc->get();
    }


    static function getTrfRest($docid){
        $getRest = DB::table('trf_result_test')
                            ->join('trf_requirement_set','trf_result_test.set_req_id','=','trf_requirement_set.id')
                            ->join('trf_testing_methods','trf_requirement_set.trf_meth_id','=','trf_testing_methods.id')
                            ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                            ->where('trf_result_test.trf_doc_id',$docid)
                            ->whereNull('trf_result_test.deleted_at')
                            ->whereNull('trf_requirement_set.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->select(
                                'trf_result_test.id',
                                'trf_result_test.set_req_id',
                                'trf_result_test.trf_doc_id',
                                'trf_result_test.supplier_result',
                                'trf_result_test.result',
                                'trf_result_test.before_test',
                                'trf_result_test.special_id',
                                'trf_result_test.number_test',
                                'trf_result_test.image_ext',
                                'trf_requirement_set.requirement_id',
                                'master_method.method_code',
                                'master_method.method_name',
                                'master_method.category'
                            )->get();
        return $getRest;
    }

    
    static function GetUser($id){
        $get = User::where('id',$id)->first();


        return ['nik'=>$get->nik,'name'=>$get->name,'position'=>$get->position];
    }

    static function generateQr($url){
        return QrCode::format('png')->size(200)->errorCorrection('H')->generate($url);
    }

    static function calculateResult($requirementid,$value,$measuring_position){
        $getMethParam = DB::table('ns_req_catg_method')->where('id',$requirementid)->select('method_code','parameter','buyer','category','category_specimen','type_specimen')->first();
        // dd($requirementid,$value,$measuring_position,$getMethParam);

        switch ($getMethParam) {
            
            case $getMethParam->buyer=="ADIDAS" && $getMethParam->method_code=="PHX-AP0701" && ($getMethParam->parameter=="Top Shrinkage" || $getMethParam->parameter=="Bottom Shrinkage" || $getMethParam->parameter=="Length" || $getMethParam->parameter=="Width") :
                    $calc = ((floatval($value['result'])-floatval($value['before']))/floatval($value['before'])) * 100;
                    $return = round($calc,1);
                break;

            case $getMethParam->buyer=="ADIDAS" && $getMethParam->method_code=="PHX-AP0701" && ($getMethParam->parameter=="Spirality" ) && $measuring_position!="Movement of sideseam/outseam (Cm)" :
                    $calc = ((floatval($value['result']))/floatval($value['before'])) * 100;
                    $return = round($calc,1);
                break;

            case $getMethParam->buyer=="ADIDAS" && $getMethParam->method_code=="PHM-AP0405" && $getMethParam->parameter=="Shrinkage" :
                    $calc = ((floatval($value['result'])-floatval($value['before']))/floatval($value['before'])) * 100;
                    $return = round($calc,1);
                break;

            case $getMethParam->buyer=="ADIDAS" && $getMethParam->method_code=="PHM-AP0419" && $getMethParam->parameter=="Gramation" :
                    $calc = ((floatval($value['result'])-floatval($value['standart']))/floatval($value['standart'])) * 100;
                    $return = round($calc,1);
                break;
                
            case ($getMethParam->buyer=='JAKO' && $getMethParam->parameter=='Shrinkage' && $getMethParam->category_specimen=='FABRIC') && ($getMethParam->method_code=="EN ISO 5077 + ISO 6330, 5 CYCLES WASHING AT 40℃ EACH FOLLOWED BY TUMBLE DRY" || $getMethParam->method_code=="JAKO NORM QMAA-UP02-06") && ($measuring_position=='Length' || $measuring_position=='Width') : 

                    $calc = ($value['result']-35)/35;
                    $return = round($calc,1);
                   
                break;

            // case $getMethParam->buyer=="JAKO" && $getMethParam->method_code="ISO 16322-1,-2,-3" && $getMethParam->parameter=="Spirality" && $measuring_position=="Spirality":
            //         $calc = (($value['result']-$value['before'])/$value['before'])*100;
            //         $return = round($calc,1);
            //     break;

            case ($getMethParam->buyer=='JAKO' && $getMethParam->method_code='ISO 16322-1,-2,-3' && $getMethParam->parameter=='Spirality' && $measuring_position='Spirality'):
                        $calc = (($value['result']-$value['before'])/$value['before'])*100;
                        $return = round($calc,1);
                break;
            
            default:
                $return = $value['result'];

                break;
        }

      
        return $return;
    }


    static function resultData($setreq,$docid){
        // $data = TestingResult::where('set_req_id',$setreq)->where('trf_doc_id',$docid)->where('number_test',$numbertest)->whereNull('deleted_at')->first();
        $data = DB::table('trf_result_test')
                            ->join('trf_requirement_set','trf_result_test.set_req_id','=','trf_requirement_set.id')
                            ->join('master_requirements','trf_requirement_set.requirement_id','=','master_requirements.id')
                            ->where('trf_result_test.set_req_id',$setreq)
                            ->where('trf_result_test.trf_doc_id',$docid)
                            ->whereNull('trf_result_test.deleted_at')
                            ->whereNull('trf_requirement_set.deleted_at')
                            ->select(
                                'trf_result_test.id',
                                'trf_result_test.supplier_result',
                                'trf_result_test.before_test',
                                'trf_result_test.standart_value',
                                'trf_result_test.testing_result',
                                'trf_result_test.result',
                                'trf_result_test.result_status',
                                'trf_result_test.number_test',
                                'trf_result_test.comment',
                                'master_requirements.uom'
                            )->get();

        return $data;
    }


    static function CalcSatusMethSpc($trfmethid,$trfdocid){
        $data = DB::table('trf_requirement_set')
                        ->join('trf_result_test','trf_requirement_set.id','=','trf_result_test.set_req_id')
                        ->where('trf_requirement_set.trf_meth_id',$trfmethid)
                        ->where('trf_result_test.trf_doc_id',$trfdocid)
                        ->whereNull('trf_requirement_set.deleted_at')
                        ->whereNull('trf_result_test.deleted_at');

        $total = $data->count();

        $pass = $data->where('trf_result_test.result_status','PASS')->count();

        $fail = $data->where('trf_result_test.result_status','FAILED')->count();
    
        if ($total!=0) {
            if ($total==$pass) {
                return 'PASS';
            }else if ($total>$pass) {
                return 'FAILED';
            }else{
                return 'ERROR';
            }
        }else{
           return ''; 
        }
    }

    static function cekEscal($trfmethid,$trfdocid){
        $cekdata = EscalationReq::where('trf_method_id',$trfmethid)->where('trf_doc_id',$trfdocid)->whereNull('deleted_at')->exists();

        return $cekdata;
    }


    // static function cekAutoClosed($trf_id){
    //     $cekResult = DB::select("SELECT * from ns_cek_trf_rest('".$trf_id."') where finrest like '%false%'");
    //     $cekStatus = TrfTesting::where('id',$trf_id)->first();
         
    //     if (empty($cekResult)) {
    //         TrfTesting::where('id',$trf_id)->update(['status'=>"CLOSED",'updated_at'=>carbon::now(),'closed_at'=>carbon::now()]);
    //     }else if(!empty($cekResult) && $cekStatus->status=='CLOSED'){
    //         TrfTesting::where('id',$trf_id)->update(['status'=>"ONPROGRESS",'updated_at'=>carbon::now(),'closed_at'=>null]);
    //     }

    //     return true;
    // }

    // edt-anf
    static function cekAutoClosed($trf_id) {
        try {
            $trf = TrfTesting::findOrFail($trf_id);
            $completeResults = DB::selectOne("SELECT EXISTS (SELECT 1 FROM ns_cek_trf_rest(?) WHERE finrest LIKE ?) AS result", [$trf_id, '%false%'])->result;
            
            $changes = [];
            
            if ($trf->closed_at !== null) {
                $changes['status'] = 'CLOSED';
            } elseif ($completeResults == false && $trf->status !== 'CLOSED') {
                $changes['status'] = 'CLOSED';
                $changes['closed_at'] = Carbon::now();
            } elseif ($completeResults == true && $trf->status == 'CLOSED') {
                $changes['status'] = 'ONPROGRESS';
                $changes['closed_at'] = null;  
            }
            
            if (!empty($changes)) {
                $changes['updated_at'] = Carbon::now();
                $trf->update($changes);
                // dd($changes);
            }
            
            return true;
        } catch (\Exception $e) {
            \Log::error("Error in cekAutoClosed for TRF {$trf_id}: " . $e->getMessage());
            return false;
        }
    }

    static function mappingSpc($ctgspc,$typespc){
        if ($ctgspc=="FABRIC") {
            return view('create_trf.modal_doc.fabric');
        }else if ($ctgspc=="MOCKUP") {
            return view('create_trf.modal_doc.mockup');
        }else if ($ctgspc=="ACCESORIES/TRIM") {
            return view('create_trf.modal_doc.accesories');
        }else if ($ctgspc=="GARMENT") {
            return view('create_trf.modal_doc.garment');
        }else if (($ctgspc=="STRIKE OFF" && $typespc=="HEAT TRANSFER") ||  $ctgspc=='PANEL' ) {
            return view('create_trf.modal_doc.heat_transfer');
        }else if ($ctgspc=="STRIKE OFF" && $typespc=="PRINTING") {
            return view('create_trf.modal_doc.printing');
        }else if ($ctgspc=="STRIKE OFF" && $typespc=="EMBROIDERY") {
            return view('create_trf.modal_doc.embro');
        }else if ($ctgspc=="STRIKE OFF" && $typespc=="PAD PRINT") {
            return view('create_trf.modal_doc.pad_print');
        }else if ($ctgspc=="STRIKE OFF" && $typespc=="BADGE") {
            return view('create_trf.modal_doc.badge');
        }else{
            return view('create_trf.modal_doc.error');
        }
    }

    static function getDue($dateVerif){
        $verif = carbon::parse($dateVerif)->format('Y-m-d');
        $pls = 3;
        $i = 1;
        $due = carbon::parse($verif)->addDays(3)->format('Y-m-d');
        $vpl = carbon::parse($verif)->addDays($i)->format('Y-m-d');
        $dayV = (int)carbon::parse($verif)->format('w');
        $dayP = (int)carbon::parse($vpl)->format('w');

        // dd($due,$dayP,$i,$pls);
        do {
           
            $vpl =carbon::parse($verif)->addDays($i)->format('Y-m-d');
            $dayP = (int)carbon::parse($vpl)->format('w');

            if ($dayP>0 && $dayP<6) {
                if (self::getHoliday($vpl)) {
                    $pls = $pls+1;
                    $due =carbon::parse($due)->addDays(1)->format('Y-m-d');


                    
               }else{
                    $due = $due;

               }
            }else{
               

               

                $pls = $pls+1;
                $due =carbon::parse($due)->addDays(1)->format('Y-m-d');


            }

       

            $i++;
           
        } while (($vpl<$due));

        return $due;
    }


    static function getHoliday($date){

        $data = DB::table('master_holiday')
                            ->join('holiday_detail','master_holiday.id','=','holiday_detail.master_holiday_id')
                            ->whereNull('master_holiday.deleted_at')
                            ->whereNull('holiday_detail.deleted_at')
                            ->where('holiday_detail.date_off',$date)
                            ->select('holiday_detail.date_off')
                            ->exists();
                            


        return $data;
    }

    static function checkSpcResult($docid){
        $data = TestingResult::where('trf_doc_id',$docid)->whereNull('deleted_at');

        $total = $data->count();


        if ($total!=0) {
            $pass = $data->where('result_status','PASS')->count();
            $fail = $data->where('result_status','FAILED')->count();

            if ($total==$pass) {
                $rtn = "PASS";
            }else if ($total>$pass) {
                $rtn = "FAILED";
            }else{
                $rtn = "ERROR";
            }
        }else{
            $rtn = "";
        }

        return $rtn;
    }

    static function cekRoleReport(){
        if ( auth::user()->hasRole(['LAB TECHNICIAN','LAB ADMIN'])) {
            $ret = ['lab'=>'disabled','fact'=>''];
        }else if (auth::user()->hasRole(['LAB SECTION HEAD','ICT','ME'])) {
            $ret = ['lab'=>'','fact'=>''];
        }else{
            $ret = ['lab'=>'','fact'=>'disabled'];
        }

        return $ret;
    }
}
