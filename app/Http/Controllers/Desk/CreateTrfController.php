<?php

namespace App\Http\Controllers\Desk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use DataTables;
use Session;


use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use App\Models\TrfTestingMethods;
use App\Models\MasterMethode;
use App\Models\MasterCategory;
use App\Models\Employee;
use App\User;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Http\Controllers\HelperController;
use App\Http\Controllers\ERP\erpLoadData;

class CreateTrfController extends Controller
{
    public function index(){
   
       return view('selfdesk.home.index');
    }

    public function getData(Request $req){
      $filter = trim($req->key);

      $data = DB::table('ns_station_view');

      if (isset($filter)) {
         $data = $data->where('trf_id','LIKE','%'.$filter.'%')
                        ->orwhere('method_code','LIKE','%'.$filter.'%')
                        ->orwhere('nik','LIKE','%'.$filter.'%')
                        ->orwhere('username','LIKE','%'.$filter.'%')
                        ->orwhere('styleart','LIKE','%'.$filter.'%');
      }else{
         $from = carbon::now()->subday(60)->format('Y-m-d H:i:s');
         $to = carbon::now()->format('Y-m-d H:i:s');

         $data = $data->wherebetween('created_at',[$from,$to])
                        ->orWhereBetween('updated_at',[$from,$to]);

      }

      return DataTables::of($data->orderby('created_at','desc'))
                        ->editColumn('test_required',function($data){

                           return HelperController::testRequired($data->test_required,$data->previous_trf_id);
                        })
                        ->editColumn('date_information_remark',function($data){

                           return HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
                        })
                        ->editColumn('id_category',function($data){

                           return '<b>Category</b> : '.$data->category.'<br><b> Category Sepcimen</b> : '.$data->category_specimen.'<br> <b>Type Specimen</b> : '.$data->type_specimen;
                        })
                        ->editColumn('status',function($data){
                           return HelperController::statusCek($data->id);
                        })
                        ->editColumn('return_test_sample',function($data){
                              if ($data->return_test_sample==true) {
                                 return '<span class="label label-success">YES</span>';
                              }else{
                                 return '<span class="label label-warning">NO</span>';
                              }
                        })
                        ->editColumn('asal_specimen',function($data){
                            $fact = DB::table('factory')->where('id',$data->factory_id)->first();

                            return $data->asal_specimen." - ".$fact->factory_name;
                        })
                        ->addColumn('action',function($data){
                           return view('_action',[
                                        'deskPrintBarcode'=>$data->id,
                                    //    'deskPrintBarcode'=>route('desk.printBarcode',['id'=>$data->id]),
                                    //    'deskPrintDetail'=>route('desk.printDetail',['id'=>$data->id])
                                    ]);
                        })
                        ->rawColumns(['test_required','date_information_remark','id_category','method_code','status','return_test_sample','asal_specimen'])
                        ->make(true);
    }

    public function createTrf(){
      $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();
      $fact = DB::table('master_lab_location')->wherenull('deleted_at')->get();
      $factory_id = \Request::session()->get('factory_id');
    //   dd($factory_id);
      return view('selfdesk.home.create')->with('buyer',$buyer)
                                          ->with('fact',$fact)
                                          ->with('factory_id',$factory_id);
    }

    public function getCtg(Request $req){
        $data = DB::table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->groupBy('category')
                    ->select('category')
                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function getCtgSpc(Request $req){
        $origin = $req->origin;

        switch ($origin) {
            case 'SSM':
                $spc = ['STRIKE OFF','GARMENT','FABRIC','MOCKUP','ACCESORIES/TRIM','HIJAB'];
                break;

            case 'WMS':
                $spc = ['FABRIC','ACCESORIES/TRIM'];
                break;

            case 'FGMS':
                $spc = ['GARMENT'];
                break;

            case 'CDMS':
                $spc = ['MOCKUP','STRIKE OFF','PANEL'];
                break;

            default:
                $spc = ['STRIKE OFF','GARMENT','FABRIC','MOCKUP','ACCESORIES/TRIM','HIJAB'];
                break;
                break;
        }

        $data = DB::table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->where('category',trim($req->category))
                    ->whereIn('category_specimen',$spc)
                    ->groupBy('category_specimen')
                    ->select('category_specimen')
                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function getTypeSpc(Request $req){
        $data = DB::table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->where('category',trim($req->category))
                    ->where('category_specimen',trim($req->category_specimen))
                    ->groupBy('type_specimen')
                    ->select('type_specimen')
                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function getDataSpecimen(Request $request){
        $type = $request->type;
        // $category_specimen = $request->category_specimen;
        // $type_specimen = $request->type_specimen;
        $origin = $request->origin;
        $key = $request->key;
        // dd($req);
        // $data = erpLoadData::getDocErp($key,$type,$category_specimen,$type_specimen,$origin);

        $data = erpLoadData::getDocErp($key,$type,$origin);

        return response()->json(['data'=>$data],200);
    }

    public function saveTrf(Request $req){
         $origin = $req->origin;
         $asal = $req->asal;
         $buyer = $req->buyer;
         $category_specimen = $req->category_specimen;
         $category = $req->category;
         $type_specimen = $req->type_specimen;
         $radio_date_info = $req->radio_date_info ;
         $date_info = $req->date_info;
         $part_to_test  = $req->part_to_test;
         $return_test  = $req->return_test;
         $no_trf = $req->no_trf;
         $data = $req->data;
         $labloction = $req->lab_location;
         $test_req = $req->test_req;
         $nik = $req->session()->get('nik');
         $name = $req->session()->get('name');
         $factory_id = $req->session()->get('factory_id');

        //  $trf_id = $this->getCode($category_specimen,$asal,$factory_id);
        //  $getCtg = MasterCategory::where('category',$category)->where('category_specimen',$category_specimen)->where('type_specimen',$type_specimen)->whereNull('deleted_at')->first();
        //  $chkTrf = TrfTesting::where('trf_id',$trf_id)->whereNull('deleted_at')->exists();

        // if ($chkTrf) {
        //     return response()->json('TRF Number was already ! ! !',422);
        // }

         try {
            DB::beginTransaction();
            $list = [];
                $getcategory = MasterCategory::where([
                    'category'=>$category,
                    'category_specimen'=>$category_specimen,
                    'type_specimen'=>$type_specimen
                ])->whereNull('deleted_at')->first();

              foreach ($data as $key => $value) {
                
                    $trf_id = self::getCode($category_specimen,$asal,$factory_id);

                    $cekTrf = TrfTesting::where('trf_id',$trf_id)->whereNull('deleted_at')->exists();
         
                    if ($cekTrf) {
                        return response()->json('TRF Number was already ! ! !',422);
                    }
                    
                    $trfTesting = TrfTesting::firstOrCreate([
                        'trf_id'=>$trf_id,
                        'buyer'=>$buyer,
                        'lab_location'=>$labloction,
                        'nik'=>$nik,
                        'platform'=>$origin,
                        'factory_id'=>$factory_id,
                        'asal_specimen'=>$asal,
                        'category_specimen'=>$category_specimen,
                        'type_specimen'=>$type_specimen,
                        'category'=>$category,
                        'date_information'=>date_format(date_create($date_info),'Y-m-d'),
                        'date_information_remark'=>$radio_date_info,
                        'test_required'=>$test_req,
                        'part_of_specimen'=>$part_to_test,
                        'created_at'=>Carbon::now(),
                        'return_test_sample'=>$return_test,
                        'status'=>"OPEN",
                        'previous_trf_id'=>$no_trf,
                        'id_category'=>$getcategory->id,
                        'username'=>$name
                    ]);



                    // $docid = array(
                    //     'trf_id'=>$trfTesting->id,
                    //     'document_type'=>trim($value['document_type']),
                    //     'document_no'=>trim($value['document_no']),
                    //     'style'=>trim($value['style']),
                    //     'article_no'=>trim($value['article_no']),
                    //     'size'=>trim($value['size']),
                    //     'color'=>trim($value['color']),
                    //     'fibre_composition'=>trim($value['fibre_composition']),
                    //     'fabric_finish'=>trim($value['fabric_finish']),
                    //     'gauge'=>trim($value['gauge']),
                    //     'fabric_weight'=>trim($value['fabric_weight']),
                    //     'plm_no'=>trim($value['plm_no']),
                    //     'care_instruction'=>trim($value['care_instruction']),
                    //     'manufacture_name'=>trim($value['manufacture_name']),
                    //     'export_to'=>trim($value['export_to']),
                    //     'nomor_roll'=>trim($value['nomor_roll']),
                    //     'batch_number'=>trim($value['batch_number']),
                    //     'item'=>trim($value['item']),
                    //     'barcode_supplier'=>trim($value['barcode_supplier']),
                    //     'additional_information'=>trim($value['additional_information']),
                    //     'yds_roll'=>trim($value['yds_roll']),
                    //     'description'=>trim($value['description']),
                    //     'season'=>trim($value['season']),
                    //     'invoice'=>trim($value['invoice']),
                    //     'fabric_color'=>trim($value['fabric_color']),
                    //     'interlining_color'=>trim($value['interlining_color']),
                    //     'qty'=>(int)trim($value['qty']),
                    //     'machine'=>trim($value['machine']),
                    //     'temperature'=>trim($value['temperature']),
                    //     'pressure'=>trim($value['pressure']),
                    //     'duration'=>trim($value['duration']),
                    //     'style_name'=>trim($value['style_name']),
                    //     'fabric_item'=>trim($value['fabric_item']),
                    //     'fabric_type'=>trim($value['fabric_type']),
                    //     'thread'=>trim($value['thread']),
                    //     'material_shell_panel'=>trim($value['material_shell_panel']),
                    //     'product_category'=>trim($value['product_category']),
                    //     'remark'=>trim($value['remark']),
                    //     'pad'=>trim($value['pad']),
                    //     'pelling'=>trim($value['pelling']),
                    //     'component'=>trim($value['component']),
                    //     'garment_size'=>trim($value['garment_size']),
                    //     'sample_type'=>trim($value['sample_type']),
                    //     'test_condition'=>trim($value['test_condition']),
                    //     'fabric_properties'=>trim($value['fabric_properties']),
                    //     'lot'=>trim($value['lot']),
                    //     'created_at'=>Carbon::now()
                    // );

                     $docid = array(
                        'trf_id'=>$trfTesting->id,
                        'document_type'=>trim($value['document_type']),
                        'document_no'=>trim($value['document_no']),
                        'style'=>trim($value['style']),
                        'article_no'=>trim($value['article_no']),
                        'size'=>trim($value['size']),
                        'color'=>trim($value['color']),
                        'fibre_composition'=>trim($value['fibre_composition']),
                        'fabric_finish'=>trim($value['fabric_finish']),
                        'gauge'=>trim($value['gauge']),
                        'fabric_weight'=>trim($value['fabric_weight']),
                        'plm_no'=>trim($value['plm_no']),
                        'care_instruction'=>trim($value['care_instruction']),
                        'manufacture_name'=>trim($value['manufacture_name']),
                        'export_to'=>trim($value['export_to']),
                        'nomor_roll'=>trim($value['nomor_roll']),
                        'batch_number'=>trim($value['batch_number']),
                        'item'=>trim($value['item']),
                        'barcode_supplier'=>trim($value['barcode_supplier']),
                        'additional_information'=>trim($value['additional_information']),
                        'yds_roll'=>trim($value['yds_roll']),
                        'description'=>trim($value['description']),
                        'season'=>trim($value['season']),
                        'invoice'=>trim($value['invoice']),
                        'fabric_color'=>trim($value['fabric_color']),
                        'interlining_color'=>trim($value['interlining_color']),
                        'qty'=>(int)trim($value['qty']),
                        'machine'=>trim($value['machine']),
                        'temperature'=>trim($value['temperature']),
                        'pressure'=>trim($value['pressure']),
                        'duration'=>trim($value['duration']),
                        'style_name'=>trim($value['style_name']),
                        'fabric_item'=>trim($value['fabric_item']),
                        'fabric_type'=>trim($value['fabric_type']),
                        'thread'=>trim($value['thread']),
                        'material_shell_panel'=>trim($value['material_shell_panel']),
                        'product_category'=>trim($value['product_category']),
                        'remark'=>trim($value['remark']),
                        'pad'=>trim($value['pad']),
                        'pelling'=>trim($value['pelling']),
                        'component'=>trim($value['component']),
                        'garment_size'=>trim($value['garment_size']),
                        'sample_type'=>trim($value['sample_type']),
                        'test_condition'=>trim($value['test_condition']),
                        'fabric_properties'=>trim($value['fabric_properties']),
                        'lot'=>trim($value['lot']),
                        'po_buyer'=>trim($value['po_buyer']),
                        'arrival_date'=>trim($value['arrival_date'])!=null ? trim($value['arrival_date']) : null,
                        'created_at'=>Carbon::now()
                    );

                    TrfTestingDocument::firstOrCreate($docid);

                    $list[]=$trfTesting->id;

              }

             
            DB::commit();
            $data_response = [
                'status'=>200,
                'output'=>"Create Trf Success ! ! !",
                'list'=>json_encode($list)
            ];
         } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                'status'=>422,
                'output'=>"Create Trf Failed ! ! !
                ".$message
            ];
         }

         return response()->json(['data'=>$data_response]);
    }


    public function printBarcode(Request $req){
        $id= json_decode($req->data);
  
        $list = TrfTesting::whereIn('id',$id)->get();
        // dd($list);
        return view('create_trf.barcode_trf',['list'=>$list]);
    }


    private function getCode($spcode,$asal,$factory_id){



        $gefac = DB::table('factory')->where('id',$factory_id)->first();
        $sp = DB::table('master_spesiment_code')->where('platform',$asal)->where('spesimen',$spcode)->whereNull('deleted_at')->first();
        $date = Carbon::now()->format('ymd');
        $last = TrfTesting::where('factory_id',$factory_id)->where('asal_specimen',$asal)->whereDate('created_at',Carbon::now()->format('Y-m-d'))->orderby('created_at','desc')->orderBy('trf_id','desc')->first();

        if ($last==null) {
            $x = (int)0;
        }else{
            $x = (int)substr($last->trf_id, -3);

        }

        $rcode = "TRF/".$gefac->factory_name."/".$date."-".$sp->code.sprintf("%03s", $x+1);

        return $rcode;
    }

    public function printDetail(Request $req){
        $id= trim($req->id);

        $data = DB::table('trf_testings')
                            ->join('master_category','trf_testings.id_category','=','master_category.id')
                            ->join('factory','trf_testings.factory_id','=','factory.id')
                            ->where('trf_testings.id',$id)
                            ->whereNull('trf_testings.deleted_at')
                            ->select(
                                    'trf_testings.id',
                                    'trf_testings.trf_id',
                                    'trf_testings.lab_location',
                                    'trf_testings.buyer',
                                    'trf_testings.nik',
                                    'trf_testings.platform',
                                    'trf_testings.asal_specimen',
                                    'trf_testings.date_information',
                                    'trf_testings.date_information_remark',
                                    'trf_testings.test_required',
                                    'trf_testings.part_of_specimen',
                                    'trf_testings.created_at',
                                    'trf_testings.return_test_sample',
                                    'trf_testings.status',
                                    'trf_testings.verified_lab_date',
                                    'trf_testings.verified_lab_by',
                                    'trf_testings.reject_by',
                                    'trf_testings.reject_date',
                                    'trf_testings.additional_information',
                                    'trf_testings.previous_trf_id',
                                    'master_category.category',
                                    'master_category.category_specimen',
                                    'master_category.type_specimen',
                                    'factory.factory_name'
                                )->first();


        $currentURL = 'http://'.$req->getHttpHost().'/api/print/report-specimen?id='.$data->id.'&notrf='.$data->trf_id;
        $qrcode =  HelperController::generateQr($currentURL);



        $testreq = isset($data->test_required) ? HelperController::testRequired($data->test_required,$data->previous_trf_id) : '';
        $dateinfo = HelperController::dateInfoSet($data->date_information,$data->date_information_remark);
        $meth = DB::table('trf_testing_methods')
                        ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                        ->whereNull('trf_testing_methods.deleted_at')
                        ->where('trf_testing_methods.trf_id',$data->id)
                        ->select('master_method.id','master_method.method_code','master_method.method_name','master_method.category','master_method.type')->get()->toArray();

        $docm = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->get();

        $users = Employee::where('nik', $data->nik)->first();




        $vefpiclab = isset($data->verified_lab_by) ?  User::where('nik', $data->verified_lab_by)->first()['name'] : "";
        $vefdatelab = isset($data->verified_lab_date) ? carbon::parse($data->verified_lab_date)->format('d-m-Y H:i:s') : "";
        $filename = "TRF_DETAIL_".$data->trf_id;

        $pdf = \PDF::loadView('create_trf.print_detail',['data'=>$data,'method'=>$meth,'docm'=>$docm,'submit_by'=>isset($users) ? $users->name : $data->nik,'test_required'=>$testreq,'dateinfo'=>$dateinfo,'labdate'=>$vefdatelab,'labpic'=>$vefpiclab,'qrcode'=>base64_encode($qrcode)])->setPaper('A4','Potrait');
        return $pdf->stream($filename);
    }
}
