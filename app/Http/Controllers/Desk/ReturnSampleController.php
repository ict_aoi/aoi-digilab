<?php

namespace App\Http\Controllers\Desk;
use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use DataTables;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HelperController;
use App\Models\TrfReturnSample;
use App\Models\TrfTesting;
use App\Models\TrfTestingDocument;
use Exception;
use PDO;

class ReturnSampleController extends Controller
{
    public function index(){
        $factory = DB::table('factory')->whereNull('deleted_at')->get();
        $buyer = DB::table('master_buyer')->whereNull('deleted_at')->get();
        return view('selfdesk.home.return_sample')->with('factory',$factory)->with('buyer',$buyer);
    }

    public function getData(Request $req){
        $submit = $req->submitdate;

        $from = date_format(date_create(trim(explode("-",$submit)[0])),'Y-m-d 00:00:00');
        $to = date_format(date_create(trim(explode("-",$submit)[1])),'Y-m-d 23:59:59');

        // dd($from,$to);
        $data = DB::table('trf_testings')
                            ->join('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                            ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                            ->join('master_category','trf_testings.id_category','master_category.id')
                            ->whereBetween('trf_testings.created_at',[$from,$to])
                            ->where([
                                'trf_testings.factory_id'=>$req->factory_id,
                                'trf_testings.buyer'=>$req->buyer,
                                'trf_testings.asal_specimen'=>$req->asal
                            ])
                            // ->where('trf_testings.factory_id',$req->factory_id)
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->select(
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.id',
                                'trf_testings.result_t2',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen',
                                DB::raw("string_agg(concat(master_method.method_code,'-',master_method.method_name), '<br>') as methods")
                            )->groupBy([
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.id',
                                'trf_testings.result_t2',
                                'trf_testings.id_category',
                                'trf_testings.username',
                                'trf_testings.closed_at',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen'
                            ])
                            ->orderBy('created_at','desc')
                            ->get();
                                // dd($data,(int)$req->factory_id,$req->asal,$req->buyer);
        return DataTables::of($data)
                                ->editColumn('trf_id',function($data){
                                    return '<label style="font-weight:bold">'.$data->trf_id.'</label> <br>
                                    <label>Submit Date : </label> '.date_format(date_create($data->created_at),'d-M-Y H:i:s');
                                })
                                ->editColumn('category',function($data){
                                    return '<label style="font-weight:bold"> Category : </label> '.$data->category.'<br>
                                    <label style="font-weight:bold"> Category Specimen : </label> '.$data->category_specimen.'<br>
                                    <label style="font-weight:bold"> Type Specimen : </label>'.$data->type_specimen;
                                })
                                ->addColumn('specimen',function($data){
                                    $spc = TrfTestingDocument::where('trf_id',$data->id)->whereNull('deleted_at')->first();

                                    return HelperController::setDoc($spc);
                                })
                                ->editColumn('lab_location',function($data){
                                    return $data->lab_location.'<br>'.$data->asal_specimen;
                                })
                                ->editColumn('nik',function($data){
                                    return $data->username.'<br> ('.$data->nik.')';
                                })
                                ->editColumn('status',function($data){
                                    return HelperController::statusCek($data->id);
                                })
                                ->addColumn('return_status',function($data){
                                    $checkret = TrfReturnSample::where('trf_id',$data->id)->whereNull('deleted_at')->first();

                                    if($checkret!=null){
                                        return '<span class="label bg-primary">Returned at '.date_format(date_create($checkret->created_at),'Y-m-d').'<br> By '.$checkret->user_name.' ('.$checkret->user_nik.')</span>';
                                    }else{
                                        return '<span class="label bg-warning">Not Yet Returned </span>';
                                    }
                                })
                                ->rawColumns(['trf_id','category','specimen','lab_location','nik','status','return_status'])
                                ->make(true);
    }

    public function scanBarcodeReturn(Request $req){
        $barcode = trim($req->barcode);

        $trf = TrfTesting::where('trf_id',$barcode)->whereNull('deleted_at')->first();
        
        if ($trf==null) {
            return response()->json("TRF not found ! ! !",422);
        }

        if ($trf->return_test_sample!=true) {
            return response()->json("TRF not to return sample ! ! !",422);
        }

        if ($trf->status!="CLOSED") {
            return response()->json("TRF status ".$trf->status." ! ! !",422);
        }

        try {

            DB::beginTransaction();
            $check = TrfReturnSample::where('trf_id',$trf->id)->exists();

            if($check){
               $response = [
                'status'=>422,
                'output'=>"TRF already return ! ! !"
               ];
            }else{
                TrfReturnSample::firstOrCreate([
                    'trf_id'=>$trf->id,
                    'user_nik'=>$req->session()->get('nik'),
                    'user_name'=>$req->session()->get('name')
                ]);
    
                $response = [
                    'status'=>200,
                    'output'=>"TRF ".$trf->trf_id." success to return by ".$req->session()->get('name')." (".$req->session()->get('nik').")"
                   ];
            }

            
            DB::commit();
        } catch (Exception $th) {
            $response = [
                'status'=>422,
                'output'=>"TRF failed to return ".$th->getMessage()." ! ! !"
               ];
        }

        return response()->json(['data'=>$response]);
    }
}
