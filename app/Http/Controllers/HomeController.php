<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function downloadApk(){
        try {
            $path = storage_path('apk/DIGILAB_1_1.0.apk');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}
