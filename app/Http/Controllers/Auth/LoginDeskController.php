<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use guest;
use DB;
use Session;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;


use App\Models\Employee;

class LoginDeskController extends Controller
{
    protected $redirectTo = '/self';


    public function showLogin(){
        return view('selfdesk.login');
    }

   
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginDesk(Request $req){
        $this->validate($req,[
            'nik' => 'required|string',
            'password' => 'required|string',
        ],[
            'nik.required' => 'NIK is required',
            'password.required' => 'Password is required',
        ]);

        $user = Employee::where('nik',$req->nik)->first();

        if (isset($user)) {
            if ($user->status=="aktif") {
                if ($user->password==trim($req->password)) {
                    if (auth::guard('desk')->loginUsingId($user->nik)) { 
                        $req->session()->put('nik',$user->nik);
                        $req->session()->put('name',$user->name);
                        // $req->session()->put('factory_id',substr($user->factory, 3));

                        switch (trim($user->factory)) {
                            case 'AOI1':
                                $factory_id="1";
                                break;

                            case 'AOI2':
                                $factory_id="2";
                                break;

                            case 'AOI3':
                                $factory_id="3";
                                break;

                            case 'DC':
                                $factory_id="4";
                                break;

                            case 'AOI3EXT':
                                $factory_id="5";
                                break;
                            
                        }

                        $req->session()->put('factory_id',$factory_id);
                        return redirect('/desk');
                    }else{
                        Auth::logout();
                        return redirect()->back()
                            ->withErrors([
                                'notif' => 'Login Failed ! ! !',
                            ]);
                    }
                }else{
                    Auth::logout();
                        return redirect()->back()
                            ->withErrors([
                                'notif' => 'Wrong Password ! ! !',
                            ]);
                }
                
            }else{
                Auth::logout();
                return redirect()->back()
                    ->withErrors([
                        'notif' => 'Your account is not active ! ! !',
                    ]);
            }
        }else{
            Auth::logout();
            return redirect()->back()
                ->withErrors([
                    'notif' => 'Your account is not found ! ! !',
                ]);
        }
        
    }

    public function logoutAction(Request $request){
        auth::logout();
        $request->session()->invalidate();

        return redirect('/desk/login');
    }
}
