<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\TrfReturnSample;
use DB;
use Auth;
use Carbon\Carbon;
use DataTables;
use Exception;

class ReturnSampleController extends Controller
{
    public function index(){
        return view('return_sample.index');
    }

    public function scanNikPic(Request $req){
        $nik = trim($req->nik);

        $employee = Employee::where('nik',$nik)->first();

        if($employee!=null){
            return response()->json([
                                    'status'=>200,
                                    'output'=>['nik'=>$employee->nik,'name'=>$employee->name]
                                ]);
        }else{
            return response()->json([
                                    'status'=>422,
                                    'output'=>"PIC not found ! ! !"
                                ]);
        }
    }

    public function scanTrf(Request $req){
        $key = trim($req->key);

        $length = strlen($key);
   
        if($length<6){
            return response("TRF ID too short ! ! !",422);
        }

        $data = DB::table('trf_testings')
                                ->join('trf_testing_document','trf_testings.id','trf_testing_document.trf_id')
                                ->join('trf_testing_methods','trf_testings.id','trf_testing_methods.trf_id')
                                ->join('master_method','trf_testing_methods.master_method_id','master_method.id')
                                ->join('master_category','trf_testings.id_category','master_category.id')
                                // ->where(function($query) use ($key){
                                //     $query->where('trf_testings.trf_id',$key)
                                //             ->orWhere('trf_testings.id',$key);
                                // })
                                ->where('trf_testings.trf_id',$key)
                                ->whereNull('trf_testings.deleted_at')
                                ->whereNull('trf_testing_document.deleted_at')
                                ->whereNull('trf_testing_methods.deleted_at')
                                ->groupBy([
                                    'trf_testings.id',
                                    'trf_testings.trf_id',
                                    'trf_testings.buyer',
                                    'trf_testings.lab_location',
                                    'trf_testings.platform',
                                    'trf_testings.factory_id',
                                    'trf_testings.asal_specimen',
                                    'trf_testings.return_test_sample',
                                    'trf_testings.status',
                                    'trf_testings.created_at',
                                    'trf_testings.nik',
                                    'trf_testings.username',
                                    'master_category.category',
                                    'master_category.category_specimen',
                                    'master_category.type_specimen',
                                    'trf_testing_document.id',
                                    'trf_testing_document.document_type',
                                    'trf_testing_document.document_no',
                                    'trf_testing_document.style',
                                    'trf_testing_document.article_no',
                                    'trf_testing_document.size',
                                    'trf_testing_document.color',
                                    'trf_testing_document.fibre_composition',
                                    'trf_testing_document.fabric_finish',
                                    'trf_testing_document.gauge',
                                    'trf_testing_document.fabric_weight',
                                    'trf_testing_document.plm_no',
                                    'trf_testing_document.care_instruction',
                                    'trf_testing_document.manufacture_name',
                                    'trf_testing_document.export_to',
                                    'trf_testing_document.nomor_roll',
                                    'trf_testing_document.batch_number',
                                    'trf_testing_document.item',
                                    'trf_testing_document.barcode_supplier',
                                    'trf_testing_document.additional_information',
                                    'trf_testing_document.yds_roll',
                                    'trf_testing_document.description',
                                    'trf_testing_document.season',
                                    'trf_testing_document.deleted_by',
                                    'trf_testing_document.invoice',
                                    'trf_testing_document.fabric_color',
                                    'trf_testing_document.interlining_color',
                                    'trf_testing_document.qty',
                                    'trf_testing_document.machine',
                                    'trf_testing_document.temperature',
                                    'trf_testing_document.pressure',
                                    'trf_testing_document.duration',
                                    'trf_testing_document.style_name',
                                    'trf_testing_document.fabric_item',
                                    'trf_testing_document.fabric_type',
                                    'trf_testing_document.thread',
                                    'trf_testing_document.material_shell_panel',
                                    'trf_testing_document.product_category',
                                    'trf_testing_document.remark',
                                    'trf_testing_document.pad',
                                    'trf_testing_document.pelling',
                                    'trf_testing_document.component',
                                    'trf_testing_document.garment_size',
                                    'trf_testing_document.sample_type',
                                    'trf_testing_document.test_condition',
                                    'trf_testing_document.fabric_properties',
                                    'trf_testing_document.lot',
                                    'trf_testing_document.po_buyer',
                                    'trf_testing_document.arrival_date'
                                ])
                                ->select(
                                    'trf_testings.id',
                                    'trf_testings.trf_id',
                                    'trf_testings.buyer',
                                    'trf_testings.lab_location',
                                    'trf_testings.platform',
                                    'trf_testings.factory_id',
                                    'trf_testings.asal_specimen',
                                    'trf_testings.return_test_sample',
                                    'trf_testings.status',
                                    'trf_testings.created_at',
                                    'trf_testings.nik',
                                    'trf_testings.username',
                                    'master_category.category',
                                    'master_category.category_specimen',
                                    'master_category.type_specimen',
                                    'trf_testing_document.document_type',
                                    'trf_testing_document.document_no',
                                    'trf_testing_document.style',
                                    'trf_testing_document.article_no',
                                    'trf_testing_document.size',
                                    'trf_testing_document.color',
                                    'trf_testing_document.fibre_composition',
                                    'trf_testing_document.fabric_finish',
                                    'trf_testing_document.gauge',
                                    'trf_testing_document.fabric_weight',
                                    'trf_testing_document.plm_no',
                                    'trf_testing_document.care_instruction',
                                    'trf_testing_document.manufacture_name',
                                    'trf_testing_document.export_to',
                                    'trf_testing_document.nomor_roll',
                                    'trf_testing_document.batch_number',
                                    'trf_testing_document.item',
                                    'trf_testing_document.barcode_supplier',
                                    'trf_testing_document.additional_information',
                                    'trf_testing_document.yds_roll',
                                    'trf_testing_document.description',
                                    'trf_testing_document.season',
                                    'trf_testing_document.deleted_by',
                                    'trf_testing_document.invoice',
                                    'trf_testing_document.fabric_color',
                                    'trf_testing_document.interlining_color',
                                    'trf_testing_document.qty',
                                    'trf_testing_document.machine',
                                    'trf_testing_document.temperature',
                                    'trf_testing_document.pressure',
                                    'trf_testing_document.duration',
                                    'trf_testing_document.style_name',
                                    'trf_testing_document.fabric_item',
                                    'trf_testing_document.fabric_type',
                                    'trf_testing_document.thread',
                                    'trf_testing_document.material_shell_panel',
                                    'trf_testing_document.product_category',
                                    'trf_testing_document.remark',
                                    'trf_testing_document.pad',
                                    'trf_testing_document.pelling',
                                    'trf_testing_document.component',
                                    'trf_testing_document.garment_size',
                                    'trf_testing_document.sample_type',
                                    'trf_testing_document.test_condition',
                                    'trf_testing_document.fabric_properties',
                                    'trf_testing_document.lot',
                                    'trf_testing_document.po_buyer',
                                    'trf_testing_document.arrival_date',
                                    DB::raw("trf_testing_document.id as trf_document_id"),
                                    DB::raw("string_agg(concat(master_method.method_code,'-',master_method.method_name), '<br>') as methods")
                                )->first();

        //    dd($data);

                if($data==null){
                    return response()->json("TRF Not Found ! ! !",422);
                }  
                
                if($data->return_test_sample==false){
                    return response()->json("TRF Not to returned ! ! !",422);
                }  
                
                if($data->status!="CLOSED"){
                    return response()->json("TRF status ".$data->status." ! ! !",422);
                }  


                $check = TrfReturnSample::where('trf_id',$data->id)->exists();

                if ($check) {
                    return response()->json("TRF already return ! ! !",422);
                }

        try {
           DB::beginTransaction();
                
          

        
                TrfReturnSample::firstOrCreate([
                    'trf_id'=>$data->id,
                    'user_nik'=>auth::user()->nik,
                    'user_name'=>auth::user()->name
                ]);

     

                
           
               



           DB::commit();
           return view('return_sample._list',['data'=>$data]);
        } catch (Exception $th) {

               return response()->json("TRF failed to return ".$th->getMessage()." ! ! !",422);
        }

    }
}
