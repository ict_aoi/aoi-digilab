<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use DataTables;

use App\Models\MasterMethode;
use App\Models\MasterSpiralityGarment;
use App\Models\TrfTesting;
use App\Models\TrfTestingMethods;
// use App\Models\ModelsTestingResult;
use App\Models\TestingResultGarmentWash;
use App\Models\masterRequirementModel;
use App\Models\TestingResult;
use App\Models\supplierResult;
use App\Http\Controllers\HelperController;
use App\Models\MasterDimenApp;
use App\Models\TrfTestingDocument;
class TrfTestingController extends Controller
{
    public function index(){
        $methods = MasterMethode::whereNull('deleted_at')->orderby('category','asc')->get();

        return view('trf_testing_methods.index')->with('methods',$methods);
    }

    public function getMeth(Request $request){
        $id = $request->id;

        $cekSpr = MasterDimenApp::where('method_id',$id)->whereNull('deleted_at')->exists();

        if ($cekSpr==false) {
            return view('trf_testing_methods.detail_methods')->with('id',$id);
        }else{
            return view('trf_testing_methods.detail_garment_wash_methods')->with('id',$id);
        }
    } 

    public function getTrfMethNospr(Request $request){
        switch (auth::user()->factory_id) {
            case '1':
                $loc = "AOI1";
                break;

            case '2':
                $loc = "AOI2";
                break;

        }
        $data = DB::table('ns_trf_req_result')

                            ->where('master_method_id',$request->id)
                            ->where(function($query){
                                $query->whereIn('status',['ONPROGRESS','VERIFIED'])
                                        ->orwhereDate('updated_at',carbon::now()->format('Y-m-d'));
                            })
                            ->where('lab_location',$loc)
                            ->orderBy('created_at','asc');

        return Datatables::of($data)
                            ->addColumn('specimen',function($data){
                                

                                // return "<b>".$data->document_type."</b> : ".$data->document_no."</br> <b>Style</b> : ".$data->style." </br> <b>Article</b> : ".$data->article_no." </br> <b>Size</b> : ".$data->size." </br> <b>Color</b> : ".$data->color;

                                $doc = TrfTestingDocument::where('id',$data->trf_doc_id)->whereNull('deleted_at')->first();

                                return HelperController::setDoc($doc);

                            })
                            ->editColumn('value_testing',function($data){
                                if (isset($data->result_id)) {                                     
                                     return HelperController::setInputResult($data->operator,$data->result_id,$data->value_testing,$data->id);
                                }else{
                                    return '';
                                }
                            })

                            ->addColumn('action',function($data){
                                return view('_action',[
                                            'model'=>$data,
                                            'setReq'=>route('trftesting.setRequir',[
                                                                'trf_id'=>$data->id,
                                                                'trf_meth_id'=>$data->trf_method_id,
                                                                'trf_doc_id'=>$data->trf_doc_id
                                                            ]),
                                            // 'setReq'=>[
                                            //             'trf_id'=>$data->id,
                                            //             'trf_meth_id'=>$data->trf_method_id,
                                            //             'trf_doc_id'=>$data->trf_doc_id
                                            //         ]
                                        ]);
                            })
                            ->editColumn('value1',function($data){
                                if (isset($data->requirement_id)) {
                                    return HelperController::setReqOption($data->requirement_id);

                                }else{
                                    return "";
                                }

                                
                            })
                            ->addColumn('supplier_result',function($data){
                                $t2val = supplierResult::where('trf_method_id',$data->trf_method_id)->where('trf_document_id',$data->trf_doc_id)->whereNull('deleted_at')->first();
                                $rest = isset($t2val->result_value) ? $t2val->result_value :null;
                                return '<input type="text" id="t2result" class="form-control inputrest" data-methid="'.$data->trf_method_id.'" data-docid="'.$data->trf_doc_id.'" data-trfid="'.$data->id.'"  value="'.$rest.'" placeholder="T2 Result">';
                            })
                            ->addColumn('testing_result',function($data){
                                if (isset($data->value_testing)) {
                                    $reqVal = HelperController::operatorVal($data->requirement_id);
                                    return HelperController::setResult($reqVal,$data->operator,$data->value_testing);
                                }else{
                                    return "";
                                }
                                

                            })
                            ->rawColumns(['specimen','action','value_testing','supplier_result','testing_result'])
                            ->make(true);
    }

    public function setRequir(Request $request){
        $trf_id = $request->trf_id;
        $trf_meth_id = $request->trf_meth_id;
        $trf_doc_id = $request->trf_doc_id;

        $data = DB::table('ns_trf_req_result')->where('id',$trf_id)->where('trf_method_id',$trf_meth_id)->where('trf_doc_id',$trf_doc_id)->first();

        return view('trf_testing_methods.set_requirement')->with('data',$data);
    }

    public function getRequirments(Request $request){
        // $trf_id = $request->id;
        $methid = $request->method_id;
        $category = $request->category;
        $category_specimen = $request->category_specimen;
    

        $list = DB::table('ns_master_req_category')
                        ->where('master_method_id',$methid)
                        ->where('category',$category)
                        ->where('category_specimen',$category_specimen)
                        ->select('type_specimen','komposisi_specimen','parameter','perlakuan_test','id')
                        ->orderBy('sequence','asc');

        return DataTables::of($list)
                            ->addColumn('checkbox',function($list){
                                return '<input type="checkbox" data-id="'.$list->id.'" name="selector[]" id="InputSelector" class="idReq" value="'.$list->id.'">';
                            })
                            ->rawColumns(['checkbox'])->make(true);
       
    }

    public function getFilType(Request $request){

        // dd($request->methodid,$request->category,$request->category_spes);
        $data = DB::table('ns_master_req_category')
                        ->where('master_method_id',$request->methodid)
                        ->where('category',$request->category)
                        ->where('category_specimen',$request->category_spes)
                        ->groupBy('type_specimen')
                        ->select('type_specimen')->get();

        return response()->json(['data'=>$data],200);
    }

    public function getFilKomp(Request $request){

        // dd($request->methodid,$request->category,$request->category_spes);
        $data = DB::table('ns_master_req_category')
                        ->where('master_method_id',$request->methodid)
                        ->where('category',$request->category)
                        ->where('category_specimen',$request->category_spes)
                        ->where('type_specimen',$request->type_specimen)
                        ->groupBy('komposisi_specimen')
                        ->select('komposisi_specimen')->get();

        return response()->json(['data'=>$data],200);
    }

    public function getFilParam(Request $request){

        // dd($request->methodid,$request->category,$request->category_spes);
        $data = DB::table('ns_master_req_category')
                        ->where('master_method_id',$request->methodid)
                        ->where('category',$request->category)
                        ->where('category_specimen',$request->category_spes)
                        ->where('type_specimen',$request->type_specimen)
                        ->where('komposisi_specimen',$request->komposisi)
                        ->groupBy('parameter')
                        ->select('parameter')->get();

        return response()->json(['data'=>$data],200);
    }

    public function getFilTest(Request $request){

        // dd($request->methodid,$request->category,$request->category_spes);
        $data = DB::table('ns_master_req_category')
                        ->where('master_method_id',$request->methodid)
                        ->where('category',$request->category)
                        ->where('category_specimen',$request->category_spes)
                        ->where('type_specimen',$request->type_specimen)
                        ->where('komposisi_specimen',$request->komposisi)
                        ->where('parameter',$request->parameter)
                        ->groupBy('perlakuan_test')
                        ->select('perlakuan_test')->get();

        return response()->json(['data'=>$data],200);
    }
 
    public function addRequirment(Request $request){
        $id = $request->trf_id;
        $trf_meth_id = $request->trf_meth_id;
        $trf_doc_id = $request->trf_doc_id;
        $requirment_id = $request->requirment;

        $cekResl = TestingResult::where('trf_method_id',$trf_meth_id)->where('trf_document_id',$trf_doc_id)->whereNull('deleted_at')->exists();

        if ($cekResl) {
            return response()->json("Requirement Is Already ! ! !",422);
        }

        try {
            DB::beginTransaction();

                TestingResult::FirstOrCreate([
                                    'trf_method_id'=>$trf_meth_id,
                                    'trf_document_id'=>$trf_doc_id,
                                    'requirement_id'=>$requirment_id,
                                    'created_by'=>auth::user()->nik
                                ]);
                $cekTrf = TrfTesting::where('id',$id)->whereNull('deleted_at')->first();

                if ($cekTrf->status=="VERIFIED") {
                    TrfTesting::where('id',$id)->update(['status'=>'ONPROGRESS','updated_at'=>carbon::now()]);
                }


                $data_response = ['status'=>200,"output"=>"Add Requirement Success ! ! ! "];
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = ['status'=>422,"output"=>"Add Requirement Field ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function addResultValue(Request $request){
        $reslid = $request->restid;
        $value = $request->val;
        $trfid = $request->trfid;

        try {
            DB::beginTransaction();
                $updt = TestingResult::where('id',$reslid)->update(['value_testing'=>$value]);

                if ($updt) {
                    HelperController::checkSetTrfStatus($trfid);
                }
                $data_response = ['status'=>200,"output"=>"Add Result Value Success ! ! ! "];
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = ['status'=>422,"output"=>"Add Result Value Field ! ! ! ".$message];
        }

        return response()->json(['data'=>$data_response]);
    }


    public function getTrfMethSpr(Request $request){
        switch (auth::user()->factory_id) {
            case '1':
                $loc = "AOI1";
                break;

            case '2':
                $loc = "AOI2";
                break;

        }

        $data = DB::table('ns_trf_req_result')
                            ->where('master_method_id',$request->id)
                            ->whereIn('status',['ONPROGRESS','VERIFIED'])
                            ->where('lab_location',$loc)
                            ->groupBy(['id','trf_id','buyer','lab_location','factory_id','asal_specimen','category_specimen','category','status','master_method_id','method_code','trf_doc_id'])
                            ->select('id','trf_id','buyer','lab_location','factory_id','asal_specimen','category_specimen','category','status','master_method_id','method_code','trf_doc_id');
        
        return DataTables::of($data)
                            ->addColumn('specimen',function($data){
                                $doc = TrfTestingDocument::where('id',$data->trf_doc_id)->first();
                                return HelperController::setDoc($doc);
                            })
                            ->addColumn('testing_result',function($data){
                                return $data->id;
                            })
                            ->addColumn('action',function($data){
                                return view('_action',[
                                            'model'=>$data,
                                            'spcTest'=>route('trftesting.inputSpcTest',['data'=>$data])
                                        ]);
                            })
                            ->rawColumns(['specimen','testing_result','action'])->make(true);


    }

    public function inputSpcTest(Request $request){
        $data = $request->data;

        $Masdms = MasterDimenApp::where('method_id',$data['master_method_id'])->where('category_specimen','FABRIC')->groupBy('method_id')->groupBy('category_specimen')->select('method_id',DB::raw("string_agg(id,',') as ids"),DB::raw("string_agg(parameter,',') parameter"))->first();
        // $getSpc = $Masdms->whereNull('deleted_at')->get();
        // $getIdparam = $Masdms->groupBy('method_id')->groupBy('category_specimen')->select(DB::raw("string_agg(id,',') as ids"),DB::raw("string_agg(parameter,',') parameter"))->first();

        $list = DB::table('ns_trf_req_result')->where('id',$data['id'])->where('master_method_id',$data['master_method_id'])->whereNotNull('result_id');

        

        return view('trf_testing_methods.specialtest')->with('id',$data['master_method_id'])->with('idparam',$Masdms)->with('list',$list);
    }
}
