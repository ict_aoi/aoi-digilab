<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('print/detail-trf','ApiMiddleController@getDetailTrf');
Route::get('print/barcode','ApiMiddleController@printBarcode');
Route::get('print/report-specimen','ApiMiddleController@reportSpecimentResult');