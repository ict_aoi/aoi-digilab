<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
    	return redirect()->route('home.index');
    }

    return redirect('/login');
});

Route::get('/auth', 'Auth\LoginController@showLogin')->name('auth.showLogin');
Route::post('/auth/loginaction', 'Auth\LoginController@loginAction')->name('auth.loginAction');
Route::post('/auth/logoutaction', 'Auth\LoginController@logoutAction')->name('auth.logoutAction');

Auth::routes();

Route::middleware('auth')->group(function(){
	Route::prefix('/dashboard')->group(function(){
		Route::get('', 'DashboardController@index')->name('home.index');
		Route::get('/get-data', 'DashboardController@getData')->name('home.getData');

		Route::get('/print-barcode', 'DashboardController@printBarcode')->name('home.printBarcode');
		Route::get('/print-detail', 'DashboardController@printDetail')->name('home.printDetail');
		Route::get('/print-report', 'DashboardController@report')->name('home.report');
	});
	

	//account setting
	Route::prefix('/user')->group(function(){
		Route::get('/myAccount', 'User\AccountController@myAccount')->name('account.myAccount');
		Route::post('/myAccount/edit', 'User\AccountController@editPassword')->name('account.editPassword');
	});

	Route::prefix('/admin')->middleware(['permission:menu-user-management'])->group(function(){
		//user account
		Route::get('/user-account', 'Admin\AdminController@user_account')->name('admin.user_account');
		Route::get('/user-account/get-data', 'Admin\AdminController@getDataUser')->name('admin.getDataUser');
		Route::get('/user-account/ajaxGetRole', 'Admin\AdminController@ajaxGetRole')->name('admin.ajaxGetRole');
		Route::get('/user-account/add-user', 'Admin\AdminController@addUser')->name('admin.addUser');
		Route::get('/user-account/edit', 'Admin\AdminController@formEditUser')->name('admin.formEditUser');
		Route::get('/user-account/edit/edit-user', 'Admin\AdminController@editAccount')->name('admin.editAccount');
		Route::get('/user-account/edit/reset-password', 'Admin\AdminController@passwordReset')->name('admin.passwordReset');
		Route::get('/user-account/innactive', 'Admin\AdminController@innactiveUser')->name('admin.innactiveUser');
		Route::get('/user-account/factory', 'Admin\AdminController@getFactory')->name('admin.getFactory');
		Route::get('/user-account/getDataNik', 'Admin\AdminController@getDataNik')->name('admin.getDataNik');

		Route::get('/user-account/getDataEmployee', 'Admin\AdminController@getDataEmployee')->name('admin.getDataEmployee');


		
		//role
		Route::get('/role', 'Admin\AdminController@formRole')->name('admin.formRole');
		Route::get('/role/get-data', 'Admin\AdminController@ajaxRoleData')->name('admin.ajaxRoleData');
		Route::get('/role/new-role', 'Admin\AdminController@newRole')->name('admin.newRole');
		Route::post('/role/add-role', 'Admin\AdminController@addRole')->name('admin.addRole');
		Route::get('/role/edit', 'Admin\AdminController@editRoleUser')->name('admin.editRoleUser');
		Route::post('/role/update-role', 'Admin\AdminController@updateRole')->name('admin.updateRole');
	});

	//master
	Route::prefix('master')->group(function(){
		Route::prefix('master-method')->middleware(['permission:menu-master'])->group(function(){
			Route::get('', 'Master\MasterDataMethodController@index')->name('method.index');
			Route::get('export', 'Master\MasterDataMethodController@export')->name('method.export');
			Route::post('import', 'Master\MasterDataMethodController@import')->name('method.import');
			Route::get('excel', 'Master\MasterDataMethodController@excel')->name('method.excel');
			Route::get('data', 'Master\MasterDataMethodController@data')->name('method.data');
			Route::post('delete', 'Master\MasterDataMethodController@delete')->name('method.delete');
			Route::post('update', 'Master\MasterDataMethodController@update')->name('method.update');
			Route::post('add', 'Master\MasterDataMethodController@addMethod')->name('method.addMethod');
		});


		Route::prefix('master-category')->middleware(['permission:menu-master'])->group(function(){
			Route::get('', 'Master\MasterDataCategoryController@index')->name('category.index');
			Route::get('export', 'Master\MasterDataCategoryController@export')->name('category.export');
			Route::post('import', 'Master\MasterDataCategoryController@import')->name('category.import');
			Route::get('excel', 'Master\MasterDataCategoryController@excel')->name('category.excel');
			Route::get('data', 'Master\MasterDataCategoryController@data')->name('category.data');
			Route::post('delete', 'Master\MasterDataCategoryController@delete')->name('category.delete');
			Route::get('edit', 'Master\MasterDataCategoryController@edit')->name('category.edit');
			Route::post('update', 'Master\MasterDataCategoryController@update')->name('category.update');
			Route::post('add', 'Master\MasterDataCategoryController@addCategory')->name('category.addCategory');
		});

		Route::prefix('/master-requirements')->middleware(['permission:menu-master'])->group(function(){
			Route::get('', 'Master\MasterDataRequirementController@index')->name('requirements.index');
			Route::get('data', 'Master\MasterDataRequirementController@data')->name('requirements.data');
			Route::get('data/detail', 'Master\MasterDataRequirementController@getDataDetail')->name('requirements.getDataDetail');	
			Route::post('update', 'Master\MasterDataRequirementController@update')->name('requirements.update');
			Route::post('delete', 'Master\MasterDataRequirementController@delete')->name('requirements.delete');
			Route::get('export', 'Master\MasterDataRequirementController@export')->name('requirements.export');
			Route::post('import', 'Master\MasterDataRequirementController@import')->name('requirements.import');
			Route::get('excel', 'Master\MasterDataRequirementController@excel')->name('requirements.excel');

			Route::get('getMeth', 'Master\MasterDataRequirementController@getMethCtg')->name('requirements.getMethCtg');
			Route::get('getCtgSpcType', 'Master\MasterDataRequirementController@getCtgSpcType')->name('requirements.getCtgSpcType');
			Route::post('addRequirment', 'Master\MasterDataRequirementController@addRequirment')->name('requirements.addRequirment');
	
		});

		
		Route::prefix('master-dimsional')->middleware(['permission:menu-master'])->group(function(){
			Route::get('', 'Master\MasterDimenAppController@index')->name('dimensiapp.index');
			Route::get('data', 'Master\MasterDimenAppController@getData')->name('dimensiapp.getData');
			Route::get('edit/data', 'Master\MasterDimenAppController@getDataEdit')->name('dimensiapp.getDataEdit');
			Route::post('edit/data/updateData', 'Master\MasterDimenAppController@updateData')->name('dimensiapp.updateData');
			Route::get('export', 'Master\MasterDimenAppController@export')->name('dimensiapp.export');
			Route::post('import', 'Master\MasterDimenAppController@import')->name('dimensiapp.import');
			Route::get('excel', 'Master\MasterDimenAppController@excel')->name('dimensiapp.excel');
			Route::post('delete', 'Master\MasterDimenAppController@delete')->name('dimensiapp.delete');
			Route::get('getMeth', 'Master\MasterDimenAppController@getMeth')->name('dimensiapp.getMeth');
			Route::get('getCtgParm', 'Master\MasterDimenAppController@getCtgParm')->name('dimensiapp.getCtgParm');
			Route::post('add', 'Master\MasterDimenAppController@addMapp')->name('dimensiapp.addMapp');

		});


		Route::prefix('master-holiday')->middleware(['permission:menu-master'])->group(function(){
			Route::get('', 'Master\MasterHolidayController@index')->name('holiday.index');
			Route::get('/data', 'Master\MasterHolidayController@getData')->name('holiday.getData');
			Route::post('/add', 'Master\MasterHolidayController@add')->name('holiday.add');	
			Route::post('/delete', 'Master\MasterHolidayController@delete')->name('holiday.delete');			

		});
	

	 
	});

	Route::prefix('trf')->group(function(){
		Route::prefix('/create')->middleware(['permission:menu-create-trf'])->group(function(){
			Route::get('','Trf\CreateTrfController@index')->name('trfcreate.index');
			Route::get('/get-data','Trf\CreateTrfController@getData')->name('trfcreate.getData');
			Route::get('/form-create','Trf\CreateTrfController@formCreate')->name('trfcreate.formCreate');
			Route::get('/form-create/getCtg','Trf\CreateTrfController@getCtg')->name('trfcreate.getCtg');
			Route::get('/form-create/getCtgSpc','Trf\CreateTrfController@getCtgSpc')->name('trfcreate.getCtgSpc');
			Route::get('/form-create/getTypeSpc','Trf\CreateTrfController@getTypeSpc')->name('trfcreate.getTypeSpc');
			Route::get('/form-create/getMeth','Trf\CreateTrfController@getMeth')->name('trfcreate.getMeth');

			Route::post('/form-create/createTrf','Trf\CreateTrfController@createTrf')->name('trfcreate.createTrf');

			Route::get('/print-barcode','Trf\CreateTrfController@printBarcode')->name('trfcreate.printBarcode');
			Route::get('/print-detail','Trf\CreateTrfController@printDetail')->name('trfcreate.printDetail');


			Route::get('/get-erp','Trf\CreateTrfController@getDocument')->name('trfcreate.getDocument');

			Route::get('/mapsModalDoc','Trf\CreateTrfController@mapsModalDoc')->name('trfcreate.mapsModalDoc');

		});

	
		Route::prefix('/dashboard-trf')->middleware(['permission:menu-trf|menu-create-trf'])->group(function(){
			Route::get('','Trf\DashboardTrfController@index')->name('dashboardTrf.index');
			Route::get('data', 'Trf\DashboardTrfController@data')->name('dashboardTrf.data');
			Route::post('approval', 'Trf\DashboardTrfController@approval')->name('dashboardTrf.approval');
 
			// Route::get('set-methode','Trf\DashboardTrfController@setMethode')->name('dashboardTrf.setMethode');
			// Route::get('set-methode/get-data','Trf\DashboardTrfController@dataSetMeth')->name('dashboardTrf.dataSetMeth');
			
			Route::get('set-methode/getMethode','Trf\DashboardTrfController@getMethode')->name('dashboardTrf.getMethode');
			Route::get('set-methode/getTrfMEth','Trf\DashboardTrfController@getTrfMEth')->name('dashboardTrf.getTrfMEth');
			Route::post('set-methode/update','Trf\DashboardTrfController@updateMth')->name('dashboardTrf.updateMth');

			


			Route::get('edit-trf','Trf\DashboardTrfController@editTrf')->name('dashboardTrf.editTrf');
			Route::get('edit-trf/getCtgMethReq','Trf\DashboardTrfController@getCtgMethReq')->name('dashboardTrf.getCtgMethReq');
			Route::get('edit-trf/mapsModalDoc','Trf\DashboardTrfController@mapsModalDoc')->name('dashboardTrf.mapsModalDoc');
			Route::post('edit-trf/updateTrf','Trf\DashboardTrfController@updateTrf')->name('dashboardTrf.updateTrf');
			

		});


	


		Route::prefix('/result')->middleware(['permission:menu-testing-trf'])->group(function(){
			Route::get('','Result\ResultController@index')->name('result.index');
			Route::get('get-data','Result\ResultController@getDataIndex')->name('result.getDataIndex');
			Route::get('set-requirment','Result\ResultController@setRequirement')->name('result.setRequirement');
			Route::get('set-requirment/get-data','Result\ResultController@getSetData')->name('result.getSetData');
			Route::get('set-requirment/getTestTreat','Result\ResultController@getTestTreat')->name('result.getTestTreat');
			Route::get('set-requirment/getParmater','Result\ResultController@getParmater')->name('result.getParmater');
			Route::post('set-requirment/createRequirement','Result\ResultController@createRequirement')->name('result.createRequirement');
			Route::get('set-requirment/delRequirement','Result\ResultController@delRequirement')->name('result.delRequirement');
			Route::get('set-requirment/inactive','Result\ResultController@inactiveRequirement')->name('result.inactiveRequirement');

			Route::post('reopen','Result\ResultController@reOpen')->name('result.reOpen');


			Route::get('set-result','Result\ResultController@setResult')->name('result.setResult');
			Route::get('set-result/get-data','Result\ResultController@getResultReg')->name('result.getResultReg');
			Route::get('set-result/input','Result\ResultController@formInput')->name('result.formInput');


			Route::get('set-result/get-form-input','Result\ResultController@inputRest')->name('result.inputRest');
			Route::post('set-result/saveResultReg','Result\ResultController@saveResultReg')->name('result.saveResultReg');
			Route::get('set-result/image','Result\ResultController@getImage')->name('result.getImage');
			Route::post('set-result/delResultReg','Result\ResultController@delResultReg')->name('result.delResultReg');

	


		});

		Route::prefix('createTrf')->middleware(['permission:menu-create-trf'])->group(function(){
			Route::get('','Trf\CreateController@index')->name('trf.create.index');
			Route::get('getData','Trf\CreateController@getData')->name('trf.create.getData');
			Route::get('create','Trf\CreateController@createTrf')->name('trf.create.createTrf');

			Route::get('getCategory','Trf\CreateController@getCategory')->name('trf.create.getCategory');
			Route::get('getCategorySpc','Trf\CreateController@getCategorySpc')->name('trf.create.getCategorySpc');
			Route::get('getTypeSpc','Trf\CreateController@getTypeSpc')->name('trf.create.getTypeSpc');
			Route::get('getMethod','Trf\CreateController@getMethod')->name('trf.create.getMethod');

			Route::get('getDataSpecimen','Trf\CreateController@getDataSpecimen')->name('trf.create.getDataSpecimen');

			Route::post('submitTrf','Trf\CreateController@submitTrf')->name('trf.create.submitTrf');

			Route::get('print-barcode','Trf\CreateController@printBarcode')->name('trf.create.printBarcode');

			Route::get('print-detailTrf','Trf\CreateController@detailTrf')->name('trf.create.detailTrf');
		});
	
	});

	Route::prefix('/report')->middleware(['permission:menu-report-test,menu-report,menu-report-logbook,menu-all-specimen'])->group(function () {
		//report
		Route::get('/testing', 'Report\ReportController@reportTest')->name('report.testing.reportTest');
		Route::get('/testing/getDataReportTest', 'Report\ReportController@getDataReportTest')->name('report.testing.getDataReportTest');
		Route::get('/testing/get-tech', 'Report\ReportController@getTech')->name('report.testing.getTech');
		Route::post('/testing/setSignature', 'Report\ReportController@setSignature')->name('report.testing.setSignature');
		Route::get('/testing/print-report-test', 'Report\ReportController@printReportTest')->name('report.testing.printReportTest');
		

		Route::get('/specimen', 'Report\ReportController@reportSpecimen')->name('report.speciment.reportSpecimen');
		Route::get('/specimen/getDataReportSpecimen', 'Report\ReportController@getDataReportSpecimen')->name('report.speciment.getDataReportSpecimen');
		Route::get('/specimen/get-tech', 'Report\ReportController@getTech')->name('report.specimen.getTech');
		Route::post('/specimen/setSignature', 'Report\ReportController@setSignature')->name('report.specimen.setSignature');
		Route::get('/specimen/print-report-specimen', 'Report\ReportController@printReportSpecimen')->name('report.specimen.printReportSpecimen');

		Route::get('/specimen/print-report-specimen', 'Report\ReportController@printReportSpecimen')->name('report.specimen.printReportSpecimen');

		Route::get('/trf', 'Report\ReportController@reportTrf')->name('report.trf.reportTrf');
		Route::post('/trf/releaseReport','Report\ReportController@releaseReport')->name('report.trf.releaseReport');
		Route::get('/trf/getDataReportTrf', 'Report\ReportController@getDataReportTrf')->name('report.trf.getDataReportTrf');
		Route::get('/trf/printReportTrf', 'Report\ReportController@printReportTrf')->name('report.trf.printReportTrf');
		Route::get('/trf/printReportTrfExt', 'Report\ReportController@printReportTrfExt')->name('report.trf.printReportTrfExt');


		Route::get('/logbook', 'Report\ReportController@reportlogbook')->name('report.logbook.reportlogbook');
		Route::get('/logbook/getReportLogbook', 'Report\ReportController@getReportLogbook')->name('report.logbook.getReportLogbook');
		Route::get('/logbook/exportLogBook', 'Report\ReportController@exportLogBook')->name('report.logbook.exportLogBook');

		Route::get('/allspecimen','Report\ReportController@reportAllspc')->name('report.allspecimen.reportAllspc');
		Route::get('/allspecimen/getDataAllspc','Report\ReportController@getDataAllspc')->name('report.allspecimen.getDataAllspc');
		Route::get('/allspecimen/exportAllspc','Report\ReportController@exportAllspc')->name('report.allspecimen.exportAllspc');
		Route::get('/allspecimen/getCategorySpc','Report\ReportController@getCategorySpc')->name('report.allspecimen.getCategorySpc');
		Route::get('/allspecimen/getTypeSpc','Report\ReportController@getTypeSpc')->name('report.allspecimen.getTypeSpc');

		Route::get('/return-specimen','Report\ReportController@reportReturn')->name('report.allspecimen.reportReturn');
		Route::get('/return-specimen/getDataReturn','Report\ReportController@getDataReturn')->name('report.allspecimen.getDataReturn');
		Route::get('/return-specimen/exportReturnSample','Report\ReportController@exportReturnSample')->name('report.allspecimen.exportReturnSample');

		Route::get('/method-result','Report\ReportController@methodResult')->name('report.methodresult.index');
		Route::get('/method-result/getMethodResult','Report\ReportController@getMethodResult')->name('report.methodresult.getMethodResult');
		Route::get('/method-result/exportResultMethod','Report\ReportController@exportResultMethod')->name('report.methodresult.exportResultMethod');
	});

	Route::prefix('/escalation')->middleware(['permission:menu-escalation-trf'])->group(function () {
		
		Route::get('','Report\EscalationController@index')->name('esc.index');
		Route::get('/getDataEcl','Report\EscalationController@getDataEcl')->name('esc.getDataEcl');
		Route::get('/viewData','Report\EscalationController@viewData')->name('esc.viewData');
		Route::post('/createEscalation','Report\EscalationController@createEscalation')->name('esc.createEscalation');
		Route::get('/viewEsc','Report\EscalationController@viewEsc')->name('esc.viewEsc');
	});


	Route::prefix('/return-specimen')->middleware(['permission:menu-return-specimen'])->group(function(){

		Route::get('','Transaction\ReturnSampleController@index')->name('return.index');
		Route::get('/nik-pic','Transaction\ReturnSampleController@scanNikPic')->name('return.scanNikPic');
		Route::get('/trf','Transaction\ReturnSampleController@scanTrf')->name('return.scanTrf');
	});

});



Route::get('/get-specimen-data', 'Trf\GetSpecimenController@getData')->name('getSpecimenData.getData');
Route::prefix('/desk')->middleware(['authdesk'])->group(function() {
	Route::get('','Desk\CreateTrfController@index')->name('desk.index');
	Route::get('/get-data','Desk\CreateTrfController@getData')->name('desk.getData');

	Route::get('/create-trf','Desk\CreateTrfController@createTrf')->name('desk.createTrf');
	Route::get('/create-trf/getCtg','Desk\CreateTrfController@getCtg')->name('desk.getCtg');
	Route::get('/create-trf/getCtgSpc','Desk\CreateTrfController@getCtgSpc')->name('desk.getCtgSpc');
	Route::get('/create-trf/getTypeSpc','Desk\CreateTrfController@getTypeSpc')->name('desk.getTypeSpc');
	Route::get('/create-trf/getDataSpecimen','Desk\CreateTrfController@getDataSpecimen')->name('desk.getDataSpecimen');
	Route::post('/create-trf/saveTrf','Desk\CreateTrfController@saveTrf')->name('desk.saveTrf');
	Route::get('/create-trf/printBarcode','Desk\CreateTrfController@printBarcode')->name('desk.printBarcode');
	Route::get('/create-trf/printDetail','Desk\CreateTrfController@printDetail')->name('desk.printDetail');

	Route::get('/return','Desk\ReturnSampleController@index')->name('desk.return.index');
	Route::get('/return/getData','Desk\ReturnSampleController@getData')->name('desk.return.getData');
	Route::post('/return/scanBarcodeReturn','Desk\ReturnSampleController@scanBarcodeReturn')->name('desk.return.scanBarcodeReturn');


});

Route::get('/desk/login','Auth\LoginDeskController@showLogin')->name('desk.login');
Route::post('/desk/loginDesk','Auth\LoginDeskController@loginDesk')->name('desk.loginDesk');
Route::get('/desk/logoutDesk','Auth\LoginDeskController@logoutAction')->name('desk.logoutAction');